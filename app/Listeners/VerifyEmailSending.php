<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\VerifyEmail;
use Illuminate\Mail\Events\MessageSending;

class VerifyEmailSending
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(MessageSending $event)
    {
        
        $to = $event->message->getTo();
        foreach ($to as $receiver_email => $value) {
            //verify email truoc khi gui
            $verify_email = VerifyEmail::where('email',$receiver_email)->first();
            if($verify_email) {//neu email da duoc verify tu truoc
                if ($verify_email->status == 1) {
                    if (strtotime($verify_email->updated_at) < strtotime('-30 days')) {//neu thoi diem verify cach day 1 thang thi verify lai
                        if(verify_email_org($receiver_email)) {//neu email van con song
                            //cap nhat lai ngay verify
                            $verify_email->updated_at = date('Y-m-d H:i:s');
                            $verify_email->save();
                        }else {
                            $verify_email->status = 2;
                            $verify_email->updated_at = date('Y-m-d H:i:s');
                            $verify_email->save();
                            unset($to[$receiver_email]);
                        }
                    }
                    //neu trang thai 1 va thoi gian verify gan day chua qua 3 thang => cho gui
                    //return true;
                }elseif ($verify_email->status == 2) {//neu lan truoc trang thai la 2 => verify lai 1 lan nua
                    if(verify_email_org($receiver_email)) {//neu ok thi update status 1 va cho phep gui
                        $verify_email->status = 1;
                        $verify_email->updated_at = date('Y-m-d H:i:s');
                        $verify_email->save();
                    }else {//neu verify lai ma khong duoc thi chuyen len trang thai 3 luon
                        $verify_email->status = 3;
                        $verify_email->updated_at = date('Y-m-d H:i:s');
                        $verify_email->save();
                        unset($to[$receiver_email]);
                    }
                }else {//trang thai 3 => blacklist khong cho phep gui, khong check lai
                    unset($to[$receiver_email]);
                }
            }else {//neu chua duoc verify bao gio
                $verify_email = new VerifyEmail;
                $verify_email->email = $receiver_email;
                $verify_email->created_at = $verify_email->updated_at = date('Y-m-d H:i:s');
                if(verify_email_org($receiver_email)) {
                    $verify_email->status = 1;
                    $verify_email->save();
                }else {
                    $verify_email->status = 2;
                    $verify_email->save();
                    unset($to[$receiver_email]);
                }
            }
        }

        if(count($to) > 0) {//neu mang cac email nhan van con so luong sau khi da qua verify
            $event->message->setTo($to);
            return true;
        }else {
            return false;
        }
    }
}
