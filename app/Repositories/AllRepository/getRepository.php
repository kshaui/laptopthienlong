<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/31/2019
 * Time: 10:32 AM
 */
namespace App\Repositories\AllRepository;
use App\News;
use App\Repositories\Repository;
use DB;
use App\Product;
use App\ProductsCategory;
use Illuminate\Support\Facades\Cache;

class getRepository extends Repository
{

    public function __construct()
    {

    }


    public function Slides(){
        if (Cache::has('slides')) {
            $slides = Cache::get('slides');
        } else {
            $slides = DB::table('slides')->where('status',1)->orderBy('order','asc')->get();
            Cache::forever('slides', $slides);
        }
        return $slides;
    }
    public function newsHome(){
        if (Cache::has('news_home')) {
            $news_home = Cache::get('news_home');
        } else {
            $news_home = News::where('status',1)->orderBy('id', 'desc')->limit(3)->get();
            Cache::forever('news_home', $news_home);
        }
        return $news_home;
    }
    public function dataSizes()
    {
        if (Cache::has('data_sizes')) {
            $data_sizes = Cache::get('data_sizes');
        } else {
            $data_sizes = DB::table('sizes')->select(['id','name'])->where('status',1)->get();
            Cache::forever('data_sizes', $data_sizes);
        }
      
        return $data_sizes;
    }
    public function dataColors()
    {
        if (Cache::has('data_colors')) {
            $data_colors = Cache::get('data_colors');
        } else {
            $data_colors = DB::table('colors')->select(['id','name','code'])->where('status',1)->get();
            Cache::forever('data_colors', $data_colors);
        }
        return $data_colors;
    }

    public function ProductBanchay()
    {
        if (Cache::has('products_banchay')) {
            $products_banchay = Cache::get('products_banchay');
        } else {
            $products_banchay = Product::select('products.*')
            ->where('status',1)
            ->leftJoin('pins', 'pins.type_id','=', 'products.id')
            ->where('type','products')
            ->where('place','banchay')
            ->orderBy('created_at','desc')
            ->limit(8)
            ->get();

            Cache::forever('products_banchay', $products_banchay);
        }
        return $products_banchay;
    }
    public function ProductNoiBat()
    {
        if (Cache::has('products_nobat')) {
            $products_nobat = Cache::get('products_nobat');
        } else {
           
            $products_nobat = Product::select('products.*')
            ->where('status',1)
            ->leftJoin('pins', 'pins.type_id','=', 'products.id')
            ->where('type','products')
            ->where('place','noibat')
            ->orderBy('created_at','desc')
            ->limit(8)
            ->get();

            Cache::forever('products_nobat', $products_nobat);
        }
        return $products_nobat;
    }

    public function ProductCategoryHome()
    {
        if (Cache::has('product_category_home')) {
            $product_category_home = Cache::get('product_category_home');
        } else {
           
            $product_category_home = ProductsCategory::select(['products_categories.name','products_categories.id','products_categories.slug','products_categories.image'])
            ->where('status',1)
            ->where('parent_id','!=',0)
            ->leftJoin('pins', 'pins.type_id','=', 'products_categories.id')
            ->where('type','products_categories')
            ->where('place','home')
            ->orderBy('value','asc')
            ->limit(5)
            ->get();

            Cache::forever('product_category_home', $product_category_home);
        }
        return $product_category_home;
    }

    public function allProduct()
    {
        if (Cache::has('all_product')) {
            $all_product = Cache::get('all_product');
        } else {
            $all_product = Product::where('status',1)
            ->orderBy('created_at','desc')
            ->limit(8)
            ->get();

            Cache::forever('all_product', $all_product);
        }
        return $all_product;
    }
    public function dataBrand()
    {
        if (Cache::has('data_brand')) {
            $data_brand = Cache::get('data_brand');
        } else {
            $data_brand = DB::table('brands')->where('status',1)
            ->orderBy('created_at','asc')
            ->get();

            Cache::forever('data_brand', $data_brand);
        }
        return $data_brand;
    }

}