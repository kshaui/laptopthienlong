<?php
/**
 * all functions
 * Các function chung nhất dùng cho core
 */

function removeSign($str){
    $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă",
        "ằ","ắ","ặ","ẳ","ẵ",
        "è","é","ẹ","ẻ","ẽ","ê","ề" ,"ế","ệ","ể","ễ",
        "ì","í","ị","ỉ","ĩ",
        "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
    ,"ờ","ớ","ợ","ở","ỡ",
        "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
        "ỳ","ý","ỵ","ỷ","ỹ",
        "đ",
        "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
    ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
        "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
        "Ì","Í","Ị","Ỉ","Ĩ",
        "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
    ,"Ờ","Ớ","Ợ","Ở","Ỡ",
        "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
        "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
        "Đ","ê","ù","à");

    $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
    ,"a","a","a","a","a","a",
        "e","e","e","e","e","e","e","e","e","e","e",
        "i","i","i","i","i",
        "o","o","o","o","o","o","o","o","o","o","o","o"
    ,"o","o","o","o","o",
        "u","u","u","u","u","u","u","u","u","u","u",
        "y","y","y","y","y",
        "d",
        "A","A","A","A","A","A","A","A","A","A","A","A"
    ,"A","A","A","A","A",
        "E","E","E","E","E","E","E","E","E","E","E",
        "I","I","I","I","I",
        "O","O","O","O","O","O","O","O","O","O","O","O"
    ,"O","O","O","O","O",
        "U","U","U","U","U","U","U","U","U","U","U",
        "Y","Y","Y","Y","Y",
        "D","e","u","a");
    return str_replace($coDau,$khongDau,$str);
}
function removeHTML($string){
    $string = preg_replace ('/<script.*?\>.*?<\/script>/si', ' ', $string);
    $string = preg_replace ('/<style.*?\>.*?<\/style>/si', ' ', $string);
    $string = preg_replace ('/<.*?\>/si', ' ', $string);
    $string = str_replace ('&nbsp;', ' ', $string);
    return $string;
}
function removeScript($string){
    $string = preg_replace ('/<script.*?\>.*?<\/script>/si', '<br />', $string);
    $string = preg_replace ('/on([a-zA-Z]*)=".*?"/si', ' ', $string);
    $string = preg_replace ('/On([a-zA-Z]*)=".*?"/si', ' ', $string);
    $string = preg_replace ("/on([a-zA-Z]*)='.*?'/si", " ", $string);
    $string = preg_replace ("/On([a-zA-Z]*)='.*?'/si", " ", $string);
    return $string;
}
function removeXML($string){
    $string = preg_replace('/<xml>.*?<\/xml>/si','',$string);
    //$string = preg_replace('/<!--.*?-->/si','',$string);
    return $string;
}
function replaceMQ($text){
    $text	= str_replace("\'", "'", $text);
    $text	= str_replace("'", "''", $text);
    return $text;
}

function removeMagicQuote($str){
    $str = str_replace("\'", "'", $str);
    $str = str_replace("\&quot;", "&quot;", $str);
    $str = str_replace("\\\\", "\\", $str);
    return $str;
}
function removeUTF8BOM($text) {
    $bom = pack('H*','EFBBBF');
    $text = preg_replace("/^$bom/", '', $text);
    return $text;
}
function slugTitle($string,$keyReplace = "/"){
    $string = removeSign($string);
    $string  =  trim(preg_replace("/[^A-Za-z0-9]/i"," ",$string)); // khong dau
    $string  =  str_replace(" ","-",$string);
    $string = str_replace("--","-",$string);
    $string = str_replace($keyReplace,"-",$string);
    return strtolower($string);
}
function cutString($str, $length, $char=" ..."){
    //Nếu chuỗi cần cắt nhỏ hơn $length thì return luôn
    $strlen	= mb_strlen($str, "UTF-8");
    if($strlen <= $length) return $str;

    //Cắt chiều dài chuỗi $str tới đoạn cần lấy
    $substr	= mb_substr($str, 0, $length, "UTF-8");
    if(mb_substr($str, $length, 1, "UTF-8") == " ") return $substr . $char;

    //Xác định dấu " " cuối cùng trong chuỗi $substr vừa cắt
    $strPoint= mb_strrpos($substr, " ", "UTF-8");

    //Return string
    if($strPoint < $length - 20) return $substr . $char;
    else return mb_substr($substr, 0, $strPoint, "UTF-8") . $char;
}
function get_url(){
    if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    return $protocol . "://" . $_SERVER['HTTP_HOST'];
}
function get_client_ip() {
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        return $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        return $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        return $_SERVER['REMOTE_ADDR'];
    else
        return 'UNKNOWN';
}

function image_by_link($link_img='',$size='') {
    // $link_img = http://domain.com/uploads/2017/06/dan-cuong-luc-sony-large.jpg
    // $size: tiny 80, small 150, medium 300, large 600
    $array_size = array('tiny','small','medium','large');
    //$link_img = str_replace(['http://old.domain'],config('app.url'),$link_img);
    if(!in_array($size,$array_size)) {
        return $link_img;
    }
    $endS = strrpos($link_img,'.'); //vị trí của cái . cuối cùng
    $img_ext = substr($link_img,$endS+1); //jpg
    $img_path_name = substr($link_img,0,$endS); //http://domain.com/uploads/2017/06/dan-cuong-luc-sony-large
    $starG = strrpos($img_path_name,'-'); // vị trí dấu - cuối cùng
    $prefix = substr($img_path_name,$starG+1); //tiny,small,medium,large hoặc 1 text bất kỳ nếu link là ảnh mặc định
    if(in_array($prefix,$array_size)) {
        $img_path_name = str_replace($prefix,'',$img_path_name); //dan-cuong-luc-sony-
        return $img_path_name.$size.'.'.$img_ext;
    }else {
        return $img_path_name.'-'.$size.'.'.$img_ext;
    }
}

/**
 * Nén string và encode cho các trường hợp xuất dữ liệu
 * @param  string $string
 */
function encode_compress_string($string) {
    return base64_encode(gzcompress($string, 9));
}

/**
 * Giải nén string và decode cho các string qua func encode_compress_string
 * @param  string $string
 */
function decode_compress_string($string) {
    return gzuncompress(base64_decode($string));
}

/**
 * Verify email qua verify-email.org
 */
function verify_email_org($email) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://app.verify-email.org/api/v1/RxmBjTr3s8j5l8pvy4uRBxrKbV60ZR7fJ4AB1TtkpYVCjo7gTP/verify/$email");
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Accept: application/json',
        'Content-Type: application/json'
    ]);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $data = curl_exec($ch);
    $data = json_decode($data,true);
    //$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if($data['status'] == 1)
        return true;
    return false;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }
    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
function getAlt($link='') {
    if ($link=='') {
        return 'no-image';
    } else {
        $link_explore = explode('/', $link);
        $img = array_pop($link_explore);
        $alt = explode('.', $img)[0];
        $alt = str_replace(['-tiny','-small','-medium','-large'], '', $alt);
        return $alt;
    }
    
}