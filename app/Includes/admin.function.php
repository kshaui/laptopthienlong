<?php
/**
 * Toàn bộ các function phần admin
 */
function checkRole($role){
    $admin_id=Auth::guard('admin')->user()->id;
    if($admin_id==1){
        return true;
    }else{
        $data=DB::table('authorizations')->where('admin_id',$admin_id)->first();
        $array_role=json_decode($data->capabilities);
        if(in_array($role, $array_role)){
            return true;
        }
        else{
            return false;
        }
    }
}
function get_slug($slug = '',$title,$table,$id_field,$slug_field,$record_id_edit = 0) {
    if($slug == '') {
        $slug = slugTitle($title);
    }else {
        $slug = slugTitle($slug);
    }
    $data=DB::table($table)->select($id_field)->where($slug_field,$slug)->first();
    if(!empty($data)) {
        if($data->$id_field == $record_id_edit) {
            return $slug;
        }else {
            $i = 1;
            while($i > 0) {
                $slug_new = $slug.'-'.$i;
                $data1=DB::table($table)->select($id_field)->where($slug_field,$slug_new)->first();
                if(!empty($data1)) {
                    $i++;
                }
                else {
                    return $slug_new;
                }
            }
        }

    }
    else {
        return $slug;
    }
}
function admin_paging($page,$page_size,$total,$prefix_link,$suffix_link) {
    if($total%($page_size) == 0) {
        $total_count = $total/($page_size);
    }else {
        $total_count = (int)($total/($page_size)) + 1;
    }
    if ($total_count <= 1) return '';
    $html = '<ul class="pagination">';
    $html .= '<li class="paginate_button"><a class="pagingCount" href="#">Trang '.$page.'/'.$total_count.'</a></li>';
    if($page > 1) {
        $html .= '<li class="paginate_button"><a class="pagingFirst" href="'.$prefix_link.'1'.$suffix_link.'">&lt;&lt;</a></li>';
    }
    // Tổng số trang nhỏ hơn 5
    if($total_count <= 5) {
        // Trang hiện tại lớn hơn 1 thì hiện nút previous
        if($page > 1) {
            $html .= '<li class="paginate_button"><a class="pagingPrevious" href="'.$prefix_link.($page - 1).$suffix_link.'">&lt;</a></li>';
        }
        for($i = 1; $i <= $total_count; $i++) {
            $html .= '<li class="paginate_button'.(($page == $i) ? " active" : "").'"><a class="pagingNumber" href="'.$prefix_link.$i.$suffix_link.'">'.$i.'</a></li>';
        }
        // Trang hiện tại nhỏ hơn tổng số trang thì hiện nút next
        if($page < $total_count) {
            $html .= '<li class="paginate_button"><a class="pagingNext" href="'.$prefix_link.($page + 1).$suffix_link.'">&gt;</a></li>';
        }
    }
    // Trang hiện tại nhỏ hơn 3 và tổng số trang lớn hơn 5
    if($page <= 3 && $total_count > 5) {
        // Trang hiện tại lớn hơn 1 thì hiện nút previous
        if($page > 1) {
            $html .= '<li class="paginate_button"><a class="pagingPrevious" href="'.$prefix_link.($page - 1).$suffix_link.'">&lt;</a></li>';
        }
        for($i = 1; $i < 5; $i++) {
            $html .= '<li class="paginate_button'.(($page == $i) ? " active" : "").'"><a class="pagingNumber" href="'.$prefix_link.$i.$suffix_link.'">'.$i.'</a></li>';
        }
        $html .= '<li class="paginate_button"><a class="pagingDot" href="#">...</a></li>';
        // Trang hiện tại nhỏ hơn tổng số trang thì hiện nút next
        if($page < $total_count) {
            $html .= '<li class="paginate_button"><a class="pagingNext" href="'.$prefix_link.($page + 1).$suffix_link.'">&gt;</a></li>';
        }
    }
    // Trang hiện tại lớn hơn 3, tổng số trang lớn hơn 5 và sau trang hiện tại 2 trang vẫn nhỏ hơn tổng số trang
    if($page > 3 && $total_count > 5 && $page + 2 <= $total_count) {
        $html .= '<li class="paginate_button"><a class="pagingPrevious" href="'.$prefix_link.($page - 1).$suffix_link.'">&lt;</a></li>
                <li class="paginate_button"><a class="pagingDot" href="#">...</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page - 2).$suffix_link.'">'.($page-2).'</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page - 1).$suffix_link.'">'.($page-1).'</a></li>
                <li class="paginate_button active"><a class="pagingNumber" href="'.$prefix_link.($page).$suffix_link.'">'.($page).'</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page + 1).$suffix_link.'">'.($page+1).'</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page + 2).$suffix_link.'">'.($page+2).'</a></li>';
        $html .= '<li class="paginate_button"><a class="pagingDot" href="#">...</a></li>';
        // Trang hiện tại nhỏ hơn tổng số trang thì hiện nút next
        if($page < $total_count) {
            $html .= '<li class="paginate_button"><a class="pagingNext" href="'.$prefix_link.($page + 1).$suffix_link.'">&gt;</a></li>';
        }
    }
    // Trang hiện tại lớn hơn 3, tổng số trang lớn hơn 5 và sau trang hiện tại 2 trang lớn hơn tổng số trang
    if($page > 3 && $total_count > 5 && $page + 2 > $total_count) {
        $html .= '<li class="paginate_button"><a class="pagingPrevious" href="'.$prefix_link.($page - 1).$suffix_link.'">&lt;</a></li>
                <li class="paginate_button"><a class="pagingDot" href="#">...</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page - 2).$suffix_link.'">'.($page-2).'</a></li>
                <li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.($page - 1).$suffix_link.'">'.($page-1).'</a></li>
                <li class="paginate_button active"><a class="pagingNumber" href="'.$prefix_link.($page).$suffix_link.'">'.($page).'</a></li>';
        for($i = $page+1; $i <= $total_count; $i++) {
            $html .= '<li class="paginate_button"><a class="pagingNumber" href="'.$prefix_link.$i.$suffix_link.'">'.$i.'</a></li>';
        }
        // Trang hiện tại nhỏ hơn tổng số trang thì hiện nút next
        if($page < $total_count) {
            $html .= '<li class="paginate_button"><a class="pagingNext" href="'.$prefix_link.($page + 1).$suffix_link.'">&gt;</a></li>';
        }
    }
    if($page < $total_count) {
        $html .= '<li class="paginate_button"><a class="pagingLast" href="'.$prefix_link.$total_count.$suffix_link.'">&gt;&gt;</a></li>';
    }
    $html .= '</ul>';
    return $html;

}
function list_timezone() {
    $zones_array = array();
    $timestamp = time();
    foreach(timezone_identifiers_list() as $key => $zone) {
        date_default_timezone_set($zone);
        $zones_array[$key]['zone'] = $zone;
        $zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
    }
    return $zones_array;
}
function change_time_to_text($time,$type = null){
    if(!is_numeric($time)){
        $time = strtotime($time);
    }
    if($type == 'update'){
        $text = 'Cập nhật khoảng ';
    }else{
        $text = '';
    }
    if((time() - $time) < 60){
        return $text.round(time()-$time).' giây trước';
    }elseif((time() - $time) < 60*60){
        return $text.round((time() - $time)/60).' phút trước';
    }elseif ((time() - $time) < 60*60*24){
        return $text.round(((time()-$time)/60)/60).' giờ trước';
    }else{
        return date('H:i:s A d/m/Y',$time);
    }
}
function show_editor_sudo_media($name_textare,$height,$default = null){
    $option = '<script>
    document.addEventListener("DOMContentLoaded", function(event) { 
        tinymce.init({
            path_absolute : "/",
            selector:\'textarea[id="'.$name_textare.'"]\',
            branding: false,
            hidden_input: false,
            relative_urls: false,
            convert_urls: false,
            height : '.$height.',
            autosave_ask_before_unload:true,
            autosave_interval:\'10s\',
            autosave_restore_when_empty:true,
            entity_encoding : "raw",
            fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 36pt 40pt 46pt 52pt 60pt",
            plugins: [
                "textcolor",
                "advlist autolink lists link image imagetools charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table autosave contextmenu paste wordcount"
            ],
            wordcount_countregex: /[\w\u2019\x27\-\u00C0-\u1FFF]+/g,
            language: "vi_VN",
            autosave_retention:"30m",
            autosave_prefix: "tinymce-autosave-{path}{query}-{id}-",
            wordcount_cleanregex: /[0-9.(),;:!?%#$?\x27\x22_+=\\\/\-]*/g,
            toolbar: "insertfile undo redo table sudomedia charmap | styleselect | sizeselect | bold italic | fontselect |  fontsizeselect | forecolor " +
            "backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent " +
            "indent | link unlink fullscreen restoredraft filemanager",
            setup: function (editor) {
                editor.addButton(\'sudomedia\', {
                  text: \'Tải ảnh\',
                  icon: \'image\',
                  label:\'Nhúng ảnh vào nội dung\',
                  onclick: function () {
                    media_popup("add","tinymce","'.$name_textare.'","Chèn ảnh vào bài viết");
                  }
                });
              },
            file_picker_callback: function() {
                    media_popup("add","tinymce","'.$name_textare.'","Chèn ảnh vào bài viết");
            }
        });
    });
    </script>';
    $option .= '<textarea id="'.$name_textare.'" name="'.$name_textare.'">'.$default.'</textarea>';
    return $option;
}

function recursive_setting_menu($data,$count = 0,$name,$is_first=false) {
    $html = '';
    if(!empty($data)) {
        if($is_first) {
            $html .= '<ol class="dd-list" id="ol-first">';
        }else {
            $html .= '<ol class="dd-list">';
        }
        foreach($data as $key => $value) {
            if(!empty($value['table']) && $value['table'] !='fix_link'){
                $getData = DB::table($value['table'])->select('slug')->where('id', $value['id'])->first();
            }
            if(isset($getData) && !empty($getData)){
                if($value['table'] != ''){
                    $slug = route('web.'.$value['table'].'.show', $getData->slug);
                }else{
                    $slug = $value['link'];
                }
            }else{
                $slug = $value['link'];
            }
            unset($getData);
            $rand = rand(1,10000);
            $html .= '<li class="dd-item" id="li-'.$count.'-'.$rand.'" data-name="'.$value['name'].'" data-link="'.$slug.'" data-table="'.$value['table'].'" data-id="'.$value['id'].'" data-taget="'.$value['taget'].'" data-rel="'.$value['rel'].'">';
            $html .= '<div class="dd-handle handle-'.$count.'-'.$rand.'">'.$value['name'];
            $html .= '</div>';
            $html .= '<p class="p-action">';
            if($value['table'] == ''){
                $html .= '<a class="a1" href="javascript:;" onclick="show_edit_menu(\''.$name.'\',\''.$count.'-'.$rand.'\');">Sửa</a>';
            }else{
                $html .= '<a class="a1" href="javascript:;" onclick="show_edit_menu1(\''.$name.'\',\''.$count.'-'.$rand.'\');">Sửa</a>';
            }
            $html .= '<a class="a2" href="javascript:;" onclick="remove_menu(\''.$name.'\',\''.$count.'-'.$rand.'\');">Xóa</a>';
            $html .= '</p>';

            $html.='<div class="add-menu edit-menu-custom edit-menu edit-menu-'.$count.'-'.$rand.'">';
            $html.='<h5>Sửa menu</h5>';
            $html.='<div class="form-group">';
            $html.='<label>Tên menu</label>';
            $html.='<input type="text" class="name-menu-edit" value="'.$value['name'].'">';
            $html.='</div>';
            if($value['table'] == ''){
                $html.='<div class="form-group">';
                $html.='<label>Link</label>';
                $html.='<input type="text" class="link-menu-edit" value="'.$slug.'">';
                $html.='</div>';
            }elseif($value['table'] == 'fix_link'){
                $list_fix_link = config('app.fix_link');
                $html .= '<div class="form-group">';
                $html .= '<label>Đường dẫn đến</label>';
                $html .= '<select class="link-menu-edit" id="">';
                foreach ($list_fix_link as $ke => $item) {
                    if($slug == $ke){
                        $html .= '<option value="'.$ke.'" selected data-slug="'.$ke.'">'.$item.'</option>';
                    }else{
                        $html .= '<option value="'.$ke.'" data-slug="'.$ke.'">'.$item.'</option>';
                    }
                }
                $html .= '</select>';
                $html .= '</div>';
            }else{
                $data_select = DB::table($value['table'])->where('status',1)->get();
                $html .= '<div class="form-group">';
                $html .= '<label>Đường dẫn đến</label>';
                $html .= '<select class="link-menu-edit" id="">';
                foreach ($data_select as $item) {
                    if($value['id'] == $item->id){
                        $html .= '<option value="'.$item->id.'" selected data-slug="'.$item->slug.'">'.$item->name.'</option>';
                    }else{
                        $html .= '<option value="'.$item->id.'" data-slug="'.$item->slug.'">'.$item->name.'</option>';
                    }
                }
                $html .= '</select>';
                $html .= '</div>';
            }
            $html.='<div class="form-group">';
            $html.='<label>Mở tab mới</label>';
            if($value['taget'] == 1){
                $html.='<input style="height: 15px;width: 20px;" checked type="checkbox" class="taget-menu-edit">';
            }else{
                $html.='<input style="height: 15px;width: 20px;" type="checkbox" class="taget-menu-edit">';
            }
            $html.='</div>';
            $html.='<div class="form-group">';
            $html.='<label>Nofollow</label>';
            if($value['rel'] == 1){
                $html.='<input style="height: 15px;width: 20px;" checked type="checkbox" class="rel-menu-edit">';
            }else{
                $html.='<input style="height: 15px;width: 20px;" type="checkbox" class="rel-menu-edit">';
            }
            $html.='</div>';
            $html.='<p class="add">';
            $html.='<img class="img2" src="/template-admin/images/spinner.gif" alt="">';
            if($value['table'] == ''){
                $html.='<a href="javascript:;" onclick="update_menu(\''.$count.'-'.$rand.'\');">Cập nhật</a>';
            }else{
                $html.='<a href="javascript:;" onclick="update_menu1(\''.$count.'-'.$rand.'\');">Cập nhật</a>';
            }
            $html.='</p>';
            $html.='<input type="hidden" class="edit-menu-btn" value="">';
            $html.='</div>';

            if(isset($value['children'])){
                $data_child = $value['children'];
            }else{
                $data_child = [];
            }
            $count +=1;
            $html .= recursive_setting_menu($data_child,$count,$name);
        }
        $html .= '</ol>';
    }
    return $html;
}
function passwordGenerate($length = 12) {
    $characters = 'abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!@#$%^&*()';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function passwordStrength($password) {
    $strength = 0;
    if (strlen($password) < 6) {//Phải có 6 ký tự trở lên
        return $strength;
    }else {
        $strength++;
    }
    if (preg_match("@\d@", $password)) {//Nên có ít nhất 1 số
        $strength++;
    }
    if (preg_match("@[A-Z]@", $password)) {//Nên có ít nhất 1 chữ hoa
        $strength++;
    }
    if (preg_match("@[a-z]@", $password)) {//Nên có ít nhất 1 chữ thường
        $strength++;
    }
    if (preg_match("@\W@", $password)) {//Nên có 1 ký tự đặc biệt
        $strength++;
    }
    if (!preg_match("@\s@", $password)) {//Không nên có ký tự rỗng
        $strength++;
    }
    return $strength;// = 6 - Tùy vào mức độ cần thiết của ứng dụng mà đưa ra số strength yêu cầu
}
function rand_string( $length ) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $size = strlen( $chars );
    $str = '';
    for( $i = 0; $i < $length; $i++ ) {
    $str .= $chars[ rand( 0, $size - 1 ) ];
     }
    return $str;
}