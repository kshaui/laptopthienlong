<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class PhukienProduct extends Model

{

     public function getUrl() {

        return route('web.phu_kien_products.show',$this->slug);

    }

    public function getImage($size = '') {

        // $size: tiny 80, small 150, medium 300, large 600

        if($this->image != '') {

            return image_by_link($this->image,$size);

        }else

            return '/assets/img/no-image.png';

    }

    public function getPrice(){

        if($this->price == 0){

            return 'Liên hệ';

        }else{

            return number_format($this->price,0,'.','.').'₫';

        }

    }

    public function getPriceOld(){

        if($this->price_old > 0){

            return number_format($this->price_old,0,'.','.').'₫';

        }else{

            return '';

        }

    }

}

