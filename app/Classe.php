<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classe extends Model
{
    protected $table = 'classes';

    public function getParent() {
        if ($this->parent_id == 0) return null;
        return \Cache::remember('class_' . $this->parent_id, 720, function() {
            return Classe::where('status',1)->where('id',$this->parent_id)->first();
        });
    }

    public function getUrl() {
        return route('web.service_categories.show',['slug' => $this->slug]);
    }

    public function getLevel() {
        $level = 1;
        $parent = $this->getParent();
        while ($parent) {
            $level++;
            $parent = $parent->getParent();
        }
        return $level;
    }

    public function getRatings() {
        return \Cache::remember('ratings_classes_' . $this->id, 1440, function() {
            return Rating::selectRaw("COUNT(id) as count, SUM(value) as sum, AVG(value) as avg")->where('type','classes')->where('type_id',$this->id)->where('value','>', 2)->first();
        });
    }

    public function getRewrite() {
        if($this->parent_id == 0) {
            $url = '/'.$this->slug;
        }else {
            $url = '/'.$this->slug;
            //$parent_category = $this->getParent();
            $parent_category = $this->getParent();
            while($parent_category) {
                $url = $parent_category->getRewrite().$url;
                $parent_category = $parent_category->getParent();
            }
        }
        return $url;
    }

    public function arrayParentId() {
        $ids = [$this->id];
        $parent_category = $this->getParent();
        while($parent_category) {
            array_push($ids,$parent_category->id);
            $parent_category = $parent_category->getParent();
        }
        return $ids;
    }

    public function arrayChildId() {
        $ids = [$this->id];
        $child_category = Category::where('status',1)->where('parent_id',$this->id)->get();
        if($child_category->count()) {
            foreach ($child_category as $value) {
                array_push($ids,$value->id);
                $child_category_2 = Category::where('status',1)->where('parent_id',$value->id)->get();
                if($child_category_2->count()) {
                    foreach ($child_category_2 as $value_2) {
                        array_push($ids,$value->id);//chay tới cấp 2, nếu nhiều cấp hơn phải đệ qui
                    }
                }
            }
        }
        return $ids;
    }

    public function caseParent() {
        $case = [];
        $parent_category = $this->getParent();
        while($parent_category) {
            array_unshift($case,$parent_category);
            $parent_category = $parent_category->getParent();
        }
        return $case;
    }

    public function childClasses() {
        return Classe::where('status',1)->where('parent_id',$this->id)->orderBy('order','ASC')->get();
    }
}
