<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;
use App\Rating;
class Service extends Model
{
    //
    protected $table = "services";
    protected $fillable =[
        'category_id',
        'name',
        'slug',
        'image',
        'slide_real',
        'price',
        'price_table',
        'option',
        'link_interal',
        'warranty',
        'promotion',
        'info',
        'related_service',
        'related_news',
        'related_fit',
        'detail',
        'instock_status',
        'tags',
        'showhome',
        'order',
        'package',
        'slides',
        'videos',
        'schema',
        'status',
    ];
    public function service_category()
    {
        return $this->belongsTo('App\ServiceCategory','category_id','id');
    }

    public function getImage($size=''){
        if (!isset($this->image)) {
            $image = asset('/template-admin/images/no-image.png');
            return $image;
        }
        return image_by_link($this->image,$size);
    }

    public function getUrl() {
        return route('web.services.show',$this->slug);
    }
      public function getPrice(){
        if($this->price == 0){
            return 'Liên hệ';
        }else{
            return number_format($this->price,0,'.','.').'₫';
        }
    }

    public function instock_status()
    {
        switch ($this->instock_status) {
            case '1': $instock_status = config('app.instock_status.1'); break;
            case '2': $instock_status = config('app.instock_status.2'); break;
            case '3': $instock_status = config('app.instock_status.3'); break;
            case '4': $instock_status = config('app.instock_status.4'); break;
            case '5': $instock_status = config('app.instock_status.5'); break;
        }
        return $instock_status;
    }

    public static function getRelated($id_related,$item_number,$category_id=0)
    {
        if (count($id_related) == 0 || $id_related[0] == "") {
            $related = Service::where('status',1)->where('instock_status','!=',5)->orderBy('id','DESC');
            if ($category_id!=0) {
                $related = $related->where('category_id',$category_id);
            }
        } else {
            $related = Service::join('service_categories','service_categories.id','services.category_id')
                            ->where('service_categories.status',1)
                            ->where('services.status',1)
                            ->where('services.instock_status','!=',5)
                            ->whereIn('services.id',$id_related)
                            ->select('services.*');
        }
        $related = $related->take($item_number)->get();
        return $related;
    }

    public function getCommentInfo() {
        $comment_customers = collect(Comment::where('parent_id',0)->where('status',1)->where('type', 'services')->where('type_id',$this->id)->orderBy('id','DESC')->get());
        $total_comment = count($comment_customers);

        $comment_customer = $comment_customers->take(5);
        $comment_admins = Comment::wherein('comment.parent_id',$comment_customer->pluck('id'))->where('comment.status',1)->join('admin_users','admin_users.id','comment.admin_id')->select('comment.*','admin_users.fullname')->get();

        $star_5 = count($comment_customers->where('rank',5));
        $star_4 = count($comment_customers->where('rank',4));
        $star_3 = count($comment_customers->where('rank',3));
        $star_2 = count($comment_customers->where('rank',2));
        $star_1 = count($comment_customers->where('rank',1));

        $vote = [];
        if ($total_comment == 0) {
            $vote[] = ['star' => 5,'number' => (int)$star_5,'percent' => 0,];
            $vote[] = ['star' => 4,'number' => (int)$star_4,'percent' => 0,];
            $vote[] = ['star' => 3,'number' => (int)$star_3,'percent' => 0,];
            $vote[] = ['star' => 2,'number' => (int)$star_2,'percent' => 0,];
            $vote[] = ['star' => 1,'number' => (int)$star_1,'percent' => 0,];
        } else {
            $vote[] = ['star' => 5,'number' => (int)$star_5,'percent' => (int)$star_5/$total_comment*100,];
            $vote[] = ['star' => 4,'number' => (int)$star_4,'percent' => (int)$star_4/$total_comment*100,];
            $vote[] = ['star' => 3,'number' => (int)$star_3,'percent' => (int)$star_3/$total_comment*100,];
            $vote[] = ['star' => 2,'number' => (int)$star_2,'percent' => (int)$star_2/$total_comment*100,];
            $vote[] = ['star' => 1,'number' => (int)$star_1,'percent' => (int)$star_1/$total_comment*100,];
        }
        
        
        $ratings = Comment::selectRaw("COUNT(id) as count, SUM(rank) as sum, AVG(rank) as avg")->where('parent_id',0)->where('status',1)->where('type', 'services')->where('type_id',$this->id)->first();

        return json_decode(json_encode(compact(
            'comment_customers',
            'comment_customer',
            'comment_admins',
            'ratings',
            'vote'
        )));
    }
   
}
