<?php
/**
 * Categories class - đệ quy lấy ra danh sách categories
 *
 * @copyright   Copyright (c) 2020 Công ty Laptop247hn,.
 * @license     http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt LGPL
 * @version     1.0.0
 * @author      thanhtranceo tranthanh.ceo96@gmail.com
 *
 */

namespace App\MyClass;
use DB;


class Categories
{
    public $table;
    public $field;
    public $index = -1;
    public $categories = [];

    /**
     * @param string $table tên bảng dữ liệu
     * @param string $field các field cẩn lấy của bảng
     */
    public function __construct($table = 'categories', $field = 'id,name,slug,parent_id,image,detail,order,status,created_at,updated_at') {
        $this->table = $table;
        $this->field = $field;
    }

    /**
     * @param int $parent_id id danh mục cha
     * @param int $level level của cấp bắt đầu
     * @param array $status trang thái lấy (1 = hoạt động, 2 = không hoạt động, 3 = thùng rác)
     * @return array mảng danh sách các categories theo thứ tự cấp độ - sắp xếp (order)
     */
    public function list_categories($parent_id = 0,$level = 0,$status = [1]) {
        $list_categories = DB::table($this->table)
            ->where('parent_id',$parent_id)
            ->whereIn('status',$status)
            ->orderBy('order','ASC')
            ->get();
        if($list_categories->count()) {
            $num = 0;
            foreach ($list_categories as $key => $value) {
                $num++;
                $this->index++;
                $field_array = explode(',',$this->field);

                for ($i=0;$i<count($field_array);$i++){
                    $field_name = $field_array[$i];
                    $this->categories[$this->index][$field_name] = $value->$field_name;
                }

                $this->categories[$this->index]["level"] = $level;

                if($num < $list_categories->count()){
                    $this->categories[$this->index]["last"] = 0;
                }
                else{
                    $this->categories[$this->index]["last"] = 1;
                }

                $list_child_categories = DB::table($this->table)
                    ->where('parent_id',$value->id)
                    ->whereIn('status',$status)
                    ->orderBy('order','ASC')
                    ->get();
                if ($list_child_categories->count()) {
                    $this->categories[$this->index]["haschild"] = 1;
                    $this->list_categories($value->id,$level+1,$status);//đệ quy lấy ra các categories con
                }else {
                    $this->categories[$this->index]["haschild"] = 0;
                }
            }
        }
        return $this->categories;
    }

    public function data_categories($parent_id = 0,$level = 0,$status = [1]) {
        $list_categories = $this->list_categories($parent_id,$level,$status);
        $array_categories = [];
        foreach($list_categories as $key=>$value) {
            $array_categories[$value['id']] = $value['name'];
        }
        return $array_categories;
    }

    public function data_select_categories($parent_id = 0,$level = 0,$status = [1]) {
        $list_categories = $this->list_categories($parent_id,$level,$status);
        $array_categories = [];
        $array_categories[""] = '--Chưa chọn danh mục--';
        foreach($list_categories as $key=>$value) {
            $prefix = '';
            for($i=0;$i<$value["level"];$i++) $prefix .= '|— ';
            $array_categories[$value['id']] = $prefix.$value['name'];
        }
        return $array_categories;
    }
    public function data_select($table)
    {
        $list_table = DB::table($table)
        ->where('status',1)
        ->get();
        $array_data =[];
        $array_data[""]='--Chọn loại--';
        foreach($list_table as $key=>$value) {
            $array_data[$value->id] = $value->name;
        }
      return $array_data;
    }
}