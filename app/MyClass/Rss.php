<?php
namespace App\MyClass;


class Rss
{
    public $title;
    public $content;
    public $main_link;
    public $copyright = 'https://happybag.vn';
    public $generator = 'HappyBag';

    /*
    Khởi tạo class
    */
    function __construct($title, $main_link=""){
        $this->title 		= $title;
        $this->main_link 	= $main_link;
        $this->content 	= "";
    }

    /*
    Print header
    */
    function rssHeader(){
        $today = getdate();

        $header = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
            '<rss version="2.0">' . "\n" .
            '	<channel>'  . "\n" .
            '		<title>' . htmlspecialchars($this->title) . '</title>' . "\n" .
            '		<copyright>' . htmlspecialchars($this->copyright) . '</copyright>' . "\n" .
            '		<generator>' . htmlspecialchars($this->generator) . '</generator>' . "\n" .
            '		<link>' . htmlspecialchars($this->main_link) . '</link>' . "\n" .
            '		<pubDate>' . date("D, d M Y H:i:s ",$today[0]) . "GMT+7" . '</pubDate>' . "\n" .
            '		<lastBuildDate>' . date("D, d M Y H:i:s ",$today[0]) . "GMT+7" . '</lastBuildDate>' . "\n";

        return $header;
    }

    /*
    Add item
    */
    function addItem($title, $description, $link, $guid = '' , $pubdate){
        $item = '			<item>' . "\n" .
            '				<title>' . htmlspecialchars($title) . '</title>' . "\n" .
            '				<description><![CDATA[' . $description . ']]></description>' . "\n" .
            '				<link>' . htmlspecialchars($link) . '</link>' . "\n" .
            '				<guid isPermaLink="false">' . htmlspecialchars($guid) . '</guid>' . "\n" .
            '				<pubDate>' . htmlspecialchars($pubdate) . '</pubDate>' . "\n" .
            '			</item>' . "\n";

        //return $item;
        $this->content .= $item;
    }

    /*
    Print header
    */
    function rssFooter(){
        $footer = '	</channel>' . "\n" .
            '</rss>';

        return $footer;
    }

    /*
    lấy nội dung rss
    */
    function getRSSContent(){
        return $this->rssHeader() .
            $this->content .
            $this->rssFooter();
    }

    /*
    Save RSS to file
    */
    function saveRSS($filename){
        $handle = fopen($filename, 'w');
        fwrite($handle, $this->getRSSContent());
        fclose($handle);
    }
}