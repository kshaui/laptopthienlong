<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
    /*
    Lấy danh mục cha
     */
    public function getParent() {
        if ($this->parent_id == 0) return null;
        return self::where('id',$this->parent_id)->first();
    }
    public function getImage($size = '') {
    	// $size: tiny 80, small 150, medium 300, large 600
        if($this->image != '') {
            return image_by_link($this->image,$size);
        }else
            return '/assets/img/no-image.png';
    }

    /*
    Lấy các danh mục con
     */
    public function getChilds() {
        return self::where('parent_id',$this->id)->orderBy('order','ASC')->get();
    }

    /*
    Lấy các danh mục ông cha ... dùng cho 1 số trường hợp như hiển thì breadcrumb
    @return mảng danh mục ông cha của danh mục hiện tại theo thứ tự
     */
    public function getParentCase() {
        $case = [];
        $parent_category = $this->getParent();
        while($parent_category) {
            array_unshift($case,$parent_category);
            $parent_category = $parent_category->getParent();
        }
        return $case;
    }

    /*
    Lấy danh sách id của cate hiện tại và con cháu đưa vào mảng 
    - dùng cho trường hợp query In danh mục con cháu 
    - fix 2 cấp, nếu đa cấp hãy viết theo pp đệ qui
     */
    public function getChildIds() {
    	$ids = [$this->id];
    	$childs_1 = $this->getChilds();
    	if($childs_1->count()) {
    		foreach ($childs_1 as $value_1) {
    			array_push($ids,$value_1->id);
    			$childs_2 = $value_1->getChilds();
		    	if($childs_2->count()) {
		    		foreach ($childs_2 as $value_2) {
		    			array_push($ids,$value_2->id);
		    		}
		    	}
    		}
    	}
    	return $ids;
    }
}
