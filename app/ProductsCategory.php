<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use DB;
class ProductsCategory extends Model
{
 
    public function product()
    {
        return $this->hasMany(Product::class,'category_id','id');
    }

    public function getIds(){
    	$ids[] = $this->id;
        $category_level2 = DB::table('products_categories')->where('status',1)->where('parent_id', $this->id)->get();
        if($category_level2){
            foreach ($category_level2 as $key => $value) {
                $ids[] = $value->id;
                $category_level3 = DB::table('products_categories')->where('status',1)->where('parent_id', $value->id)->get();
                if($category_level3){
                    foreach($category_level3 as $val){
                        $ids[] = $val->id;
                    }
                }
            }
        }
        return $ids;
    }
    public function getUrl() {
        return route('web.products_categories.show',$this->slug);
    }
    /*
    Lấy danh mục cha
     */
    public function getParent() {
        if ($this->parent_id == 0) return null;
        return self::where('id',$this->parent_id)->first();
    }
    public function getChildren(){
        return self::where('parent_id',$this->id)->get();
    }
    /*
    Lấy các danh mục ông cha ... dùng cho 1 số trường hợp như hiển thì breadcrumb
    @return mảng danh mục ông cha của danh mục hiện tại theo thứ tự
     */
    public function getParentCase() {
        $case = [];
        $parent_category = $this->getParent();
        while($parent_category) {
            array_unshift($case,$parent_category);
            $parent_category = $parent_category->getParent();
        }
        return $case;
    }
}
