<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ServiceCategory extends Model
{
    protected $table = "service_categories";

    public function getUrl() {
        return route('web.service_categories.show',['slug' => $this->slug]);
    }

    public function getDetail() {
    	return replace_detail($this->detail);
    }
}
