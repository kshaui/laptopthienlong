<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class PhukienProductCategory extends Model

{

     public function getUrl() {

        return route('web.phu_kien_products_categories.show',$this->slug);

    }

}

