<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class News extends Model
{
    public function getUrl() {
    	return route('web.news.show',$this->slug);
    }
    public function getImage($size = '') {
    	// $size: tiny 80, small 150, medium 300, large 600
        if($this->image != '') {
            return image_by_link($this->image,$size);
        }else
            return '/assets/img/no-image.png';
    }
    public function getAdminUser(){
        $data = DB::table('news')->where('id', $this->id)->first();
        $admin_user = DB::table('admin_users')->where('id',$data->admin_user_id)->first();
        if($admin_user->display_name != ''){
            return $admin_user->display_name;
        }else{
            return 'Quản trị viên';
        }
    }
}
