<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Comment extends Model
{
    // public function getReplyComment($type){
    //     return DB::table('comments')->where('parent_id', $this->id)->where('type', $type)->where('status',1)->get();
    // }
    // public function countReplyComment($type){
    // 	return DB::table('comments')->where('parent_id', $this->id)->where('type', $type)->where('status',1)->count();
    // }

    public function getReply($id)
    {
        return DB::table('replies')->where('comment_id',$id)->get();
    }
    public function getReply2($id)
    {
     	return DB::table('comments')->where('parent_id',$id)->where('status',1)->get();
    }
}