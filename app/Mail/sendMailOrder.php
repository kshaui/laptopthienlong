<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendMailOrder extends Mailable
{
    use Queueable, SerializesModels;

    private $data_user;
    private $data_insert;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data_user, $data_insert)
    {
        $this->data_user = json_decode(base64_decode($data_user));
        $this->data_insert = json_decode(base64_decode($data_insert));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('tranthanh.ceo96@gmail.com')->subject('[Chú ý] - Có 1 đơn hàng mới trên website Laptop247hn')->view('emails.notification_order',[
            'data_user'=>$this->data_user[0],'data_insert'=>$this->data_insert
        ]);
    }
}