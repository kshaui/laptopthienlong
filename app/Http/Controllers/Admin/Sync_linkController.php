<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;

class Sync_linkController extends Controller
{
    function __construct()
    {
        $this->module_name = 'Link chuyển tiếp';
        $this->table_name = 'sync_link';
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('old_link','Link cũ','string',1);
        $listdata->add('new_link','Link chuyển tiếp','string',1);
        $listdata->add('created_at','Thời gian');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->checkRole($this->table_name.'_create');
        $form = new MyForm();
        $data_form[] = $form->text('old_link','',1,'Link cũ');
        $data_form[] = $form->text('new_link','',1,'Link chuyển tiếp');
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->checkRole($this->table_name.'_create');

        $this->validate_form($request,'old_link',1,'Bạn chưa link cũ');
        $this->validate_form($request,'new_link',1,'Chưa nhập link chuyển tiếp');

        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 1;
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng
        $data_insert = compact('new_link','old_link','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);

        // $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);

        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update(['status'=>4]);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
