<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use Auth;

class NewsController extends Controller
{
    function __construct()
    {
        $this->module_name = 'tin tức';
        $this->table_name = 'news';
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $categories = new Categories('news_categories');
        $array_categories = $categories->data_select_categories();

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('image','Ảnh đại diện','string');
        $listdata->add('name','Tiêu đề tin','string',1);
        $listdata->add('category_id','Danh mục','array',1,$array_categories);
        $listdata->add('','Thông tin');
        $listdata->add('home','Ghim trang chủ','pins');
        $listdata->add('hot','Ghim hot','pins');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data','array_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkRole($this->table_name.'_create');

        $categories = new Categories('news_categories');
        $array_categories = $categories->data_select_categories();

        $form = new MyForm();
        $data_form[] = $form->text('name','',1,'Tiêu đề tin','',1,'slug');
        $data_form[] = $form->slug('slug','');
        $data_form[] = $form->select('category_id',0,1,'Danh mục',$array_categories);
        $data_form[] = $form->image('image','',0,'Ảnh đại diện','Chọn làm ảnh đại diện','Để hiện thị đẹp nhất. Đề nghị kích thước ảnh 750x450');
        $data_form[] = $form->textarea('description','',0,'Mô tả ngắn');
        $data_form[] = $form->editor('detail','',0,'Nội dung');
        // $data_form[] = $form->tags('tags','',0,'Tags');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->checkRole($this->table_name.'_create');
        $data_form = $request->all();
        //Kiểm tra xem slug đã tồn tại và có status = 4 trong DB chưa. nếu tồn tại thì xóa đi
        $this->checkSlug($this->table_name, $data_form['slug']);

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống',1,'Đường dẫn bị trùng');

        $created_at = $updated_at = date("Y-m-d H:i:s");
        $status = 2;
        $admin_user_id = Auth::guard('admin')->user()->id;
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng
        $data_insert = compact('admin_user_id','category_id','name','slug','image','description','detail','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);

        // if($tags != null) {
        //     $array_tags = explode(',',$tags);
        //     foreach($array_tags as $tag_name) {
        //         $tag_id = $this->inTags($tag_name);
        //         DB::table('news_tags_map')->insert(['news_id'=>$id_insert,'tag_id'=>$tag_id]);
        //     }
        // }

        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $categories = new Categories('news_categories');
        $array_categories = $categories->data_select_categories();

        // $list_tags = DB::table('tags')->join('news_tags_map','tags.id','=','news_tags_map.tag_id')->where('news_id',$id)->get();
        // $array_tags = [];
        // if ($list_tags->count()) {
        //     foreach ($list_tags as $value) {
        //         $array_tags[] = $value->name;
        //     }
        // }
        // $tags = implode(',',$array_tags);

        $form = new MyForm();
        $data_form[] = $form->text('name',$data_edit->name,1,'Tiêu đề tin','',1,'slug');
        $data_form[] = $form->slug('slug',$data_edit->slug);
        $data_form[] = $form->select('category_id',$data_edit->category_id,1,'Danh mục',$array_categories);
        $data_form[] = $form->image('image',$data_edit->image,0,'Ảnh đại diện','Chọn làm ảnh đại diện','Để hiện thị đẹp nhất. Đề nghị kích thước ảnh 750x450');
        $data_form[] = $form->textarea('description',$data_edit->description,0,'Mô tả ngắn');
        $data_form[] = $form->editor('detail',$data_edit->detail,0,'Nội dung');
        // $data_form[] = $form->tags('tags',$tags,0,'Tags');
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
        $data_form[] = $form->action('edit',route('web.'.$this->table_name.'.show',$data_edit->slug));
        return view('admin.layouts.edit',compact('data_form','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống');

        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
        $data_update = compact('category_id','name','slug','image','description','detail','status','updated_at');

        DB::table($this->table_name)->where('id',$id)->update($data_update);

        DB::table('news_tags_map')->where('news_id',$id)->delete();
        if($tags != null) {
            $array_tags = explode(',',$tags);
            foreach($array_tags as $tag_name) {
                $tag_id = $this->inTags($tag_name);
                DB::table('news_tags_map')->insert(['news_id'=>$id,'tag_id'=>$tag_id]);
            }
        }

        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
        
        $old = [
            'category_id'=>$data_edit->category_id,
            'name'=>$data_edit->name,
            'slug'=>$data_edit->slug,
            'image'=>$data_edit->image,
            'description'=>$data_edit->description,
            'detail'=>$data_edit->detail,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);

        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
                'slug' => DB::raw("CONCAT(slug, '--delete--".time()."')")
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }

    }
}
