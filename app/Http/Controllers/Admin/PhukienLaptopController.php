<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\MyClass\MyForm;

use App\MyClass\ListData;

use App\MyClass\Categories;

use DB;

use Illuminate\Support\Facades\Auth;

class PhukienLaptopController extends Controller

{

   function __construct()



    {



        $this->module_name = 'Sản phẩm phụ kiện laptop';



        $this->table_name = 'phukien_products';



        $this->has_google_shopping = true;



        parent::__construct();



    }



    public function index(Request $request)



    {



        $this->checkRole($this->table_name.'_access');







        $categories = new Categories('phukien_product_categories');



        $array_categories = $categories->data_select_categories();







        $listdata = new ListData($request,$this->table_name);

        $listdata->add('sku','Mã phụ kiện ','string',1);

        $listdata->add('image','Ảnh đại diện','string');



        $listdata->add('name','Tên phụ kiện','string',1);



        $listdata->add('','Giá thấp','int',1);



        $listdata->add('','Giá cao','int',1);



        $listdata->add('category_id','Danh mục','array',1,$array_categories);



        $listdata->add('updated_at','Thời điểm cập nhật','range',1);



        // $listdata->add('noibat','Ghim sản phẩm nổi bật','pins');



        $listdata->add('show_home','Ghim phụ kiện ra trang chủ','pins');

        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);



        $listdata->add('','Sửa','edit');



        $listdata->add('','Xóa','delete');







        $data = $listdata->data();



        return view('admin.layouts.list',compact('data','array_categories'));



    }



    public function create()



    {



        $this->checkRole($this->table_name.'_create');

        $categories = new Categories('phukien_product_categories');

        $array_categories = $categories->data_select_categories();

        $warehouse_status = config('app.warehouse_status');

        $form = new MyForm();

        $data_form[] = $form->text('name','',1,'Tên sản phẩm','',1,'slug');



        $data_form[] = $form->slug('slug','');



        $data_form[] = $form->select('category_id',0,1,'Danh mục',$array_categories);





        $data_form[] = $form->text('sku','',0,'Mã sản phẩm');



        $data_form[] = $form->select('warehouse_status',0,0,'Tình trạng',$warehouse_status);



        $data_form[] = $form->number('price','',0,'Giá thấp');



        $data_form[] = $form->number('price_old','',0,'Giá cao');



        $data_form[] = $form->number('guarantee','',0,'Thời gian bảo hành');



        $data_form[] = $form->image('image','',0);



        $data_form[] = $form->slide('slides','',0);



        $data_form[] = $form->related('related_products','',0,'Chọn 4 sản phẩm liên quan','Tìm theo tên sản phẩm ...','phukien_products');



        $data_form[] = $form->editor('description','',0,'Mô tả sản phẩm','');



        $data_form[] = $form->editor('detail','',0,'Nội dung');



        $data_form[] = $form->editor('promotion','',0,'Khuyến mại','');



        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');







        $data_form[] = $form->action('add');



        return view('admin.layouts.create',compact('data_form'));



    }



    public function store(Request $request)



    {



        $this->checkRole($this->table_name.'_create');



        $data_form = $request->all();



        //Kiểm tra xem slug đã tồn tại và có status = 4 trong DB chưa. nếu tồn tại thì xóa đi



        $this->checkSlug($this->table_name, $data_form['slug']);



        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');



        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống',1,'Đường dẫn bị trùng');



        $this->validate_form($request,'category_id',1,'Bạn chưa chọn danh mục');



        $created_at = $updated_at = date("Y-m-d H:i:s");

        $status = 2;

        $user_id = Auth::guard('admin')->user()->id;

        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng







        if (isset($slides) && $slides != '') {



            $slides = implode(',', $slides);



        }else {



            $slides = '';



        }



        if (isset($related_products)) {



            $related_products = implode(',',$related_products);



        }else {



            $related_products = '';



        }



        $sku = '#pk'.time();



        $check_sku = DB::table($this->table_name)->where('status','!=',4)->where('sku',$sku)->get();



        if(!empty($check_sku)){



            $sku = '#pk'.time();



        }



        $data_insert = compact('category_id','user_id','name','slug','price','price_old','image','slides','description','detail','sku','warehouse_status','related_products','promotion','guarantee','status','created_at','updated_at');



        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);



        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);



        $this->googleShopping($id_insert,$google_shopping_brand,$google_shopping_category,$google_shopping_instock,$google_shopping_itemcondition);



        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);



        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);



    }



    public function show($id)



    {



        //



    }



    public function edit($id)



    {



        $this->checkRole($this->table_name.'_edit');







        // $data_edit = DB::table($this->table_name)->where('id',$id)->first();



         $data_edit = DB::table($this->table_name)->where('id',$id)->first();



        $categories = new Categories('phukien_product_categories');



        $array_categories = $categories->data_select_categories();



        $warehouse_status = config('app.warehouse_status');



        $form = new MyForm();



        $data_form[] = $form->text('name',$data_edit->name,1,'Tên sản phẩm','',1,'slug');



        $data_form[] = $form->slug('slug',$data_edit->slug);



        $data_form[] = $form->select('category_id',$data_edit->category_id,1,'Danh mục',$array_categories);



        $data_form[] = $form->number('price',$data_edit->price,0,'Giá thấp');



        $data_form[] = $form->number('price_old',$data_edit->price_old,0,'Giá cao');



        $data_form[] = $form->number('guarantee',$data_edit->guarantee,0,'Thời gian bảo hành');



        $data_form[] = $form->image('image',$data_edit->image,0);



        $data_form[] = $form->slide('slides',explode(',',$data_edit->slides),0);



        $data_form[] = $form->related('related_products',$data_edit->related_products,0,'Chọn 3 sản phẩm liên quan','Tìm theo tên sản phẩm ...','products');



        $data_form[] = $form->editor('description',$data_edit->description,0,'Mô tả sản phẩm','');



        $data_form[] = $form->editor('promotion',$data_edit->promotion,0,'Khuyến mại','');



        $data_form[] = $form->editor('detail',$data_edit->detail,0,'Nội dung');



        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');







        $data_form[] = $form->action('edit',route('web.phu_kien_products.show',$data_edit->slug));



        return view('admin.layouts.edit',compact('data_form','id'));



    }



    public function update(Request $request, $id)



    {



        $this->checkRole($this->table_name.'_edit');



        $data_edit = DB::table($this->table_name)->where('id',$id)->first();







        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');



        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống');



        $this->validate_form($request,'category_id',1,'Bạn chưa chọn danh mục');







        $updated_at = date("Y-m-d H:i:s");



        $data_form = $request->all();



        $status = 2;



        extract($data_form,EXTR_OVERWRITE);







        if (isset($slides) && $slides != '') {



            $slides = implode(',', $slides);



        }else {



            $slides = '';



        }



        if (isset($related_products)) {



            $related_products = implode(',',$related_products);



        }else {



            $related_products = '';



        }







        $data_update = compact('category_id','name','slug','price','price_old','image','slides','description','detail','related_products','promotion','guarantee','status','updated_at');



        DB::table($this->table_name)->where('id',$id)->update($data_update);





        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);



        $this->googleShopping($id,$google_shopping_brand,$google_shopping_category,$google_shopping_instock,$google_shopping_itemcondition);



        



        $old = [



            'name'=>$data_edit->name,



            'slug'=>$data_edit->slug,



            'price'=>$data_edit->price,



            'price_old'=>$data_edit->price_old,



            'image'=>$data_edit->image,



            'slides'=>$data_edit->slides,



            'description'=>$data_edit->description,



            'detail'=>$data_edit->detail,



            'related_products'=>$data_edit->related_products,



            'status'=>$data_edit->status,



            'updated_at'=>$data_edit->updated_at



        ];



        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);







        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);



    }



    public function destroy($id)



    {



        if($this->hasRole($this->table_name.'_delete')) {



            DB::table($this->table_name)->where('id',$id)->update([



                'status'=>4,



                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),



                'slug' => DB::raw("CONCAT(slug, '--delete--".time()."')")



            ]);



            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);



            return response()->json(['status'=>1,'message'=>'Xóa thành công']);



        }else {



            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);



        }



    }

}

