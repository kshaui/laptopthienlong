<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Product;
use Request;
use DB;
use Mail;
use App\Mail\resetPassword;
use App\MyClass\VerifyEmail;
class AjaxController extends Controller
{
    public function updateStatus(){
    	if(Request::ajax()){
			$id=Request::get('id');
			$table=Request::get('table');
			$status=Request::get('status');
		}
		DB::table($table)->where('id',$id)->update(['status'=>$status]);
    }
    public function deleteOne(){
    	if(Request::ajax()){
			$id=Request::get('id');
			$table=Request::get('table');
		}
        if($table=='news'){
            DB::table($table)->where('id',$id)->update(['status'=>4]);
            DB::table('tags_map')->where('id_news',$id)->delete();
        }else{
		  DB::table($table)->where('id',$id)->update(['status'=>4]);
        }
    }
    public function getSlug(){
    	if(Request::ajax()){
    		$title=Request::get('title');
    	}
    	$slug=slugTitle($title);
    	echo $slug;
    }
    public function quickEditCate(){
        if(Request::ajax()){
            $id=Request::get('id');
            $cate_name=Request::get('cate_name');
            $cate_slug=Request::get('cate_slug');
            $cate_order=Request::get('cate_order');
        }
        DB::table('categories')->where('id',$id)->update(['name' => $cate_name, 'slug'=>$cate_slug,'order'=>$cate_order]);
    }
    public function quickEditTags(){
        if(Request::ajax()){
            $id=Request::get('id');
            $tag_name=Request::get('tag_name');
            $tag_slug=Request::get('tag_slug');
        }
        DB::table('tags')->where('id',$id)->update(['name' => $tag_name, 'slug'=>$tag_slug]);
    }
    public function quickEditProduct(){
        if(Request::ajax()){
            $id=Request::get('id');
            $pro_name=Request::get('pro_name');
            $pro_slug=Request::get('pro_slug');
            $pro_price=Request::get('pro_price');
            $pro_code=Request::get('pro_code');
        }
        $pro_price=str_replace(',','',$pro_price);
        if(empty($pro_price)){
            $pro_price=0;
        }
        DB::table('products')->where('id',$id)->update(['name' => $pro_name, 'slug'=>$pro_slug,'price'=>$pro_price,'code'=>$pro_code]);
    }
    public function quickEditSlier(){
        if(Request::ajax()){
            $id=Request::get('id');
            $sli_name=Request::get('sli_name');
            $sli_link=Request::get('sli_link');
            $sli_order=Request::get('sli_order');
        }
        DB::table('sliders')->where('id',$id)->update(['name' => $sli_name, 'order'=>$sli_order,'link'=>$sli_link]);
    }
    public function resetPassword(){
        if(Request::ajax()){
            $email=Request::get('email');
            $data = DB::table('admin_users')->where('email', $email)->where('status',1)->first();
            $result['message'] = '';
            $result['status'] = 0;

            $vmail = new VerifyEmail();
            $vmail->setStreamTimeoutWait(20);

            $vmail->setEmailFrom('tranthanh.ceo96@gmail.com');
            if ($vmail->check($email)) {
                if($data){
                    $password = rand_string(8);
                    $data_email['password'] = $password;
                    DB::table('admin_users')->where('email', $email)->update(['password'=>bcrypt($password)]);
                    Mail::to($email)->send(new resetPassword($data_email));
                    $result['message'] = 'Vui lòng kiểm tra email bạn vừa nhập để nhận lại mật khẩu mới';
                    $result['status'] = 1;
                }else{
                    $result['message'] = 'Email bạn vừa nhập không tồn tại hoặc không được phép hoạt động trên hệ thống. Vui lòng liên hệ quản trị cấp cao hơn';
                }
            } elseif (VerifyEmail::validate($email)) {
                $result['message'] = 'Email không tồn tại !';
            } else {
                $result['message'] = 'Email không hợp lệ!';
            }
            return json_encode($result);
        }
    }
    public function labelSale(){
        if(Request::ajax()){
            $id=Request::get('id');
            $value=Request::get('value');
            $updated_at = date("Y-m-d H:i:s");
            DB::table('products')->where('id',$id)->update(['label_sale'=>$value,'updated_at'=>$updated_at]);
            return 1;
        }
    }
     public function banChay(){
        if(Request::ajax()){
            $id=Request::get('id');
            $value=Request::get('value');
            $updated_at = date("Y-m-d H:i:s");
            DB::table('products')->where('id',$id)->update(['ban_chay'=>$value,'updated_at'=>$updated_at]);
            return 1;
        }
    }

    public function duplicate(){
        if(Request::ajax()){
            $id=Request::get('id');
            $table=Request::get('table');
        }
        if ($table=='products') {
            $product = Product::find($id);
            // $product_color_map = DB::table('product_color_map')->where('product_id',$product->id)->get();
           
            $id_insert = Product::insertGetId(['name'=>$product->name.'(copy)','category_id'=>$product->category_id,'slug'=>$product->slug.'-copy','image'=>$product->image,'slides'=>$product->slides,'specifications'=>$product->specifications,'description'=>$product->description,'detail'=>$product->detail,'specifications'=>$product->specifications,'price'=>$product->price,'price_old'=>$product->price_old,'status'=>$product->status,'warehouse_status'=>$product->warehouse_status,'related_products'=>$product->related_products,'promotion'=>$product->promotion,'guarantee'=>$product->guarantee,'created_at'=>date("Y-m-d H:i:s")]);
            // foreach ($product_color_map as $key => $value) {
            //     DB::table('product_color_map')->insert(['product_id'=>$id_insert,'color_id'=>$value->color_id]);
            // }
          
            DB::table('products_categories_map')->insert(['product_id'=>$id_insert,'category_id'=>$product->category_id]);
            $result['status'] = 1;
        }else if($table=='news'){
            $news = News::find($id);
            News::insert(['name'=>$news->name."(copy)",'category_id'=>$news->category_id,'admin_id'=>$news->admin_id,'slug'=>$news->slug."-copy",'image'=>$news->image,'detail'=>$news->detail,'related_news'=>$news->related_news,'related_designs'=>$news->related_designs,'related_products'=>$news->related_products,'status'=>1,'created_at'=>date("Y-m-d H:i:s")]);
            $result['status'] = 1;
        }else{
            $result['status'] = 0;
        }

        return json_encode($result);
    }
    public function DeleteCache()
    {
        cache::flush();
       return 1;
    }
}
