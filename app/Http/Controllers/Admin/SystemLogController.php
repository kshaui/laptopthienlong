<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use App\Product;
use Illuminate\Support\Facades\Auth;

class SystemLogController extends Controller
{
    function __construct()
    {
        $this->module_name = 'logs hệ thống';
        $this->table_name = 'system_logs';
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $list_admins = DB::table('admin_users')->get();
        $array_admins = [];
        foreach ($list_admins as $value) {
            $array_admins[$value->id] = $value->name;
        }

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('admin_id','Người thao tác','array',1,$array_admins);
        $listdata->add('ip','Địa chỉ IP','string');
        $listdata->add('time','Thời điểm','timestamp',0);
        $listdata->add('title','Nội dung','string',0);
        $listdata->add('type','Kiểu hành động','logs',1,config('app.log_type'));
        $listdata->add('table','Module thao tác','logs',1,config('app.log_table'));
        $listdata->add('','Chi tiết','string');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data','array_admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo "Tính năng này không khả dụng";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->checkRole($this->table_name.'_access');

        $list_admins = DB::table('admin_users')->get();
        $array_admins = [];
        foreach ($list_admins as $value) {
            $array_admins[$value->id] = $value->name;
        }

        $log = DB::table($this->table_name)->where('id',$id)->first();
        $data = DB::table($log->table)->where('id',$log->table_id)->first();
        if($log) {
            return view('admin.system_logs.show',compact('log','array_admins','data'));
        }else {
            exit('Không tìm thấy log');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
