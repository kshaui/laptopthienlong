<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
class CouponController extends Controller
{
    function __construct()
    {
        $this->module_name = 'chương trình khuyến mại';
        $this->table_name = 'coupons';
        parent::__construct();
    }
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('name','Tên chương trình','string',1);
        $listdata->add('description','Mô tả');
        $listdata->add('','Bắt đầu - Kết thúc');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data'));
    }

    public function create()
    {
        $this->checkRole($this->table_name.'_create');

        $form = new MyForm();
        $data_form[] = $form->text('name','',1,'Tên chương trình','');
        $data_form[] = $form->editor('description','',0,'Mô tả chương trình','Xuống dòng với mỗi mô tả mới');
        $data_form[] = $form->related('related_products','',0,'Chọn sản phẩm áp dụng','Tìm theo tên sản phẩm ...','products');
        $data_form[] = $form->datetimepicker('start','',1,'Thời điểm bắt đầu');
        $data_form[] = $form->datetimepicker('end','',1,'Thời điểm kết thúc');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));
    }

    public function store(Request $request)
    {
        $this->checkRole($this->table_name.'_create');
        $data_form = $request->all();
        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');

        $created_at = $updated_at = date("Y-m-d H:i:s");
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng
        
        $data_insert = compact('name','description','start','end','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);
        foreach ($related_products as $key => $value) {
            DB::table('product_coupon_map')->insert(['product_id'=>$value,'coupon_id'=>$id_insert]);
        }

        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
 
    }

    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();
        $product_coupon_map = DB::table('product_coupon_map')->where('coupon_id',$id)->pluck('product_id')->toArray();
        $related_products = implode(',', $product_coupon_map);
        $form = new MyForm();
        $data_form[] = $form->text('name',$data_edit->name,1,'Tên chương trình khuyến mại');
        $data_form[] = $form->editor('description',$data_edit->description,0,'Mô tả chương trình','Xuống dòng với mỗi mô tả mới');
        $data_form[] = $form->related('related_products',$related_products,0,'Chọn sản phẩm áp dụng','Tìm theo tên sản phẩm ...','products');
        $data_form[] = $form->datetimepicker('start',$data_edit->start,1,'Thời điểm bắt đầu');
        $data_form[] = $form->datetimepicker('end',$data_edit->end,1,'Thời điểm kết thúc');
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
        $data_form[] = $form->action('edit');
        return view('admin.layouts.edit',compact('data_form','id'));
    }

  
    public function update(Request $request, $id)
    {
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');

        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
        
        $data_update = compact('name','description','start','end','status','updated_at');

        DB::table($this->table_name)->where('id',$id)->update($data_update);

        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
        
        $old = [
            'name'=>$data_edit->name,
            'description'=>$data_edit->description,
            'start'=>$data_edit->start,
            'end'=>$data_edit->end,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        DB::table('product_coupon_map')->where('coupon_id',$id)->delete();
        foreach ($related_products as $key => $value) {
            DB::table('product_coupon_map')->insert(['product_id'=>$value,'coupon_id'=>$id]);
        }
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);

        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
   
    }

   
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')")
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
