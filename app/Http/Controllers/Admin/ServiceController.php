<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use App\Service;
class ServiceController extends Controller
{
    function __construct()
    {
        $this->module_name = 'sửa chữa';
        $this->table_name = 'services';
        parent::__construct();
    }
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $categories = new Categories('service_categories');
        $array_categories = $categories->data_select_categories();

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('image','Ảnh đại diện','string');
        $listdata->add('name','Tên','string',1);
        $listdata->add('category_id','Danh mục','array',1,$array_categories);
        $listdata->add('','Thông tin');
        $listdata->add('highlight','Ghim Nổi bật','pins');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data','array_categories'));        
    }
    public function create()
    {
        //
        $this->checkRole($this->table_name.'_create');

        $categories = new Categories('service_categories');
        $array_categories = $categories->data_select_categories();
        
        $classes = new Categories('classes');
        $array_classes = $classes->data_select_categories();

        $array_instock_status = config('app.instock_status');
        $form = new MyForm();
     
        $data_form[] = $form->text('name','',1,'Tên','',1,'slug');
        $data_form[] = $form->slug('slug','');
        $data_form[] = $form->select('category_id',0,1,'Danh mục hãng',$array_categories);
        $data_form[] = $form->select('class_id',0,1,'Danh mục loại',$array_classes);
        $data_form[] = $form->image('image','',0);
        $data_form[] = $form->select('instock_status','',0,'Trạng thái hàng',$array_instock_status);
        $data_form[] = $form->text('warranty','',0,'Bảo hành','VD: 6 tháng');
       
        // $data_form[] = $form->textarea('info','',0,'Thông tin cơ bản','Xuống dòng với mỗi thông tin');
        $data_form[] = $form->textarea('promotion','',0,'Khuyến mãi','Xuống dòng với mỗi khuyến mãi');
      
        $data_form[] = $form->text('price','',0,'Giá dịch vụ');
        $data_form[] = $form->editor('price_table','',0,'Bảng giá dịch vụ');
       
     
        $data_form[] = $form->checkbox('internal_link',0,1,'Link nội bộ');
     
        $data_form[] = $form->related('related_service',"",0,"Chọn 3 dịch vụ liên quan","Tìm theo tên dịch vụ",'services');
        $data_form[] = $form->related('related_news',"",0,"Chọn tin tức liên quan","Tìm theo tên tin",'news');
        $data_form[] = $form->text('videos',"",0,'Video sản phẩm','Dán link video url từ youtube vào đây');
        $data_form[] = $form->textarea('infor','',0,'Mô tả ');
        $data_form[] = $form->editor('detail','',0,'Nội dung');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');

        return view('admin.layouts.create',compact('data_form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->checkRole($this->table_name.'_create');

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống',1,'Đường dẫn bị trùng');
        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
       
        if (isset($related_service)) {
            $related_service = implode(',',$related_service);
        }else {
            $related_service = '';
        }
        if (isset($related_news)) {
            $related_news = implode(',',$related_news);
        }else {
            $related_news = '';
        }
       
        if(isset($internal_link)){
            $detail = insert_internal_link($detail);
        }
        $data_insert = compact('category_id','class_id','name','slug','image','videos','price','price_table','warranty','promotion','instock_status','related_service','related_news','infor','detail','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);
        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $categories = new Categories('service_categories');
        $array_categories = $categories->data_select_categories();

        $classes = new Categories('classes');
        $array_classes = $classes->data_select_categories();

        $array_instock_status = config('app.instock_status');


        //Xử lý price table đối với bản cũ là dữ liệu json
        

        $form = new MyForm();
        $data_form[] = $form->text('name',$data_edit->name,1,'Tiêu đề tin','',1,'slug');
        $data_form[] = $form->slug('slug',$data_edit->slug);
        $data_form[] = $form->select('category_id',$data_edit->category_id,1,'Danh mục',$array_categories);
        $data_form[] = $form->select('class_id',$data_edit->class_id,0,'Danh mục loại',$array_classes);
        $data_form[] = $form->image('image',$data_edit->image,0);
        $data_form[] = $form->select('instock_status','',0,'Trạng thái hàng',$array_instock_status);
        $data_form[] = $form->text('warranty',$data_edit->warranty,0,'Bảo hành','VD: 6 tháng');
        $data_form[] = $form->textarea('promotion',$data_edit->promotion,0,'Khuyến mại');
        $data_form[] = $form->text('price',$data_edit->price,0,'Giá dịch vụ');
         $data_form[] = $form->editor('price_table',$data_edit->price_table,0,'Bảng giá dịch vụ');

         $data_form[] = $form->related('related_service',$data_edit->related_service,0,"Chọn 3 dịch vụ liên quan","Tìm theo tên dịch vụ",'services');
        $data_form[] = $form->related('related_news',$data_edit->related_news,0,"Chọn tin tức liên quan","Tìm theo tên tin",'news');
        $data_form[] = $form->text('videos',"",$data_edit->videos,'Video sản phẩm','Dán link video url từ youtube vào đây');
        $data_form[] = $form->textarea('infor',$data_edit->infor,0,'Mô tả ');
        $data_form[] = $form->editor('detail',$data_edit->detail,0,'Nội dung');

     
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
        $data_form[] = $form->action('edit',route('web.services.show',$data_edit->slug));
        
        return view('admin.layouts.edit',compact('data_form','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống');
        $this->validate_form($request,'category_id',1,'Bạn chưa chọn danh mục');
        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        extract($data_form,EXTR_OVERWRITE);
        if (isset($related_service)) {
            $related_service = implode(',',$related_service);
        }else {
            $related_service = '';
        }
        if (isset($related_news)) {
            $related_news = implode(',',$related_news);
        }else {
            $related_news = '';
        }
     
        if(isset($internal_link)){
            $detail = insert_internal_link($detail);
        }
        if (isset($status)) {
            $status = 1;
        } else {
            $status = 2;
        }
        $data_update = compact('category_id','class_id','name','slug','image','videos','price','price_table','warranty','instock_status','promotion','related_news','related_service','infor','detail','status','created_at','updated_at');
        DB::table($this->table_name)->where('id',$id)->update($data_update);
        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
        $old = [
            'category_id'=>$data_edit->category_id,
            'class_id'=>$data_edit->class_id,
            'name'=>$data_edit->name,
            'slug'=>$data_edit->slug,
            'image'=>$data_edit->image,
            'detail'=>$data_edit->detail,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);
        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
                'slug' => DB::raw("CONCAT(slug, '--delete--".time()."')")
            ]);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
