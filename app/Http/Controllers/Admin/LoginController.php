<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    //hàm khởi tạo xác thực là guest của admin khi truy cập
    public function __construct()
    {
    }

    public function login(Request $request)
    {
        $field = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'name';
        $request->merge([$field => $request->input('login')]);

        if (Auth::guard('admin')->attempt($request->only($field, 'password')))
        {
            $this->systemLog('Đăng nhập hệ thống','login');
            return redirect('/admin_1996');
        }

        return redirect('/admin_1996/login')->withErrors([
            'error' => 'Sai tài khoản hoặc mật khẩu',
        ]);
    }

    public function showLoginForm()
    {
        if (!Auth::guard('admin')->check()){
            return view('admin.auth.login');
        }else{
            return redirect()->route('admin.dashboard');
        }
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }
    
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        if (!Auth::check() && !Auth::guard('admin')->check()) {
            $request->session()->flush();
            $request->session()->regenerate();
        }
        return redirect(route('admin.login'));
    }
    public function systemLog($title,$type='',$table='',$table_id=0,$detail='') {
        $ip = get_client_ip();
        $admin_id = Auth::guard('admin')->user()->id;
        $status = 1;
        $time = date("Y-m-d H:i:s");
        if($detail != '') {
            $detail = encode_compress_string(json_encode($detail));
        }
        DB::table('system_logs')->insert(compact('admin_id','ip','time','title','type','table','table_id','detail','status'));
    }
}
