<?php



namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Cache;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller as BaseController;



use Auth;

use DB;

use View;



class Controller extends BaseController

{

    public $module_name;

    public $table_name;

    public $has_seo = true;

    public $has_google_shopping = false;



    function __construct()

    {
 cache::flush();

        View::share('module_name', $this->module_name);

        View::share('table_name',$this->table_name);

        View::share('has_seo',$this->has_seo);

        View::share('has_google_shopping',$this->has_google_shopping);

    }



    /**

     * Check xem có quyền hay không

     * @param $role

     * @return bool

     */

    public function hasRole($role) {

        $admin_id = Auth::guard('admin')->user()->id;

        if($admin_id != 1){

            $authorizations = DB::table('authorizations')->where('admin_id',$admin_id)->first();

            $array_role = json_decode($authorizations->capabilities);

            if(!in_array($role, $array_role)){

                return false;

            }

        }

        return true;

    }



    /**

     * Check xem có quyền hay không, nếu không response luôn về 403

     * @param $role

     * @return bool|\Illuminate\Http\RedirectResponse

     */

    public function checkRole($role) {

        $admin_id = Auth::guard('admin')->user()->id;

        if($admin_id != 1){

            $authorizations = DB::table('authorizations')->where('admin_id',$admin_id)->first();

            $array_role = json_decode($authorizations->capabilities);

            if(!in_array($role, $array_role)){

                //return redirect()->route('admin.permission.denied');

                exit('Permission denied');

            }

        }

        return true;

    }



    public function permissionDenied() {

        return view('admin.layouts.403');

    }



    public function getSlug(Request $request){

        if($request->ajax()){

            $title = $request->get('title');

            $slug = slugTitle($title);

            echo $slug;

        }

    }

    public function checkSlug($table, $slug){

        DB::table($table)->where('status',4)->where('slug', $slug)->delete();

    }

    public function metaSeo($type_id,$title,$description,$robots) {

        $data_meta_seo = DB::table('meta_seo')->where('type',$this->table_name)->where('type_id',$type_id)->first();

        if(isset($data_meta_seo)){

            $update_data_seo = ['title'=>$title,'description'=>$description,'robots'=>$robots];

            DB::table('meta_seo')->where('type',$this->table_name)->where('type_id',$type_id)->update($update_data_seo);

        }

        else{

            $insert_data_seo=['type'=>$this->table_name, 'type_id'=>$type_id,'title'=>$title,'description'=>$description,'robots'=>$robots];

            DB::table('meta_seo')->insert($insert_data_seo);

        }

        return;

    }

    public function googleShopping($type_id,$brand,$category,$instock,$itemcondition) {

        $data = DB::table('google_shopping')->where('type',$this->table_name)->where('type_id',$type_id)->first();

        if(isset($data)){

            $update_data = ['brand'=>$brand,'category'=>$category,'instock'=>$instock,'itemcondition'=>$itemcondition];

            DB::table('google_shopping')->where('type',$this->table_name)->where('type_id',$type_id)->update($update_data);

        }

        else{

            $insert_data=['type'=>$this->table_name, 'type_id'=>$type_id,'brand'=>$brand,'category'=>$category,'instock'=>$instock,'itemcondition'=>$itemcondition];

            DB::table('google_shopping')->insert($insert_data);

        }

        return;

    }



    public function uniqueSlug($slug,$table) {

        $check = DB::table($table)->where('slug',$slug)->first();

        if ($check) {

            $i = 1;

            while ($i > 0) {

                $new_slug = $slug.'-'.$i;

                $check_again = DB::table($table)->where('slug',$new_slug)->first();

                if ($check_again) {

                    $i++;

                }else {

                    return $new_slug;

                }

            }

        }

        return $slug;

    }



    public function inTags($tag_name) {

        $check_name = DB::table('tags')->where('name',$tag_name)->first();

        if($check_name) {

            return $check_name->id;

        }else {

            $tag_slug = $this->uniqueSlug(slugTitle($tag_name),'tags');

            $created_at = date("Y-m-d H:i:s");

            $insert_id = DB::table('tags')->insertGetId(['name'=>$tag_name,'slug'=>$tag_slug,'status'=>1,'created_at'=>$created_at,'updated_at'=>$created_at]);

            return $insert_id;

        }

    }



    public function validate_form($request,$field, $required = 1, $required_message = '', $unique = 0, $unique_message = '') {

        if ($required)

            $request->validate([$field => 'required'],[$field.'.required' => $required_message]);

        if ($unique)

            $request->validate([$field => 'unique:'.$this->table_name],[$field.'.unique' => $unique_message]);

        return;

    }



    public function deleteAll(Request $request) {

        if($request->ajax()){

            $ids = $request->get('ids');

            $table = $request->get('table');

            if ($this->hasRole($table.'_delete')) {

                $delete_count = DB::table($table)->whereIn('id',$ids)->update(['status'=>4]);

                if ($delete_count > 0) {

                    $module_name = isset(config('app.log_table')[$table]) ? config('app.log_table')[$table] : 'không xác định';

                    foreach ($ids as $id) {

                        $this->systemLogs('Xóa '.$module_name,'delete',$table,$id);

                    }

                    return response()->json(['status'=>1,'message'=>'Có '.$delete_count.' bản ghi đã được xóa','ids'=>$ids,'table'=>$table]);

                }else {

                    return response()->json(['status'=>0,'message'=>'Có lỗi xảy ra với thao tác']);

                }

            }else {

                return response()->json(['status'=>0,'message'=>'Bạn không có quyền xóa']);

            }

        }else {

            return response()->json(['status'=>0,'message'=>'Hành động bị cấm !']);

        }

    }

    public function trashAll(Request $request) {

        if($request->ajax()){

            $ids = $request->get('ids');

            $table = $request->get('table');

            if ($this->hasRole($table.'_edit')) {

                $trash_count = DB::table($table)->whereIn('id',$ids)->update(['status'=>3]);

                if ($trash_count > 0) {

                    $module_name = isset(config('app.log_table')[$table]) ? config('app.log_table')[$table] : 'không xác định';

                    foreach ($ids as $id) {

                        $this->systemLogs('Thay đổi status = 3 '.$module_name,'update',$table,$id);

                    }

                    return response()->json(['status'=>1,'message'=>'Có '.$trash_count.' bản ghi đã được đưa vào thùng rác','ids'=>$ids,'table'=>$table]);

                }else {

                    return response()->json(['status'=>0,'message'=>'Có lỗi xảy ra với thao tác']);

                }

            }else {

                return response()->json(['status'=>0,'message'=>'Bạn không có quyền sửa']);

            }

        }else {

            return response()->json(['status'=>0,'message'=>'Hành động bị cấm !']);

        }

    }

    public function deactiveAll(Request $request) {

        if($request->ajax()){

            $ids = $request->get('ids');

            $table = $request->get('table');

            if ($this->hasRole($table.'_edit')) {

                $deactive_count = DB::table($table)->whereIn('id',$ids)->update(['status'=>2]);

                if ($deactive_count > 0) {

                    $module_name = isset(config('app.log_table')[$table]) ? config('app.log_table')[$table] : 'không xác định';

                    foreach ($ids as $id) {

                        $this->systemLogs('Thay đổi status = 2 '.$module_name,'update',$table,$id);

                    }

                    return response()->json(['status'=>1,'message'=>'Có '.$deactive_count.' bản ghi đã chuyển sang trạng thái không hoạt động','ids'=>$ids,'table'=>$table]);

                }else {

                    return response()->json(['status'=>0,'message'=>'Có lỗi xảy ra với thao tác']);

                }

            }else {

                return response()->json(['status'=>0,'message'=>'Bạn không có quyền sửa']);

            }

        }else {

            return response()->json(['status'=>0,'message'=>'Hành động bị cấm !']);

        }

    }

    public function saveOne(Request $request) {

        if($request->ajax()){

            $id = $request->get('id');

            $table = $request->get('table');

            $data = $request->get('data');

            $data = json_decode($data,true);

            if ($this->hasRole($table.'_edit')) {

                $check = DB::table($table)->where('id',$id)->first();

                if ($check) {

                    $save_count = DB::table($table)->where('id',$id)->update($data);

                    if ($save_count == 1) {



                        $module_name = isset(config('app.log_table')[$table]) ? config('app.log_table')[$table] : 'không xác định';

                        $this->systemLogs('Lưu nhanh '.$module_name,'update',$table,$id,['old'=>null,'new'=>$data]);



                        return response()->json(['status'=>1,'message'=>'Sửa thành công','ids'=>$id,'table'=>$table]);

                    }else {

                        return response()->json(['status'=>0,'message'=>'Bạn chưa thay đổi dữ liệu hoặc có lỗi xảy ra với thao tác']);

                    }

                }else {

                    return response()->json(['status'=>0,'message'=>'Không tìm thấy dữ liệu cần sửa']);

                }

            }else {

                return response()->json(['status'=>0,'message'=>'Bạn không có quyền sửa']);

            }

        }else {

            return response()->json(['status'=>0,'message'=>'Hành động bị cấm !']);

        }

    }

    public function saveAll(Request $request) {

        if($request->ajax()){

            $table = $request->get('table');

            $data = $request->get('data');

            $data = json_decode($data,true);

            if ($this->hasRole($table.'_edit')) {

                $count = 0;

                $ids = [];

                $module_name = isset(config('app.log_table')[$table]) ? config('app.log_table')[$table] : 'không xác định';

                foreach ($data as $value) {

                    if(isset($value['id'])) {

                        $id = $value['id'];

                        $ids[] = $id;

                        unset($value['id']);

                        $save_count = DB::table($table)->where('id',$id)->update($value);

                        if ($save_count == 1) {

                            $this->systemLogs('Lưu nhanh '.$module_name,'update',$table,$id,['old'=>null,'new'=>$value]);

                            $count++;

                        }

                    }

                }



                if ($count > 0) {

                    return response()->json(['status'=>1,'message'=>'Sửa thành công '.$count.' bản ghi','ids'=>implode(',',$ids),'table'=>$table]);

                }else {

                    return response()->json(['status'=>0,'message'=>'Bạn chưa thay đổi dữ liệu hoặc có lỗi xảy ra với thao tác','ids'=>implode(',',$ids),'table'=>$table]);

                }

            }else {

                return response()->json(['status'=>0,'message'=>'Bạn không có quyền sửa']);

            }

        }else {

            return response()->json(['status'=>0,'message'=>'Hành động bị cấm !']);

        }

    }



    public function relateSuggest(Request $request) {

        if($request->ajax()){

            $table = $request->get('table');

            $id = $request->get('id');

            $name = $request->get('name');

            $key = $request->get('key');

            $relate_list = DB::table($table)->where($name,'like','%'.$key.'%')->orderBy($id,'DESC')->limit(10)->offset(0)->get();

            if ($relate_list->count()) {

                $result = [];

                foreach ($relate_list as $relate) {

                    $result[] = ['id'=>$relate->$id,'name'=>$relate->$name];

                }

                return response()->json(['status'=>1,'message'=>'Tìm thấy dữ liệu','data'=>$result]);

            }else {

                return response()->json(['status'=>0,'message'=>'Không tìm thấy dữ liệu']);

            }

        }else {

            return response()->json(['status'=>0,'message'=>'Hành động bị cấm !']);

        }

    }



    public function pins(Request $request) {

        if($request->ajax()){

            $type_id = $request->get('type_id');

            $type = $request->get('type');

            $place = $request->get('place');

            $value = $request->get('value');

            $value = intval($value);

            if ($value > 0) {

                $data_pin = DB::table('pins')->where('type',$type)->where('type_id',$type_id)->where('place',$place)->first();

                if($data_pin){

                    $save = DB::table('pins')->where('type',$type)->where('type_id',$type_id)->where('place',$place)->update(['value'=>$value]);

                }else{

                    $save = DB::table('pins')->insert(['type'=>$type,'type_id'=>$type_id,'place'=>$place,'value'=>$value]);

                }

            }else {

                $save = DB::table('pins')->where('type',$type)->where('type_id',$type_id)->where('place',$place)->delete();

            }

            if($save) {

                return response()->json(['status'=>1,'message'=>'Ghim thành công']);

            }

            return response()->json(['status'=>0,'message'=>'Lỗi không xác định, có thể bạn chưa thực hiện ghim']);

        }else {

            return response()->json(['status'=>0,'message'=>'Hành động bị cấm !']);

        }

    }



    public function systemLogs($title,$type='',$table='',$table_id=0,$detail='') {

        $ip = get_client_ip();

        $admin_id = Auth::guard('admin')->user()->id;

        $status=1;

        $time = date("Y-m-d H:i:s");

        if($detail != '') {

            $detail = encode_compress_string(json_encode($detail));

        }

        DB::table('system_logs')->insert(compact('admin_id','ip','time','title','type','table','table_id','detail','status'));

    }

    public function replyComment(Request $request){

        if($request->ajax()){

            $content = $request->get('content');

            $name = $request->get('name');

            $id = $request->get('id');

            $data_comment = DB::table('comments')->where('id', $id)->first();

            $created_at = $updated_at = date("Y-m-d H:i:s");

            if($data_comment->parent_id != 0){

                $parent_id = $data_comment->parent_id;

            }else{

                $parent_id = $data_comment->id;

            }

            DB::table('comments')->insert(['type_id'=>$data_comment->type_id,'type'=>$data_comment->type,'parent_id'=>$parent_id,'gender'=>1,'name'=>$name,'email'=>'is_admin','phone'=>'0000000000','content'=>$content,'status'=>1,'created_at'=>$created_at,'updated_at'=>$updated_at]);

            return 1;

        }

    }

}

