<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;

class NewsCategoryController extends Controller
{
    function __construct()
    {
        $this->module_name = 'danh mục tin tức';
        $this->table_name = 'news_categories';
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('name','Tên danh mục','string',1);
        $listdata->add('','Thông tin');
        $listdata->add('order','Sắp xếp','string',0);
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data_categories(0,0,[1,2,3]);
        return view('admin.layouts.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkRole($this->table_name.'_create');

        $categories = new Categories($this->table_name);
        $array_categories = $categories->data_select_categories();

        $form = new MyForm();
        $data_form[] = $form->select('parent_id',0,0,'Danh mục cha',$array_categories);
        $data_form[] = $form->text('name','',1,'Tên danh mục','',1,'slug');
        $data_form[] = $form->slug('slug','');
        $data_form[] = $form->image('image','',0);
        $data_form[] = $form->editor('detail','',0,'Nội dung');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->checkRole($this->table_name.'_create');
        $data_form = $request->all();
        //Kiểm tra xem slug đã tồn tại và có status = 4 trong DB chưa. nếu tồn tại thì xóa đi
        $this->checkSlug($this->table_name, $data_form['slug']);
        
        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống',1,'Đường dẫn bị trùng');

        $created_at = $updated_at = date("Y-m-d H:i:s");
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng
        if(!isset($parent_id)) $parent_id = 0;//không chọn danh mục cha
        $data_insert = compact('parent_id','name','slug','image','detail','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);
        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $categories = new Categories($this->table_name);
        $array_categories = $categories->data_select_categories();

        //Thêm id của cat hiện tại và các cat con của nó vào danh sách disable ở mục chọn cat cha
        $disable_categories = new Categories($this->table_name);
        $array_disable = $disable_categories->data_categories($id);
        $array_disable = array_keys($array_disable);//mảng ids con cháu của cat hiện tại
        array_push($array_disable, $id);//push thêm id của chính nó để loại trừ

        $form = new MyForm();
        $data_form[] = $form->select('parent_id',$data_edit->parent_id,0,'Danh mục cha',$array_categories,$array_disable);
        $data_form[] = $form->text('name',$data_edit->name,1,'Tiêu đề','',1,'slug');
        $data_form[] = $form->slug('slug',$data_edit->slug);
        $data_form[] = $form->image('image',$data_edit->image,0);
        $data_form[] = $form->editor('detail',$data_edit->detail,0,'Nội dung');
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
        $data_form[] = $form->action('edit',route('web.'.$this->table_name.'.show',$data_edit->slug));
        return view('admin.layouts.edit',compact('data_form','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');
        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống');

        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
        if(!isset($parent_id)) $parent_id = 0;//không chọn danh mục cha
        $data_update = compact('parent_id','name','slug','image','detail','status','updated_at');

        DB::table($this->table_name)->where('id',$id)->update($data_update);
        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);
        
        $old = [
            'parent_id'=>$data_edit->parent_id,
            'name'=>$data_edit->name,
            'slug'=>$data_edit->slug,
            'image'=>$data_edit->image,
            'detail'=>$data_edit->detail,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);

        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
                'slug' => DB::raw("CONCAT(slug, '--delete--".time()."')")
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
