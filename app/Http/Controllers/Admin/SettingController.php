<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
/**
 * Class SettingController
 * @package App\Http\Controllers\Admin
 *
 * Phần cấu hình của core chúng ta chia thành 2 phần chính để tiện cho tối ưu query ngoài frontend:
 * - Cấu hình trang chủ: Seo (title, description, ...) của trang chủ, các nội dung chỉ hiện thị ở trang chủ như banner, các nội dung cố định ...
 * - Cấu hình chung: cho các nội dung chung trên toàn bộ các trang như: html head, hotline, link ở header, các thông tin ở footer ...
 *
 */

class SettingController extends Controller
{
    private function postData($setting_name, $data){
        unset($data['_token']);
        unset($data['submit']);
        $data = base64_encode(json_encode($data));
        if(DB::table('options')->where('name',$setting_name)->exists()){
            DB::table('options')->where('name',$setting_name)->update(['value'=>$data]);
        }else{
            DB::table('options')->insert(['name'=>$setting_name,'value'=>$data]);
        }
    }
    public function home(Request $request) {
        $setting_name = 'home';
        $title = "Cấu hình trang chủ";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }

        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        $data_form[] = $form->text('meta_title',isset($data['meta_title']) ? $data['meta_title'] : '',0,'Meta title');
        $data_form[] = $form->textarea('meta_description',isset($data['meta_description']) ? $data['meta_description'] : '',0,'Meta description','');
        $data_form[] = $form->text('hotline',isset($data['hotline']) ? $data['hotline'] : '',0,'Hotline');

        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));

    }
    public function general(Request $request) {
        $setting_name = 'general';
        $title = "Cấu hình chung";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        // post data
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        //
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        
        $data_form[] = $form->checkbox('robots',isset($data['robots']) ? $data['robots'] : '',1,' Cho phép google lập chỉ mục nội dung Website');//Cấu hình quan trọng
        $data_form[] = $form->textarea('html_head',isset($data['html_head']) ? $data['html_head'] : '',0,'Các thẻ html chèn vào head','');
        $data_form[] = $form->textarea('html_body',isset($data['html_body']) ? $data['html_body'] : '',0,'Các thẻ html chèn vào body','');
        $data_form[] = $form->text('notification_email',isset($data['notification_email']) ? $data['notification_email'] : '',0,'Email nhận thông báo (đơn hàng mới, liên hệ)','');

        $data_form[] = $form->title('Thông tin hiển thị header và footer');
        $data_form[] = $form->image('image_favicon',isset($data['image_favicon']) ? $data['image_favicon'] : '',0,'ảnh favicon','');
       
        $data_form[] = $form->image('logo_header',isset($data['logo_header']) ? $data['logo_header'] : '',0,'Logo hiển thị header','');
        $data_form[] = $form->image('logo_footer',isset($data['logo_footer']) ? $data['logo_footer'] : '',0,'Logo hiển thị footer','');
        $data_form[] = $form->text('hotline',isset($data['hotline']) ? $data['hotline'] : '',0,'Hotline hiển thị header');
        $data_form[] = $form->textarea('info_footer',isset($data['info_footer']) ? $data['info_footer'] : '',0,'Nội dung giới thiệu footer','');
        $data_form[] = $form->textarea('contacts_footer',isset($data['contacts_footer']) ? $data['contacts_footer'] : '',0,'Nội dung liên hệ footer','Xuống dòng mới mỗi thông tin mới');

        $data_form[] = $form->title('Cấu hình khuyến mại cho sản phẩm laptop');
        $data_form[] = $form->editor('promotion_laptop',isset($data['promotion_laptop']) ? $data['promotion_laptop'] : '',0,'Nội dung','Xuống dòng mới mỗi thông tin mới');


        $data_form[] = $form->title('Cấu hình khuyến mại cho dịch vụ sửa chữa');
        $data_form[] = $form->editor('promotion_service',isset($data['promotion_service']) ? $data['promotion_service'] : '',0,'Nội dung','Xuống dòng mới mỗi thông tin mới');


        $data_form[] = $form->title('Cấu hình trang chi tiết sản phẩm');
        $data_form[] = $form->textarea('doi_tra',isset($data['doi_tra']) ? $data['doi_tra'] : '',0,'Chính sách đổi trả','Xuống dòng mới mỗi thông tin mới');
        $data_form[] = $form->textarea('bao_quan',isset($data['bao_quan']) ? $data['bao_quan'] : '',0,'Hướng dẫn bảo quản','Xuống dòng mới mỗi thông tin mới');

        $data_form[] = $form->title('Cấu hình trang liên hệ');
        $data_form[] = $form->text('address',isset($data['address']) ? $data['address'] : '',0,'Địa chỉ');
        $data_form[] = $form->text('email',isset($data['email']) ? $data['email'] : '',0,'Email');
        $data_form[] = $form->text('phone',isset($data['phone']) ? $data['phone'] : '',0,'SĐT');
        $data_form[] = $form->text('fa_clock_o',isset($data['fa_clock_o']) ? $data['fa_clock_o'] : '',0,'Giờ mở cửa trong tuần');
        $data_form[] = $form->textarea('maps_contact',isset($data['maps_contact']) ? $data['maps_contact'] : '',0,'Iframe google map','');

        $data_form[] = $form->title('Cấu hình dịch vụ bên dưới slide trang chủ');
        $data_form[] = $form->textarea('ma_giam_gia',isset($data['ma_giam_gia']) ? $data['ma_giam_gia'] : '',0,'Chi tiết mã giảm gía','');
        $data_form[] = $form->textarea('ho_tro',isset($data['ho_tro']) ? $data['ho_tro'] : '',0,'Chi tiết hỗ trợ 24/7','');
        $data_form[] = $form->textarea('chinh_sach_doi_tra',isset($data['chinh_sach_doi_tra']) ? $data['chinh_sach_doi_tra'] : '',0,'Chi tiết Chính sách đổi trả','');
        $data_form[] = $form->textarea('khach_hang_trung_thanh',isset($data['khach_hang_trung_thanh']) ? $data['khach_hang_trung_thanh'] : '',0,'Chi tiết khách hàng trung thành','');

        
        $data_form[] = $form->title('Cấu hình trang thanh toán');
        $data_form[] = $form->text('payments',isset($data['payments']) ? $data['payments'] : '',0,'Link hướng dẫn thanh toán chuyển khoản');

        $data_form[] = $form->customMenu('menu_header', isset($data['menu_header']) ? $data['menu_header'] : '','Cấu hình menu header');

        $data_form[] = $form->customMenu('menu_primary', isset($data['menu_primary']) ? $data['menu_primary'] : '','Cấu hình menu danh mục');

        $data_form[] = $form->customMenu('menu_footer', isset($data['menu_footer']) ? $data['menu_footer'] : '','Cấu hình menu footer');

        $data_form[] = $form->customMenu('menu_about_us', isset($data['menu_about_us']) ? $data['menu_about_us'] : '','Danh sách trang đơn "Về Chúng Tôi" hiển thị footer',['pages']);

        

        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));
    }


    public function advertisement(Request $request) {
        $setting_name = 'advertisement';
        $title = "Banner quảng cáo";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }

        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();
        $data_form[] = $form->image('banner_ads',isset($data['banner_ads']) ? $data['banner_ads'] : '',0,'Chọn ảnh  quảng cáo','Chọn');
        $data_form[] = $form->image('banner_ads_mobile',isset($data['banner_ads_mobile']) ? $data['banner_ads_mobile'] : '',0,'Chọn ảnh quảng cáo cho mobile','Chọn');
        $data_form[] = $form->text('seconds',isset($data['seconds']) ? $data['seconds'] : '',0,'Số giây','Lưu ý tính theo ms ');
        $data_form[] = $form->text('link_popup',isset($data['link_popup']) ? $data['link_popup'] : '',0,'Link popup quảng cáo',' ');

        $data_form[] = $form->checkbox('status_popup',isset($data['status_popup']) ? $data['status_popup'] : '0',1,'Chọn chế độ bật (tắt) Popup','');

        $data_form[] = $form->action('editconfig');
      
        return view('admin.layouts.setting', compact('data_form','title','data'));

    }


    public function google_shopping(Request $request) {
        $setting_name = 'google_shopping';
        $title = "Cấu hình google_shopping";

        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();

        // post data
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        //
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
        $form = new MyForm();

        $data_form[] = $form->custom('admin.layouts.google_shopping_info');
        
        $data_form[] = $form->text('brand',isset($data['brand']) ? $data['brand'] : '',0,'Thương hiệu mặc định','');
        $data_form[] = $form->text('category',isset($data['category']) ? $data['category'] : '',0,'Danh mục Google shopping mặc định','');
        $data_form[] = $form->select('instock',isset($data['instock']) ? $data['instock'] : '',0,'Tình trạng kho hàng mặc định',[''=>'Mặc định','còn hàng'=>'còn hàng','hết hàng'=>'hết hàng']);
        $data_form[] = $form->select('itemcondition',isset($data['itemcondition']) ? $data['itemcondition'] : '',0,'Tình trạng sản phẩm mặc định',[''=>'Mặc định','mới'=>'mới','cũ'=>'cũ']);

        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title'));
    }


   public function seo_category(Request $request){
        $setting_name = 'seo_category';
        $title = "Cấu hình Nội dung Seo cho danh mục";
        $this->checkRole('settings_'.$setting_name);
        $data = $request->all();
        if(isset($data['submit'])){
            $this->postData($setting_name, $data);
        }
        $option = DB::table('options')->select('value')->where('name',$setting_name)->first();
        if(empty($option)){
            $data = [];
        }else{
            $data = json_decode(base64_decode($option->value),true);
        }
       
        $detail_fit = isset($data["detail_fit"])?$data["detail_fit"]:"";
        $title_fit = isset($data["title_fit"])?$data["title_fit"]:"";
        $desc_fit = isset($data["desc_fit"])?$data["desc_fit"]:"";
        $detail_service = isset($data["detail_service"])?$data["detail_service"]:"";
        $title_service = isset($data["title_service"])?$data["title_service"]:"";
        $desc_service = isset($data["desc_service"])?$data["desc_service"]:"";
        $detail_news = isset($data["detail_news"])?$data["detail_news"]:"";
        $title_news = isset($data["title_news"])?$data["title_news"]:"";
        $desc_news = isset($data["desc_news"])?$data["desc_news"]:"";
        $list_seo = compact('detail_fit','title_fit','desc_fit','detail_service','title_service','desc_service','detail_news','title_news','desc_news');
        $form = new MyForm();
        $data_form[] = $form->custom('admin.layouts.setting.seo');
        
        $data_form[] = $form->action('editconfig');
        return view('admin.layouts.setting', compact('data_form','title','list_seo'));
        
    }

}
