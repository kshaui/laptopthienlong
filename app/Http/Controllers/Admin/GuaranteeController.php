<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use DB;
use Carbon\Carbon;
class GuaranteeController extends Controller
{
     function __construct()
    {
        $this->module_name = 'Bảo hành';
        $this->table_name = 'guarantees';
        parent::__construct();
    }
    public function index(Request $request)
    {
        $baohang = DB::table($this->table_name)->where('end_date','<=',date('Y-m-d'))->get();
        if ($baohang) {
            foreach ($baohang as $value) {
                DB::table($this->table_name)->where('id',$value->id)->update([
                    'guarantee_time'=>'Hết bảo hành',
                    'status'=>2,
                ]);
                // $end_date = $value->end_date;
                // $date_delete = strtotime(date("Y-m-d", strtotime($end_date)) . " +7 day");
                // $date_delete = strftime("%Y-%m-%d", $date_delete);
                // if (strtotime($date_delete)<=strtotime(date('Y-m-d'))) {
                //      DB::table($this->table_name)->where('id',$value->id)->delete();
                // }
            }
        }
        $this->checkRole($this->table_name.'_access');
        $listdata = new ListData($request,$this->table_name);
        $listdata->add('name','Thông tin khách hàng','string',1);
        $listdata->add('product_name','Tên sản phẩm','string',1);
        $listdata->add('total_costs','Tổng tiền','string',0);
        $listdata->add('guarantee_time','Bảo hành bao lâu','string',0);
        $listdata->add('start_date','Ngày xuất kho','string',0);
        $listdata->add('end_date','Ngày hết bảo hành','string',0);
        $listdata->add('','Thời gian còn bảo hành','string',0);
        $listdata->add('updated_at','Thời điểm cập nhật','range',1);
        $listdata->add('status','Trạng thái','status',1,[1=>'Còn bảo hành',2=>'Hết bảo hành']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');
        $data = $listdata->data();
        return view('admin.layouts.list',compact('data'));
    }
    public function create()
    {
        $this->checkRole($this->table_name.'_create');
        $form = new MyForm();
        $data_form[] = $form->text('name','',0,'Nhập tên khách hàng');
        $data_form[] = $form->text('email','',0,'Nhập email');
        $data_form[] = $form->text('phone','',0,'Nhập số điện thoại');
        $data_form[] = $form->text('address','',0,'Nhập địa chỉ');
        $data_form[] = $form->text('total_costs','',0,'Giá tiền');
        $data_form[] = $form->datepicker('start_date','',0,'Ngày xuất kho');
        $data_form[] = $form->text('guarantee_time','',0,'Bảo hành bao lâu');
        $data_form[] = $form->editor('product_name','',0,'Tên sản phẩm');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));
    }
    public function store(Request $request)
    {
         $this->checkRole($this->table_name.'_create');
        $data_form = $request->all();
        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');

        $created_at = $updated_at = date("Y-m-d H:i:s");
        $status = 1;
        $start_date = $data_form['start_date'];
        $guarantee_time =$data_form['guarantee_time'];
        $end_date = strtotime(date("Y-m-d", strtotime($start_date)) . " +$guarantee_time month");
        $end_date = strftime("%Y-%m-%d", $end_date);
        
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng
        $data_insert = compact('name','email','phone','address','product_name','total_costs','guarantee_time','start_date','end_date','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);
        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $form = new MyForm();

        $data_form[] = $form->text('name',$data_edit->name,0,'Nhập tên khách hàng');
        $data_form[] = $form->text('email',$data_edit->email,0,'Nhập email');
        $data_form[] = $form->text('phone',$data_edit->phone,0,'Nhập số điện thoại');
        $data_form[] = $form->text('address',$data_edit->address,0,'Nhập địa chỉ');
        $data_form[] = $form->text('total_costs',$data_edit->total_costs,0,'Giá tiền');
        $data_form[] = $form->datepicker('start_date',$data_edit->start_date,0,'Ngày xuất kho');
        $data_form[] = $form->text('guarantee_time',$data_edit->guarantee_time,0,'Bảo hành bao lâu');
        $data_form[] = $form->editor('product_name',$data_edit->product_name,0,'Tên sản phẩm');
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
      
        $data_form[] = $form->action('edit');
        return view('admin.layouts.edit',compact('data_form','id'));
    }
    public function update(Request $request, $id)
    {
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tên');
       
        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 1;
       
        
        $start_date = $data_form['start_date'];
        $guarantee_time =$data_form['guarantee_time'];
        $end_date = strtotime(date("Y-m-d", strtotime($start_date)) . " +$guarantee_time month");
        $end_date = strftime("%Y-%m-%d", $end_date);
       
        extract($data_form,EXTR_OVERWRITE);
        $data_update = compact('name','email','phone','address','product_name','total_costs','guarantee_time','start_date','end_date','status','updated_at');
        DB::table($this->table_name)->where('id',$id)->update($data_update);
        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);

       
       
        
        $old = [
            'name'=>$data_edit->name,
            
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);

        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
   
    }
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
