<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use DB;
class CommentsController extends Controller
{
   
    function __construct()
    {
        $this->module_name = 'íĐánh giá bình luận';
        $this->table_name = 'comments';
        parent::__construct();
    }

    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');

        $arr_stt = [1 => 'Đã duyệt', 2 => 'Chờ', 3 => 'Hủy'];
        $listdata = new ListData($request,$this->table_name,'id');
        $listdata->add('status','Trạng thái','array',1,[1=>'Đã duyệt',2=>'Chờ',3=>'Hủy']);
        $listdata->add('name','Thông tin khách','string',1);
        $listdata->add('content','Nội dung','string',0);
        $listdata->add('',' Đã trả lời');
        $listdata->add('','Trả lời');
        $listdata->add('','Thông tin','string',0);
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data','arr_stt'));
    }

    public function create()
    {
          
        $this->checkRole($this->table_name.'_create');

        $form = new MyForm();
        $data_form[] = $form->text('admin',Auth::guard('admin')->user()->name,1,'Admin','');
        $data_form[] = $form->textarea('content','',1,'Nội dung trả lời','Nội dung trả lời');
        //$data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));
    }

    public function store(Request $request)
    {
        $this->checkRole($this->table_name.'_create');

        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng

        //$id_insert = DB::table($this->table_name)->insertGetId($data_insert);

        DB::table('replies')->insert(['admin_id' => Auth::guard('admin')->user()->id,
                                    'content' => $request->content,
                                    'created_at' => $created_at,
                                    'updated_at' => $updated_at,
                                ]);
        
        return redirect(route($this->table_name.'.'.$redirect))->with(['flash_level'=>'success','flash_message'=>'Trả lời thành công!']);
 
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function reply($id)
    {
        $review=DB::table($this->table_name)->where('id',$id)->first();
        $reply = DB::table('replies')->where('comment_id',$review->id)->get();
        return view('admin.comments.reply',compact('id','review','reply'));
    }
    public function postReply($id,Request $request)
    {
        $this->validate($request, 
                [
                'content' => 'required|min:3'
                ],
                [
                'content.required' => 'Bạn chưa nhập nội dung trả lời'  
                ]);
        DB::table('replies')->insert([
                'admin'=>$request->admin,
                'comment_id'=>$id,
                'content'=>$request->content,
                'created_at'=>date("Y-m-d H:i:s"),
                'updated_at'=>date("Y-m-d H:i:s")
                ]);
        return redirect()->route('comments.index')->with(['flash_level'=>'success','flash_message'=>'Trả lời thành công!']);
    }
  
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
