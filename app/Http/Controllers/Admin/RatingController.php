<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use App\Product;
class RatingController extends Controller
{
    public function __construct()
    {
        $this->module_name = 'Đáng giá sao';
        $this->table_name = 'votes';
        $this->has_seo = false;
        parent::__construct();

    }
    public function index(Request $request)
    {
        $this->checkRole($this->table_name.'_access');
        $votes = DB::table($this->table_name)->pluck('type_id');
        $array = Product::where('status',1)->orderBy('created_at','desc')->whereIn('id',$votes)->pluck('name','id')->toArray();
        $listdata = new ListData($request,$this->table_name);
        $listdata->add('name','Tên Sản phẩm','string',1);
        $listdata->add('type_id','Danh mục','array',1,$array);
        $listdata->add('value','Số lượng sao','string',1);
        // $listdata->add('host','Máy chủ','string',1);
        $listdata->add('','Thông tin');
        // $listdata->add('status','Trạng thái','status',1,[1=>'Đã duyệt',2=>'Chưa duyệt']);
        $listdata->add('status','Trạng thái','',1,[1=>'Đơn hàng mới',2=>'Đã tiếp nhận',3=>'Hoàn thành',4=>'Hủy']);

        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');
        $data = $listdata->data();
        return view('admin.layouts.list',compact('data'));
    }
    public function create()
    {
        $this->checkRole($this->table_name.'_create');
        $array = Product::where('status',1)->orderBy('created_at','desc')->pluck('name','id')->toArray();
       
        $form = new MyForm();
        $data_form[] = $form->select('type_id',0,1,'Danh mục',$array);
        $data_form[] = $form->text('value','',1,'Số lượng sao','Tối đa 5 sao');
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));
    }
    public function store(Request $request)
    {
        $this->checkRole($this->table_name.'_create');
        $data_form = $request->all();

        $this->validate_form($request,'type_id',1,'Bạn chưa chọn tour');
        $created_at = $updated_at = date("Y-m-d H:i:s");

        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng
        $type = 'products';
        $host = $_SERVER['SERVER_ADDR'];
        $data_insert = compact('type_id','value','type','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);
        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$request->redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }
    public function show($id)
    {

    }
    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();
        $form = new MyForm();
        $data_form[] = $form->text('value',$data_edit->value,1,'Số lượng sao');
        // $data_form[] = $form->text('host',$data_edit->host,'','Host');
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
        $data_form[] = $form->action('edit');
        return view('admin.layouts.edit',compact('data_form','id'));
    }
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
        $data_update = compact('value','status','updated_at');
        DB::table($this->table_name)->where('id',$id)->update($data_update);
        $old = [
            'type_id'=>$data_edit->type_id,
            'value'=>$data_edit->value,
          
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at,
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);
        return redirect(route($this->table_name.'.'.$request->redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
}
