<?php



namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\MyClass\MyForm;

use App\MyClass\ListData;

use App\MyClass\Categories;

use DB;

use App\Product;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller

{

    function __construct()

    {

        $this->module_name = 'sản phẩm';

        $this->table_name = 'products';

        $this->has_google_shopping = true;

        parent::__construct();

    }

    public function index(Request $request)

    {

        $this->checkRole($this->table_name.'_access');



        $categories = new Categories('products_categories');

        $array_categories = $categories->data_select_categories();

        $label_sale_array = [
            0=>'Không được ghim khuyến mại',
            1=>'Được ghim khuyến mại'
        ];
        $banchay_array = [
            0=>'Không được ghim bán chạy',
            1=>'Được ghim bán chạy'
        ];


        $listdata = new ListData($request,$this->table_name);

        $listdata->add('image','Ảnh đại diện','string');

        $listdata->add('name','Tên sản phẩm','string',1);

        $listdata->add('','Giá thấp','int',1);

        $listdata->add('','Giá cao','int',1);

        $listdata->add('category_id','Danh mục','array',1,$array_categories);

        $listdata->add('updated_at','Thời điểm cập nhật','range',1);

        $listdata->add('label_sale','Sản phẩm khuyến mãi','array',1,$label_sale_array);

        $listdata->add('ban_chay','Sản phẩm bán chạy','array',1,$banchay_array);

        $listdata->add('copy','Copy thêm');

        // $listdata->add('noibat','Ghim sản phẩm nổi bật','pins');

        // $listdata->add('banchay','Ghim sản phẩm bán chạy','pins');

        // $listdata->add('san_pham_xu_huong','Ghim sản phẩm theo xu hướng','pins');

        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);

        $listdata->add('','Sửa','edit');

        $listdata->add('','Xóa','delete');



        $data = $listdata->data();

        return view('admin.layouts.list',compact('data','array_categories'));

    }

    public function create()

    {

        $this->checkRole($this->table_name.'_create');



        $categories = new Categories('products_categories');

        $array_categories = $categories->data_select_categories();

       

        
    $array_brands = DB::table('brands')->pluck('name','id')->toArray();




        $warehouse_status = config('app.warehouse_status');

      

        $size =  new Categories('sizes');

        $array_size = $size->data_select('sizes');



        $array_ocung = config('app.ocung');

        $array_ram = config('app.ram');

        $array_core = config('app.core');

        $array_type_laptop = config('app.type_laptop');

        $form = new MyForm();

        $data_form[] = $form->text('name','',1,'Tên sản phẩm','',1,'slug');

        $data_form[] = $form->slug('slug','');

        $data_form[] = $form->select('category_id',0,1,'Danh mục',$array_categories);

        $data_form[] = $form->select('brand_id','',0,'Thương hiệu',$array_brands);

        $data_form[] = $form->text('sku','',0,'Mã sản phẩm');

        $data_form[] = $form->select('warehouse_status',0,0,'Tình trạng',$warehouse_status);

        $data_form[] = $form->number('price','',0,'Giá thấp');

        $data_form[] = $form->number('price_old','',0,'Giá cao');

        $data_form[] = $form->number('guarantee','',0,'Thời gian bảo hành');

        $data_form[] = $form->select('ocung',0,0,'Ổ cứng',$array_ocung);

        $data_form[] = $form->select('ram',0,0,'Ram',$array_ram);

        $data_form[] = $form->select('core',0,0,'Thế hệ laptop',$array_core);

        $data_form[] = $form->select('type_laptop',0,0,'Thế loại laptop',$array_type_laptop);

        $data_form[] = $form->select('size_id','',0,'Độ rộng màn hình',$array_size);

        $data_form[] = $form->custom('admin.customs.colors');

        $data_form[] = $form->image('image','',0);

        $data_form[] = $form->slide('slides','',0);

        $data_form[] = $form->related('related_products','',0,'Chọn 4 sản phẩm liên quan','Tìm theo tên sản phẩm ...','products');

        $data_form[] = $form->editor('specifications','',0,'Thông số sản phẩm','');

        $data_form[] = $form->editor('description','<div class=" table-responsive">
<table class="table ">
<tbody>
<tr>
<td>Chipset</td>
<td>Tích hợp cùng CPU</td>
</tr>
<tr>
<td>CPU</td>
<td>
<div class="l badge">
<p>Intel® Core™ i5-4300U Processor(4M Cache, up to 2.6 GHz)</p>
</div>
</td>
</tr>
<tr>
<td>GPU</td>
<td>Intel Graphics 4400</td>
</tr>
<tr>
<td>Memory</td>
<td>4 GB PC3L bus 1600 (có thể nâng cấp lên 16 GB)</td>
</tr>
<tr>
<td>HDD</td>
<td>SSD 128GB ( Tùy biến nâng cấp 2 ổ cứng)</td>
</tr>
<tr>
<td>Màn hình</td>
<td>14 Inches LED Blacklit</td>
</tr>
<tr>
<td>Độ phân giải</td>
<td> HD 1366*768 ( Có phiên bản màn HD+ 1600*900 thêm 300.000)</td>
</tr>
<tr>
<td>Wireless</td>
<td>Intel Wifi</td>
</tr>
<tr>
<td>LAN</td>
<td>10/100/1000 Mbps</td>
</tr>
<tr>
<td>Battery</td>
<td>4 Cells</td>
</tr>
<tr>
<td>OS</td>
<td>Windows® 8.1 / Windows 10 Professional 64 bit</td>
</tr>
<tr>
<td>Ổ quang</td>
<td> Không có</td>
</tr>
<tr>
<td>Cân nặng</td>
<td>1.6 Kg</td>
</tr>
<tr>
<td>Màu sắc</td>
<td>Hợp kim màu bạc xám sang trọng</td>
</tr>
<tr>
<td>Các cổng kết nối</td>
<td>4x USB 3.0 (1 cổng sạc ngay cả khi tắt nguồn)<br /> HDMI<br /> Smartcard<br /> Docking<br /> 3.5 External Microphone<br /> 3.5 External Speaker
<p> </p>
<h4>Camera, đèn nền phím sáng, vân tay không cộng thêm tiền</h4>
</td>
</tr>
</tbody>
</table>
</div>',0,'Mô tả sản phẩm','');

        $data_form[] = $form->editor('promotion','<p>🎁 1 túi đựng Laptop</p>
<p>🎁 1 chuột không dây</p>',0,'Khuyến mại','');

        $data_form[] = $form->editor('detail','',0,'Nội dung');

        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');



        $data_form[] = $form->action('add');

        return view('admin.layouts.create',compact('data_form'));

    }

    public function store(Request $request)

    {

        $this->checkRole($this->table_name.'_create');

        $data_form = $request->all();

        //Kiểm tra xem slug đã tồn tại và có status = 4 trong DB chưa. nếu tồn tại thì xóa đi

        $this->checkSlug($this->table_name, $data_form['slug']);



        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');

        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống',1,'Đường dẫn bị trùng');

        $this->validate_form($request,'category_id',1,'Bạn chưa chọn danh mục');



        $created_at = $updated_at = date("Y-m-d H:i:s");

        $status = 2;

        $label_sale = 0;
        $user_id = Auth::guard('admin')->user()->id;
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng



        if (isset($slides) && $slides != '') {

            $slides = implode(',', $slides);

        }else {

            $slides = '';

        }

        if (isset($related_products)) {

            $related_products = implode(',',$related_products);

        }else {

            $related_products = '';

        }

        $sku = '#lt'.time();

        $check_sku = DB::table($this->table_name)->where('status','!=',4)->where('sku',$sku)->get();

        if(!empty($check_sku)){

            $sku = '#lt'.time();

        }

        $data_insert = compact('category_id','user_id','size_id','brand_id','name','slug','price','price_old','image','slides','specifications','description','detail','sku','warehouse_status','related_products','promotion','ocung','ram','core','type_laptop','guarantee','status','created_at','updated_at');

        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);



        if(isset($colors)){

            foreach($colors as $value){

                if($value != 0){

                    DB::table('product_color_maps')->insert(['product_id'=>$id_insert,'color_id'=>$value]);

                }

            }

        }

        if(isset($image_color)){

            foreach($image_color as $key => $value){

                if($colors[$key] != 0){

                    DB::table('attribute_images')->insert(['product_id'=>$id_insert,'color_id'=>$colors[$key],'image'=>$value]);

                }

            }

        }



        $this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);

        $this->googleShopping($id_insert,$google_shopping_brand,$google_shopping_category,$google_shopping_instock,$google_shopping_itemcondition);

        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);

        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);

    }

    public function show($id)

    {

        //

    }

    public function edit($id)

    {

        $this->checkRole($this->table_name.'_edit');



        // $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $product = Product::find($id);



        $categories = new Categories('products_categories');

        $array_categories = $categories->data_select_categories();

        $warehouse_status = config('app.warehouse_status');

       

       $array_brands = DB::table('brands')->pluck('name','id')->toArray();

        $size =  new Categories('sizes');

        $array_size = $size->data_select('sizes');

        $array_ocung = config('app.ocung');

        $array_ram = config('app.ram');

        $array_core = config('app.core');

        $array_type_laptop = config('app.type_laptop');

        $form = new MyForm();

        $data_form[] = $form->text('name',$product->name,1,'Tên sản phẩm','',1,'slug');

        $data_form[] = $form->slug('slug',$product->slug);

        $data_form[] = $form->select('category_id',$product->category_id,1,'Danh mục',$array_categories);

        $data_form[] = $form->select('brand_id',$product->brand_id,0,'Thương hiệu',$array_brands);

        $data_form[] = $form->text('sku',$product->sku,0,'Mã sản phẩm');

        $data_form[] = $form->select('warehouse_status',$product->warehouse_status,0,'Tình trạng',$warehouse_status);

        $data_form[] = $form->number('price',$product->price,0,'Giá thấp');

        $data_form[] = $form->number('price_old',$product->price_old,0,'Giá cao');

        $data_form[] = $form->number('guarantee',$product->guarantee,0,'Thời gian bảo hành');

        $data_form[] = $form->select('ocung',$product->ocung,0,'Ổ cứng',$array_ocung);

        $data_form[] = $form->select('ram',$product->ram,0,'Ram',$array_ram);

        $data_form[] = $form->select('core',$product->core,0,'Thế hệ laptop',$array_core);

        $data_form[] = $form->select('type_laptop',$product->type_laptop,0,'Thế loại laptop',$array_type_laptop);

        $data_form[] = $form->select('size_id',$product->size_id,0,'Độ rộng màn hình',$array_size);

        $data_form[] = $form->custom('admin.customs.colors', $product);

        $data_form[] = $form->image('image',$product->image,0);

        $data_form[] = $form->slide('slides',explode(',',$product->slides),0);

        $data_form[] = $form->related('related_products',$product->related_products,0,'Chọn 3 sản phẩm liên quan','Tìm theo tên sản phẩm ...','products');

        $data_form[] = $form->editor('specifications',$product->specifications,0,'Thông số sản phẩm','');

        $data_form[] = $form->editor('description',$product->description,0,'Mô tả sản phẩm','');

        $data_form[] = $form->editor('promotion',$product->promotion,0,'Khuyến mại','');

        $data_form[] = $form->editor('detail',$product->detail,0,'Nội dung');

        $data_form[] = $form->checkbox('status',$product->status,1,'Kích hoạt');



        $data_form[] = $form->action('edit',route('web.'.$this->table_name.'.show',$product->slug));

        return view('admin.layouts.edit',compact('data_form','id'));

    }

    public function update(Request $request, $id)

    {

        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();



        $this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');

        $this->validate_form($request,'slug',1,'Đường dẫn không được để trống');

        $this->validate_form($request,'category_id',1,'Bạn chưa chọn danh mục');



        $updated_at = date("Y-m-d H:i:s");

        $data_form = $request->all();

        $status = 2;

        extract($data_form,EXTR_OVERWRITE);



        if (isset($slides) && $slides != '') {

            $slides = implode(',', $slides);

        }else {

            $slides = '';

        }

        if (isset($related_products)) {

            $related_products = implode(',',$related_products);

        }else {

            $related_products = '';

        }



        $data_update = compact('category_id','size_id','brand_id','name','slug','price','price_old','sku','warehouse_status','image','slides','specifications','description','detail','related_products','promotion','ocung','ram','core','type_laptop','guarantee','status','updated_at');



        DB::table($this->table_name)->where('id',$id)->update($data_update);



      



        if(isset($colors)){

            DB::table('product_color_maps')->where('product_id', $id)->delete();

            foreach($colors as $value){

                if($value != 0){

                    DB::table('product_color_maps')->insert(['product_id'=>$id,'color_id'=>$value]);

                }

            }

        }else{

            DB::table('product_color_maps')->where('product_id', $id)->delete();

        }

        if(isset($image_color)){

            DB::table('attribute_images')->where('product_id', $id)->delete();

            foreach($image_color as $key => $value){

                if($colors[$key] != 0){

                    DB::table('attribute_images')->insert(['product_id'=>$id,'color_id'=>$colors[$key],'image'=>$value]);

                }

            }

        }else{

            DB::table('attribute_images')->where('product_id', $id)->delete();

        }



        $this->metaSeo($id,$seo_title,$seo_description,$seo_robots);

        $this->googleShopping($id,$google_shopping_brand,$google_shopping_category,$google_shopping_instock,$google_shopping_itemcondition);

        

        $old = [

            'name'=>$data_edit->name,

            'slug'=>$data_edit->slug,

            'price'=>$data_edit->price,

            'price_old'=>$data_edit->price_old,

            'image'=>$data_edit->image,

            'slides'=>$data_edit->slides,

            'specifications'=>$data_edit->specifications,

            'description'=>$data_edit->description,

            'detail'=>$data_edit->detail,

            'related_products'=>$data_edit->related_products,

            'status'=>$data_edit->status,

            'updated_at'=>$data_edit->updated_at

        ];

        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);



        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);

    }

    public function destroy($id)

    {

        if($this->hasRole($this->table_name.'_delete')) {

            DB::table($this->table_name)->where('id',$id)->update([

                'status'=>4,

                'name' => DB::raw("CONCAT(name, '--delete--".time()."')"),

                'slug' => DB::raw("CONCAT(slug, '--delete--".time()."')")

            ]);

            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);

            return response()->json(['status'=>1,'message'=>'Xóa thành công']);

        }else {

            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);

        }

    }

}

