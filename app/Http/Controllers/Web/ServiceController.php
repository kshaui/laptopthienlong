<?php

namespace App\Http\Controllers\Web;
use DB;
use Illuminate\Http\Request;
use App\Service;
use App\ServiceCategory;
use App\News;
use App\Comment;
use App\Product;
use Cache;
use App\Classe;
class ServiceController extends Controller
{
    public function index($slug = null)
    {   

        $service = Service::where('slug', $slug)->where('status',1)->firstOrFail();



        $service_category = Classe::where('id',$service->class_id)->where('status',1)->first();

        
        $service_category_parent = Classe::where('id',$service_category->parent_id)->where('status',1)->first();
         $relate_service = Service::where('status',1)->where('id','!=',$service->id)->where('class_id',$service_category->id)->limit(6)->orderBy('created_at','desc')->get();

        $orders = DB::table('orders')->where('status','!=',4)->orderBy('created_at','desc')->paginate(5);

          $comments = Comment::where('status',1)->where('type_id', $service->id)->where('type','services')->where('parent_id',0)->orderBy('created_at','desc')->get();


           $count_vote = DB::table('votes')->where('type','services')->where('type_id', $service->id)->count();
       
        $agv_collect = collect(DB::table('votes')->where('type_id', $service->id)->where('type','services')->avg('value'));
        $votes =  DB::table('votes')->where('type_id', $service->id)->where('type','services')->pluck('value')->toArray();

         if (Cache::has('products_banchay')) {
            $products_banchay = Cache::get('products_banchay');
        }else{
            $products_banchay = Product::where('ban_chay',1)->where('status',1)->limit(8)->get();
             Cache::forever('products_banchay', $products_banchay);
        }
       

        $config_promotion = DB::table('options')->select('value')->where('name','promotion')->first();
        if($config_promotion){
            $config_promotion = json_decode(base64_decode($config_promotion->value),true);
        }
        $promotion = ($service->promotion)?$service->promotion:$config_promotion["promotion_default_service"];
        $promotion = preg_split('/\n|\r\n/',$promotion);
        $slide_real = [];
      
        $meta_seo = $this->meta_seo('services',$service->id,[
            'title' => $service->name.' - Laptop247hn',
            'description'=> cutString(removeHTML($service->detail),150),
            'url' => route('web.services.show',$service->slug),
            'image' => $service->image
        ]);
        $admin_bar_edit = route('services.edit',$service->id);
        $breadcrumbs = [
            ['name'=>'Sửa chữa laptop','url'=>route('web.service_categories.showall')],
        ];
        if($service_category_parent) {
            $breadcrumbs[] = ['name'=>$service_category_parent->name,'url'=>route('web.service_categories.show', ['slug'=>$service_category_parent->slug])];
        }
        if($service_category) {
            $breadcrumbs[] = ['name'=>$service_category->name,'url'=>route('web.service_categories.show', ['slug'=>$service_category->slug])];
        }
        return view('web.services.index', compact(
            'service', 
            'service_category',
            'promotion',
            'service_category_parent',
            'meta_seo',
            'admin_bar_edit',
            'breadcrumbs',
            'orders',
            'comments',
            'count_vote',
            'agv_collect',
            'votes',
            'products_banchay',
            'relate_service'
        ));
    }
  
  
}
