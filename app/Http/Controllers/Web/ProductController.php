<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Repositories\AllRepository\getRepository;
use App\Product;
use App\ProductsCategory;
use DB;
use App\Comment;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
class ProductController extends Controller
{
 
    public function show(Request $request,$slug)
    {
        $product = Product::where('status',1)->where('slug',$slug)->firstOrFail();
        if(is_null($product)){
            return abort(404);
        }
        $gift_product = DB::table('product_coupon_map')->where('product_coupon_map.product_id',$product->id)
        ->join('coupons','coupons.id','=','product_coupon_map.coupon_id')
        ->where('coupons.status',1)
        ->where('coupons.start','<',Carbon::now())
        ->where('coupons.end','>',Carbon::now())
        ->first();
     
        $category = ProductsCategory::where('id', $product->category_id)->first();
        $relate_product = Product::where('status',1)->where('id','!=',$product->id)->where('category_id',$category->id)->limit(6)->orderBy('created_at','desc')->get();
        $comments = Comment::where('status',1)->where('type_id', $product->id)->where('type','products')->where('parent_id',0)->orderBy('created_at','desc')->get();
       
        $count_vote = DB::table('votes')->where('type','products')->where('type_id', $product->id)->count();
       
        $agv_collect = collect(DB::table('votes')->where('type_id', $product->id)->where('type','products')->avg('value'));
        $votes =  DB::table('votes')->where('type_id', $product->id)->where('type','products')->pluck('value')->toArray();

        $orders = DB::table('orders')->where('status','!=',4)->orderBy('created_at','desc')->paginate(5);

        $breadcrumbs = [
            ['name'=> 'Laptop247hn','url' => '/laptop'],       
            ['name' => $category->name, 'url' => $category->getUrl()],
            ['name' => $product->name, 'url' => $product->getUrl()],
        ];

        $meta_seo = $this->meta_seo('products',$product->id,[
            'title' => $product->name ?? 'Sản phẩm - Công ty laptop247hn',
            'description'=> cutString(strip_tags($product->detail), 160),
            'url' => $product->getUrl(),
            'image' => $product->image
        ]);
        $you_have = Session::get('you_have_seen');
            if(isset($you_have)){
                // if(count($you_have)<=2){
                $request->session()->push('you_have_seen',$product->id );
                // }

            }else{

                $request->session()->push('you_have_seen',$product->id );
            }
        // $admin_bar_edit = route('products.edit', $product->id);
        return view('web.products.detail', compact('product','gift_product','meta_seo','relate_product','breadcrumbs','comments','agv_collect','count_vote','votes','orders'));
    }
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
