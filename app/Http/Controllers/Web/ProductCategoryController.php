<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Repositories\AllRepository\getRepository;
use App\Product;
use App\ProductsCategory;
use DB;
use App\Comment;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
class ProductCategoryController extends Controller
{
    public function index(Request $request, $slug = null)
    {
         $this->repository = new getRepository();
        $data_sizes =  $this->repository->dataSizes();
        $data_colors =  $this->repository->dataColors();
        $data_brand =  $this->repository->dataBrand();
        if (Cache::has('config_general')) {
            $config_general = Cache::get('config_general');
        } else {
            $config_general = DB::table('options')->select('value')->where('name','general')->first();
            $config_general = json_decode(base64_decode($config_general->value),true);
            Cache::forever('config_general', $config_general);
        }

         if (Cache::has('config_home')) {
            $config_home = Cache::get('config_home');
        } else {
            $config_home = DB::table('options')->select('value')->where('name','home')->first();
            $config_home = json_decode(base64_decode($config_home->value),true);
            Cache::forever('config_home', $config_home);
        }
            $type_laptop = $request->type_laptop;
            $sort_price = $request->sort_price;
            $filter_size = $request->sizes;
            $filter_price = $request->filter_price;
            $filter_color = $request->colors;
            $filter_brand = $request->brand;
            $filter_ocung = $request->ocung;
            $filter_ram = $request->ram;
            $filter_core = $request->core;
            $page_size = 36;
            $products = Product::where('status',1);
            if($filter_brand !=''){
                $products = $products->where('brand_id', $filter_brand);
            }
            if($filter_ocung !=''){
                $products = $products->where('ocung', $filter_ocung);
            }
            if($filter_ram !=''){
                $products = $products->where('ram', $filter_ram);
            }
            if($filter_core !=''){
                $products = $products->where('core', $filter_core);
            }
            if($type_laptop !=''){
                $products = $products->where('type_laptop', $type_laptop);
            }
            if($filter_price != ''){
                switch ($filter_price){
                    case '1':
                        $products = $products->where('price','<',3000000);
                        break;
                    case '2':
                        $products = $products->whereBetween('price',[3000000,5000000]);
                        break;
                    case '3':
                        $products = $products->whereBetween('price',[5000000,10000000]);
                        break;
                    case '4':
                        $products = $products->whereBetween('price',[10000000,15000000]);
                        break;
                    case '5':
                        $products = $products->whereBetween('price',[15000000,20000000]);
                        break;
                    case '6':
                        $products = $products->where('price','>',20000000);
                        break;
                }
            }
            if($filter_size != ''){
                $products = $products->where('size_id', $filter_size);
            }
            if($filter_color != ''){
                $data_color_map = DB::table('product_color_maps')->where('color_id', $filter_color)->pluck('product_id')->toArray();
                $products = $products->whereIn('id', $data_color_map);
            }
    
            if($sort_price){
                if($sort_price == 'new'){
                    $products = $products->orderBy('id','desc')->paginate($page_size);
                }elseif($sort_price == 'asc'){
                    $products = $products->orderBy('price','asc')->paginate($page_size);
                }elseif($sort_price == 'desc'){
                    $products = $products->orderBy('price','desc')->paginate($page_size);
                }
            }else{
                $products = $products->orderBy('id','desc')->paginate($page_size);
               
            }
            $agv_product_collect = collect(DB::table('votes')->whereIn('type_id', $products->pluck('id'))->where('type','products')->get());
   
            $array_type_laptop = config('app.type_laptop');
             if($type_laptop !='' ){
                $title = $array_type_laptop[$type_laptop];
                $title_seo = $array_type_laptop[$type_laptop];
             }else{
                 $title = 'Laptop247hn';
             }

            $breadcrumbs = [
                ['name'=> $title_seo ?? 'Laptop247hn','url' => '/laptop'],            
                
            ];
            $meta_seo = $this->meta_seo('','0   ',[
                'title' => $title_seo ?? 'Sản phẩm - Công ty laptop247hn',
                'description'=> $config_home['meta_description'],
                'url' => route('web.products_categories.index'),
                'image'=> @$config_general['logo_header'] ,
            ]);

           
            return view('web.products.index', compact('meta_seo','breadcrumbs','products','data_sizes','data_colors','filter_size','filter_price','filter_color','sort_price','data_brand','filter_brand','agv_product_collect','filter_ocung','filter_ram','filter_core','title'));


    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request, $slug = null)
    {
         $this->repository = new getRepository();
        $data_sizes =  $this->repository->dataSizes();
        $data_colors =  $this->repository->dataColors();
        $data_brand =  $this->repository->dataBrand();


        if (Cache::has('config_general')) {
            $config_general = Cache::get('config_general');
        } else {
            $config_general = DB::table('options')->select('value')->where('name','general')->first();
            $config_general = json_decode(base64_decode($config_general->value),true);
            Cache::forever('config_general', $config_general);
        }

         if (Cache::has('config_home')) {
            $config_home = Cache::get('config_home');
        } else {
            $config_home = DB::table('options')->select('value')->where('name','home')->first();
            $config_home = json_decode(base64_decode($config_home->value),true);
            Cache::forever('config_home', $config_home);
        }

        $sort_price = $request->sort_price;
            $filter_size = $request->sizes;
            $filter_price = $request->filter_price;
            $filter_color = $request->colors;
            $filter_brand = $request->brand;
            $filter_ocung = $request->ocung;
            $filter_ram = $request->ram;
            $filter_core = $request->core;
            $category = ProductsCategory::where('status',1)->where('slug',$slug)->firstOrFail();
            $ids[] = $category->id;          
            if($category->parent_id == 0){
                $category_chidle = ProductsCategory::where('status',1)->where('parent_id',$category->id)->pluck('id')->toArray();
                foreach($category_chidle as $key=>$value){
                    $ids[] = $value;
                }
                $products = Product::where('status',1)->whereIn('category_id',$ids);
              
            }
            else{
                $products = Product::where('status',1)->where('category_id',$category->id);
            }
            
            $page_size = 36;
           
            if($filter_price != ''){
                switch ($filter_price){
                    case '1':
                        $products = $products->where('price','<',3000000);
                        break;
                    case '2':
                        $products = $products->whereBetween('price',[3000000,5000000]);
                        break;
                    case '3':
                        $products = $products->whereBetween('price',[5000000,10000000]);
                        break;
                    case '4':
                        $products = $products->whereBetween('price',[10000000,15000000]);
                        break;
                    case '5':
                        $products = $products->whereBetween('price',[15000000,20000000]);
                        break;
                    case '6':
                        $products = $products->where('price','>',20000000);
                        break;
                }
            }
            if($filter_size != ''){
                $products = $products->where('size_id', $filter_size);
            }
            if($filter_color != ''){
                $data_color_map = DB::table('product_color_maps')->where('color_id', $filter_color)->pluck('product_id')->toArray();
                $products = $products->whereIn('id', $data_color_map);
            }
            if($filter_brand !=''){
                $products = $products->where('brand_id', $filter_brand);
            }
            if($filter_ocung !=''){
                $products = $products->where('ocung', $filter_ocung);
            }
            if($filter_ram !=''){
                $products = $products->where('ram', $filter_ram);
            }
            if($filter_core !=''){
                $products = $products->where('core', $filter_core);
            }

            if($sort_price){
                if($sort_price == 'new'){
                    $products = $products->orderBy('id','desc')->paginate($page_size);
                }elseif($sort_price == 'asc'){
                    $products = $products->orderBy('price','asc')->paginate($page_size);
                }elseif($sort_price == 'desc'){
                    $products = $products->orderBy('price','desc')->paginate($page_size);
                }
            }else{
                $products = $products->orderBy('id','desc')->paginate($page_size);
              
            }
           
            $agv_product_collect = collect(DB::table('votes')->whereIn('type_id', $products->pluck('id'))->where('type','products')->get());
   
            $breadcrumbs = [
                ['name'=> 'Laptop247hn','url' => '/laptop'],            
                ['name' => $category->name, 'url' => $category->getUrl()],
                
            ];

            $meta_seo = $this->meta_seo('products_categories',$category->id,[
                'title' => $category->name ?? 'Sản phẩm - Công ty laptop247hn',
                'description'=> cutString(strip_tags($category->detail), 160) ?? $config_home['meta_description'],
                'url' => route('web.products_categories.show', $category->slug),
                 'image' => $category->image ?? $config_general['logo_header'],
            ]);
             $title = $category->name;
            return view('web.products.index', compact('breadcrumbs','category','products','meta_seo','data_sizes','data_colors','filter_size','filter_price','filter_color','sort_price','data_brand','filter_brand','agv_product_collect','filter_ocung','filter_ram','filter_core','title'));
    }
   
    public function khuyenMai(Request $request)
   {
        $this->repository = new getRepository();
        $data_sizes =  $this->repository->dataSizes();
        $data_colors =  $this->repository->dataColors();
        $data_brand =  $this->repository->dataBrand();


        if (Cache::has('config_general')) {
            $config_general = Cache::get('config_general');
        } else {
            $config_general = DB::table('options')->select('value')->where('name','general')->first();
            $config_general = json_decode(base64_decode($config_general->value),true);
            Cache::forever('config_general', $config_general);
        }

         if (Cache::has('config_home')) {
            $config_home = Cache::get('config_home');
        } else {
            $config_home = DB::table('options')->select('value')->where('name','home')->first();
            $config_home = json_decode(base64_decode($config_home->value),true);
            Cache::forever('config_home', $config_home);
        }

        $sort_price = $request->sort_price;
            $filter_size = $request->sizes;
            $filter_price = $request->filter_price;
            $filter_color = $request->colors;
            $filter_brand = $request->brand;
            $filter_ocung = $request->ocung;
            $filter_ram = $request->ram;
            $filter_core = $request->core;
           
            $products = Product::where('status',1)->where('price_old','!=',null);
            
            
            $page_size = 36;
           
            if($filter_price != ''){
                switch ($filter_price){
                    case '1':
                        $products = $products->where('price','<',3000000);
                        break;
                    case '2':
                        $products = $products->whereBetween('price',[3000000,5000000]);
                        break;
                    case '3':
                        $products = $products->whereBetween('price',[5000000,10000000]);
                        break;
                    case '4':
                        $products = $products->whereBetween('price',[10000000,15000000]);
                        break;
                    case '5':
                        $products = $products->whereBetween('price',[15000000,20000000]);
                        break;
                    case '6':
                        $products = $products->where('price','>',20000000);
                        break;
                }
            }
            if($filter_size != ''){
                $products = $products->where('size_id', $filter_size);
            }
            if($filter_color != ''){
                $data_color_map = DB::table('product_color_maps')->where('color_id', $filter_color)->pluck('product_id')->toArray();
                $products = $products->whereIn('id', $data_color_map);
            }
            if($filter_brand !=''){
                $products = $products->where('brand_id', $filter_brand);
            }
            if($filter_ocung !=''){
                $products = $products->where('ocung', $filter_ocung);
            }
            if($filter_ram !=''){
                $products = $products->where('ram', $filter_ram);
            }
            if($filter_core !=''){
                $products = $products->where('core', $filter_core);
            }

            if($sort_price){
                if($sort_price == 'new'){
                    $products = $products->orderBy('id','desc')->paginate($page_size);
                }elseif($sort_price == 'asc'){
                    $products = $products->orderBy('price','asc')->paginate($page_size);
                }elseif($sort_price == 'desc'){
                    $products = $products->orderBy('price','desc')->paginate($page_size);
                }
            }else{
                $products = $products->orderBy('id','desc')->paginate($page_size);
              
            }
           
            $agv_product_collect = collect(DB::table('votes')->whereIn('type_id', $products->pluck('id'))->where('type','products')->get());
   
            $breadcrumbs = [
                ['name'=> 'Laptop247hn','url' => '/laptop'],            
                ['name' => 'Khuyến mại', 'url' => 'khuyen-mai'],
                
            ];
             $meta_seo = $this->meta_seo('',0,[
               'title' => 'Sản phẩm khuyến mại -  Công ty laptop247hn',
                'description'=> $config_home['meta_description'],
                'url' => url('/khuyen-mai'),
                'image' => $config_general['logo_header'] ,
            ]);

            $title = 'Sản phẩm khuyến mại';
            return view('web.products.index', compact('breadcrumbs','products','data_sizes','data_colors','filter_size','filter_price','filter_color','sort_price','data_brand','filter_brand','agv_product_collect','filter_ocung','filter_ram','filter_core','title','meta_seo'));
   }
}
