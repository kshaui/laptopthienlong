<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use DB;
use Mail;
use App\Mail\notificationContact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data_form = $request->all();
        extract($data_form,EXTR_OVERWRITE);
        $db_insert = [
            'email'=>$email,
            'phone'=>$phone,
            'name'=>$name,
            'content'=>$content,
            'created_at'=>date("Y-m-d H:i:s")
        ];
        $config_general = DB::table('options')->where('name','general')->first();
        $config_general = json_decode(base64_decode($config_general->value));
        $send_email = $config_general->notification_email;
        Mail::to($send_email)->send(new notificationContact($db_insert));
        DB::table('contacts')->insert($db_insert);
        return redirect()->route('web.contact.show')->with('messages','Bạn đã gửi thông tin liên hệ thành công !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Liên hệ',
            'description'=> '',
        ]);
        $breadcrumbs = [
            ['name'=> 'Liên hệ','url' => '/lien-he'],               
        ];
        return view('web.contacts.index', compact('meta_seo','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
