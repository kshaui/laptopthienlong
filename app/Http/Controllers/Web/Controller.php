<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use App\Repositories\AllRepository\getRepository;
use DB;
use View;
use App\ProductsCategory;
use Illuminate\Support\Facades\Cache;
class Controller extends BaseController
{
    public function __construct()
    {
        // cache::flush();
    
        $variable = 'variable';
        View::share('variable',$variable);
        if (Cache::has('config_home')) {
            $config_home = Cache::get('config_home');
        } else {
            $config_home = DB::table('options')->select('value')->where('name','home')->first();
            $config_home = json_decode(base64_decode($config_home->value),true);
            Cache::forever('config_home', $config_home);
        }

         if(cache::has('brand_home')) {
        $brand_home = Cache::get('brand_home');
        }else{
         $brand_home = DB::table('brands')->where('status',1)->get();
         Cache::forever('brand_home', $brand_home);
        }
          View::share('brand_home',$brand_home);
            if (Cache::has('config_general')) {
                $config_general = Cache::get('config_general');
            } else {
                $config_general = DB::table('options')->select('value')->where('name','general')->first();
                $config_general = json_decode(base64_decode($config_general->value),true);
                Cache::forever('config_general', $config_general);
            }
            View::share('config_home',$config_home);
            View::share('config_general',$config_general);
        if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
            $is_mobile = false;
        } elseif ( strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false // many mobile devices (all iPhone, iPad, etc.)
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false
            || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mobi') !== false ) {
            $is_mobile = true;
        } else {
            $is_mobile = false;
        }
        View::share('is_mobile', $is_mobile);
    }

    /**
     * @param string $type - tên kiểu trong bảng meta_seo
     * @param int $id - id trong bảng meta_seo
     * @param array $options - các trường mặc định hoặc không có trong bảng meta_seo: title | description | robots | type | url | image |
     */
    public function meta_seo($type='',$id=0,$options) {
        $meta_seo = [];
        if(count($options)) {
            foreach ($options as $key=>$value) {
                $meta_seo[$key] = $value;
            }
        }
        if ($type != '' && $id != 0) {
            $data_seo = DB::table('meta_seo')->where('type',$type)->where('type_id',$id)->first();
            if($data_seo) {
                if($data_seo->title!=''){
                    $meta_seo['title'] = $data_seo->title;
                }
                if($data_seo->description!=''){
                    $meta_seo['description'] = $data_seo->description;
                }
                $meta_seo['robots'] = $data_seo->robots;
            }
        }
        return $meta_seo;
    }
}
