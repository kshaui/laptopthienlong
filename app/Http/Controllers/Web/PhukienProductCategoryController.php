<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use DB;
use App\PhukienProduct;
use App\PhukienProductCategory;
class PhukienProductCategoryController extends Controller
{
    public function index(Request $request, $slug = null)
    {
        $page_size = 16;
        $phukien_products = PhukienProduct::where('status',1)->paginate($page_size);
        $breadcrumbs = [
                ['name'=> 'Phụ kiện','url' => '/phu-kien'],            
                
            ];
        $meta_seo = $this->meta_seo('','0   ',[
            'title' => 'Phụ kiện',
            'description'=> 'phụ kiện',
            'url' => route('web.phu_kien_products_categories.index')
        ]);
        return view('web.phukiens.index', compact('meta_seo','breadcrumbs','phukien_products'));
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request, $slug = null)
    {
        $page_size = 16;
        $category_phukien = PhukienProductCategory::where('status',1)->where('slug',$slug)->firstOrFail();
        $phukien_products = PhukienProduct::where('category_id',$category_phukien->id)->where('status',1)->paginate($page_size);
        $breadcrumbs = [
                ['name'=> 'Phụ kiện','url' => '/phu-kien'],            
                ['name' => $category_phukien->name, 'url' => $category_phukien->getUrl()],   
            ];

        $meta_seo = $this->meta_seo('phukien_product_categories',$category_phukien->id,[
            'title' => $category_phukien->name,
            'description'=> cutString(strip_tags($category_phukien->detail), 160),
            'url' => $category_phukien->getUrl()
        ]);
         return view('web.phukiens.index', compact('meta_seo','breadcrumbs','phukien_products'));
           
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}
