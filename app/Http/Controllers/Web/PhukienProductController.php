<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\PhukienProduct;
use App\PhukienProductCategory;
use App\Comment;
class PhukienProductController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request,$slug)
    {
         $product_phukien = PhukienProduct::where('status',1)->where('slug',$slug)->firstOrFail();
        if(is_null($product_phukien)){
            return abort(404);
        }
         $category_phukien = PhukienProductCategory::where('id', $product_phukien->category_id)->first();
         $comments = Comment::where('status',1)->where('type_id', $product_phukien->id)->where('type','phukien_products')->where('parent_id',0)->orderBy('created_at','desc')->get();
       
        $breadcrumbs = [
            ['name'=> 'Phụ kiẹn','url' => '/phu-kien'],       
            ['name' => $category_phukien->name, 'url' => $category_phukien->getUrl()],
            ['name' => $product_phukien->name, 'url' => $product_phukien->getUrl()],
        ];

        $meta_seo = $this->meta_seo('phukien_product_categories',$category_phukien->id,[
            'title' => $product_phukien->name,
            'description'=> cutString(strip_tags($product_phukien->detail), 160),
            'url' => $product_phukien->getUrl(),
            'image' => $product_phukien->getImage()
        ]);
        return view('web.phukiens.detail', compact('product_phukien','breadcrumbs','meta_seo','breadcrumbs','comments'));
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}
