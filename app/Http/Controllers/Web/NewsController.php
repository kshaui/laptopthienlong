<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\News;
use App\NewsCategory;
use App\Comment;
use DB;
class NewsController extends Controller
{
    public function index($slug = null)
    {
        if($slug == null) {
            $news = News::where('status',1)->orderBy('created_at','desc')->limit(5)->get();
            $meta_seo = $this->meta_seo('',0,
                [
                    'title' => 'Tin tức',
                    'description'=> 'Tin tức bên tranthanhlaptop',
                    'url' => url('').'/tin-tuc',
                ]);
            $new_other =  News::where('status',1)->wherenotIn('id', $news->pluck('id'))->orderBy('created_at','desc')->paginate(10);
            $breadcrumbs = [
                ['name'=> 'Tin tức','url' => '/tin-tuc'],         
            ];
            return view('web.news.index', compact('breadcrumbs','meta_seo','news','new_other'));
        }else {
            $category = NewsCategory::where('status',1)->where('slug',$slug)->firstOrFail();
            $news = News::where('status',1)->where('category_id', $category->id)->orderBy('created_at','desc')->limit(5)->get();
             $meta_seo = $this->meta_seo('news_categories',$category->id,[
                'title' => $category->name,
                'description'=> cutString(strip_tags($category->detail), 160),
                'url' => route('web.news_categories.show', $category->slug),
                'image'=> $category->getImage(),
            ]);
			$new_other =  News::where('status',1)->where('category_id', $category->id)->wherenotIn('id', $news->pluck('id'))->orderBy('created_at','desc')->paginate(10);
            $breadcrumbs = [
                ['name'=> 'Tin tức','url' => '/tin-tuc'],   
                ['name'=>$category->name,'url'=>route('web.news_categories.show', $category->slug)] ,     
            ];
            return view('web.news.index', compact('breadcrumbs','meta_seo','category','news','new_other'));
        }
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show($slug)
    {
        $new = News::where('status',1)->where('slug',$slug)->firstOrFail();
        $category = NewsCategory::where('id', $new->category_id)->first();
        $list_category = NewsCategory::where('status',1)->where('parent_id',0)->orderBy('id','desc')->get();
        $news_related = News::where('status',1)->where('id','<>', $new->id)->where('category_id', $category->id)->limit(5)->orderBy('created_at','desc')->get();

        $comments = Comment::where('status',1)->where('type_id', $new->id)->where('type','news')->where('parent_id',0)->orderBy('created_at','desc')->get();
       
        $meta_seo = $this->meta_seo('news',$new->id,[
            'title' => $new->name,
            'description'=> cutString(strip_tags($new->detail), 160),
            'url' => route('web.news.show', $new->slug),
            'image'=> $new->getImage(),
        ]);
        $breadcrumbs = [
            ['name' => 'Tin tức', 'url' => '/tin-tuc'],
            ['name' => $category->name, 'url' => route('web.news_categories.show', $category->slug)],
            ['name' => $new->name, 'url' => '/'],

        ];
        return view('web.news.detail', compact('new','meta_seo','news_related','list_category','breadcrumbs','comments'));
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}
