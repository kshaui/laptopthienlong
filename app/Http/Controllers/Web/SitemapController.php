<?php



namespace App\Http\Controllers\Web;



use Illuminate\Http\Request;

use Illuminate\Http\Response;

use Barryvdh\Debugbar\Facade as Debugbar;

use DB;

use App\Product;

use App\News;

use App\Page;
use App\Service;


class SitemapController extends Controller

{

    public $domain;

    public $sitemap = '';

    public $page_size = 200;

    public $lastmod;



    public function __construct()

    {

        Debugbar::disable();

        $this->domain = config('app.url');

        $this->lastmod = date('Y-m-d');

    }

    public function build($template) {

        $content = '<?xml version="1.0" encoding="UTF-8"?>';

        $content .= '<?xml-stylesheet type="text/xsl" href="/assets/sudo-sitemap.xsl"?>';

        $content .= '<'.$template.' xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

        '.$this->sitemap.'

        </'.$template.'>';

        return response($content, '200')->header('Content-Type', 'text/xml');

    }

    public function index() {

        $this->sitemap .= '<sitemap>

            <loc>'.$this->domain.'/sitemap-misc.xml</loc>

            <lastmod>'.$this->lastmod.'</lastmod>

        </sitemap>';



        $this->sitemap .= '<sitemap>

            <loc>'.$this->domain.'/sitemap-categories.xml</loc>

            <lastmod>'.$this->lastmod.'</lastmod>

        </sitemap>';



        $total_products = Product::where('status',1)->count();

        if($total_products%($this->page_size) == 0) {

            $page_total = $total_products/($this->page_size);

        }else {

            $page_total = (int)($total_products/($this->page_size)) + 1;

        }

        $total_services = Service::where('status',1)->count();
        if($total_services%($this->page_size) == 0) {
            $page_total = $total_services/($this->page_size);
        }else {
            $page_total = (int)($total_services/($this->page_size)) + 1;
        }
        for($i=1;$i<=$page_total;$i++) {
            $this->sitemap .= '<sitemap>
                <loc>'.$this->domain.'/sitemap-services-page-'.$i.'.xml</loc>
                <lastmod>'.$this->lastmod.'</lastmod>
            </sitemap>';
        }


        for($i=1;$i<=$page_total;$i++) {

            $this->sitemap .= '<sitemap>

                <loc>'.$this->domain.'/sitemap-products-page-'.$i.'.xml</loc>

                <lastmod>'.$this->lastmod.'</lastmod>

            </sitemap>';

        }



        $total_news = News::where('status',1)->count();

        if($total_news%($this->page_size) == 0) {

            $page_total = $total_news/($this->page_size);

        }else {

            $page_total = (int)($total_news/($this->page_size)) + 1;

        }

        for($i=1;$i<=$page_total;$i++) {

            $this->sitemap .= '<sitemap>

                <loc>'.$this->domain.'/sitemap-news-page-'.$i.'.xml</loc>

                <lastmod>'.$this->lastmod.'</lastmod>

            </sitemap>';

        }



        $this->sitemap .= '<sitemap>

            <loc>'.$this->domain.'/sitemap-pages.xml</loc>

            <lastmod>'.$this->lastmod.'</lastmod>

        </sitemap>';



        $this->sitemap .= '<sitemap>

            <loc>'.$this->domain.'/sitemap-contact.xml</loc>

            <lastmod>'.$this->lastmod.'</lastmod>

        </sitemap>';



        return $this->build('sitemapindex');

    }

    public function misc() {

        $this->sitemap .= '<url>

                            <loc>'.$this->domain.'</loc>

                            <lastmod>'.$this->lastmod.'</lastmod>

                            <changefreq>daily</changefreq>

                            <priority>1.00</priority>

                          </url>';

        $this->sitemap .= '<url>

                            <loc>'.$this->domain.'/sitemap.xml</loc>

                            <lastmod>'.$this->lastmod.'</lastmod>

                            <changefreq>monthly</changefreq>

                            <priority>0.5</priority>

                          </url>';



        return $this->build('urlset');

    }

    public function categories() {



        $news = DB::table('news_categories')->where('status',1)->get();

        $products_cateogries = DB::table('products_categories')->where('status',1)->get();

        $service_categories = DB::table('service_categories')->where('status',1)->get();


        
        $this->sitemap .= '<url>';

        $this->sitemap .= '<loc>'.route('web.products_categories.index').'</loc>';

        $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';

        $this->sitemap .= '<changefreq>weekly</changefreq>';

        $this->sitemap .= '<priority>0.80</priority>';

        $this->sitemap .= '</url>';


        foreach ($products_cateogries as $value) {

            $this->sitemap .= '<url>';

            $this->sitemap .= '<loc>'.$this->domain.'/'.$value->slug.'/</loc>';

            $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';

            $this->sitemap .= '<changefreq>weekly</changefreq>';

            $this->sitemap .= '<priority>0.80</priority>';

            $this->sitemap .= '</url>';

        }

        $this->sitemap .= '<url>';
        $this->sitemap .= '<loc>'.route('web.service_categories.showall').'/</loc>';
        $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';
        $this->sitemap .= '<changefreq>weekly</changefreq>';
        $this->sitemap .= '<priority>0.80</priority>';
        $this->sitemap .= '</url>';
        foreach ($service_categories as $value) {
            $this->sitemap .= '<url>';
            $this->sitemap .= '<loc>'.route('web.service_categories.show',$value->slug).'/</loc>';
            $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';
            $this->sitemap .= '<changefreq>weekly</changefreq>';
            $this->sitemap .= '<priority>0.80</priority>';
            $this->sitemap .= '</url>';
        }

        $this->sitemap .= '<url>';

        $this->sitemap .= '<loc>'.$this->domain.'/'.'tin-tuc/</loc>';

        $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';

        $this->sitemap .= '<changefreq>weekly</changefreq>';

        $this->sitemap .= '<priority>0.80</priority>';

        $this->sitemap .= '</url>';

        foreach ($news as $value) {

            $this->sitemap .= '<url>';

            $this->sitemap .= '<loc>'.$this->domain.'/tin-tuc/'.$value->slug.'/</loc>';

            $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';

            $this->sitemap .= '<changefreq>weekly</changefreq>';

            $this->sitemap .= '<priority>0.80</priority>';

            $this->sitemap .= '</url>';

        }

        return $this->build('urlset');

    }

    public function show($slug,$page) {

        $page_start = ($page - 1)*$this->page_size;

        if($slug == 'news'){

            return $this->news($page_start);

        }elseif($slug == 'products'){

            return $this->products($page_start);

        }elseif($slug == 'services'){

            return $this->services($page_start);

        }

    }

    public function products($offset) {



        $categories = Product::where('status',1)->offset($offset)->limit($this->page_size)->get();

        foreach ($categories as $value) {

            $this->sitemap .= '<url>';

            $this->sitemap .= '<loc>'.$this->domain.'/'.$value->slug.'.html</loc>';

            $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';

            $this->sitemap .= '<changefreq>weekly</changefreq>';

            $this->sitemap .= '<priority>0.50</priority>';

            $this->sitemap .= '</url>';

        }



        return $this->build('urlset');

    }

    public function services($offset) {

        $categories = Service::where('status',1)->offset($offset)->limit($this->page_size)->get();
        foreach ($categories as $value) {
            $this->sitemap .= '<url>';
            $this->sitemap .= '<loc>'.route('web.services.show',$value->slug).'</loc>';
            $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';
            $this->sitemap .= '<changefreq>weekly</changefreq>';
            $this->sitemap .= '<priority>0.50</priority>';
            $this->sitemap .= '</url>';
        }

        return $this->build('urlset');
    }

	public function news($offset) {



        $categories = News::where('status',1)->offset($offset)->limit($this->page_size)->get();

        foreach ($categories as $value) {

            $this->sitemap .= '<url>';

            $this->sitemap .= '<loc>'.$this->domain.'/tin-tuc/'.$value->slug.'.html</loc>';

            $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';

            $this->sitemap .= '<changefreq>weekly</changefreq>';

            $this->sitemap .= '<priority>0.50</priority>';

            $this->sitemap .= '</url>';

        }



        return $this->build('urlset');

    }

    public function pages(){

        $data = DB::table('pages')->where('status',1)->get();

        foreach ($data as $key=>$value){

            $this->sitemap .= '<url>';

            $this->sitemap .= '<loc>'.$this->domain.'/trang/'.$value->slug.'.html</loc>';

            $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';

            $this->sitemap .= '<changefreq>weekly</changefreq>';

            $this->sitemap .= '<priority>0.50</priority>';

            $this->sitemap .= '</url>';

        }

        return $this->build('urlset');

    }

    public function contact() {

        $this->sitemap .= '<url>';

        $this->sitemap .= '<loc>'.$this->domain.'/lien-he/</loc>';

        $this->sitemap .= '<lastmod>'.$this->lastmod.'</lastmod>';

        $this->sitemap .= '<changefreq>weekly</changefreq>';

        $this->sitemap .= '<priority>0.80</priority>';

        $this->sitemap .= '</url>';



        return $this->build('urlset');

    }

}

