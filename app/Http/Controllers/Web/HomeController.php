<?php

namespace App\Http\Controllers\Web;
use App\Repositories\AllRepository\getRepository;
use Illuminate\Http\Request;
use App\MyClass\Categories;
use DB;
use App\Product;
use App\ProductsCategory;
use App\News;
use Carbon\Carbon;
use App\PhukienProduct;
use App\Service;
use App\PhukienProductCategory;
use Illuminate\Support\Facades\Cache;
class HomeController extends Controller
{
    public function index()
    {
        $menu_home =1;
        $this->repository = new getRepository();
        if (Cache::has('config_home')) {
            $config_home = Cache::get('config_home');
        } else {
            $config_home = DB::table('options')->select('value')->where('name','home')->first();
            $config_home = json_decode(base64_decode($config_home->value),true);
            Cache::forever('config_home', $config_home);
        }
        if (Cache::has('config_general')) {
            $config_general = Cache::get('config_general');
        } else {
            $config_general = DB::table('options')->select('value')->where('name','general')->first();
            $config_general = json_decode(base64_decode($config_general->value),true);
            Cache::forever('config_general', $config_general);
        }

         if (Cache::has('news_home')) {
            $news_home = Cache::get('news_home');
         }else{
            $news_home = News::where('status',1)->leftJoin('pins', 'pins.type_id','=', 'news.id')->where('type','news')->where('place','home')->orderBy('value','asc')->get();
            Cache::forever('news_home', $news_home);
         }
        $slides = $this->repository->Slides();
        if (Cache::has('products_banchay')) {
            $products_banchay = Cache::get('products_banchay');
        }else{
            $products_banchay = Product::where('ban_chay',1)->where('status',1)->limit(8)->get();
             Cache::forever('products_banchay', $products_banchay);
        }
        $agv_product_banchay_collect = collect(DB::table('votes')->whereIn('type_id', $products_banchay->pluck('id'))->where('type','products')->get());
   
        
      // $products_nobat =  $this->repository->ProductNoiBat(); 
      // $agv_product_noibat_collect = collect(DB::table('votes')->whereIn('type_id', $products_nobat->pluck('id'))->where('type','products')->get());
   
      $product_category_home =  $this->repository->ProductCategoryHome();
  

      $all_product =  $this->repository->allProduct();
      $agv_all_product_collect = collect(DB::table('votes')->whereIn('type_id', $all_product->pluck('id'))->where('type','products')->get());
 

      if(cache::has('config_ads')) {
        $config_ads = Cache::get('config_ads');
    }else{
        $config_ads = DB::table('options')->select('value')->where('name','advertisement')->first();
        if(!empty($config_ads)){
            $config_ads = json_decode(base64_decode($config_ads->value),true);
            Cache::forever('config_ads', $config_ads);
        }
    }

    if(cache::has('product_sales')) {
        $product_sales = Cache::get('product_sales');
    }else{
        $product_sales = Product::join('product_coupon_map','product_coupon_map.product_id','=','products.id')->join('coupons','coupons.id','=','product_coupon_map.coupon_id')->where('coupons.status',1)->where('coupons.start','<',Carbon::now())->where('coupons.end','>',Carbon::now())->selectRaw('products.*,coupons.name as name_coupons,coupons.end as end ')->get();
     Cache::forever('product_sales', $product_sales);
    }
     if(cache::has('brand_home')) {
        $brand_home = Cache::get('brand_home');
    }else{
        $brand_home = DB::table('brands')->leftJoin('pins', 'pins.type_id','=', 'brands.id')->where('type','brands')->where('place','home')->orderBy('value','asc')->where('status',1)->get();
        Cache::forever('brand_home', $brand_home);
    }

    if(cache::has('products_categories_xu_huong')) {
        $products_categories_xu_huong = Cache::get('products_categories_xu_huong');
    }else{
        $products_categories_xu_huong = ProductsCategory::select(['products_categories.*'])->where('status',1)->where('parent_id','!=', 0)->leftJoin('pins', 'pins.type_id','=', 'products_categories.id')->where('type','products_categories')->where('place','home_xu_huong')->orderBy('value','asc')->get();
        Cache::forever('products_categories_xu_huong', $products_categories_xu_huong);
    }
   

    if(cache::has('product_xu_huong_collect')) {
        $product_xu_huong_collect = Cache::get('product_xu_huong_collect');
    }else{
        $product_xu_huong_collect = collect(Product::where('status',1)->whereIn('category_id',$products_categories_xu_huong->pluck('id')->toArray())->get());
        Cache::forever('product_xu_huong_collect', $product_xu_huong_collect);
    }

    $agv_product_xu_huon_collect = collect(DB::table('votes')->whereIn('type_id', $product_xu_huong_collect->pluck('id'))->where('type','products')->get());

     // dd($product_xu_huong_collect);

    if(cache::has('product_khuyen_mai_hot')) {
        $product_khuyen_mai_hot = Cache::get('product_khuyen_mai_hot');
    }else{
        $product_khuyen_mai_hot = Product::where('label_sale',1)->where('status',1)->limit('5')->get();
        Cache::forever('product_khuyen_mai_hot', $product_khuyen_mai_hot);
    }
    
     $agv_khuyen_mai_hot_collect = collect(DB::table('votes')->whereIn('type_id', $product_khuyen_mai_hot->pluck('id'))->where('type','products')->get());

     //dịch vụ sửa chữa laptop

      $services_highlight = Service::where('status',1)
            ->leftJoin('pins', 'services.id', '=', 'pins.type_id')->where('type','services')->where('place','highlight')->orderBy('pins.value','asc')
            ->limit(15)->get();
   

        $meta_seo = $this->meta_seo('',0,[
           'title' => $config_home['meta_title'],
           'description'=> $config_home['meta_description'],
            'url' => url('/'),
            'image' => $config_general['logo_header'] ,
        ]);
        $admin_bar_edit = route('admin.setting.home');
        return view('web.home',compact('config_ads','meta_seo','admin_bar_edit','slides','products_banchay','product_category_home','all_product','agv_all_product_collect','agv_product_banchay_collect','product_sales','brand_home','products_categories_xu_huong','product_xu_huong_collect','agv_product_xu_huon_collect','product_khuyen_mai_hot','agv_khuyen_mai_hot_collect','news_home','menu_home','services_highlight'));
    }
    public function page_not_found(){
        return view('web.page_not_found');
    }
    public function search(Request $request) {
        $keyword = $request->keyword;
        $key        = str_replace(" ","%",$keyword);
        $key        = str_replace("\'","'",$key);
        $key        = str_replace("'","''",$key);
        $products = Product::where('status',1)->where('name','like','%'.$key.'%')->limit(16)->get();
        $agv_product_collect = collect(DB::table('votes')->whereIn('type_id', $products->pluck('id'))->where('type','products')->get());
        $news = News::where('status',1)->where('name','like','%'.$key.'%')->limit(10)->get();
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Tìm kiếm sản phẩm',
            'description'=> '',
        ]);
        return view('web.searchs.index', compact('keyword','meta_seo','products','news','agv_product_collect'));
    }
}