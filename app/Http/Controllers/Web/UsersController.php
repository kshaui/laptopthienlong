<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
class UsersController extends Controller
{
    public function index()
    {
        return view('web.users.index');
    }
    public function order()
    {
       if(Auth::check()){
            $orders = DB::table('orders')->where('status','!=',4)->where('user_id',Auth::user()->id)->orderBy('created_at','DESC')->paginate(5);
             return view('web.users.order',compact('orders'));
       }
    }
    public function check_orders(Request $request)
    {
        $order= DB::table('orders')->where('status','!=',4)->where('code_order',$request->code_check)->
        where('phone',$request->phone_check)->first();
        // dd($order);
        return view('web.users.check_orders',compact('order'));

    }
    public function Kiem_Tra_bao_hanh(Request $request)
    {
        $guarantees= DB::table('guarantees')->
        where('phone',$request->phone_bh)->get();
        // dd($guarantees);
        return view('web.users.check_bh',compact('guarantees'));
    }
}
