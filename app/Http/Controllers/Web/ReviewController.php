<?php

namespace App\Http\Controllers\Web;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Cookie;
use Session;
use App\News;
use App\Product;
use Mail;
class ReviewController extends Controller
{
    public function addComment(Request $request){
        $id_insert = DB::table('comments')->insertGetId(
            [
            'type'=>$request->type,
            'type_id' => $request->data_id,
            'parent_id' => 0,
            'name' => $request->cmt_name,
            'phone'=>$request->cmt_phone,
            'email' => $request->cmt_email,
            'content'=> $request->cmt_content,
            'status' => 2,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
            ]);
            $msg = '<p class="p-name"><span>'.substr(removeSign($request->cmt_name),0,1).'</span>'.$request->cmt_name.'</p>
                    <p class="p-content">'.$request->cmt_content.'</p>
                    <p class="p-action"><span>Trả lời</span><span>Vừa xong</span></p>';
                    
        //    $config_general = DB::table('options')->where('name','general')->first();
        //    $config_general = json_decode(base64_decode($config_general->value));
        //    $send_email = $config_general->email;
        //    Mail::to($send_email)->send(new Comment(
        //    [
        //        'id_insert'=>$id_insert,
        //        'name'=>$request->cmt_name,
        //        'email'=>$request->cmt_email,
        //        'content'=>$request->cmt_content,
        //    ]));

         $san_phan_comment = DB::table($request->type)->where('status',1)->where('id',$request->data_id)->first();
            $data_comment = [
                'name' => $request->cmt_name,
                'phone'=>$request->cmt_phone,
                'email' => $request->cmt_email,
                'content'=> $request->cmt_content,
                'name_sp'=>$san_phan_comment->name,
                'link_sp'=> route('review.reply', $id_insert) ,
            ];
            // Mail::send('emails.comment_mail',$data_comment,function($msg){
            //     $msg->from('tranthanhhauinb@gmail.com','Khách hàng bình luận bài viết tại Laptop247hn');
            //     $msg->to('tranthanh.ceo96@gmail.com','thanh')->subject('Thông báo Khách hàng bình luận bài viết tại laptop247hn');
            // });
        Session::flash('flash_message', 'Send message successfully!');

        // return view('form');
            return response()->json(array('msg'=> $msg), 200);
    }

    public function replyComment(Request $request)
    {
        $type = $request->type;
        $type_id = $request->type_id;
        $review_id = $request->review_id;
        $name = $request->name;
        $email = $request->email;
           $content = $request->content;
        $created_at = date("Y-m-d H:i:s");
        $db_insert = [
            'type' => $type,
            'parent_id' => $review_id,
            'type_id' => $type_id,
            'name' => $name,
            'email' => $email,
               'content' => $content,
            'status' => 2,
            'created_at' => $created_at,
            'updated_at' => $created_at,
        ];
        DB::table('comments')->insert($db_insert);
        return 1;
    }
}
