<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use Request;
use DB;
use App\Product;
use App\ProductsCategory;
use Cart;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendMailOrder;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
class AjaxController extends Controller
{
    public function getDistricts(){
        if(Request::ajax()){
            $province_id = Request::get('province_id');
            $data = DB::table('district')->where('parent_id', $province_id)->get();
            $result['html']='<option value="0">-- Chọn quận huyện --</option>';
            foreach ($data as $key => $value) {
                $result['html'].='<option value="'.$value->id.'">'.$value->name.'</option>';
            }
            return json_encode($result);
        }
    }
    public function getSize(){
        if(Request::ajax()){
            $category_id=Request::get('category_id');
            $calculation_size=Request::get('calculation_size');
            $cao=Request::get('cao');
            $nang=Request::get('nang');
            $nguc=Request::get('nguc');
            $co=Request::get('co');
            $vai=Request::get('vai');
            $bung=Request::get('bung');
            $mong=Request::get('mong');
            $dui=Request::get('dui');
            $result['status'] = 1;
            $result['size'] = '';
            if($calculation_size == 1){
                $data_calculation_size = DB::table('calculation_size')->where('calculation_size_id', 1)->where('status',1)->orderBy('id','desc')->first();
                $data_cao = explode(',', $data_calculation_size->cao);
                $data_nang = explode(',', $data_calculation_size->nang);
                $data_size = explode(',', $data_calculation_size->size);
                foreach ($data_cao as $key => $value) {
                    if($cao <= $value){
                        if(!isset($i)){
                            $i = $key;
                        }
                    }
                    if($nang <= $data_nang[$key]){
                        if(!isset($k)){
                            $k = $key;
                        }
                    }
                }
                $result['size'] = $data_size[$this->getNumber($i, $k)];
            }elseif($calculation_size == 2){
                $data_calculation_size = DB::table('calculation_size')->where('calculation_size_id', 2)->where('status',1)->orderBy('id','desc')->first();

                $data_nguc = explode(',', $data_calculation_size->nguc);
                $data_co = explode(',', $data_calculation_size->co);
                $data_vai = explode(',', $data_calculation_size->vai);
                $data_size = explode(',', $data_calculation_size->size);
                foreach ($data_nguc as $key => $value) {
                    if($nguc <= $value){
                        if(!isset($i)){
                            $i = $key;
                        }
                    }
                    if($co <= $data_co[$key]){
                        if(!isset($k)){
                            $k = $key;
                        }
                    }
                    if($vai <= $data_vai[$key]){
                        if(!isset($j)){
                            $j = $key;
                        }
                    }
                }
                $result['size'] = $data_size[$this->getNumber($i, $k, $j)];

            }elseif($calculation_size == 3){
                $data_calculation_size = DB::table('calculation_size')->where('calculation_size_id', 3)->where('status',1)->orderBy('id','desc')->first();
                $data_bung = explode(',', $data_calculation_size->bung);
                $data_mong = explode(',', $data_calculation_size->mong);
                $data_dui = explode(',', $data_calculation_size->dui);
                $data_size = explode(',', $data_calculation_size->size);
                foreach ($data_bung as $key => $value) {
                    if($bung <= $value){
                        if(!isset($i)){
                            $i = $key;
                        }
                    }
                    if($mong <= $data_mong[$key]){
                        if(!isset($k)){
                            $k = $key;
                        }
                    }
                    if($dui <= $data_dui[$key]){
                        if(!isset($j)){
                            $j = $key;
                        }
                    }
                }
                $result['size'] = $data_size[$this->getNumber($i, $k, $j)];
            }
            return json_encode($result);
        }
    }
    public function subEmail(){
        if(Request::ajax()){
            $email=Request::get('email');
            $created_at = $updated_at = date('Y/m/d H:i:s');
            DB::table("email_subscribe")->insert(['email'=>$email, 'status'=>1,'created_at'=>$created_at,'updated_at'=>$updated_at]);
            return 1;
        }
    }
    public function order(){
        if(Request::ajax()){
            $name=Request::get('name');
            $phone=Request::get('phone');
            $email=Request::get('email');
            $address=Request::get('address');
            $note=Request::get('note');
            $payments=Request::get('payments');
        }
        Session::put('name', $name);
        Session::put('phone', $phone);
        Session::put('email', $email);
        Session::put('address', $address);
        Session::put('note', $note);
        Session::put('payments', $payments);

        return 1;
    }
    public function payment(){
        if(Request::ajax()){
            $user_id = @Auth::user()->id;
            $name=Request::get('name');
            $phone=Request::get('phone');
            $email=Request::get('email');
            $address=Request::get('address');
            $province_id=Request::get('province_id');
            $district_id=Request::get('district_id');
            $note=Request::get('note');
            $payments=Request::get('payments');
            $code_order = '#th'.substr($phone,6).time();
            $check_code_order = DB::table('orders')->where('status','!=',4)->where('code_order',$code_order)->get();
            if(!empty($check_code_order)){
                $code_order = '#th'.substr($phone,6).time();
            }
            $cart = Cart::content();
            $insert = [];
            $i=0;
            foreach ($cart as $key => $value) {
                $insert[$i]['id'] = $value->id;
                $insert[$i]['qty'] = $value->qty;
                $insert[$i]['price'] = $value->price;
                $insert[$i]['name'] = $value->name;
                $insert[$i]['size_id'] = $value->options['size_id'];
                $insert[$i]['color_id'] = $value->options['color_id'];
                $insert[$i]['image'] = $value->options['image'];
                $insert[$i]['slug'] = $value->options['slug'];
                $i++;
            }
            $insert=base64_encode(json_encode($insert));
            $created_at = $updated_at = date('Y-m-d H:i:s');
            DB::table('orders')->insert(['user_id'=>$user_id,'content'=>$insert,'code_order'=> $code_order,'name'=>$name,'email'=>$email,'phone'=>$phone,'address'=>$address,'note'=>$note,'payments'=>$payments,'status'=>2,'created_at'=>$created_at,'updated_at'=>$updated_at,'province_id'=>$province_id,'district_id'=>$district_id]);

            // $data_user = ['name'=>$name,'phone'=>$phone,'email'=>$email,'address'=>$address,'note'=>$note];
            // $data_user=base64_encode(json_encode($data_user));
            // Mail::send('emails.order_mail',$data_user,function($msg){
            //     $msg->from('tranthanhhauinb@gmail.com','khách hàng mua hàng tại Laptop247hn');
            //     $msg->to('tranthanh.ceo96@gmail.com','thanh')->subject('Thông báo khách hàng mua hàng tại laptop247hn');
            // });
            Cart::destroy();
            return 1;
        }
    }
    public function removeCart(){
        if(Request::ajax()){
            $rowId=Request::get('rowId');
            Cart::remove($rowId);
            return 1;
        }
    }
    public function updateColor(){
        if(Request::ajax()){
            $rowId=Request::get('rowId');
            $color_id = Request::get('color_id');
            $cart = Cart::get($rowId);
            $data_attr_image = DB::table('attribute_images')->select('image')->where('product_id', $cart->id)->where('color_id', $color_id)->first();
            // dump($data_attr_image);
            if($data_attr_image->image != ''){
                $image = $data_attr_image->image;
            }else{
                $image = $cart->options['image'];
            }
            Cart::update($rowId, ['options'=>['image'=>$image,'slug'=>$cart->options['slug'],'color_id'=>$color_id,'size_id'=>$cart->options['size_id']]]);
            $result['image'] = '';
            $result['status'] = 1;
            $data = DB::table('attribute_images')->where('product_id', $cart->id)->where('color_id', $color_id)->first();
            if($data){
                $result['image'] = $data->image;
            }
            return json_encode($result);
        }
    }
    public function updateSize(){
        if(Request::ajax()){
            $rowId=Request::get('rowId');
            $size_id = Request::get('size_id');
            $cart = Cart::get($rowId);
            Cart::update($rowId, ['options'=>['image'=>$cart->options['image'],'slug'=>$cart->options['slug'],'size_id'=>$size_id,'color_id'=>$cart->options['color_id']]]);
            return 1;
        }
    }
    public function updateQty(){
        if(Request::ajax()){
            $rowId=Request::get('rowId');
            $type=Request::get('type');
            $cart = Cart::get($rowId);
            $qty = $cart->qty + $type;
            Cart::update($rowId, $qty);
            $cart = Cart::get($rowId);
            $result['status'] = 1;
            $result['count_cart'] = Cart::count();
            $result['total_price'] = number_format($cart->price*$cart->qty).' VNĐ';
            $result['value_total'] = Cart::subtotal(0,',').' VNĐ';
            return json_encode($result);
        }
    }
    public function addToCart(){
        if(Request::ajax()){
            $color_id=Request::get('color_id');
            $size_id=Request::get('size_id');
            $qty=Request::get('qty');
            $product_id=Request::get('product_id');
            $product = Product::where('id', $product_id)->first();
            $image = '';
            if($color_id != 0){
                $data_attr_image = DB::table('attribute_images')->select('image')->where('product_id', $product_id)->where('color_id', $color_id)->first();
                $image = $data_attr_image->image;
            }
            if($image == ''){
                $image = $product->image;
            }
            Cart::add(['id' => $product_id, 'name' => $product->name, 'qty' => $qty, 'price' => $product->price ?? 0, 'weight' => 100, 'options' => ['image'=>$image,'slug'=>$product->slug,'size_id' => $size_id,'color_id'=>$color_id]]);
            $result['status'] = 1;
            $result['html'] = '';
            $result['count_cart'] = Cart::count();
            $count_cart = Cart::count();
            $cart = Cart::content();
            $result['html'] = view('web.popup_cart', compact('cart','count_cart','product'))->render();
            return response()->json($result);
        }
    }
    
    public function replyComment(){
        if(Request::ajax()){
            $gender=Request::get('gender');
            $name=Request::get('name');
            $email=Request::get('email');
            $phone=Request::get('phone');
            $content=Request::get('content');
            $comment_id=Request::get('comment_id');
            $created_at=$updated_at=date('Y-m-d H:i:s');
            $data = DB::table('comments')->where('id', $comment_id)->first();
            DB::table('comments')->insert([
                'type_id' => $data->type_id,
                'type' => $data->type,
                'parent_id'  => $comment_id,
                'gender'    => $gender,
                'name'=>$name,
                'email'=>$email,
                'phone'=>$phone,
                'content'=>$content,
                'status'=>1,
                'created_at'=>$created_at,
                'updated_at'=>$updated_at
            ]);
          
            return 1;
        }
    }
    public function voteStart(){
        if(Request::ajax()){
            $type_id = Request::get('type_id');
            $vote= Request::get('vote');
            $type = Request::get('type');
            $created_at=$updated_at=date('Y-m-d H:i:s');
            DB::table('votes')->insert(['type_id'=>$type_id, 'type'=>$type, 'value'=>$vote, 'created_at'=>$created_at, 'updated_at'=>$updated_at]);
            setcookie('vote_'.$type_id, $vote, time() + (24*60*60), "/");
            return 1;
        }
    }
    public function getNumber($a, $b, $c = 0){
        if($a >= $b){
            $k = $a;
        }else{
            $k = $b;
        }
        if($k >= $c){
            $i = $k;
        }else{
            $i = $c;
        }
        return $i;
    }
    public function AjaxBrand()
    {
       if(Request::ajax()){
        $slug = Request::get('slug');
            if($slug == 'tat-ca'){
                $products = Product::where('status',1)->get();
                $agv_all_product_collect = collect(DB::table('votes')->whereIn('type_id', $products->pluck('id'))->where('type','products')->get());
                $result['status'] = 1;
                $result['html'] = view('web.layouts.data_product_ajax', compact('products','agv_all_product_collect'))->render(); 
            }else{
                $brand = DB::table('brands')->where('status',1)->where('slug',$slug)->first();  
                $products = Product::where('status',1)->where('brand_id',$brand->id)->get();
                $agv_all_product_collect = collect(DB::table('votes')->whereIn('type_id', $products->pluck('id'))->where('type','products')->get());
                if($products){
                    $result['slug'] = $brand->slug;
                    $result['html'] = view('web.layouts.data_product_ajax', compact('products','agv_all_product_collect'))->render();
                }              
                  
            }
            return json_encode($result);   
       }
    }

    public function login()
    {
        $email = Input::get('email');
        $password = Input::get('password');
        if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1])) {
            return 1;
        } else {
            if (Auth::attempt(['name' => $email, 'password' => $password, 'status' => 1])) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function register()
    {
        if (Request::ajax()) {
            $name = Input::get('name');
            $email = Input::get('email');
            $password = Input::get('password');
            $fullname = Input::get('fullname', '');
            $check_email = DB::table('users')->where('email', $email)->where('status', '!=', 4)->first();
            $check_name = DB::table('users')->where('name', $name)->where('status', '!=', 4)->first();
            $today = date("Y-m-d H:i:s");
            $data['email'] = $email;
            $data['name'] = $name;

            $result['status'] = 0;
            $result['message'] = "";
            if (!empty($check_name)) {
                $result['message'] = "Tên đăng nhập đã được sử dụng";
                $result['status'] = -1;
            } elseif (!empty($check_email)) {
                $result['message'] = "Email đã được sử dụng";
                $result['status'] = 0;
            } else {
                $check = DB::table('users')->where('email', $email)->where('status', 4)->first();
                if (!empty($check)) {
                    DB::table('users')->where('id', $check->id)->delete();
                }
                $db_insert = [
                    'name' => $name,
                    'email' => $email,
                    'password' => bcrypt($password),
                    'created_at' => $today,
                    'updated_at' => $today,
                    'image' => '',
                    'fullname' => $fullname,
                    'phone' => '',
                    'status' => 2,
                ];
                $id_insert = DB::table('users')->insertGetId($db_insert);
                $data['id'] = $id_insert;
                $data['password'] = $password;

                // Mail::to($email)->send(new comfirmEmail($data));
                $result['status'] = 1;
            }
            return json_encode($result);
        }
    }

    public function dangKyEmail()
    {
        if (Request::ajax()) {
            $email = Input::get('email');
            $today = date("Y-m-d H:i:s");
            $db_insert = [
                'email' => $email,
                'created_at' => $today,
                'updated_at' => $today,
                'status' => 2,
            ];
            $id_insert = DB::table('email_subscribe')->insertGetId($db_insert);
            // $data=['email_dang_ky'=>$email];
            // Mail::send('emails.dang_ky_mail',$data,function($msg){
            //     $msg->from('tranthanhhauinb@gmail.com','Khách hàng đăng ký gửi mail tại Laptop247hn');
            //     $msg->to('tranthanh.ceo96@gmail.com','thanh')->subject('Thông báo đăng ký email tại laptop247hn');
            // });
            return 1;
        }
    }

     //đặt lịch sửa chữa
    public function orderService()
    {
        if(Request::ajax()){
            extract(Request::all(),EXTR_OVERWRITE);
            DB::table('orders')->insertGetid([
                'type_id'=>$id,
                'name'=>$name,
                'phone'=>$phone,
                'email'=>$email,
                'address'=>$address,
                'status'=>2,
                'note'=>$note,
                'type'=>$type,
            ]);
            return 1;
        }
    }
    //xem nhieu
    public function viewShowProduct()
    {
        if(Request::ajax()){
            extract(Request::all(),EXTR_OVERWRITE);
            $from = ($page-1)*$count;
            $products = Product::where('status',1)->skip($from)->take($count)->get();
            $agv_product_collect = collect(DB::table('votes')->whereIn('type_id', $products->pluck('id'))->where('type','products')->get());
            return view('web.layouts.data_product_ajax',compact('products','agv_product_collect'))->render();
        }
    }

}
