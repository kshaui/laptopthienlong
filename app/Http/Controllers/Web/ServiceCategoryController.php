<?php

namespace App\Http\Controllers\Web;
use DB;
use Illuminate\Http\Request;
use App\Service;
use App\ServiceCategory;
use App\ServiceCategoryMap;
use App\Classe;
class ServiceCategoryController extends Controller
{
    public function showall()
    {
      
        $services = Service::where('status', 1)->where('instock_status','!=',5)->select('id', 'name', 'slug', 'image', 'price', 'promotion');

        $services_categories_11 = Classe::where('status',1)->where('parent_id',0)->get();
        $chiled_services_categories_collect =collect( Classe::where('status',1)->whereIn('parent_id',$services_categories_11->pluck('id','id')->toArray())->get() );  

        $services = $services->orderBy('instock_status')->orderBy('price','DESC')->paginate(36);
        
        $meta_seo = $this->meta_seo('',0,[
            'title' =>"Dịch vụ sửa chữa".' - Laptop247hn',
            'description'=> "Dịch vụ sửa chữa",
            'url' => route('web.service_categories.showall')
        ]);

        $breadcrumbs = [
            ['name'=>'Sửa chữa laptop','url'=>route('web.service_categories.showall')],
        ];
        return view('web.services_categories.showall', compact('services','meta_seo','breadcrumbs','services_categories_11','chiled_services_categories_collect'));
    }
    public function index($slug = null)
    {   

        $services_categories_11 = Classe::where('status',1)->where('parent_id',0)->get();
        $chiled_services_categories_collect =collect( Classe::where('status',1)->whereIn('parent_id',$services_categories_11->pluck('id','id')->toArray())->get() );
       

        $service_category = Classe::where('slug', $slug)->where('status',1)->first();
     
        $service_category_parent = Classe::where('id',$service_category->parent_id)->where('status',1)->first();

        if($service_category->parent_id == 0){
            $services_categories = Classe::where('status',1)->where('parent_id', $service_category->id)
            ->get();
        }else{
            $services_categories = Classe::where('status',1)->where('parent_id', $service_category->parent_id)
           ->get();
        }
        $service_category_childs = Classe::where('parent_id', $service_category->id)->where('status', 1)->orderBy('order','asc')->select('id','name', 'slug')->take(7)->get();
        $service_category_childs_id = [];
        array_push($service_category_childs_id, $service_category->id);
        foreach ($service_category_childs as $value) {
            array_push($service_category_childs_id, $value->id);
        }
        $services = Service::wherein('class_id', $service_category_childs_id)->where('status', 1)->where('instock_status','!=',5)->orderBy('price', 'DESC');


        
        $services =   $services->paginate(36);

        $meta_seo = $this->meta_seo('classes',$service_category->id,[
            'title' => $service_category->name.' - Laptop247hn',
            'description'=> cutString(removeHTML($service_category->detail),150),
            'url' => route('web.service_categories.show',$service_category->slug),
        ]);
        $breadcrumbs = [
            ['name'=>'Sửa chữa laptop','url'=>route('web.service_categories.showall')],  
        ];
        $slug_parent = '';
        if($service_category_parent) {
            $breadcrumbs[] = ['name'=>$service_category_parent->name,'url'=>route('web.service_categories.show', ['slug'=>$service_category_parent->slug])];
            $slug_parent = $service_category_parent->slug;
        }
        $slug = $service_category->slug;
        
        return view('web.services_categories.index', compact('services' , 'service_category','meta_seo','breadcrumbs','slug','services_categories_11','chiled_services_categories_collect','slug_parent'));
    }
}
