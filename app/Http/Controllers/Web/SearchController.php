<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\Product;
use App;
use App\News;
use DB;

class SearchController extends Controller
{
    public function show(Request $request)
    {
    	$keyword = $request->keyword;
        $key        = str_replace(" ","%",$keyword);
        $key        = str_replace("\'","'",$key);
        $key        = str_replace("'","''",$key);
        $products = Product::where('status',1)->where('name','like','%'.$key.'%')->limit(16)->get();
        $news = News::where('status',1)->where('name','like','%'.$key.'%')->limit(10)->get();
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Tìm kiếm sản phẩm',
            'description'=> '',
        ]);
        return view('web.searchs.index', compact('keyword','meta_seo','products','news'));
    }

    public function suggest(Request $request){
        $key = $request->search;
        $key = str_replace(" ","%",$key);
        $key = str_replace("\'","'",$key);
        $key = str_replace("'","''",$key);
        $products = Product::where('status',1)->where('name','like','%'.$key.'%')->take(3)->get();
        $news = News::where('status',1)->where('name','like','%'.$key.'%')->take(3)->get();
        $data =  [];
        foreach ($products as $key => $value) {
        	 $data[] = [
                'type' => 'Sản phẩm',
                'name' => $value->name,
                'url' => $value->getUrl(),
                'image' => $value->image,
                'price' => $value->price,
            ];
        }
        foreach ($news as $key => $value) {
             $data[] = [
                'type' => 'Tin tức',
                'name' => $value->name,
                'url' => $value->getUrl(),
                'image' => $value->image,
                'price' => '',
            ];
        }
        return $data;
    }
}
