<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Barryvdh\Debugbar\Facade as Debugbar;
use App\MyClass\Rss;
use App\News;

class FeedsController extends Controller
{
    public function rss() {
        Debugbar::disable();
        $app_url = config('app.url');
        $rss = new Rss('Tin tức mới cập nhật',$app_url);
        $data = News::where('status',1)->orderBy('created_at', 'desc')->limit(36)->get();
        foreach($data as $value) {
            $name = $value->name;
            $url = route('web.news.show', $value->slug);
            $image = $value->image;
            $summary = cutString(removeHTML($value->detail),175);
            $description = '<div align="center">
                                 <a href="'.$url.'">
                                    <img width="200" src="'.$image.'" />
                                 </a>
                              </div>
                              <br />'.$summary;
            $rss->additem($name,$description,$url,$url,date('d/m/Y h:i:s a',strtotime($value->created_at)));
        }
        //header ("Content-Type:application/xml");
        //echo $rss->getRSSContent();
        return response($rss->getRSSContent(), '200')->header('Content-Type', 'application/xml');
    }
}
