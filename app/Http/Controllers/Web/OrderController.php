<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use DB;
use App\Product;
use Cart;

class OrderController extends Controller
{
    public function show()
    {
        $cart = Cart::content();
        if(!empty($cart)){
            $product_collect = collect(DB::table('products')->whereIn('id',$cart->pluck('id'))->get());
        }
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Giỏ hàng',
            'description'=> '',
            'url' => route('web.orders.show'),
        ]);
        return view('web.orders', compact('meta_seo','cart','product_collect'));
    }
    public function payment(){
        $cart = Cart::content();
       $provinces = DB::table('province')->get();
       if (is_null($provinces)){
           abort(404);
       }
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Tiến hành thanh toán',
            'description'=> '',
        ]);
        return view('web.payments', compact('meta_seo','cart','provinces'));
    }
    public function success(){
        $orders = DB::table('orders')->orderBy('id','desc')->first();
        $meta_seo = $this->meta_seo('',0,[
            'title' => 'Đặt hàng thành công',
            'description'=> '',
        ]);
        return view('web.order_success', compact('meta_seo','orders'));
    }
}
