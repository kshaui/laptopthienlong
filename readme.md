# Hướng dẫn sử dụng core #
group tranthanh Core giúp làm việc nhanh hơn, rất cần các đóng góp tối ưu từ người sử dụng trong quá trình sử dụng

## Các bước khởi tạo dự án từ group tranthanh Core ##
- Clone core từ git repository
- Tao database cho dự án
- Tạo file `.env` copy nội dung từ `.env.example` và config url, thông tin database
- Chạy composer install để require các package được định nghĩa trong composer.json
- Chay php artisan key:generate
- Tiến hành chạy migration (php artisan migrate)
- Tiến hành chạy seeder (php artisan db:seed) được tài khoản quản trị admin / group tranthanhvn

## Các thông tin core ##
- Route tách biệt phần admin (`/routes/admin.php`) và web (`/routes/web.php`)
- Các controller tách biện admin và web (`app/Http/Controller/Admin` và `app/Http/Controller/Web`), tất cả các controller tạo mới trong 2 thư mục này phải extend lại `Controller` cùng namespace
- Core đã có sẵn các module: cấu hình (settings), upload ảnh (media), log hệ thống, trang đơn, tin tức và danh mục tin tức, sản phẩm và danh mục sản phẩm
- Các module được khai báo và định nghĩa trong `/config/modules.php` để hiện thị trong menu sidebar (xem layout app: `/resources/views/admin/layouts/app.blade.php`)
- Phần quyền quản trị theo module, thực hiện trong "**quản lý tài khoản quản trị**" và lưu trữ trong bảng `authorizations`
- Các route module được khai báo theo qui chuẩn **Resource Controller** trong `/routes/admin.php` (vd: `Route::resource('products','Admin\ProductController',['middleware'=>'auth-admin']);`)
- Các module code theo qui chuẩn **Resource Controller** với các action: `index` (danh sách), `create` (view thêm mới), `store` (lưu), `edit` (view sửa), `update` (cập nhật), `destroy` (xóa => chuyển status 4 (các table yêu cầu có field `status` kiểu tinyInt))
- Các Controller Admin phải extend lại Controller trong `app/Http/Controller/Admin/Controller.php` và buộc phải có khởi tạo khai báo `module_name` và `table_name`, ví dụ:

		function __construct()
		{
			$this->module_name = 'tin tức';
			$this->table_name = 'news';
			parent::__construct();
		}

- Các module dạng danh mục code giống module `NewsCategory` hoặc `ProductsCategory` mẫu
- Các module khác code giống module `News` hoặc `Products` mẫu
- Action `index` ở các controller cho trang danh sách sử dụng `ListData` class trong `app\MyClass\ListData`, data sau khi qua add các field sẽ đổ về `admin.layouts.list` để hiện thị
- Action `create` và `edit` ở các controller cho trang thêm mới, sửa sử dụng class `MyForm` trong `app\MyClass\MyForm`, data đổ về các layouts tương ứng các layout này sẽ include `admin.layouts.form` để hiển thị form chi tiết
- Action `store` và `update` ở các controller cho lưu và cập nhật sử dụng method `validate_form` kế thừa từ Controller Admin, các data gửi lên (nhận từ `$data_form = $request->all();`) phải giống tên với dữ liệu trong db được add từ các action create và edit sau khi gọi `extract($data_form,EXTR_OVERWRITE);` sẽ sinh ra các biến độc lập sau đó tiến hành insert/update cuối cùng thêm các thông tin về SEO và Log
- Action destroy chỉ chuyển status của bản ghi về 4, core không xóa vĩnh viễn bản ghi khỏi db đề phòng trường hợp cần khôi phục do xóa nhầm hoặc lý do khác
- Các dữ liệu nên có field slug để tùy chỉnh url
- Mọi dữ liệu đều có thể thêm các thuộc tính title, description tùy chỉnh lưu ở bảng meta_seo

## Ví dụ về code 1 module mới ##
Ví dụ về module tư vấn (**advisories**) dạng tin có danh mục tương tự như module tin tức (**news**)

Tạo table cho danh mục (**advisory_categories**): `php artisan make:migration create_advisory_categories_table` nội dung action `up()`:

    Schema::create('advisory_categories', function (Blueprint $table) {
		$table->increments('id');
		$table->integer('parent_id')->default(0);
		$table->string('name');
		$table->string('slug');
		$table->text('image')->nullable();
		$table->longText('detail')->nullable();
		$table->integer('order')->default(0);
		$table->tinyInteger('status')->default(1);
		$table->timestamps();	
		$table->unique('id','id_UNIQUE');
	});

Tạo table tư vấn (**advisories**):  `php artisan make:migration create_advisories_table` nội dung action `up()`:

    Schema::create('advisories', function (Blueprint $table) {
		$table->increments('id');
		$table->integer('category_id')->default(0);
		$table->string('name');
		$table->string('slug');
		$table->text('image')->nullable();
		$table->longText('detail')->nullable();
		$table->tinyInteger('status')->default(1);
		$table->timestamps();
		$table->unique('id','id_UNIQUE');
	});

Chạy migrate: `php artisan migrate`

Tạo Controller resource:

    php artisan make:controller Admin\AdvisoryCategoryController --resource
	php artisan make:controller Admin\AdvisoryController --resource

Thêm routes/admin.php: 

    Route::resource('advisory_categories','Admin\AdvisoryCategoryController',['middleware'=>'auth-admin']);
	Route::resource('advisories','Admin\AdvisoryController',['middleware'=>'auth-admin']);

Khai báo module trong `/config/modules.php` để hiển thị

    'module' => [
		...
		'advisories_categories' => ['access','create','edit','delete'],
		'advisories' => ['access','create','edit','delete'],
		...
	],
	'name' => [
		'...
		'advisories_categories'=>'Danh mục tư vấn',
		'advisories'=>'Tư vấn',
		...
	],
	'icon' => [
		...
		'advisories_categories'=>'fa-bars',
		'advisories'=>'fa-newspaper-o',
		...
	]

Lúc này menu của "**Danh mục tư vấn**" và "**Tư vấn**" đã hiển thị trong danh sách module sidebar của trang quản trị, để đưa danh mục vào menu con của "**Tư vấn**" => fix cứng lại ở `/resources/views/admin/layouts/app.blade.php`

    @case('advisories_categories') @break
	@case('advisories')
	@if(checkRole($key.'_access'))
		<li><a><i class="fa {!! config('modules.icon')[$key] !!}"></i>{!! config('modules.name')[$key] !!} <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				@if(checkRole($key.'_create'))
					<li><a href="{!! route($key.'.create') !!}">Thêm mới</a></li>
				@endif
				<li><a href="{!! route($key.'.index') !!}">Danh sách</a></li>
				@if(checkRole('advisories_categories'))
					<li><a href="{!! route('advisories_categories.index') !!}">Danh mục</a></li>
				@endif
			</ul>
		</li>
	@endif
	@break

Như vậy đã đầy đủ Route resource và Controller resource cho 2 module, mọi tính năng của controller tham khảo theo module **news** và **news_categories**

`AdvisoryCategoryController` bỏ `use App\Http\Controllers\Controller;` để extend đúng Controller trong namespace `App\Http\Controllers\Admin;` sau đó use các class cần thiết

    use App\MyClass\MyForm;
	use App\MyClass\ListData;
	use App\MyClass\Categories;
	use DB;

`AdvisoriesCategoryController` thêm khởi tạo sau đó thêm code trong các action tương tự

    function __construct()
	{
		$this->module_name = 'danh mục tư vấn';
		$this->table_name = 'advisories_categories';
		parent::__construct();
	}

Riêng `admin.layouts.list` include index của module nên phải thêm view 

`AdvisoryController` làm tương tự như trên theo `NewsController` tuy nhiên cần thay `news_categories` thành `advisory_categories` cho danh mục tương ứng trong code. Ngoài ra module tư vấn này thiết kế không có tag nên cần bỏ các đoạn code liên quan đến tag ở các action

## Một số thành phần có sẵn khác ##
### SystemLogs ###
Module cho phép quản trị xem lại hành động của các quản trị viên khác. Thông tin được lưu trữ trong bảng `system_logs`
		
Các hành động như đăng nhập, module quản trị đã được viết sẵn. Các hành động theo module tùy chỉnh cho từng website sẽ phải tự viết vào các action `store`, `update`, `delete` của từng module.

Ví dụ lưu lại hành đông thêm mới tin tức ở `app/Http/Controller/Admin/NewsController/store` `$this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);`

Ví dụ lưu lại hành động chỉnh sửa tin tức ở  `app/Http/Controller/Admin/NewsController/update` `$this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);`

Module systemLogs này view ở `app/Http/Controller/Admin/SystemLogController`

### Hỗ trợ tùy chỉnh seo - meta_seo ###
Thông tin cấu hình title, description, robots cho các dữ liệu. Cấu trúc bảng dữ liệu meta_seo trong `/database/migrations/create_meta_seo_table.php`. `type` chính là tên bảng dữ liệu, `type_id` là giá trị id của bảng đó
		
Mặc định tất cả các module sẽ tự động có meta_seo do cấu hình từ property `$has_seo` ở `app/Http/Controller/Admin/Controller`. Nếu muốn 1 module nào không hiển thị phần cấu hình seo này thì ghi đè property `$has_seo` thành `false` ở constructor của module đó.

Layouts cấu hình seo `admin.layouts.metaseo` sẽ được include vào `admin.layouts.create` và `admin.layouts.edit` cho các module sau khi check `$has_seo`

Ở các action store và update phải chủ động lưu meta_seo bằng cách gọi function `metaSeo()` đã viết ở `app/Http/Controller/Admin/Controller` ví dụ: `$this->metaSeo($id_insert,$seo_title,$seo_description,$seo_robots);`

Việc hiển thị thông tin meta_seo ngoài front-end sẽ thông qua cách gọi dữ liệu từ func `meta_seo()` đã viết ở `app/Http/Controller/Web/Controller` để đổ ra view và layout `views/web/layouts/app` đã include `web.layouts.seo`

### Thư viện ảnh media ###
Tính năng được viết trong `app/Http/Controller/Admin/MediaController` gọi ra ở các module bằng `$data_form[] = $form->image('image','',0);`
		
Ngoài ảnh gốc, các ảnh up lên sẽ được resize theo chiều rộng kích thước cấu hình ở property `$imageSize`, sinh ra các ảnh với hậu tố tương ứng ví dụ `ten-anh-large.jpg`

Core hỗ trợ 2 kiểu storage là `local` và `objectstorage` sử dụng dịch vụ của z.com theo chuẩn openstack. Kiểu storage được định nghĩa ở `config/app/storage_type`

Với kiểu storage local ảnh sẽ lưu cùng thư mục với code trong folder uploads định nghĩa trong `config/app/disks/local`

Với kiểu storage objectstorage ảnh sẽ lưu trên cloud của z.com được cấu hình trong `config/app/disks/sop`. Mỗi dự án mới sẽ cần tạo 1 container cho dự án và thay đổi tên `container` và `container_url`. Container được make bằng command: `php artisan CreateContainerObjectStorage container_name`

Gọi ảnh ngoài front-end nên gọi theo kích thước phù hợp với design bằng cách gọi các resize tương ứng. Ví dụ về cách gọi ảnh đã viết mẫu trong func `getImage` trong `app/News.php`


### Module Cấu hình ###
Cấu hình các thành phần cố định của 1 website sẽ lưu ở bảng `options` và được code trong `app/Http/Controller/Admin/SettingController` với name là tên cấu hình và value là `base64_encode(json_encode($data));`
		
Các view của cấu hình sẽ sử dụng là `MyClass\MyForm` và đưa về view `admin.layouts.setting`

### Cấu hình googleshopping ###
Cấu hình chung của googleshopping cho toàn trang sẽ lưu ở phần setting
		
Các module có cấu hình googleshopping sẽ lưu ở bảng `google_shopping` tương đối giống cấu hình meta_seo

Ngoài front-end phần cấu hình googleshopping của mỗi module sẽ ghi đè lên cấu hình toàn trang nếu được setting hoặc sẽ sử dụng cấu hình của toàn trang nếu không được setting

### Admin bar ###
Admin bar giúp quản trị web dễ dang tìm link chỉnh sửa trang front-end hiện tại 
		
Ở action show của trang hiện tại chỉ cần khai báo link chỉnh sửa vào variable `$admin_bar_edit` rồi truyền nó qua view. Admin bar sẽ tự hiển thị vì `web.layouts/app` đã `include('web.layouts.adminbar')`. Xem ví dụ ở `app/Http/Controllers/Web/HomeController`  `$admin_bar_edit = route('admin.setting.home');`

### Nút xem cho các dữ liệu ở trang chỉnh sửa ###
Nút xem dữ liệu ngoài front-end từ trang edit trong quản trị giúp người dùng dễ dang xem nội dung mình đang biên tập.
		
Quy định `route/web` cần đặt tên route theo quy chuẩn `web.table.show`

Xem ví dụ ở `app/Http/Controller/Admin/NewsController/edit` `$data_form[] = $form->action('edit',route('web.'.$this->table_name.'.show',$data_edit->slug));`

### Validate dữ liệu ###
Dữ liệu sẽ được validate theo 2 kiểu là `require`(bắt buộc) và `unique`(duy nhất). Validate sẽ qua 2 bước bằng `javascript` khi click thêm hoặc sửa và bằng `php` ở các action store, update
		
Định nghĩa validate require bằng param `require` = 1 của class `MyClass\MyForm`. Data build ra view sẽ có dữ liệu `$array_valid` để gọi func validForm ở cách view `create`, `edit`. Func `validForm` được định nghĩa trong `public/template-admin/js/script.js`

Để chắc chắn và không tin tưởng dữ liệu gửi lên từ quản trị cần validate lại bằng func `validate_form` đã viết ở `app/Http/Controller/Admin/Controller` ở các action store, update của mỗi module, ví dụ: `$this->validate_form($request,'name',1,'Bạn chưa nhập tiêu đề');`

### Tính năng ghim dữ liệu - Pins ###
Các dữ liệu dạng list cần ghim sẽ lưu trong bảng pins để dễ dang tùy chỉnh và queries ngoài front-end

Ở trang list trong back-end cần add 1 param với type là `pins`. Xem ví dụ ở `app/Http/Controller/Admin/NewsController/index`

### Quick edit ở trang list ###
Các dữ liệu cần sửa nhanh ở trang danh sách sẽ gọi các input, select với tên là tên field trong database và class là `quick-edit`. Xem ví dụ ở `views/admin/news_categories/index` cho field `order`
		
Các dữ liệu này sẽ tự lưu khi nhấn icon save. Ajax sẽ được gửi từ func `save_one` và `save_all` ở file `/public/template-admin/js/script.js` gửi đến `app/Http/Controller/Admin/Controller` `saveOne` và `saveAll`

### Gửi và verify mail ###
Core đang sử dụng dịch vụ email AWS SES, với mỗi dự án mới cần lấy thông tin từ lead và cấu hình phần email trong .env

	MAIL_DRIVER=ses
	MAIL_FROM_ADDRESS=no-reply@grouptranthanh.vn
	MAIL_FROM_NAME=group_tranthanh
	SES_KEY=
	SES_SECRET=
	SES_REGION=us-west-2

Mỗi email gửi đi sẽ tự động được verify qua listener VerifyEmailSending nhờ dịch vụ của verify-email.org

.

.

.

### Update module tải ảnh media thêm drive do(digitalocean) - làm drive mặc định ###
Mặc định đã thay đổi drive trong config.app.storage_type sang digitalocean

Với mỗi dự án mới cần thay đổi cấu hình .env DO_FOLDER là tên folder muốn lưu ảnh dự án (thường là tên dự án, ví dụ: vietchem)

Domain gốc dẫn đến ảnh mà DO cung cấp https://group tranthanh.sgp1.digitaloceanspaces.com đã được DNS đồng thời từ domain https://group tranthanhspaces.com chúng ta sẽ thống nhất dùng cdn.group tranthanhspaces.com để hiển thị ảnh cho chuyên nghiệp hơn 

----------
Updateting ...