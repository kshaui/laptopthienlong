
<h3 style="margin:15px 0 5px;color:#ce171f;padding:2px 3px 2px 0;font-size:16px;text-transform:uppercase">
                    Thông tin khách hàng</h3>
<table style="border:1px solid #ff000;color:#333;width:100%;border-collapse:collapse;margin-top:6px">
                    <tbody>
                    <tr>
                        <th style="background:#eee;border:1px solid #ddd;color:#333;text-align:left;padding:2px 0 2px 5px;width:170px">
                            Full name
                        </th>
                        <td style="background:#eee;border:1px solid #ddd;color:#333;padding:2px 0 2px 5px">Ms
                            . {{ @$name }}</td>
                    </tr>
                    <tr>
                        <th style="border:1px solid #ddd;color:#333;text-align:left;padding:2px 0 2px 5px;width:170px">
                            Email
                        </th>
                        <td style="border:1px solid #ddd;color:#333;padding:2px 0 2px 5px"><a href="mailto:tranthanhhauinb@gmail.com" target="_blank">{{ @$email }}</a></td>
                    </tr>
                    <tr>
                        <th style="border:1px solid #ddd;color:#333;text-align:left;padding:2px 0 2px 5px;width:170px">
                            Telephone
                        </th>
                        <td style="border:1px solid #ddd;color:#333;padding:2px 0 2px 5px"><a href="tel:{{ @$phone }}">{{ @$phone }}</a></td>
                    </tr>
                    <tr>
                        <th style="background:#eee;border:1px solid #ddd;color:#333;text-align:left;padding:2px 0 2px 5px;width:170px">
                            Address
                        </th>
                        <td style="background:#eee;border:1px solid #ddd;color:#333;padding:2px 0 2px 5px">{{ @$address }}</td>
                    </tr>
                    <tr>
                        <th style="background:#eee;border:1px solid #ddd;color:#333;text-align:left;padding:2px 0 2px 5px;width:170px">
                           Ghi chú
                        </th>
                        <td style="background:#eee;border:1px solid #ddd;color:#333;padding:2px 0 2px 5px">{{ @$note }}</td>
                    </tr>
                   
                    </tbody>
                </table>