{{ csrf_field() }}
@php
foreach($data_form as $value) {
	switch($value['store']) {
		case 'text':
		@endphp
			<div class="form-group">
			    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
			    <div class="controls col-md-9 col-sm-10 col-xs-12">
			      	<input class="form-control @if($value['slug']==1){!! 'slug-title' !!}@endif" type="text" name="{!! $value['name'] !!}" id="{!! $value['name'] !!}" value="{!! $value['value'] !!}" placeholder="{!! $value['placeholder'] !!}" {!! $value['extra'] !!} @if($value['slug']==1) data-slug="{!! $value['slugField'] !!}" @endif>
			    </div>
		  	</div>
		@php                  
		break; 

		case 'number':
		@endphp
			<div class="form-group">
			    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
			    <div class="controls col-md-9 col-sm-10 col-xs-12">
			      	<input class="form-control @if($value['slug']==1){!! 'slug-title' !!}@endif" type="number" name="{!! $value['name'] !!}" id="{!! $value['name'] !!}" value="{!! $value['value'] !!}" placeholder="{!! $value['placeholder'] !!}" {!! $value['extra'] !!} @if($value['slug']==1) data-slug="{!! $value['slugField'] !!}" @endif>
			    </div>
		  	</div>
		@php                  
		break; 

		case 'slug':
		@endphp
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick">* </span> {!! $value['title'] !!}</label>
				<div class="controls col-md-9 col-sm-10 col-xs-12">
					<input class="form-control slug-field" type="text" name="{!! $value['name'] !!}" id="{!! $value['name'] !!}" value="{!! $value['value'] !!}">
				</div>
			</div>
		@php                  
		break; 

		case 'password':
		@endphp
			<div class="form-group">
			    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
			    <div class="controls col-md-9 col-sm-10 col-xs-12">
			      	<input class="form-control" type="password" name="{!! $value['name'] !!}" id="{!! $value['name'] !!}" value="{!! $value['value'] !!}" placeholder="{!! $value['placeholder'] !!}">
			    </div>
		  	</div>
		@php                  
		break;

		case 'passwordGenerate':
		@endphp
			<div class="form-group">
			    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
			    <div class="controls col-md-9 col-sm-10 col-xs-12">
			    	<div class="input-group">
						<input type="password" class="form-control" name="{!! $value['name'] !!}" id="{!! $value['name'] !!}" value="{!! $value['value'] !!}" placeholder="{!! $value['placeholder'] !!}">
						<span class="input-group-btn">
							<button type="button" class="btn btn-primary">Tạo tự động</button>
						</span>
					</div>
					<div id="strength">
			    		<span class="result"></span>
				    	<span class="str-box box1"><div></div></span>
				      	<span class="str-box box2"><div></div></span>
				      	<span class="str-box box3"><div></div></span>
				      	<span class="str-box box4"><div></div></span>
				      	<span class="str-box box5"><div></div></span>
			    	</div>
			    </div>
		  	</div>
		  	<script type="text/javascript">
		  		jQuery(document).ready(function($){
		  			$('#{!! $value['name'] !!}').closest('.input-group').find('.btn').on('click',function(){
	  					var pwd = password_generator();
	  					var pwd_value = prompt("Mật khẩu đã được tạo tự động, hãy sao chép và lưu lại!", pwd);
	  					if (pwd_value != null) {
						    $('#{!! $value['name'] !!}').val(pwd_value);
						    if($('#{!! $value['confirm'] !!}').length) {
						    	$('#{!! $value['confirm'] !!}').val(pwd_value);
						    }
						    var strength = password_strength(pwd_value);
				        	show_password_strength(strength);
					  	}
		  			});
		  			$('#{!! $value['name'] !!}').keyup(function(){
				    	$('#strength .result').html('');
				        var strength = password_strength($(this).val());
				        show_password_strength(strength);
				    });
				    function show_password_strength(strength) {
				    	var strength = (strength)?(strength):(0);
				    	switch(strength) {
						  	case 1:
						    	$('#strength').removeClass().addClass('strength1');
						    	$('#strength .result').html('Rất yếu');
						    	break;
						  	case 2:
						    	$('#strength').removeClass().addClass('strength2');
						    	$('#strength .result').html('Yếu');
						    	break;
						  	case 3:
						    	$('#strength').removeClass().addClass('strength3');
						    	$('#strength .result').html('Bình thường');
						    	break;
						  	case 4:
						    	$('#strength').removeClass().addClass('strength4');
						    	$('#strength .result').html('Tốt');
						    	break;
						  	case 5:
						    	$('#strength').removeClass().addClass('strength5');
						    	$('#strength .result').html('Mạnh');
						    	break;
						  	default:
						    	$('#strength').removeClass();
						}
				    }
		  		});
		  	</script>
		@php                  
		break;  

		case 'image':
		@endphp
			<div class="form-group">
	            <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
	            <div class="controls col-md-9 col-sm-10 col-xs-12">
	                <a class="btn btn-primary" href="javascript:;" onclick="media_popup('add','single','{!!$value['name']!!}','{!! $value['title_btn'] !!}');">{!! $value['title'] !!}</a>
	                <div class="clear"></div>
	                <input id="{!! $value['name'] !!}" type="hidden" name="{!! $value['name'] !!}" value="{!!$value['value']!!}">
	                <img src="{!!$value['value']!!}" class="thumb-img" style="width: 200px;">
	                @if($value['helper_text'] != '')<p class="help-block">{!! $value['helper_text'] !!}</p>@endif
	            </div>
	        </div>
    	@php                  
		break;

		case 'slide':
		@endphp
		<div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
			<div class="controls col-md-9 col-sm-10 col-xs-12">
				<div id="{!! $value['name'] !!}" class="slide-wrap slide-check">
					<a href="javascript:;" onclick="media_popup('add','slide','{!!$value['name']!!}','{!! $value['title_btn'] !!}');">
						<button class="btn btn-success btn-sm" type="button"><i class="fa fa-plus-square"></i> Thêm ảnh </button>
					</a>
					<div class="clear"></div>
					@php
						$str_val = '';
						$str_id = $value['name'];
						if(!empty($value['value'])) {
							foreach($value['value'] as $v) {
								if($v != '') {
									$str_val .= '<div class="result_image_item">
												<input type="hidden" name="'.$str_id.'[]" value="'.$v.'">
												<img src="'.$v.'" alt="Không có ảnh"/>
												<a href="javascript:;" class="del_img" onclick="return media_remove_item(this);"><i class="fa fa-times"></i></a>
											</div>';
								}
							}
						}
					@endphp
					<div class="result_image">
						{!!$str_val!!}
					</div>
				</div>
				@if($value['helper_text'] != '')<p class="help-block">{!! $value['helper_text'] !!}</p>@endif
			</div>
		</div>
		@php
		break;

		case 'datepicker':
		@endphp
		<div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
			<div class="controls col-md-9 col-sm-10 col-xs-12">
				<input class="form-control datepicker" type="text" name="{!! $value['name'] !!}" id="{!! $value['name'] !!}" value="{!! $value['value'] ? date('Y-m-d',strtotime($value['value'])) : '' !!}" autocomplete="off">
			</div>
		</div>
		@php
		break;

		case 'datetimepicker':
		@endphp
		<div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
			<div class="controls col-md-9 col-sm-10 col-xs-12">
				<input class="form-control datetimepicker" type="text" name="{!! $value['name'] !!}" id="{!! $value['name'] !!}" value="{!! $value['value'] !!}" autocomplete="off">
			</div>
		</div>
		@php
		break;

    	case 'related':
		@endphp
		<div class="form-group form-relate" data-name="{!! $value['name'] !!}">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
			<div class="controls col-md-9 col-sm-10 col-xs-12">
				<input class="form-control relate-search" type="text" name="search_{!! $value['name'] !!}" id="{!! $value['name'] !!}" value="" placeholder="{!! $value['placeholder'] !!}" data-table="{!! $value['relate_table'] !!}" data-id="{!! $value['relate_id'] !!}" data-name="{!! $value['relate_name'] !!}">
				<div class="clear"></div>
				<ul class="relate-suggest"></ul>
				<div class="clear"></div>
				<div class="relate-result">
					@php
						if ($value['value'] != '') {
							$relate_list = DB::table($value['relate_table'])->whereIn($value['relate_id'],explode(',',$value['value']))->get();
							if ($relate_list->count()) {
								foreach ($relate_list as $relate) {
								$relate = (array)$relate;
					@endphp
					<p class="relate-item">
						<input type="hidden" name="{!! $value['name'] !!}[]" value="{!! $relate[$value['relate_id']] !!}">
						{!! $relate[$value['relate_name']] !!}
						<a href="javascript:;" class="relate-item-remove"><i class="fa fa-times"></i></a>
					</p>
					@php
						}
					}
				}
					@endphp
				</div>
			</div>
		</div>
		@php
		break;

    case 'select':
		@endphp
		<div class="form-group">
		    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
		    <div class="controls col-md-9 col-sm-10 col-xs-12">
			    <select name="{!! $value['name'] !!}" id="{!! $value['name'] !!}" class="form-control">
			        @foreach($value['options'] as $k => $v)
			        	@php
							$selected = '';
							if($value['value']==$k) {
								$selected = ' selected="selected"';
							}
							$disabled = '';
							if(in_array($k,$value['disabled'])) {
								$disabled = ' disabled="disabled"';
							}
			        	@endphp
			        	<option value="{!! $k !!}"{!!$selected!!}{!!$disabled!!}>{!! $v !!}</option>
			        @endforeach()
			    </select>
		    </div>
		 </div>
		@php
		break;

		case 'multi_cate':
		@endphp
		<div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
			<div class="controls col-md-9 col-sm-10 col-xs-12">
				<div id="{!! $value['name'] !!}" class="multi-cate-wrap multi-check">
				@foreach($value['options'] as $k => $v)
					@php
						$str_level = '';
						for($i=0;$i<$v['level'];$i++) {
							$str_level .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						}
					@endphp
					<label>{!! $str_level !!}<input type="checkbox" name="{!! $value['name'] !!}[]" value="{{ $v['id'] }}" @if(in_array($v['id'],$value['value'])) checked="checked" @endif > {{ $v['name'] }}</label><br>
				@endforeach()
				</div>
			</div>
		</div>
		@php
		break;

    case 'tags':
		@endphp
		<div class="form-group">
			<label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
			<div class="controls col-md-9 col-sm-10 col-xs-12">
				<input class="form-control tags" type="text" name="{!! $value['name'] !!}" id="{!! $value['name'] !!}" value="{!! $value['value'] !!}">
			</div>
		</div>
		@php
		break;

    case 'textarea':
		@endphp
		<div class="form-group">
		    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
		    <div class="controls col-md-9 col-sm-10 col-xs-12">
			    <textarea rows="6" name="{!! $value['name'] !!}" id="{!! $value['name'] !!}" class="form-control" placeholder="{!! $value['placeholder'] !!}">{!! $value['value'] !!}</textarea>
		    </div>
		 </div>
		@php                  
		break;

		case 'hidden': 
		@endphp
			<input type="hidden" name="{!! $value['name'] !!}" id="{!! $value['id'] !!}" class="form-control" value="{!! $value['value'] !!}" >
		@php                  
		break;

		case 'checkbox': 
		@endphp
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12">{!! $value['title'] !!}</label>
				<div class="controls col-md-9 col-sm-10 col-xs-12">
				<div style="position: relative;">
   					<i class="fa icon-green font-size17 mgt7 @php if($value['checked']==$value['value']){ echo 'fa-check-square-o';}else{ echo 'fa-square-o';} @endphp">
			            <input style="position: absolute;
			               top: -20%;
			               display: block;
			               height: 140%;
			               margin: 0px;
			               padding: 0px;
			               border: 0px;
			               opacity: 0;
			               background: rgb(255, 255, 255);" type="checkbox" name="{!! $value['name'] !!}" value="{!! $value['checked'] !!}" id="{!! $value['name'] !!}" @php if($value['checked']==$value['value']){ echo 'checked';} @endphp  onclick="return check_one(this)"></i>
           			</div>
           		</div>
           	</div>
		@php                  
		break;

		case 'custom':
		@endphp
			@include($value['template'])
		@php              
		break;

		case 'custommenu':
		@endphp
			@include($value['template'])
		@php              
		break;

		case 'editor':
		@endphp
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-12">@if($value['required']==1)<span class="form-asterick">* </span>@endif {!! $value['title'] !!}</label>
				<div class="controls col-md-9 col-sm-10 col-xs-12">
					<textarea id="{!! $value['name'] !!}" name="{!! $value['name'] !!}">{!! $value['value'] !!}</textarea>
					<script>
                        document.addEventListener("DOMContentLoaded", function(event) {
                                tinymce.init({
                                    path_absolute : "/",
                                    selector:'textarea[id="{!! $value['name'] !!}"]',
                                branding: false,
                                    hidden_input: false,
                                    relative_urls: false,
                                    convert_urls: false,
                                    height : 400,
                                    autosave_ask_before_unload:true,
                                    autosave_interval:'10s',
                                autosave_restore_when_empty:true,
                                    entity_encoding : "raw",
                                    fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 36pt 40pt 46pt 52pt 60pt",
                                    plugins: [
                                    "textcolor",
                                    "advlist autolink lists link image imagetools charmap print preview anchor",
                                    "searchreplace visualblocks code fullscreen",
                                    "insertdatetime media table autosave contextmenu paste wordcount"
                                ],
                                    wordcount_countregex: /[\w\u2019\x27\-\u00C0-\u1FFF]+/g,
                                    language: "vi_VN",
                                    autosave_retention:"30m",
                                    autosave_prefix: "tinymce-autosave-{path}{query}-{id}-",
                                    wordcount_cleanregex: /[0-9.(),;:!?%#$?\x27\x22_+=\\\/\-]*/g,
                                    toolbar: "insertfile undo redo table sudomedia charmap | styleselect | sizeselect | bold italic | fontselect |  fontsizeselect | forecolor " +
                                "backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent " +
                                "indent | link unlink fullscreen restoredraft filemanager",
                                    setup: function (editor) {
                                    editor.addButton('sudomedia', {
                                    text: 'Tải ảnh',
                                    icon: 'image',
                                    label:'Nhúng ảnh vào nội dung',
                                    onclick: function () {
                                        media_popup("add","tinymce","{!!$value['name']!!}","Chèn ảnh vào bài viết");
                                    }
                                });
                            },
                            file_picker_callback: function() {
                            media_popup("add","tinymce","{!!$value['name']!!}","Chèn ảnh vào bài viết");
                        }
                        });
                        });
					</script>
				</div>
			</div>
		@php                  
		break;

		case 'edit': 
		@endphp
			<div class="form-actions">
				<button type="submit" name="redirect" value="edit" class="btn btn-success">Lưu lại</button>&nbsp;
				<button type="submit" name="redirect" value="index" class="btn btn-info">Lưu lại & thoát</button>&nbsp;
				@if($value['preview'] != '')
					<a href="{!! $value['preview'] !!}" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> Xem</a>
				@endif
				<button type="button" onclick="window.location='{!! route($table_name.'.index') !!}'" class="btn btn-danger">Thoát</button>&nbsp;
			</div>
		@php                  
		break;

		case 'editconfig': 
		@endphp
			<div class="form-actions">
				<button type="submit" name="submit" id="submit-form" value="edit" class="btn btn-success">Lưu cấu hình</button>&nbsp;
			</div>
		@php                  
		break;

		case 'add': 
		@endphp
			<div class="form-actions">
			<button type="submit" name="redirect" value="edit" class="btn btn-success">Thêm mới</button>&nbsp;
			<button type="submit" name="redirect" value="index" class="btn btn-info">Thêm & thoát</button>&nbsp;
			<button type="reset" class="btn">Nhập lại</button>&nbsp;
			<button type="button" onclick="window.location='{!! route($table_name.'.index') !!}'" class="btn btn-danger">Thoát</button>&nbsp;
		</div>	
		@php                  
		break;

		case 'start_group':
		@endphp
			<div class="alert alert-info row" data-toggle="collapse" data-target="#{!!$value['id']!!}" style="cursor: pointer">
				<div class="col-md-2 col-sm-2 col-xs-12 text-right"><b>{!!$value['name']!!}</b></div>
			</div>
			<div id="{!!$value['id']!!}" class="collapse in">
		@php
		break;

		case 'title':
		@endphp
			<span class="section" style="margin-top:30px;float: left;clear: both;">{{ $value['title'] }}</span>
		@php
		break;

		case 'end_group':
		@endphp
			</div>
		@php
		break;
}
}
@endphp