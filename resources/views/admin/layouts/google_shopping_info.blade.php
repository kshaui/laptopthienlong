<div class="form-group">
	<label class="control-label col-md-2 col-sm-2 col-xs-12"> Hướng dẫn</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
  		<p>- Link chạy cronjob datafeed: <input class="form-control" type="text" name="" disabled="disabled" value="{{config('app.url')}}/datafeeds" /></p>
  		<p>- Link kết quả datafeed: <input class="form-control" type="text" name="" disabled="disabled" value="{{config('app.url')}}/uploads/products.txt" /></p>
  		<p>- Cấu hình ở trang này là cấu hình cho toàn trang</p>
  		<p>- Cấu hình ở trang này sẽ bị ghi đè nếu ở trang danh mục có cấu hình</p>
  		<p>- Cấu hình ở trang danh mục sẽ bị ghi đè nếu ở trang chi tiết có cấu hình</p>
	</div>
</div>