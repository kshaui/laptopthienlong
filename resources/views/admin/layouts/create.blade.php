@extends('admin.layouts.app')
@section('title')
    <h3>Thêm mới {{$module_name}}</h3>
@endsection()
@section('title2')
    <h2>Những trường đánh dấu (<span style="color:red;">*</span>) là bắt buộc nhập</h2>
@endsection()
@section('content')
    @include('errors.error')

    @php
    $array_valid = [];
    foreach($data_form as $value) {
        if(isset($value['required']) && $value['required'] == 1) {
            $array_valid[] = $value['name'];
        }
    }
    $string_valid = '';
    if(count($array_valid) > 0) {
        $string_valid = 'onsubmit="validForm(this,\''.implode(',',$array_valid).'\');return false;"';
    }
    @endphp

    <form action="{!! route($table_name.'.store') !!}" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" {!! $string_valid !!}>

        @include('admin.layouts.form')
        @if($has_seo)
            @php
            $meta_seo_type = $table_name;
            $meta_seo_type_id = 0;
            @endphp
            @include('admin.layouts.metaseo')
        @endif
        @if($has_google_shopping)
            @php
            $google_shopping_type = $table_name;
            $google_shopping_type_id = 0;
            @endphp
            @include('admin.layouts.google_shopping')
        @endif
    </form>

@endsection()
@section('script')
    <script>
        $(document).ready(function(){
            $('.slug-title').on("change",function(){
                var field = $(this).attr('data-slug');
                var title = $(this).val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    type:'post',
                    url:'/admin_1996/ajax/get-slug',
                    data:{'_token':token,title:title},
                    success:function(html){
                        $('#'+field).val(html);
                    }
                })
            });
        });
    </script>
@endsection()

