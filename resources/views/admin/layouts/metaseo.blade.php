<div class="wrap_seometa">
    <h2>Nội dung cho Seo</h2>
    @php
    $meta_seo_title = '';
    $meta_seo_description = '';
    $meta_seo_robots = 'Index,Follow';
    if($meta_seo_type != '' && $meta_seo_type_id != 0) {
        $meta_seo = DB::table('meta_seo')->where('type',$meta_seo_type)->where('type_id',$meta_seo_type_id)->first();
        if($meta_seo) {
            $meta_seo_title = $meta_seo->title;
            $meta_seo_description = $meta_seo->description;
            $meta_seo_robots = $meta_seo->robots;
        }
    }
    @endphp
    <div class="preview_snippet">
        <h4>Xem trước hiển thị tìm kiếm trên Google</h4>
        <div class="preview_snippet_main">
            <h3 class="preview_snippet_title">{{$meta_seo_title}}</h3>
            <p class="preview_snippet_link">http://{{$_SERVER['HTTP_HOST']}}/<span></span>.html &nbsp;&nbsp;&nbsp; <a class="btn btn-xs btn-warning" href="#slug">Sửa đường dẫn</a></p>
            <p class="preview_snippet_des">{{$meta_seo_description}}</p>
        </div>
    </div>
    <div class="form-group">
        <label for="{{$meta_seo_type}}_title" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Tiêu đề cho thẻ meta title (SEO)</label>
        <div class="controls  col-sm-9">
            <input type="text" placeholder="" class="form-control col-sm-9 in_title" id="{{$meta_seo_type}}_title" value="{{$meta_seo_title}}" name="seo_title">
            <span class="in_title_count">{{strlen(utf8_decode($meta_seo_title))}}</span> Ký tự. Tiêu đề (title) tốt nhất khoảng 60 - 70 ký tự
        </div>
    </div>
    <div class="form-group">
        <label for="{{$meta_seo_type}}_description" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Đoạn mô tả cho thẻ meta description (SEO)</label>
        <div class="controls col-sm-9">
            <textarea placeholder="" class="span6 in_des" id="{{$meta_seo_type}}_description" name="seo_description" style="width:100%;height:60px">{{$meta_seo_description}}</textarea>
            <span class="in_des_count">{{strlen(utf8_decode($meta_seo_description))}}</span> Ký tự. Mô tả (description) tốt nhất khoảng 120 - 160 ký tự
        </div>
    </div>
    <div class="form-group">
        <label for="{{$meta_seo_type}}_robots" class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Điều hướng Robots</label>
        <div class="controls col-sm-3">
            <select id="{{$meta_seo_type}}_robots" name="seo_robots" class="form-control">
                <option value="Index,Follow"{!!($meta_seo_robots == 'Index,Follow' ? ' selected="selected"' : '')!!}>Index,Follow</option>
                <option value="Noindex,Nofollow"{!!($meta_seo_robots == 'Noindex,Nofollow' ? ' selected="selected"' : '')!!}>Noindex,Nofollow</option>
                <option value="Index,Nofollow"{!!($meta_seo_robots == 'Index,Nofollow' ? ' selected="selected"' : '')!!}>Index,Nofollow</option>
                <option value="Noindex,Follow"{!!($meta_seo_robots == 'Noindex,Follow' ? ' selected="selected"' : '')!!}>Noindex,Follow</option>
            </select>
        </div>
    </div>
</div>