<hr>
@php
$google_shopping_brand = '';
$google_shopping_category = '';
$google_shopping_instock = '';
$google_shopping_itemcondition = '';
if($google_shopping_type != '' && $google_shopping_type_id != 0) {
    $google_shopping = DB::table('google_shopping')->where('type',$google_shopping_type)->where('type_id',$google_shopping_type_id)->first();
    if($google_shopping) {
        $google_shopping_brand = $google_shopping->brand;
        $google_shopping_category = $google_shopping->category;
        $google_shopping_instock = $google_shopping->instock;
        $google_shopping_itemcondition = $google_shopping->itemcondition;
    }
}
@endphp
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          	<div class="x_title">
            	<h2>Cấu hình Google Shopping</h2>
            	<ul class="nav navbar-right panel_toolbox">
          			<li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
            	</ul>
            	<div class="clearfix"></div>
          	</div>
          	<div class="x_content">
				<div class="form-group">
	                <label class="col-sm-3 control-label">Thương hiệu</label>
	                <div class="col-sm-9">
	                	<input class="form-control" type="text" name="google_shopping_brand" value="{{$google_shopping_brand}}" placeholder="VD: Apple">
	                </div>
              	</div>	
				<div class="form-group">
	                <label class="col-sm-3 control-label">Danh mục google</label>
	                <div class="col-sm-9">
	                	<input class="form-control" type="text" name="google_shopping_category" value="{{$google_shopping_category}}" placeholder="VD: Electronics > Communications > Telephony > Mobile Phone Accessories > Mobile Phone Replacement Parts">
	                </div>
              	</div>	
				<div class="form-group">
	                <label class="col-sm-3 control-label">Tình trạng kho hàng</label>
	                <div class="col-sm-9">
	                	<select name="google_shopping_instock" class="form-control">
			                <option value=""{!!($google_shopping_instock == '' ? ' selected="selected"' : '')!!}>Mặc định</option>
			                <option value="còn hàng"{!!($google_shopping_instock == 'còn hàng' ? ' selected="selected"' : '')!!}>còn hàng</option>
			                <option value="hết hàng"{!!($google_shopping_instock == 'hết hàng' ? ' selected="selected"' : '')!!}>hết hàng</option>
			            </select>
	                </div>
              	</div>	
				<div class="form-group">
	                <label class="col-sm-3 control-label">Tình trạng sản phẩm</label>
	                <div class="col-sm-9">
	                	<select name="google_shopping_itemcondition" class="form-control">
			                <option value=""{!!($google_shopping_itemcondition == '' ? ' selected="selected"' : '')!!}>Mặc định</option>
			                <option value="mới"{!!($google_shopping_itemcondition == 'mới' ? ' selected="selected"' : '')!!}>mới</option>
			                <option value="cũ"{!!($google_shopping_itemcondition == 'cũ' ? ' selected="selected"' : '')!!}>cũ</option>
			            </select>
	                </div>
              	</div>			
          	</div>
        </div>
      </div>
</div>