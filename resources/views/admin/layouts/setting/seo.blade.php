
<div class="alert alert-info" data-toggle="collapse" data-target="#seo_fit">
	<strong>Nội Dung Seo cho laptop</strong>
</div>
<div id="seo_fit" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Mô tả danh mục</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<textarea id="text_fit" name="detail_fit" aria-hidden="true" style="">{!!$list_seo["detail_fit"]!!}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Tiêu đề cho thẻ meta title</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<textarea id="" name="title_fit" aria-hidden="true" style="" class="form-control">{!!$list_seo["title_fit"]!!}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Đoạn mô tả cho thẻ meta description</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<textarea id="" name="desc_fit" aria-hidden="true" style="" class="form-control" rows="5">{!!$list_seo["desc_fit"]!!}</textarea>
		</div>
	</div>
</div>
<div class="alert alert-info" data-toggle="collapse" data-target="#seo_service">
	<strong>Nội Dung Seo cho Sửa chữa</strong>
</div>
<div id="seo_service" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Mô tả danh mục</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<textarea id="text_service" name="detail_service" aria-hidden="true" style="">{!!$list_seo["detail_service"]!!}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Tiêu đề cho thẻ meta title</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<textarea id="" name="title_service" aria-hidden="true" style="" class="form-control">{!!$list_seo["title_service"]!!}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Đoạn mô tả cho thẻ meta description</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<textarea id="" name="desc_service" aria-hidden="true" style="" class="form-control" rows="5">{!!$list_seo["desc_service"]!!}</textarea>
		</div>
	</div>
</div>



<div class="alert alert-info" data-toggle="collapse" data-target="#seo_news">
	<strong>Nội Dung Seo cho Tin tức</strong>
</div>
<div id="seo_news" class="collapse" style="height: auto;">
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Mô tả danh mục</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<textarea id="text_news" name="detail_news" aria-hidden="true" style="">{!!$list_seo["detail_news"]!!}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Tiêu đề cho thẻ meta title</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<textarea id="" name="title_news" aria-hidden="true" style="" class="form-control">{!!$list_seo["title_news"]!!}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"> Đoạn mô tả cho thẻ meta description</label>
		<div class="controls col-md-9 col-sm-10 col-xs-12">
			<textarea id="" name="desc_news" aria-hidden="true" style="" class="form-control" rows="5">{!!$list_seo["desc_news"]!!}</textarea>
		</div>
	</div>
</div>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
tinymce.init({
path_absolute : "/",
selector:'textarea[id="text_phone"]',
branding: false,
hidden_input: false,
relative_urls: false,
convert_urls: false,
height : 400,
autosave_ask_before_unload:true,
autosave_interval:'10s',
autosave_restore_when_empty:true,
entity_encoding : "raw",
fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 36pt 40pt 46pt 52pt 60pt",
plugins: [
"textcolor",
"advlist autolink lists link image imagetools charmap print preview anchor",
"searchreplace visualblocks code fullscreen",
"insertdatetime media table autosave contextmenu paste wordcount"
],
wordcount_countregex: /[\w\u2019\x27\-\u00C0-\u1FFF]+/g,
language: "vi_VN",
autosave_retention:"30m",
autosave_prefix: "tinymce-autosave-{path}{query}-{id}-",
wordcount_cleanregex: /[0-9.(),;:!?%#$?\x27\x22_+=\\\/\-]*/g,
toolbar: "insertfile undo redo table sudomedia charmap | styleselect | sizeselect | bold italic | fontselect |  fontsizeselect | forecolor " +
"backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent " +
"indent | link unlink fullscreen restoredraft filemanager",
setup: function (editor) {
editor.addButton('sudomedia', {
text: 'Tải ảnh',
icon: 'image',
label:'Nhúng ảnh vào nội dung',
onclick: function () {
media_popup("add","tinymce","text_phone","Chèn ảnh vào bài viết");
}
});
},
file_picker_callback: function() {
media_popup("add","tinymce","text_phone","Chèn ảnh vào bài viết");
}
});
});
document.addEventListener("DOMContentLoaded", function(event) {
tinymce.init({
path_absolute : "/",
selector:'textarea[id="text_tablet"]',
branding: false,
hidden_input: false,
relative_urls: false,
convert_urls: false,
height : 400,
autosave_ask_before_unload:true,
autosave_interval:'10s',
autosave_restore_when_empty:true,
entity_encoding : "raw",
fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 36pt 40pt 46pt 52pt 60pt",
plugins: [
"textcolor",
"advlist autolink lists link image imagetools charmap print preview anchor",
"searchreplace visualblocks code fullscreen",
"insertdatetime media table autosave contextmenu paste wordcount"
],
wordcount_countregex: /[\w\u2019\x27\-\u00C0-\u1FFF]+/g,
language: "vi_VN",
autosave_retention:"30m",
autosave_prefix: "tinymce-autosave-{path}{query}-{id}-",
wordcount_cleanregex: /[0-9.(),;:!?%#$?\x27\x22_+=\\\/\-]*/g,
toolbar: "insertfile undo redo table sudomedia charmap | styleselect | sizeselect | bold italic | fontselect |  fontsizeselect | forecolor " +
"backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent " +
"indent | link unlink fullscreen restoredraft filemanager",
setup: function (editor) {
editor.addButton('sudomedia', {
text: 'Tải ảnh',
icon: 'image',
label:'Nhúng ảnh vào nội dung',
onclick: function () {
media_popup("add","tinymce","text_tablet","Chèn ảnh vào bài viết");
}
});
},
file_picker_callback: function() {
media_popup("add","tinymce","text_tablet","Chèn ảnh vào bài viết");
}
});
});
document.addEventListener("DOMContentLoaded", function(event) {
tinymce.init({
path_absolute : "/",
selector:'textarea[id="text_fit"]',
branding: false,
hidden_input: false,
relative_urls: false,
convert_urls: false,
height : 400,
autosave_ask_before_unload:true,
autosave_interval:'10s',
autosave_restore_when_empty:true,
entity_encoding : "raw",
fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 36pt 40pt 46pt 52pt 60pt",
plugins: [
"textcolor",
"advlist autolink lists link image imagetools charmap print preview anchor",
"searchreplace visualblocks code fullscreen",
"insertdatetime media table autosave contextmenu paste wordcount"
],
wordcount_countregex: /[\w\u2019\x27\-\u00C0-\u1FFF]+/g,
language: "vi_VN",
autosave_retention:"30m",
autosave_prefix: "tinymce-autosave-{path}{query}-{id}-",
wordcount_cleanregex: /[0-9.(),;:!?%#$?\x27\x22_+=\\\/\-]*/g,
toolbar: "insertfile undo redo table sudomedia charmap | styleselect | sizeselect | bold italic | fontselect |  fontsizeselect | forecolor " +
"backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent " +
"indent | link unlink fullscreen restoredraft filemanager",
setup: function (editor) {
editor.addButton('sudomedia', {
text: 'Tải ảnh',
icon: 'image',
label:'Nhúng ảnh vào nội dung',
onclick: function () {
media_popup("add","tinymce","text_fit","Chèn ảnh vào bài viết");
}
});
},
file_picker_callback: function() {
media_popup("add","tinymce","text_fit","Chèn ảnh vào bài viết");
}
});
});
document.addEventListener("DOMContentLoaded", function(event) {
tinymce.init({
path_absolute : "/",
selector:'textarea[id="text_service"]',
branding: false,
hidden_input: false,
relative_urls: false,
convert_urls: false,
height : 400,
autosave_ask_before_unload:true,
autosave_interval:'10s',
autosave_restore_when_empty:true,
entity_encoding : "raw",
fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 36pt 40pt 46pt 52pt 60pt",
plugins: [
"textcolor",
"advlist autolink lists link image imagetools charmap print preview anchor",
"searchreplace visualblocks code fullscreen",
"insertdatetime media table autosave contextmenu paste wordcount"
],
wordcount_countregex: /[\w\u2019\x27\-\u00C0-\u1FFF]+/g,
language: "vi_VN",
autosave_retention:"30m",
autosave_prefix: "tinymce-autosave-{path}{query}-{id}-",
wordcount_cleanregex: /[0-9.(),;:!?%#$?\x27\x22_+=\\\/\-]*/g,
toolbar: "insertfile undo redo table sudomedia charmap | styleselect | sizeselect | bold italic | fontselect |  fontsizeselect | forecolor " +
"backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent " +
"indent | link unlink fullscreen restoredraft filemanager",
setup: function (editor) {
editor.addButton('sudomedia', {
text: 'Tải ảnh',
icon: 'image',
label:'Nhúng ảnh vào nội dung',
onclick: function () {
media_popup("add","tinymce","text_service","Chèn ảnh vào bài viết");
}
});
},
file_picker_callback: function() {
media_popup("add","tinymce","text_service","Chèn ảnh vào bài viết");
}
});
});
document.addEventListener("DOMContentLoaded", function(event) {
tinymce.init({
path_absolute : "/",
selector:'textarea[id="text_news"]',
branding: false,
hidden_input: false,
relative_urls: false,
convert_urls: false,
height : 400,
autosave_ask_before_unload:true,
autosave_interval:'10s',
autosave_restore_when_empty:true,
entity_encoding : "raw",
fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 36pt 40pt 46pt 52pt 60pt",
plugins: [
"textcolor",
"advlist autolink lists link image imagetools charmap print preview anchor",
"searchreplace visualblocks code fullscreen",
"insertdatetime media table autosave contextmenu paste wordcount"
],
wordcount_countregex: /[\w\u2019\x27\-\u00C0-\u1FFF]+/g,
language: "vi_VN",
autosave_retention:"30m",
autosave_prefix: "tinymce-autosave-{path}{query}-{id}-",
wordcount_cleanregex: /[0-9.(),;:!?%#$?\x27\x22_+=\\\/\-]*/g,
toolbar: "insertfile undo redo table sudomedia charmap | styleselect | sizeselect | bold italic | fontselect |  fontsizeselect | forecolor " +
"backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent " +
"indent | link unlink fullscreen restoredraft filemanager",
setup: function (editor) {
editor.addButton('sudomedia', {
text: 'Tải ảnh',
icon: 'image',
label:'Nhúng ảnh vào nội dung',
onclick: function () {
media_popup("add","tinymce","text_news","Chèn ảnh vào bài viết");
}
});
},
file_picker_callback: function() {
media_popup("add","tinymce","text_news","Chèn ảnh vào bài viết");
}
});
});
</script>