@extends('admin.layouts.app')
@php
    extract($data,EXTR_OVERWRITE);
    /*
    $table = $data['table'];
    $show_data = $data['show_data'];;
    $is_search = $data['is_search'];
    $id_name = $data['id_name'];
    $field = $data['field'];
    $label = $data['label'];
    $type = $data['type'];
    $search = $data['search'];
    $search_option = $data['search_option'];
    $search_option_value = $data['search_option_value'];
    $total_record = $data['total_record'];
    $page_size = $data['page_size'];*/
@endphp
@section('title')
    <h3>Danh sách {{$module_name}}</h3>
@endsection()
@section('title2')
    <div class="row">
        <div class="col-sm-8 col-md-10">
        @if(count($search) > 0)
            <div id="search">
                <form class="form-inline" action="" method="get" accept-charset="utf-8">
                    <input type="hidden" name="search" value="1">
                    @php
                    foreach($search as $key=>$value) {
                        $search_field_name = $field[$key];
                        switch ($type[$key]) {
                            case 'string':
                                @endphp
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" name="{!! $value !!}" placeholder="{!! $label[$key] !!}" value="{!! $search_option_value[$value] !!}"/>
                                </div>
                                @php
                                break;
                            case 'array':
                                @endphp
                                <div class="form-group">
                                    <select class="form-control input-sm" name="{!! $value !!}">
                                        <option value="-1">Tất cả {!! $label[$key] !!}</option>
                                        @php
                                            foreach ($search_option[$key] as $k=>$v) {
                                                $selected = (($search_option_value[$value] != null && $k == $search_option_value[$value]) ? 'selected="selected"' : '');
                                        @endphp
                                        <option value="{!! $k !!}" {!! $selected !!}>{!! $v !!}</option>
                                        @php
                                            }
                                        @endphp
                                    </select>
                                </div>
                                @php
                                break;
                            case 'status':
                                @endphp
                                <div class="form-group">
                                    <select class="form-control input-sm" name="{!! $value !!}">
                                        <option value="-1">{!! $label[$key] !!}</option>
                                        @php
                                            foreach ($search_option[$key] as $k=>$v) {
                                                $selected = (($search_option_value[$value] != null && $k == $search_option_value[$value]) ? 'selected="selected"' : '');
                                        @endphp
                                        <option value="{!! $k !!}" {!! $selected !!}>{!! $v !!}</option>
                                        @php
                                            }
                                        @endphp
                                    </select>
                                </div>
                                @php
                                break;
                            case 'logs':
                                @endphp
                                <div class="form-group">
                                    <select class="form-control input-sm" name="{!! $value !!}">
                                        <option value="-1">Tất cả {!! $label[$key] !!}</option>
                                        @php
                                            foreach ($search_option[$key] as $k=>$v) {
                                                $selected = (($search_option_value[$value] != null && $k == $search_option_value[$value]) ? 'selected="selected"' : '');
                                        @endphp
                                        <option value="{!! $k !!}" {!! $selected !!}>{!! $v !!}</option>
                                        @php
                                            }
                                        @endphp
                                    </select>
                                </div>
                                @php
                                break;
                            case 'range':
                                @endphp
                                <div class="form-group">
                                    <div id="{!! $value !!}" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span> <i class="fa fa-caret-down"></i>
                                        <input id="{!! $value !!}_start" type="hidden" name="{!! $value !!}_start" value="">
                                        <input id="{!! $value !!}_end" type="hidden" name="{!! $value !!}_end" value="">
                                    </div>
                                    <script type="text/javascript">
                                    $(function() {
                                        var start = moment('{{isset($search_option_value[$value]['start']) ? $search_option_value[$value]['start'] : '1970-01-01'}}');
                                        var end = moment('{{isset($search_option_value[$value]['end']) ? $search_option_value[$value]['end'] : ''}}');
                                        function cb(start, end) {
                                            if(start.format('DD/MM/YYYY') == '01/01/1970') {
                                                $('#{!! $value !!} span').html('Tất cả {!! $label[$key] !!}');
                                                $('#{!! $value !!}_start').val('');
                                                $('#{!! $value !!}_end').val('');
                                            }else {
                                                $('#{!! $value !!} span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                                                $('#{!! $value !!}_start').val(start.format('YYYY-MM-DD HH:mm:ss'));
                                                $('#{!! $value !!}_end').val(end.format('YYYY-MM-DD HH:mm:ss'));
                                            }
                                        }
                                        $('#{!! $value !!}').daterangepicker({
                                            startDate: start,
                                            endDate: end,
                                            timePicker: true,
                                            timePicker24Hour: true,
                                            timePickerSeconds: true,
                                            ranges: {
                                               'Tất cả': [moment('1970-01-01'), moment().endOf('day')],
                                               'Hôm nay': [moment().startOf('day'), moment().endOf('day')],
                                               'Hôm qua': [moment().startOf('day').subtract(1, 'days'), moment().endOf('day').subtract(1, 'days')],
                                               '7 ngày qua': [moment().startOf('day').subtract(6, 'days'), moment()],
                                               '30 ngày qua': [moment().startOf('day').subtract(29, 'days'), moment()],
                                               'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                                               'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                            },
                                            locale: {
                                                applyLabel: "Chọn",
                                                cancelLabel: "Xóa",
                                                fromLabel: "Từ",
                                                toLabel: "Đến",
                                                customRangeLabel: "Tùy chọn",
                                                daysOfWeek: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                                                monthNames: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
                                                firstDay: 1
                                            }
                                        }, cb);

                                        cb(start, end);

                                    });
                                    </script>
                                </div>
                                @php
                                break;
                        }
                    }
                    @endphp
                                
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <button type="submit" class="btn btn-primary btn-sm">Tìm kiếm</button>
                    </div>
                </form>
            </div>
        @endif
        </div>
        <div class="col-sm-4 col-md-2 text-right">
        @if($table_name != 'system_logs')
            <a href="{!! route($table.'.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Thêm mới {{$module_name}}</a>
        @endif
        </div>
    </div>
@endsection()
@section('content')
    @include('errors.alert')
    <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
        <thead>
            <tr id="0">
                <th class="center">STT</th>
                <th class="center well">
                    <div class="" style="position: relative;">
                        <i class="fa icon-green fa-square-o font-size17 " style="position: relative;">
                            <input style="position: absolute;
                        top: -20%;
                        display: block;
                        z-index: 999;
                        width: 140%;
                        height: 140%;
                        margin: 0px;
                        padding: 0px;
                        border: 0px;
                        opacity: 0;
                        background: rgb(255, 255, 255);" type="checkbox" id="check_all" class="check" onclick="return check_all()">
                        </i>
                    </div>
                </th>
                <th class="center no-sorting"><i onclick="return save_all('{!! $table !!}')" style="cursor: pointer;" class="fa fa-save pointer icon-green font-size17"></i></th>
                @php
                foreach($label as $key=>$label){
                    echo '<th class="center">'.$label.'</th>';
                }
                @endphp
            </tr>
        </thead>
        <tbody>
        @foreach($show_data as $key => $value)
            <tr class="record-data" id="record-{!! $value->id !!}" data-id="{!! $value->id !!}">
                <td class="center">{!! $key + 1 !!}</td>
                <td class="center well">
                    <div class="" style="position: relative;">
                        <i class="fa icon-green fa-square-o font-size17 fa_{!! $value->id !!}" style="position: relative;">
                            <input style="position: absolute;
						top: -20%;
						display: block;
						z-index: 999;
						width: 140%;
						height: 140%;
						margin: 0px;
						padding: 0px;
						border: 0px;
						opacity: 0;
						background: rgb(255, 255, 255);" type="checkbox" name="" id="id_{!! $value->id !!}"  class="check" onclick="check_one(this)">
                        </i>
                    </div>
                </td>
                <td class="center">
                    <i onclick="return save_one({!! $value->id !!},'{!! $table !!}')" style="cursor: pointer;" class="fa fa-save pointer icon-green font-size17 fa-save-{!! $value->id !!}"></i>
                    <img style="display: none;" class="loading loading_save_{!! $value->id !!}" src="{!! url('/template-admin/images/loading.gif') !!}" alt="">
                </td>
                @include('admin.'.$table.'.index')
                @foreach($type as $k=>$t)
                    @if($t == 'pins')
                        <td class="pins-group" style="width: 100px;">
                        @php
                            $place = $field[$k];
                            $pins = DB::table('pins')->where('type',$table)->where('type_id',$value->id)->where('place',$place)->first();
                        @endphp
                        <div class="input-group">
                            <input type="number" class="form-control" min="0" value="{{isset($pins->value) ? $pins->value : '0'}}" style="width:69px;display:inline-block;"/>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-pins" data-type="{{$table}}" data-place="{{$place}}"><i class="fa fa-thumb-tack" aria-hidden="true"></i></button>
                            </span>
                        </div>
                        </td>
                    @endif
                    @if($t == 'status')
                        <td class="center">
                            @if($table == 'comments')
                                <select class="form-control quick-edit" name="status" onchange="check_edit(this)">
                                    <option value="1" @if($value->status==1){!! 'selected' !!} @endif()>Đã duyệt</option>
                                    <option value="2" @if($value->status==2){!! 'selected' !!} @endif()>Chưa duyệt</option>
                                </select>
                            @else
                                <select class="form-control quick-edit" name="status" onchange="check_edit(this)">
                                    <option value="1" @if($value->status==1){!! 'selected' !!} @endif()>Hoạt động</option>
                                    <option value="2" @if($value->status==2){!! 'selected' !!} @endif()>Không hoạt động</option>
                                    <option value="3" @if($value->status==3){!! 'selected' !!} @endif()>Thùng rác</option>
                                </select>
                            @endif
                        </td>
                    @endif
                    @if($t == 'edit')
                        <td class="center">
                            <a href="{!! route($table.'.edit', $value->id) !!}"><i class="fa fa-pencil-square-o icon-green font-size17"></i></a>
                        </td>
                    @endif
                    @if($t == 'delete')
                        <td class="center">
                            <a class="delete-record" href="javascript:;" onclick="delete_one('{!! $value->id !!}','{!! route($table_name.'.destroy',$value->id) !!}')"><i class="fa fa-trash-o icon-red font-size17"></i></a>
                        </td>
                    @endif
                @endforeach


            </tr>
        @endforeach()
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-8">
            <p style="display: inline; padding-left: 15px;">Tổng {!! $total_record !!}</p>
            <p style="cursor: pointer; padding-left: 15px;display: inline;" onclick="deactive_all('{!! $table !!}')"><i class="fa fa-ban icon-red font-size17"  aria-hidden="true"></i>Không hoạt động</p>
            <p style="cursor: pointer;display: inline; padding-left: 15px;" onclick="trash_all('{!! $table !!}')"><i class="fa fa-trash-o font-size17"  aria-hidden="true"></i>Chuyển vào thùng rác</p>
            {{--
            <p style="cursor: pointer; display: inline; padding-left: 15px;" onclick="delete_all('{!! $table !!}')"><i class="fa fa-trash-o icon-red font-size17"></i>Xóa vĩnh viễn</p>
            --}}
        </div>
        <div class="col-sm-4">
            {!! admin_paging($page,$page_size,$total_record,$prefix_link,'') !!}
        </div>
    </div>
@endsection()
@section('script')
    <script src="/assets/js/popup.js"></script>
    <script>
        function show_sale(id){
            if($('.show-sale-'+id).prop('checked')){
                var value=1;
            }else{
                var value=0;
            }
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {value: value,id:id},
                url: '/admin_1996/ajax/label-sale',
                success: function (data) {
                    $('.show-sale-'+id).css('display','inline');
                    $('.img-show-sale-'+id).css('display','none');
                },beforeSend:function(){
                    $('.show-sale-'+id).css('display','none');
                    $('.img-show-sale-'+id).css('display','block');
                }
            });
        }

         function ban_chay(id){
            if($('.ban-chay-'+id).prop('checked')){
                var value=1;
            }else{
                var value=0;
            }
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {value: value,id:id},
                url: '/admin_1996/ajax/ban-chay',
                success: function (data) {
                    $('.ban-chay-'+id).css('display','inline');
                    $('.img-ban-chay-'+id).css('display','none');
                },beforeSend:function(){
                    $('.ban-chay-'+id).css('display','none');
                    $('.img-ban-chay-'+id).css('display','block');
                }
            });
        }
    </script>
@endsection