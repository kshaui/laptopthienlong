<span class="section" style="margin-top:30px;width: 100%;float: left;clear: both;">{!! $value['title'] !!}</span>
<div id="{!! $value['name'] !!}" class="clearfix" style="margin-bottom: 20px;">
<div class="action-menu">
	<div class="custom-link option-menu" onclick="toggle_menu('custom-link', '{!! $value['name'] !!}')">
		<p>Liên kết tự tạo <i class="fa fa-caret-down" aria-hidden="true"></i></p>
	</div>
	<div class="add-menu add-menu-custom-link">
    	<h5>Thêm mới menu</h5>
    	<div class="form-group">
    		<label>Tên menu</label>
    		<input type="text" class="name-menu">
    	</div>
    	<div class="form-group">
    		<label>Link</label>
    		<input type="text" class="link-menu">
    	</div>
    	<div class="form-group">
    		<label>Mở tab mới</label>
    		<input style="height: 15px;width: 20px;" type="checkbox" class="taget-menu">
    	</div>
    	<div class="form-group">
    		<label>Nofollow</label>
    		<input style="height: 15px;width: 20px;" type="checkbox" class="rel-menu">
    	</div>
		<p class="add">
			<img class="img1" src="/template-admin/images/spinner.gif" alt="">
			<a href="javascript:;" onclick="add_menu('{!! $value['name'] !!}');">Thêm mới</a>
		</p>
	</div>
	@foreach($value['data_table'] as $k => $v)
		@php
			$data_select = DB::table($v)->where('status',1)->get();
		@endphp
		<div class="{!! $v !!} option-menu" onclick="toggle_menu('{!! $v !!}', '{!! $value['name'] !!}')">
			<p>{!! config('modules.name')[$v] !!}<i class="fa fa-caret-down" aria-hidden="true"></i></p>
		</div>
		<div class="add-menu add-menu-{!! $v !!}">
	    	<h5>Thêm mới menu</h5>
	    	<div class="form-group">
	    		<label>Tên menu</label>
	    		<input type="text" class="name-menu-{!! $v !!}">
	    	</div>
	    	<div class="form-group">
	    		<label>Đường dẫn đến</label>
	    		<select class="link-menu-{!! $v !!}" id="">
		    		@foreach($data_select as $item)
		    			<option value="{!! $item->id !!}" data-slug="{!! route('web.'.$v.'.show',$item->slug) !!}">{!! $item->name !!}</option>
		    		@endforeach
	    		</select>
	    	</div>
	    	<div class="form-group">
	    		<label>Mở tab mới</label>
	    		<input style="height: 15px;width: 20px;" type="checkbox" class="taget-menu-{!! $v !!}">
	    	</div>
	    	<div class="form-group">
	    		<label>Nofollow</label>
	    		<input style="height: 15px;width: 20px;" type="checkbox" class="rel-menu-{!! $v !!}">
	    	</div>
			<p class="add">
				<img class="img1" src="/template-admin/images/spinner.gif" alt="">
				<a href="javascript:;" onclick="add_menu1('{!! $value['name'] !!}','{!! $v !!}');">Thêm mới</a>
			</p>
		</div>
	@endforeach
	@php
		$list_fix_link = config('app.fix_link');
	@endphp
	<div class="custom-link option-menu" onclick="toggle_menu('fix-link', '{!! $value['name'] !!}')">
		<p>Liên kết cố định <i class="fa fa-caret-down" aria-hidden="true"></i></p>
	</div>
	<div class="add-menu add-menu-fix-link">
		<h5>Thêm mới menu</h5>
    	<div class="form-group">
    		<label>Tên menu</label>
    		<input type="text" class="name-menu">
    	</div>
    	<div class="form-group">
    		<label>Đường dẫn đến</label>
    		<select class="link-menu id="">
	    		@foreach($list_fix_link as $k => $v)
		    		<option value="{!! $k !!}" data-slug="{!! $k !!}">{!! $v !!}</option>
		    	@endforeach
    		</select>
    	</div>
    	<div class="form-group">
    		<label>Mở tab mới</label>
    		<input style="height: 15px;width: 20px;" type="checkbox" class="taget-menu">
    	</div>
    	<div class="form-group">
    		<label>Nofollow</label>
    		<input style="height: 15px;width: 20px;" type="checkbox" class="rel-menu">
    	</div>
		<p class="add">
			<img class="img1" src="/template-admin/images/spinner.gif" alt="">
			<a href="javascript:;" onclick="add_menu2('{!! $value['name'] !!}');">Thêm mới</a>
		</p>
	</div>
</div>
<input type="hidden" name="{!! $value['name'] !!}" class="{!! $value['name'] !!}" value="">
<div class="manager-menu">
	@php
		$data_menu = json_decode($value['value'], 1);
	@endphp
	<h5>Cây menu</h5>
	<div class="content-menu">
		<div class="dd" id="nestable-{!! $value['name'] !!}">
			@if(!empty($value['value']))
				@if($value['value'] == '[]')
					<ol class="dd-list" id="ol-first">
					</ol>
				@else
					{!! recursive_setting_menu($data_menu,0, $value['name'],true) !!}
				@endif
			@else
				<ol class="dd-list" id="ol-first">
				</ol>
			@endif
		</div>
	</div>
</div>
</div>
<script>
	$(document).ready(function(){
        var updateOutput = function(e)
        {
            var list   = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };
        $('#nestable-{!! $value['name'] !!}').nestable({
            group: 1
        })

        $('#submit-form').on('click',function(){
            updateOutput($('#nestable-{!! $value['name'] !!}').data('output', $('.{!! $value['name'] !!}')));
            $('#form-menu').submit();
        });
    });
	
	
</script>
