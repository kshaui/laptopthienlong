<td>{{ $value->sku }}</td>
<td>
    <img style="height: 100px;" src="{!! $value->image !!}" onerror="this.src='/template-admin/images/no-image.png'">
</td>
<td>
    <strong><a class="row-title" href="{!! route($table.'.edit', $value->id) !!}" title="Sửa bài: {!! $value->name !!}">{!! $value->name !!}</a> </strong>
</td>
<td>
    {!! number_format($value->price) !!} đ
</td>
<td>
    {!! number_format($value->price_old) !!} đ
</td>
<td>
    {{ isset($array_categories[$value->category_id]) ? $array_categories[$value->category_id] : 'Không xác định'}}
</td>
<td>
    <strong>Thêm lúc:</strong> {!! $value->created_at !!}
    <br>
    <strong>Cập nhật:</strong> {!! $value->updated_at !!}
</td>

