@php
$district = DB::table('district')->where('id', $value->district_id)->first();
 $province = DB::table('province')->where('id', $value->province_id)->first();

@endphp
<th>{{ $value->code_order }}</th>

<td>
    <p>Họ Tên: <span>{!! $value->name !!}</span></p>
    <p>SĐT: <span><a href="tel:{!! $value->phone !!}">{!! $value->phone !!}</a></span></p>
    <p>Email: <span><a href="mailto:{!! $value->email !!}">{!! $value->email !!}</a></span></p>
    <p>Địa chỉ: <span>{!! $value->address !!}</span></p>
   @if(!empty($province))
   <p>Tỉnh / Thành phố: <span>{!! $province->name !!}</span></p>
   @endif
   @if(!empty($district))
   <p>Quận / Huyện: <span>{!! $district->name !!}</span></p>
    @endif
</td>
@php
	$total_price=0;
	$list_product = json_decode(base64_decode($value->content),1);
	// dump($list_product);die;
@endphp
<td>
	@if(isset($list_product) && count($list_product)>0 )
		@foreach($list_product as $val)
			@php
				$size = DB::table('sizes')->where('id',$val['size_id'])->first();
				$color = DB::table('colors')->where('id',$val['color_id'])->first();
			@endphp
		    <div style="margin-bottom: 20px;border-bottom: 1px solid #dedede;">
		    	<p style="margin: 0;">Sản phẩm: <a href="{!! route('web.products.show', $val['slug']) !!}" target="_blank" style="font-size: 14px;font-weight: bold;">{!! $val['name'] !!}</a></p>
		    	<p style="margin: 0;">Size: <span style="font-size: 14px;font-weight: bold;">{!! (!empty($size)) ? $size->name : 'Không xác định' !!}</span></p>
		    	<p style="margin: 0;">Color: <span style="font-size: 14px;font-weight: bold;">{!! (!empty($color)) ? $color->name : 'Không xác định' !!}</span></p>
			    <p style="margin: 0;">Giá: <span style="font-size: 14px;font-weight: bold;">{!! number_format($val['price']) !!} VNĐ</span></p>
			    <p style="margin: 0;">Số lượng: <span style="font-size: 14px;font-weight: bold;">{!! $val['qty'] !!}</span></p>
		    </div>
		    @php
		    	$total_price+=$val['qty']*$val['price'];
		    @endphp
		@endforeach
	@endif

	<p>Tổng số tiền cần thanh toán: <span>{!! number_format($total_price) !!} VNĐ</span></p>
</td>
<td>
	{!! @config('app.payments')[$value->payments] !!}
</td>
<td>
	{!! $value->note !!}
</td>
<td class="center">
	{!! change_time_to_text($value->created_at) !!}
</td>