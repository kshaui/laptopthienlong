<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Administrator Managerment</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{!! url('/template-admin/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/font-awesome.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/jquery.datetimepicker.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/js/fancybox/jquery.fancybox.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/custom.min.css') !!}" rel="stylesheet">
    <link href="{!! url('/template-admin/css/style1.css') !!}" rel="stylesheet">
    <style>
        .log-key-item {
            background: #f4f5f6;
            padding: 5px;
            margin: 0 5px 10px;
        }
    </style>
</head>
<body style="background: #fff;">
<div class="container body">
    <div class="main_container">
        <div class="row">
            <div class="col-md-12">
                <h1>Chi tiết dữ liệu hoạt động</h1>
                <p>Thực hiện bởi: <b>{{$array_admins[$log->admin_id]}}</b> - IP: <b>{{$log->ip}}</b> - Lúc: <b>{{date('H:i:s d/m/Y',strtotime($log->time))}}</b></p>
                <p>Tên bản ghi: <b>{!! !empty($data) ? $data->name : 'Không xác định' !!}</b> - Trên module: <b>{!! config('app.log_table')[$log->table] ?? $log->table !!}</b> - Kiểu hành động: <b>{!! config('app.log_type')[$log->type] ?? $log->type !!}</b></p>
            </div>
        </div>
        @php
        if($log->detail !=''){
            $detail = json_decode(decode_compress_string($log->detail),true);
        }else{
            $detail = [];
        }
        // dump($detail);die;
        switch ($log->table) {
            case 'products':
                $array_key_name = [
                    'name' => 'Tên sản phẩm',
                    'slug' => 'Đường dẫn',
                    'image' => 'Ảnh',
                    'slides' => 'Slide',
                    'description' => 'Mô tả',
                    'specifications' => 'Thông số',
                    'price_low' => 'Giá thấp',
                    'price_high' => 'Giá cao',
                    'detail' => 'Chi tiết',
                    'admin_id' => 'ID quản trị',
                    'status' => 'Trạng thái',
                    'created_at' => 'Thêm lúc',
                    'updated_at' => 'Cập nhật'
                ];
                break;
            default:
                $array_key_name = [
                    'parent_id' => 'ID cha',
                    'name' => 'Tên',
                    'slug' => 'Đường dẫn',
                    'image' => 'Ảnh',
                    'slides' => 'Slide',
                    'description' => 'Mô tả',
                    'detail' => 'Chi tiết',
                    'status' => 'Trạng thái',
                    'created_at' => 'Thêm lúc',
                    'updated_at' => 'Cập nhật'
                ];
                break;
        }
        @endphp
        @if(isset($detail['old']) && isset($detail['new']))
            <div class="row">
                <div class="col-md-6">
                    <h3>Dữ liệu cũ</h3>
                    @foreach($detail['old'] as $key=>$value)
                        <div class="log-key-item row">
                            <div class="col-md-1"><b>{{isset($array_key_name[$key]) ? $array_key_name[$key] : $key}}</b></div>
                            <div class="col-md-11">{!! $value !!}</div>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-6">
                    <h3>Dữ liệu mới</h3>
                    @foreach($detail['new'] as $key=>$value)
                        <div class="log-key-item row">
                            <div class="col-md-1"><b>{{isset($array_key_name[$key]) ? $array_key_name[$key] : $key}}</b></div>
                            <div class="col-md-11">{!! $value !!}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        @elseif(!isset($detail['old']) && isset($detail['new']))
            <div class="row">
                <div class="col-md-12">
                    @foreach($detail['new'] as $key=>$value)
                        <div class="log-key-item row">
                            <div class="col-md-1"><b>{{isset($array_key_name[$key]) ? $array_key_name[$key] : $key}}</b></div>
                            <div class="col-md-11">{!! $value !!}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        @elseif(count($detail) > 0)
            <div class="row">
                <div class="col-md-12">
                    @foreach($detail as $key=>$value)
                        <div class="log-key-item row">
                            <div class="col-md-1"><b>{{isset($array_key_name[$key]) ? $array_key_name[$key] : $key}}</b></div>
                            <div class="col-md-11">{!! $value !!}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</div>
</body>
</html>
