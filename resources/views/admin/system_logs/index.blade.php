<td>
    {{$array_admins[$value->admin_id]}}
</td>
<td>
    {{$value->ip}}
</td>
<td>
    {{date('H:i:s d/m/Y',strtotime($value->time))}}
</td>
<td>
    {{$value->title}}
</td>
<td>
    {!! config('app.log_type')[$value->type] ?? $value->type !!}
</td>
<td>
    {!! config('app.log_table')[$value->table] ?? $value->table !!}
</td>
<td>
	@if($value->type != 'login')
    	<a href="/admin_1996/system_logs/{{$value->id}}" target="_blank">Chi tiết</a>
    @endif
</td>