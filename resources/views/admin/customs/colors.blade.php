@php
	$colors = DB::table('colors')->where('status',1)->get();
	$color_choose = [];
	$product = isset($value['value']) ? $value['value']: '';
	if(!empty($product)){
		$color_choose = DB::table('product_color_maps')->where('product_id', $product->id)->pluck('color_id')->toArray();
	}
@endphp
<div class="form-group" style="padding-bottom: 20px;">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">Chọn màu</label>
    <div class="controls col-md-9 col-sm-10 col-xs-12" style="">
    	<div class="row">
            <div class="col-md-12 form-group">
                <a href="javascript:;" id="add-colors" class="btn btn-info">+ Thêm màu</a>
            </div>
        </div>
        @foreach($color_choose as $key => $value)
        	@php
        		$attr_image = DB::table('attribute_images')->where('product_id', $product->id)->where('color_id', $value)->first();
        		$data_color = DB::table('colors')->where('id', $value)->first();
        	@endphp
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-md-4 form-group">
		            <select name="colors[]" class="choose-color" style="width: calc(100% - 50px);height: 35px;padding-left: 10px;font-size: 16px;">
		            	<option data-code="#fff" value="0">Chọn màu</option>
		            	@foreach($colors as $val)
		            		<option data-code="{!! $val->code !!}" @if($val->id == $value) selected @endif value="{!! $val->id !!}">{!! $val->name !!}</option>
		            	@endforeach
		            </select>
		            <a class="color-demo" style="float: right;width: 40px;height: 35px;background: {!! $data_color->code !!};margin-left: 10px;border: 1px solid #ddd;"></a>
		        </div>
		        <div class="col-md-2 form-group">
		            <div class="controls col-sm-12">
		                <a style="padding: 7px 10px;" class="btn btn-primary btn-xs" href="javascript:;" onclick="media_popup('add','single','image_color_{!! $attr_image->id !!}','Chọn ảnh');">Chọn ảnh cho màu</a>
		                <div class="clear"></div>
		                <input id="image_color_{!! $attr_image->id !!}" type="hidden" name="image_color[]" value="{!! $attr_image->image !!}">
		                <img src="{!! $attr_image->image !!}" class="thumb-img" style="width: 100px;margin-top: 6px;">
		            </div>
		        </div>
		        <div class="col-md-2 form-group">
		            <a href="javascript:;" class="btn btn-xs btn-warning remove-color">Xóa</a>
		        </div>
		    </div>
	    @endforeach
    </div>
</div>
<script>
	function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
	$('#add-colors').on('click', function(){
		var rand = getRandomInt(1,999);
		var str = '<div class="row" style="margin-top: 10px;">' +
                '<div class="col-md-4 form-group">' +
                '<select name="colors[]" class="choose-color" style="width: calc(100% - 50px);height: 35px;padding-left: 10px;font-size: 16px;">' +
                '<option data-code="#fff" value="0">Chọn màu</option>';
                @foreach($colors as $value)
                str += '<option data-code="{!! $value->code !!}" value="{!! $value->id !!}">{!! $value->name !!}</option>';
                @endforeach
            str += '</select>' +
                '<a class="color-demo" style="float: right;width: 40px;height: 35px;background: #fff;margin-left: 10px;border: 1px solid #ddd;"></a>' +
                '</div>' +
                '<div class="col-md-2 form-group">' +
                '<div class="controls col-sm-12">' +
                '<a style="padding: 7px 10px;" class="btn btn-primary btn-xs" href="javascript:;" onclick="media_popup(\'add\',\'single\',\'image_color_'+rand+'\',\'Chọn ảnh\');">Chọn ảnh cho màu</a>' +
                '<div class="clear"></div>' +
                '<input id="image_color_'+rand+'" type="hidden" name="image_color[]" value="">' +
                '<img src="" class="thumb-img" style="width: 100px;margin-top: 6px;">' +
                '</div>' +
                '</div>' +
                '<div class="col-md-2 form-group">' +
                '<a href="javascript:;" class="btn btn-xs btn-warning remove-color">Xóa</a>' +
                '</div>' +
                '</div>';
        var btn_group = $(this).closest('.controls');
        btn_group.append(str);
	});
	$('body').on('click','.remove-color',function () {
        $(this).closest('.row').remove();
    });
	$('body').on('change','.choose-color',function () {
        var code = $('option:selected', this).attr('data-code');
		var btn_group = $(this).closest('.row');
		$(btn_group).find('.color-demo').css('background', code);
    });
</script>