@php
    $data = isset($value['value']) ? $value['value'] : '';
    if($data != ""){
        $cao = explode(',', $data->cao);
        $nang = explode(',', $data->nang);
        $nguc = explode(',', $data->nguc);
        $co = explode(',', $data->co);
        $vai = explode(',', $data->vai);
        $bung = explode(',', $data->bung);
        $mong = explode(',', $data->mong);
        $dui = explode(',', $data->dui);
        $size = explode(',', $data->size);
    }
@endphp
<input type="hidden" name="calculation_size_id" value="0">
<div class="form-group" style="padding-bottom: 20px;">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">Thêm thuộc tính</label>
    <div class="controls col-md-9 col-sm-10 col-xs-12" style="">
    	<div class="row">
            <div class="col-md-12 form-group">
                <a href="javascript:;" id="add-attribute" class="btn btn-info">+ Thêm thuộc tính</a>
            </div>
        </div>
        @if($data != "")
            @if($data->calculation_size_id == 1)
                @foreach($cao as $key => $value)
                    <div class="row" style="margin-top: 10px;">
                    	<div class="col-md-3 form-group">
                            <label class="col-md-6">Chiều cao lớn nhất (cm): </label>
                            <div class="col-md-6">
            	               <input class="form-control" type="number" name="cao[]" id="" value="{!! $value !!}">
                            </div>
            	        </div>
            	        <div class="col-md-3 form-group">
                            <label class="col-md-6">Cân nặng lớn nhất (kg): </label>
                            <div class="col-md-6">
                               <input class="form-control" type="number" name="nang[]" id="" value="{!! $nang[$key] !!}">
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label class="col-md-6">Size: </label>
                            <div class="col-md-6">
                               <input class="form-control" type="text" name="size[]" id="" value="{!! $size[$key] !!}">
                            </div>
                        </div>
            	        <div class="col-md-2 form-group">
            	            <a href="javascript:;" class="btn btn-xs btn-warning remove-attribute">Xóa</a>
            	        </div>
            	    </div>
                @endforeach
            @elseif($data->calculation_size_id == 2)
                @foreach($nguc as $key => $value)
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-3 form-group">
                            <label class="col-md-6">Vòng ngực lớn nhất (cm): </label>
                            <div class="col-md-6">
                               <input class="form-control" type="number" name="nguc[]" id="" value="{!! $value !!}">
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label class="col-md-6">Vòng cổ lớn nhất (cm): </label>
                            <div class="col-md-6">
                               <input class="form-control" type="number" name="co[]" id="" value="{!! $co[$key] !!}">
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label class="col-md-6">Độ rộng vai lớn nhất (cm): </label>
                            <div class="col-md-6">
                               <input class="form-control" type="number" name="vai[]" id="" value="{!! $vai[$key] !!}">
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label class="col-md-6">Size: </label>
                            <div class="col-md-6">
                               <input class="form-control" type="text" name="size[]" id="" value="{!! $size[$key] !!}">
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <a href="javascript:;" class="btn btn-xs btn-warning remove-attribute">Xóa</a>
                        </div>
                    </div>
                @endforeach
            @elseif($data->calculation_size_id == 3)
                @foreach($bung as $key => $value)
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-3 form-group">
                            <label class="col-md-6">Vòng bụng lớn nhất (cm): </label>
                            <div class="col-md-6">
                               <input class="form-control" type="number" name="bung[]" id="" value="{!! $value !!}">
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label class="col-md-6">Vòng mông lớn nhất (cm): </label>
                            <div class="col-md-6">
                               <input class="form-control" type="number" name="mong[]" id="" value="{!! $mong[$key] !!}">
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label class="col-md-6">Vòng đùi lớn nhất (cm): </label>
                            <div class="col-md-6">
                               <input class="form-control" type="number" name="dui[]" id="" value="{!! $dui[$key] !!}">
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label class="col-md-6">Size: </label>
                            <div class="col-md-6">
                               <input class="form-control" type="text" name="size[]" id="" value="{!! $size[$key] !!}">
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <a href="javascript:;" class="btn btn-xs btn-warning remove-attribute">Xóa</a>
                        </div>
                    </div>
                @endforeach
            @endif
        @endif
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#calculation_size_id').on('change', function(){
            var calculation_size_id = $(this).val();
            $('input[name="calculation_size_id"]').val(calculation_size_id);
            $(this).attr('disabled','disabled');
        });
        @if(isset($data) && !empty($data))
            $('#calculation_size_id').attr('disabled','disabled');
        @endif
    });
	function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
	$('#add-attribute').on('click', function(){
        var calculation_size_id = $('#calculation_size_id').val();
        if(calculation_size_id == 0){
            alert('Vui lòng chọn cách tính');
        }else{
            var rand = getRandomInt(1,999);
            if(calculation_size_id == 1){
                var str = '<div class="row" style="margin-top: 10px;">' +
                '<div class="col-md-3 form-group">'+
                '<label class="col-md-6">Chiều cao lớn nhất (cm): </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="number" name="cao[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-3 form-group">'+
                '<label class="col-md-6">Cân nặng lớn nhất (kg): </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="number" name="nang[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-3 form-group">'+
                '<label class="col-md-6">Size: </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="text" name="size[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-2 form-group">'+
                '<a href="javascript:;" class="btn btn-xs btn-warning remove-attribute">Xóa</a>'+
                '</div>'+
                '</div>';
            }else if(calculation_size_id == 2){
                var str = '<div class="row" style="margin-top: 10px;">' +
                '<div class="col-md-3 form-group">'+
                '<label class="col-md-6">Vòng ngực lớn nhất (cm): </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="number" name="nguc[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-3 form-group">'+
                '<label class="col-md-6">Vòng cổ lớn nhất (cm): </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="number" name="co[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-3 form-group">'+
                '<label class="col-md-6">Độ rộng vai lớn nhất (cm): </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="number" name="vai[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-2 form-group">'+
                '<label class="col-md-6">Size: </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="text" name="size[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-1 form-group">'+
                '<a href="javascript:;" class="btn btn-xs btn-warning remove-attribute">Xóa</a>'+
                '</div>'+
                '</div>';
            }else if(calculation_size_id == 3){
                var str = '<div class="row" style="margin-top: 10px;">' +
                '<div class="col-md-3 form-group">'+
                '<label class="col-md-6">Vòng bụng lớn nhất (cm): </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="number" name="bung[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-3 form-group">'+
                '<label class="col-md-6">Vòng mông lớn nhất (cm): </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="number" name="mong[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-3 form-group">'+
                '<label class="col-md-6">Vòng đùi lớn nhất (cm): </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="number" name="dui[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-2 form-group">'+
                '<label class="col-md-6">Size: </label>'+
                '<div class="col-md-6">'+
                '<input class="form-control" type="text" name="size[]" id="" value="">'+
                '</div>'+
                '</div>'+
                '<div class="col-md-1 form-group">'+
                '<a href="javascript:;" class="btn btn-xs btn-warning remove-attribute">Xóa</a>'+
                '</div>'+
                '</div>';
            }
        }
        var btn_group = $(this).closest('.controls');
        btn_group.append(str);
    });
	$('body').on('click','.remove-attribute',function () {
        $(this).closest('.row').remove();
    });
</script>