@php
    $data_edit = isset($value['value']) ? $value['value'] : '';
	$data_calculation_size = DB::table('calculation_size')->where('status',1)->get();
    $calculation_size = [];
    foreach($data_calculation_size as $value){
        $calculation_size[$value->id] = config('app.calculation_size')[$value->calculation_size_id];
    }
    $list_calculation_size = [];
    if($data_edit != ""){
    	$list_calculation_size = explode(',', $data_edit->calculation_size);
    }
@endphp
<div class="form-group">
	<label class="control-label col-md-2 col-sm-2 col-xs-12"><span class="form-asterick"></span>Chọn cách tính toán size</label>
	<div class="controls col-md-9 col-sm-10 col-xs-12">
		<div id="category_id" class="multi-cate-wrap multi-check">
			@foreach($calculation_size as $key => $value)
				<label><input type="checkbox" name="calculation_size[]" value="{!! $key !!}" @if(in_array($key, $list_calculation_size)) checked @endif> {!! $value !!}</label><br>
			@endforeach
		</div>
	</div>
</div>