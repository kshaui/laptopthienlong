<td class="center">{!! $value->name !!}</td>
<td class="center">{!! $value->display_name !!}</td>
<td class="center">{!! $value->email !!}</td>
<td class="">
    <table class="inline-role-table">
    @foreach(config('modules.module') as $key => $item)
        <tr>
            <td class="text-right"><strong>{!! config('modules.name')[$key] !!}</strong></td>
            <td>
            @foreach($item as $k=>$v)
                @php
                    $label = 'label-default';
                    if(in_array($key.'_'.$v,$role[$value->id]))
                        $label = 'label-success';
                @endphp
                <span class="label {!! $label !!}" data-role="{!! $key.'_'.$v !!}">{!! config('modules.name')[$v] !!}</span>
            @endforeach
            </td>
        </tr>
    @endforeach
    </table>
</td>
<style>
    .inline-role-table td {
        padding: 5px 10px;
    }
</style>