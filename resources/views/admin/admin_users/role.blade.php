<div class="well">
    <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Phân quyền</label>
        <div class="controls col-sm-9 list-role">
            <label class="fl">
                <div class="" style="position: relative;">
                    <i class="fa icon-green font-size17 fa-square-o" style="position: relative;">
                        <input style="position: absolute;
						top: 15px;
						display: block;
						margin: 0px;
						padding: 0px;
						border: 0px;
						opacity: 0;
						background: rgb(255, 255, 255);" type="checkbox" id="check_all" class="check" onclick="return check_all_role()">
                    </i>Toàn quyền
                </div>
            </label>
        </div>
    </div>
    @foreach(config('modules.module') as $key => $item)
        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">{!! config('modules.name')[$key] !!}</label>
            <div class="controls col-sm-9 list-role">
                @foreach($item as $value)
                    @php
                    $checked='';
                    $check='fa-square-o';
                    if(!isset($role)) $role = [];
                    if(in_array($key.'_'.$value, $role)){
                        $checked='checked';
                        $check='fa-check-square-o';
                    }
                    @endphp
                    <label class="fl">
                        <div class="" style="position: relative;">
                            <i class="fa {!! $check !!} icon-green font-size17">
                                <input style="position: absolute;
						top: -20%;
						left: -20%;
						display: block;
						width: 140%;
						height: 140%;
						margin: 0px;
						padding: 0px;
						border: 0px;
						opacity: 0;
						background: rgb(255, 255, 255);" type="checkbox" id="{!! $key.'_'.$value !!}" class="check" name="role[{!! $key.'_'.$value !!}]" value="1" {!! $checked !!} onclick="return check_one(this)"/>
                            </i> {!! config('modules.name')[$value] !!}
                        </div>
                    </label>
                @endforeach()
            </div>
        </div>
    @endforeach()
</div>