<td>
    <img style="height: 100px;" src="{!! $value->image !!}" onerror="this.src='/public/template-admin/images/no-image.png'">
</td>
<td>
    <strong><a class="row-title" href="{!! route($table.'.edit', $value->id) !!}" title="Sửa bài: {!! $value->name !!}">{!! $value->name !!}</a> </strong>
</td>
<td>
	@if(isset($array_categories[$value->category_id]))
    {{$array_categories[$value->category_id]}}
    @else
    {{"Không xác định"}}
    @endif
</td>
<td>
    <strong>Thêm lúc:</strong> {!! $value->created_at !!}
    <br>
    <strong>Cập nhật:</strong> {!! $value->updated_at !!}
    <br>
    @php
        $system_logs = DB::table('system_logs')->where('table','services')->where('table_id',$value->id)->orderBy('id','DESC')->first();
        if (isset($system_logs)) {
            $user = DB::table('admin_users')->where('id',$system_logs->admin_id)->first();
        }
    @endphp
    @if (isset($user->name))
        <strong>Cập nhật cuối: </strong> {{$user->name}}
    @else
        <strong>Cập nhật cuối: </strong>Chưa rõ
    @endif
</td>


