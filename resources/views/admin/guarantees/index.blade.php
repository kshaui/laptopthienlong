<td>
    <p>Họ Tên: <span>{!! $value->name !!}</span></p>
    <p>SĐT: <a href="tel:{!! $value->phone !!}">{!! $value->phone !!}</a></p>
    <p>Email: <span>{!! $value->email !!}</span></p>
    <p>Địa chỉ: <span>{!! $value->address !!}</span></p>
    <p>Tông tiền: <span>{!! number_format($value->total_costs) !!}</span></p>
</td>
<td><strong>{!! $value->product_name !!}</strong></td>
<td><strong>{!! $value->total_costs !!}</strong></td>
<td><strong>{!! $value->guarantee_time !!}</strong></td>
<td><strong>{!! $value->start_date !!}</strong></td>
<td><strong>{!! $value->end_date !!}</strong></td>
@php

$date_now = date("Y-m-d");
$end_date = date("Y-m-d", strtotime($value->end_date) ) ;
$days = (strtotime($end_date) - strtotime($date_now)) / (60 * 60 * 24);

@endphp

<td>Còn {{ $days }} ngày</td>
<td>
	<strong>Thêm lúc:</strong> {!! $value->created_at !!}
    <br>
    <strong>Cập nhật:</strong> {!! $value->updated_at !!}
</td>