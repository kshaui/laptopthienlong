<td>
    <img style="height: 100px;" src="{!! $value->image !!}" onerror="this.src='/template-admin/images/no-image.png'">
</td>
<td>
    <strong><a class="row-title" href="{!! route($table.'.edit', $value->id) !!}" title="Sửa bài: {!! $value->name !!}">{!! $value->name !!}</a> </strong>
</td>
<td>
    {!! number_format($value->price) !!} đ
</td>
<td>
    {!! number_format($value->price_old) !!} đ
</td>
<td>
    {{ isset($array_categories[$value->category_id]) ? $array_categories[$value->category_id] : 'Không xác định'}}
</td>
<td>
    <strong>Thêm lúc:</strong> {!! $value->created_at !!}
    <br>
    <strong>Cập nhật:</strong> {!! $value->updated_at !!}
</td>
<td class="center" style="position: relative;width: 80px;">
    <input style="width: 20px;height: 16px;" type="checkbox" class="show-sale-{!! $value->id !!}" @if($value->label_sale ==1) checked="checked" @endif value="{!!$value->label_sale!!}" class="form-control quick-edit" onchange="show_sale({!!$value->id!!})">
    <img class="img-show-sale-{!! $value->id !!}" style="width: 20px;height: 20px;position: absolute;top: 10px;left: 29px;display: none;" src="/assets/images/loading2.gif" alt="">
</td>

<td class="center" style="position: relative;width: 80px;">
    <input style="width: 20px;height: 16px;" type="checkbox" class="ban-chay-{!! $value->id !!}" @if($value->ban_chay ==1) checked="checked" @endif value="{!!$value->ban_chay!!}" class="form-control quick-edit" onchange="ban_chay({!!$value->id!!})">
    <img class="img-ban-chay-{!! $value->id !!}" style="width: 20px;height: 20px;position: absolute;top: 10px;left: 29px;display: none;" src="/assets/images/loading2.gif" alt="">
</td>

<td>
    <a class="duplicate" href="javascript:;" data-id="{!!$value->id!!}" data-type="products"><i class="fa fa-copy"></i></a>
</td>