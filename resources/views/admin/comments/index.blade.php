{{-- <td>
	@if($value->email != 'is_admin')
		<p>Họ tên: <strong>{!! $value->name !!}</strong></p>
		<p>Email: <strong>{!! $value->email !!}</strong></p>
		<p>Phone: <strong>{!! $value->phone !!}</strong></p>
		<p>Giới tính: <strong>{!! ($value->gender == 1) ? 'Nam' : 'Nữ' !!}</strong></p>
	@else
		<p>Quản trị viên trả lời</p>
	@endif
</td>
@php
	$data_post = DB::table($value->type)->where('id', $value->type_id)->first();
@endphp
<td>
<strong><a class="row-title" href="{!! route('web.'.$value->type.'.show', $data_post->slug) !!}" target="_blank">{!! $data_post->name !!}</a> </strong>
</td>
<td>
	{!! $value->content !!}
</td>
<td>
	Thêm bình luận lúc: {!! change_time_to_text($value->created_at) !!}
</td>
<td style="text-align: center;">
	@if($value->email != 'is_admin')
	<i style="cursor: pointer;" onclick="show_popup_reply({!! $value->id !!})" class="fa fa-reply-all" aria-hidden="true"></i>
	@endif
</td> --}}


@php
	$reply = DB::table('replies')->where('comment_id',$value->id)->get();
@endphp
<td class="text-center" style="width: 130px;">
	<select name="status" class="form-control quick-edit" onchange="check_edit(this)">
		@foreach($arr_stt as $k => $v)
		<option value="{{$k}}" @if($value->status == $k) selected="" @endif>{{$v}}</option>
		@endforeach
	</select>
	<br>
	@if($value->type == 'products')
		@php
		$products = DB::table('products')->where('id',$value->type_id)->first();
		$category = DB::table('products_categories')->where('id',$products->category_id)->first();
		@endphp
		@if($products != null)
    		<strong>Sản phẩm : </strong><a target="_blank" class="row-title" href="{!!route('web.products.show',$products->slug)!!}" title="Sửa bài: ">{!! $products->name !!}</a>
    	@endif 
    @elseif($value->type == 'news')
    	@php
		$news = DB::table('news')->where('id',$value->type_id)->first();
		@endphp
		@if($news != null)
     		<strong> Tin tức : </strong><a target="_blank" class="row-title" href="{!!route('web.news.show',$news->slug)!!}" title="Sửa bài: ">{!! $news->name !!}</a> 
     	@endif
	@elseif($value->type == 'credit')
    	@php
		$products = DB::table('products')->where('id',$value->type_id)->first();
		$category = DB::table('products_categories')->where('id',$products->category_id)->first();
		@endphp
		@if($products != null)
    		<strong>Sản phẩm trả góp : </strong><a target="_blank" class="row-title" href="{!!route('web.products.show',[$category->slug,$products->slug])!!}" title="Sửa bài: ">{!! $products->name !!}</a>
    	@endif 
	@endif
</td>
<td>
	<strong>  Tên : </strong>{!!$value->name!!} <br>
	<strong>  Email : </strong> {!!$value->email!!}<br>
	<strong>  Số điện thoại : </strong> {!!$value->phone!!}
</td>
<td style="width: 300px;">
    {!!$value->content!!} <br>
</td>
<td>
	@if(!empty($reply))
	@foreach($reply as $k => $val)
	<li>{!!$val->content!!}</li>
	@endforeach
	@endif
</td>
<td>
	<a href="{!! route('review.reply', $value->id) !!}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Trả lời</a>
</td>
<td>
    <strong>Thêm lúc:</strong> {!! change_time_to_text($value->created_at) !!}
</td>
 
