@extends('admin.layouts.app')
@section('title')
	<h3>Trả lời bình luận</h3>
@endsection
@section('css')
	<link href="{!! url('/template-admin/css/comment.css') !!}" rel="stylesheet">
@endsection
@section('content')
@include('errors.error')
<div class="row">
    <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="comment_item comment_item_parent">
            <div class="comment_item_author">
                <div class="comment_item_author_avatext">{!!substr($review->name,0,1)!!}</div>
                <span class="comment_item_author_name">{!!$review->name!!}</span>
            </div>
            <div class="comment_item_content">{!!$review->content!!}</div>
            <div class="comment_item_meta">
                <span class="comment_item_time" data="2018-09-17 15:22:18">{!!time_elapsed_string($review->created_at)!!}</span>
            </div>
            @if(!empty($reply))
            @foreach($reply as $k => $val)
            <div class="comment_item_reply_wrap  has_child">
                <div class="comment_item comment_item_child">
                    <div class="comment_item_author">
                        <div class="comment_item_author_avatext">{!!substr($val->admin,0,1)!!}</div>
                        <span class="comment_item_author_name">{!!$val->admin!!}</span>
                        <span class="comment_item_author_admin">Quản trị viên</span>                        
                    </div>
                    <div class="comment_item_content">{!!$val->content!!}
                    </div>
                    <div class="comment_item_meta">
                        <span class="comment_item_time" >{!!time_elapsed_string($val->created_at)!!}</span>
                    </div>
                </div>
            </div>
            @endforeach
			@endif
        </div>
    </div>
</div>
<form action="{!! route('review.reply.submit',$id) !!}" class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST">
	{{ method_field('PUT') }}
	<input type="hidden" name="_token" value="{!!csrf_token()!!}">
	<div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Quản trị viên</label>
        <div class="controls col-md-9 col-sm-10 col-xs-12">
            <input class="form-control" type="text" name="admin" id="name" value="{!!Auth::guard('admin')->user()->name!!}" placeholder="">
        </div>
    </div>
	<div class="form-group">
	    <label class="control-label col-md-2 col-sm-2 col-xs-12">Nội dung trả lời</label>
	    <div class="controls col-md-9 col-sm-10 col-xs-12">
	        <textarea rows="6" name="content" id="content" class="form-control" placeholder=""></textarea>
	    </div>
	</div>
	<div class="form-group">
	    <label class="control-label col-md-2 col-sm-2 col-xs-12">&nbsp;</label>
	    <div class="controls col-md-9 col-sm-10 col-xs-12">
	        <button type="submit" name="redirect" value="edit" class="btn btn-success">Trả lời</button>&nbsp;
	        <button type="reset" class="btn">Nhập lại</button>&nbsp;
	        <button type="button" onclick="window.location='{!!route('comments.index')!!}'" class="btn btn-danger">Thoát</button>&nbsp;
    	</div>
	</div>
</form>
@endsection