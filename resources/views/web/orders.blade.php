@extends('web.layouts.app')
@section('head')
	<link rel="stylesheet" href="/assets/css/cart.min.css">
@endsection
@section('content')


<div class="h-wrap">
    <div class="container">
    	 @if(Cart::count() > 0)
	        <div class="bk-process">
	            <div class="row col-mar-0">
	                <div class="col-4 col-item">
	                    <div class="item current">
	                        <div class="num">1</div>
	                        <p>Thông tin giỏ hàng</p>
	                    </div>
	                </div>
	                <div class="col-4 col-item text-center">
	                    <div class="item">
	                        <div class="num">3</div>
	                        <p>đặt hàng thanh toán</p>
	                    </div>
	                </div>
	                <div class="col-4 col-item text-right">
	                    <div class="item">
	                        <div class="num">3</div>
	                        <p>hoàn tất</p>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row">
	            <div class="col-md-8">
	                <div class="cart-tb" id="orders">
	                    <table>
	                        <thead>
	                        <tr>
	                            <th>Thông tin sản phẩm</th>
	                            <th>Giá</th>
	                            <th>Số lượng</th>
	                        </tr>
	                        </thead>
	                        <tbody>
	                        	@foreach($cart as $key => $value)
									@php	
										$product = $product_collect->where('id',$value->id)->first();
									@endphp
			                        <tr id="{!! $value->rowId !!}">
			                            <td>
			                                <a class="img" href="#" title="">
			                                    <img  src="/template-admin/images/loading1.gif" class="lazy" data-original="{!! image_by_link($product->image, 'small') !!}" alt="">
			                                </a>
			                                <div class="ct-img">
			                                    <a class="smooth title" href="{!! route('web.products.show', $product->slug) !!}" title="">{!! $value->name !!}</a>
			                                    <a class="smooth remove remove-cart" href="javascript:;" title=""><i class="icon_trash_alt"></i></a>
			                                </div>
			                            </td>
			                            <td class="price total-price">{!! ($value->price > 0) ? number_format($value->price).' VNĐ' : 'Liên hệ' !!}</td>
			                            <td>
			                                <div class="i-number">
			                                    <button  data-id="{!! $value->rowId !!}" class=" tru n-ctrl down smooth"></button>
			                                    <input type="text" disabled value="{!! $value->qty !!}" class="number-product" name="qty" min="1" max="10" value="1">
			                                    <button  data-id="{!! $value->rowId !!}" class="n-ctrl up smooth cong "></button>
			                                </div>
			                            </td>
			                        </tr>
	                      		@endforeach
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	            <div class="col-md-4">
	                <div class="cart-info">
	                    <h4 class="i-title">Thông tin đơn hàng</h4>
	                    <div class="table">
	                        <div class="cell">Tạm tính</div>
	                        <div class="cell text-right"><strong>{!! Cart::subtotal(0,',') !!} VNĐ</strong></div>
	                    </div>
	                    <div class="coupon-fr">
	                        <input type="text" placeholder="Nhập mã giảm giá" name="">
	                        <button type="" class="smooth">Áp dụng</button>
	                    </div>
	                    <div class="table top">
	                        <div class="cell">Tổng cộng</div>
	                        <div class="cell text-right"><strong class="orange"  style="line-height: 1">
	                        	<span class="value-total">{!! Cart::subtotal(0,',') !!} VNĐ </span></strong>
	                            <p style="color: #999">(Giá đã bao gồm thuế VAT)</p>
	                        </div>
	                    </div>
	                    @if(Cart::count() > 0)
	                    	<a  href="{{ route('web.orders.payment') }}">
		                    	<button class="booking-btn smooth">Tiến hành đặt hàng</button>
		                    </a>
	                    @endif
	                </div>
	            </div>
	        </div>
	        <br>
        @endif
          <section class="section your_cart"  @if(Cart::count() > 0) style="display: none" @endif>
            <div class="empty_cart" style="display:block ; text-align: center;
            padding-bottom: 200px;
            padding-top: 150px;">
                <p class="title" style="    color: #757575;
                font-size: 14px;
                line-height: 18px;
                margin-bottom: 27px;">Không có sản phẩm nào trong giỏ hàng của bạn!</p>
                <div class="continue">
                    <a style="color: #f57224;
                    border: 1px solid #f57224;
                    text-decoration: none;
                    margin: 0;
                    height: 48px;
                    padding: 0 36px;
                    font-size: 14px;
                    line-height: 46px;
                    display: inline-block;
                    transition: 0.3s all;" href="/">Tiếp tục mua sắm</a>
                </div>
            </div>
        </section>
    </div>
</div>


@endsection
@section('foot')
	<script>
		$(document).ready(function(){
			$('#orders .remove-cart').on('click', function(){
				var rowId = $(this).closest('tr').attr('id');
				$.ajax({
		        	type: 'post',
		        	dataType: 'json',
		        	data: {rowId:rowId},
		        	url: '/ajax/remove-cart',
		        	success:function(data){
		        		if(data == 1){
		        			$('#'+rowId).remove();
		        		}
		        	}
		        });
			});
			$('#orders .size').on('change', function(){
				var size_id = $(this).val();
				var rowId = $(this).closest('tr').attr('id');
				$.ajax({
		        	type: 'post',
		        	dataType: 'json',
		        	data: {rowId:rowId, size_id:size_id},
		        	url: '/ajax/update-size',
		        	success:function(){
		        		window.location.reload();
		        	}
		        });
			});
			$('#orders .color').on('change', function(){
				var color_id = $(this).val();
				var rowId = $(this).closest('tr').attr('id');
				$.ajax({
		        	type: 'post',
		        	dataType: 'json',
		        	data: {rowId:rowId, color_id:color_id},
		        	url: '/ajax/update-color',
		        	success:function(data){
		        		if(data.image != ''){
		        			$('#'+rowId+' .image img').attr('src', data.image);
		        		}
		        		window.location.reload();
		        	}
		        });
			});
			$('#orders .tru').on('click', function(){
				var rowId = $(this).attr('data-id');
			    var number = $('#'+rowId+' input.number-product').val();
			    var type = -1;
			    if(number > 1){
			        number = Number(number)-1;
			        $.ajax({
			        	type: 'post',
			        	dataType: 'json',
			        	data: {rowId:rowId, type:type},
			        	url: '/ajax/update-qty',
			        	success:function(data){
			    			$('#'+rowId+' input.number-product').val(number);
			    			$('#'+rowId+' .total-price').html(data.total_price);
			    			$('#total-cart').html(data.count_cart);
			    			$('#orders .value-total').html(data.value_total);
			        	}
			        });
			    }
			});
			$('#orders .cong').on('click', function(){
				var rowId = $(this).attr('data-id');
			    var number = $('#'+rowId+' input.number-product').val();
			    var type = 1;
			    number = Number(number)+1;
			    $.ajax({
		        	type: 'post',
		        	dataType: 'json',
		        	data: {rowId:rowId, type:type},
		        	url: '/ajax/update-qty',
		        	success:function(data){
			    		$('#'+rowId+' input.number-product').val(number);
			    		$('#'+rowId+' .total-price').html(data.total_price);
			    		$('#total-cart').html(data.count_cart);
			    		$('#orders .value-total').html(data.value_total);
		        	}
		        });
			});
		});
	</script>
@endsection