{{-- <div class="rating-stars text-center" data-type="{!!$type!!}" data-id="{!!$data_id!!}">
		<p class="succ"></p>
		@php
		$avg = round($rate->avg * 2) / 2;
		@endphp

	    <ul id='stars1'>
	    	Đánh giá của bạn : 
	    	@for($i = 0; $i < 5; $i ++)
	    	@if($i < $avg && $i + 0.5 == $avg)
	    	<li class="star selected" data-value='{!!$i+1!!}'>
	      		<i class="fa fa-star-half-o fa-fw" aria-hidden="true"></i>
	      	</li>
	    	@elseif($i < $avg)
			<li class='star selected' data-value='{!!$i+1!!}'>
	        	<i class='fa fa-star fa-fw'></i>
	      	</li>
	      	@else
	      	<li class='star' data-value='{!!$i+1!!}'>
	        	<i class='fa fa-star fa-fw'></i>
	      	</li>
	      	@endif
	      	@endfor
	      	<span>(<span id="count-rate">{!!$rate->count!!}</span> đánh giá)</span>
	    </ul>
	<div class="clear"></div>
</div> --}}
@if(isset($product))
@php
$vote = 0;
if(isset($_COOKIE['vote_'.$product->id])){
    $vote = $_COOKIE['vote_'.$product->id];
}
@endphp
<div class="rating-action" data-id="{!! $product->id !!}" data-type="products">
@for($i = 1; $i<= 5; $i ++)
    @if($vote != 0)
        @if($i <= $vote)
            <a class="rating-action-btn rating-action-btn1" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
        @else
            <a class="rating-action-btn rating-action-btn1" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star-o" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
        @endif
    @else
        <a class="rating-action-btn rating-action-btn2" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star-o" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
    @endif
@endfor
</div>
@endif