 
<!--Begin: header-->
<header @if(isset($menu_home) && $menu_home ==1 ) @else class="sub"  @endif>
    <div class="container">
        <div class="top">
            <div class="row col-mar-0 align-items-center">
                <div class="col-5 col-lg-3">
                    <div class="head-left">
                        <a class="logo" href="/" title="">
                            <img src="{{ image_by_link( @$config_general['logo_header'] ,'large') }}" data-original="{{ image_by_link( @$config_general['logo_header'] ,'large') }}" class="lazy"/>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-lg-6 d-none d-lg-block">
                    <div class="head-center">
                        <nav class="main-nav">
                            <ul>
                                <li><a class="smooth" href="#" title=""><i class="ic ic-marker"></i> Cửa hàng gần
                                    nhất</a></li>
                                <li><a class="smooth" href="{{ route('web.products_categories.khuyen-mai') }}" title=""><i class="icon_gift_alt"></i> Sản phẩm khuyến
                                    mãi</a></li>
                                <li><a class="smooth show-order-check" href="#" title="">Kiểm tra tình trạng đơn
                                    hàng</a></li>
                                <li><a class="smooth show-bao-hanh" href="#" title="">Kiểm tra bảo hành</a></li>
                                <li><a class="smooth blog" href="/tin-tuc" title="">Blog</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-7 col-lg-3 text-right">
                    <div class="head-right">
                        <a class="smooth hc-hotline" href="tel:{!! @$config_general['hotline'] !!}" title="" rel="nofollow,noindex">
                            <i class="ic ic-hotline"></i><strong>{!! @$config_general['hotline'] !!}</strong>
                        </a>

                        <button class="menu-btn nav-open" type="button"><i></i></button>
                    </div>
                </div>
            </div>

        </div>
        <div class="main-bar">
            <div class="row col-mar-0 align-items-center">
                <div class="col-12 col-md-4 col-lg-3 static align-self-start">
                    <div class="head-left">
                        <div class="h-category">
                            <h3 class="title"><span>Danh mục sản phẩm</span><i class="fa fa-list-ul"></i></h3>
                            <div class="ct">
                                <div class="scroll-bar">
                                    @php
                                        $menu_primary = json_decode($config_general['menu_primary']);
                                    @endphp
                                    <ul>
                                        @foreach($menu_primary as $value)
                                            <li>
                                                <a class="smooth"  href="#" title=""
                                             @if(isset($value->children) && count($value->children) >0)    data-sub="{{ $value->link }}" @endif >{!!$value->name!!}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @foreach($menu_primary as $value)  
                                    @php
                                        if(isset($value->children)){
                                            $menu_primary_level2 = $value->children;
                                        }
                                    @endphp 
                                    @if(isset($value->children) && count($value->children) >0)
                                       
                                            
                                                <div class="cate-sub-pane" data-sub-pane="{{ $value->link }}"
                                                     style="">
                                                    <button class="back"><em class="fa fa-angle-left"></em>&nbsp;&nbsp;Quay lại</button>
                                                    <div class="row">
                                                         @foreach($menu_primary_level2 as $val)
                                                             @php
                                                                if(isset($val->children)){
                                                                    $menu_primary_level3 = $val->children;
                                                                }
                                                            @endphp
                                                            <div class="col-lg-4">
                                                                <h4 
                                                                @if(isset($val->children) && count($val->children) >0)
                                                                class="s-title"

                                                                @endif

                                                                >

                                                                    <a class="smooth" href="{{ $val->link }}" title="">{{ $val->name }}</a></h4>
                                                                <ul>
                                                                     @if(isset($val->children) && count($val->children) >0)
                                                                    @foreach($menu_primary_level3 as $v)
                                                                        <li><a class="smooth" href="{{ $v->link }}" title="">{{ $v->name }}</a></li>
                                                                   @endforeach
                                                                   @endif
                                                                </ul>
                                                            </div>
                                                         
                                                       @endforeach
                                                    </div>
                                                </div>
                                           
                                       
                                    @endif
                                @endforeach
                               

                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-8 col-lg-6">
                    <div class="head-center">
                         <form class="search-form"  id="form-search"  action="/search" method="GET">
                            <input type="text"  name="keyword" placeholder="Nhập từ khóa tìm kiếm">
                            <button type="submit"><i class="ic ic-search"></i> <span>Tìm kiếm</span></button>
                             <div class="suggest-search" style="display: none;">
                                <ul class="suggest-search-list">
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-3 text-right  d-none d-lg-block">
                    <div class="head-right">
                        <div class="h-cart">
                            <a class="smooth"  href="/gio-hang" title="">
                                <i class="ic ic-cart" ></i>
                                <span id="total-cart">{!! Cart::count() !!}</span>
                            </a>
                        </div>
                        <div class="h-account">
                            @if(!Auth::check())
                                <a class="smooth" href="#" title="" data-toggle="modal" data-target="#popupLogin">
                                    <i class="ic ic-user"></i>
                                    <em>Tài khoản</em>
                                    <span>Đăng nhập & đăng ký</span>
                                </a>
                            @else
                                <a href="{{ route('web.users.information') }}">
                                    <i class="ic ic-user"></i>
                                    <em>{{ Auth::user()->name }}</em>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<header class="fixed">
    <div class="container">
        <div class="table">
            <div class="cell">
                <a class="logo" href="/" title="">
                     <img src="{{ image_by_link( @$config_general['logo_header'] ,'large') }}" data-original="{{ image_by_link( @$config_general['logo_header'] ,'large') }}" class="lazy"/>
                </a>
            </div>
            <div class="cell">
                <div class="h-category">
                    <h3 class="title"><span>Danh mục sản phẩm</span><i class="fa fa-list-ul"></i></h3>
                    <div class="ct">
                        <div class="scroll-bar">
                            @php
                                $menu_primary = json_decode($config_general['menu_primary']);
                            @endphp
                            <ul>
                                @foreach($menu_primary as $value)
                                <li><a class="smooth" href="#" title="">{{$value->name}}</a>
                                    @php
                                        if(isset($value->children)){
                                            $menu_primary_level2 = $value->children;
                                        }
                                    @endphp
                                    @if(isset($value->children) && count($value->children) >0)
                                        <ul>
                                            @foreach($menu_primary_level2 as $val)
                                            @php
                                                if(isset($val->children)){
                                                    $menu_primary_level3 = $val->children;
                                                }
                                            @endphp
                                                <li><a class="smooth" href="#" title=""
                                                 @if(isset($val->children) && count($val->children) >0) data-sub="{{ $val->link }}" @endif>{{ $val->name }}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                                @endforeach
                                
                            </ul>
                        </div>
                         @foreach($menu_primary as $value)  
                            @php
                                if(isset($value->children)){
                                    $menu_primary_level2 = $value->children;
                                }
                            @endphp 
                            @if(isset($value->children) && count($value->children) >0)
                                @foreach($menu_primary_level2 as $val)
                                     @php
                                        if(isset($val->children)){
                                            $menu_primary_level3 = $val->children;
                                        }
                                    @endphp
                                    @if(isset($val->children) && count($val->children) >0)
                                        <div class="cate-sub-pane" data-sub-pane="{{ $val->link }}"
                                             >
                                            <button class="back"><em class="fa fa-angle-left"></em>&nbsp;&nbsp;Quay lại</button>
                                            <div class="row">
                                                
                                                    <div class="col-lg-4">
                                                        <h4 class="s-title"><a class="smooth" href="{{ $val->link }}" title="">{{ $val->name }}</a></h4>
                                                        <ul>
                                                            @foreach($menu_primary_level3 as $v)
                                                                <li><a class="smooth" href="{{ $v->link }}" title="">{{ $v->name }}</a></li>
                                                           @endforeach
                                                        </ul>
                                                    </div>
                                                 
                                              
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach

                    </div>

                </div>
            </div>
            <div class="cell">
                <form class="search-form" id="form-search" action="/search" method="GET">
                    <input type="text" name="keyword" placeholder="Nhập từ khóa tìm kiếm">
                    <button type="submit"><i class="ic ic-search"></i> <span>Tìm kiếm</span></button>
                     <div class="suggest-search" style="display: none;">
                        <ul class="suggest-search-list">
                        </ul>
                    </div>
                </form>
            </div>
            <div class="cell">
                <div class="h-cart">
                    <a class="smooth" href="/gio-hang" title="">
                        <i class="ic ic-cart"></i>
                        <span id="total-cart">{!! Cart::count() !!}</span>
                    </a>
                </div>
                <div class="h-account">
                    @if(!Auth::check())
                        <a class="smooth" href="#" title="" data-toggle="modal" data-target="#popupLogin">
                            <i class="ic ic-user"></i>
                            <em>Tài khoản</em>
                            <span>Đăng nhập & đăng ký</span>
                        </a>
                    @else
                        <a href="{{ route('web.users.information') }}">
                            <i class="ic ic-user"></i>
                            <em>{{ Auth::user()->name }}</em>
                        </a>
                    @endif
                </div>
            </div>
            <div class="cell text-right">
                <a class="smooth hc-hotline" href="tel:{!! @$config_general['hotline'] !!}" title="" rel="nofollow,noindex">
                    <i class="ic ic-hotline"></i><strong>{!! @$config_general['hotline'] !!}</strong>
                </a>
            </div>
        </div>
    </div>
</header>

<div class="m-nav">
    <button class="menu-btn act nav-close" type="button"><i></i></button>
    <div class="nav-ct"></div>
</div>

<form action="{{ route('web.check_orders') }}" method="get" accept-charset="utf-8">
   
{{ csrf_field() }}
<div class="order-check">
    <h4 class="title">Kiểm tra đơn hàng</h4>
    <p>Vui lòng nhập email hoặc SĐT</p>
    <div class="line">
        <input type="text" name="phone_check">
    </div>
    <p>Vui lòng nhập mã đơn hàng</p>
    <div class="line">
        <input type="text" name="code_check" placeholder="Vd: 123456789">
        <button type="submit"><i class="fa fa-angle-right"></i></button>
    </div>
    <p>Nếu bạn có thắc mắc nào khác, vui lòng liên hệ Hotline: <a class="smooth" href="tel:{!! @$config_general['hotline'] !!}" title=""
                                                                  rel="nofollow,noindex">{!! @$config_general['hotline'] !!}</a></p>
</div>
<!-- end header -->
</form>


<form action="{{ route('web.Kiem_Tra_bao_hanh') }}" method="get" accept-charset="utf-8">
   
{{ csrf_field() }}
<div class="order-bao-hanh">
    <h4 class="title">Kiểm tra bảo hành</h4>
    <p>Vui lòng nhập SĐT</p>
    <div class="line">
        <input type="text" name="phone_bh" placeholder="Vd: {!! @$config_general['hotline'] !!}">
        <button type="submit"><i class="fa fa-angle-right"></i></button>
    </div>
    <p>Nếu bạn có thắc mắc nào khác, vui lòng liên hệ Hotline: <a class="smooth" href="tel:{!! @$config_general['hotline'] !!}" title=""
    rel="nofollow,noindex">{!! @$config_general['hotline'] !!}</a></p>
</div>

</form>