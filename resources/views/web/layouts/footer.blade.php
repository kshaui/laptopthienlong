@php
$you_have_seen_session = Session::get('you_have_seen');
    if (isset($you_have_seen_session)){
        $you_have_seen =\App\Product::where('status',1)->whereIn('id',$you_have_seen_session)->select('id','name','image','slug','price','price_old')->get();
    }
    
@endphp

<div class="h-watched">
    <div class="container">
        <div class="w-pane">
            <h4 class="i-title">Sản phẩm đã xem</h4>
            <div class="list">
                @if(isset($you_have_seen) )
                    @foreach($you_have_seen as $value)
                        <div class="item">
                            <a class="i-img" href="{{ $value->getUrl() }}"><img src="{{ $value->getImage() }}" data-original="{{ $value->getImage() }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->name }}"/></a>
                            <h3 class="s-title"><a class="smooth" href="{{ $value->getUrl() }}" title="">{{ $value->name }}</a></h3>
                            <div class="caption">
                                <div class="product smooth">
                                    <a class="img smooth" href="{{ $value->getUrl() }}" title="">
                                        <img src="{{ $value->getImage() }}" data-original="{{ $value->getImage() }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->name }}"/>
                                    </a>
                                    <div class="info">
                                        <h3 class="title"><a class="smooth" href="{{ $value->getUrl() }}" title="">{{ $value->name }}</a>
                                        </h3>
                                        <div class="price">
                                            <strong>{{ $value->getPrice()}}</strong>
                                            <del>{{ $value->getPriceOld() }} </del>
                                        </div>
                                        <a class="smooth buy-now" href="javascript:;" onclick="add_to_cart({!! $value->id !!},1);" title="">Thêm vào<br> giỏ hàng</a>
                                    </div>
                                     @if (!empty($value->price_old))
                                        @php
                                            $price_tick =(($value->price_old - $value->price)/($value->price_old))*100;
                                        @endphp
                                    <div class="tick">-{{  round( $price_tick) }}%</div>
                                     @endif
                                    <div class="gif"><i class="icon_gift"></i></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                
            </div>
            <a class="smooth view-all" href="#" title=""><i class="fa fa-angle-right"></i> Hiển thị tất cả các sản phẩm
                đã xem</a>
        </div>
    </div>
</div>
<footer>
    <div class="subscriber">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-8">
                    <p class="subs-title">Nhập địa chỉ email để nhận mã giảm giá và cập nhật các chương trình ưu đãi tốt
                        nhất</p>
                    <form class="subs-form">
                        <input type="text" placeholder="Nhập địa chỉ email của bạn">
                        <button type="submit">Đăng ký <i class="ic ic-arrow2"></i></button>
                    </form>
                    <div class="social">
                        <span>Follow us on</span>
                        <a class="smooth t" href="#" title=""><i class="social_twitter"></i></a>
                        <a class="smooth p" href="#" title=""><i class="social_pinterest"></i></a>
                        <a class="smooth f" href="#" title=""><i class="social_facebook"></i></a>
                        <a class="smooth y" href="#" title=""><i class="social_youtube"></i></a>
                        <a class="smooth g" href="#" title=""><i class="social_googleplus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-sm-4">
                    <h4 class="f-title">Tài khoản</h4>
                    <ul>
                        <li><a class="smooth" href="#" title="">Thông tin tài khoản</a></li>
                        <li><a class="smooth" href="#" title="">Giỏ hàng</a></li>
                        <li><a class="smooth" href="#" title="">Sản phẩm yêu thích</a></li>
                        <li><a class="smooth" href="#" title="">Lịch sử xem hàng</a></li>
                    </ul>
                </div>
               <!--  <div class="col-lg-2 col-sm-4">
                    <h4 class="f-title">Danh mục sản phẩm</h4>
                    <ul>
                        <li><a class="smooth" href="#" title="">Máy móc thiết bị cơ khí</a></li>
                        <li><a class="smooth" href="#" title="">Dụng cụ cơ khí</a></li>
                        <li><a class="smooth" href="#" title="">Thiết bị tiện ích gia đình</a></li>
                        <li><a class="smooth" href="#" title="">Bộ combo tiện ích</a></li>
                    </ul>
                </div> -->
                <div class="col-lg-2 col-sm-4">
                    <h4 class="f-title">Về laptop247hn</h4>
                    <ul>
                        <li><a class="smooth" href="#" title="">Giới thiệu</a></li>
                        <li><a class="smooth" href="#" title="">Hoạt động xã hội</a></li>
                        <li><a class="smooth" href="#" title="">Blog</a></li>
                        <li><a class="smooth" href="#" title="">Góc báo chí</a></li>
                        <li><a class="smooth" href="#" title="">Tuyển dụng</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-8">
                    <h4 class="f-title">Chính sách bán hàng</h4>
                    <ul>
                        <li><a class="smooth" href="#" title="">Quy định & hình thức thanh toán</a></li>
                        <li><a class="smooth" href="#" title="">Chính sách vận chuyển & giao hàng</a></li>
                        <li><a class="smooth" href="#" title="">Chính sách bảo hành & bảo trì</a></li>
                        <li><a class="smooth" href="#" title="">Chính sách đổi trả hàng & hoàn tiền</a></li>
                        <li><a class="smooth" href="#" title="">Chính sách bảo mật & chia sẻ thông tin</a></li>
                        <li><a class="smooth" href="#" title="">Câu hỏi thường gặp</a></li>
                    </ul>
                </div>
               <!--  <div class="col-lg-3 col-sm-4">
                    <h4 class="f-title"><i class="ic ic-mobile"></i> Thông báo bộ công thương</h4>
                    <img src="/images/qr.jpg" alt="" title=""/>
                </div> -->
            </div>

          <!--   <div class="h-brand">
                <span>Thương hiệu nổi bật</span>
                <div class="brand-cas">
                    <div class="slick-slide">
                        <a class="brand smooth" href="#" title="">
                            <img src="/images/br1.png" alt="" title=""/>
                        </a>
                    </div>
                    <div class="slick-slide">
                        <a class="brand smooth" href="#" title="">
                            <img src="/images/br2.png" alt="" title=""/>
                        </a>
                    </div>
                    <div class="slick-slide">
                        <a class="brand smooth" href="#" title="">
                            <img src="/images/br3.png" alt="" title=""/>
                        </a>
                    </div>
                    <div class="slick-slide">
                        <a class="brand smooth" href="#" title="">
                            <img src="/images/br4.png" alt="" title=""/>
                        </a>
                    </div>
                    <div class="slick-slide">
                        <a class="brand smooth" href="#" title="">
                            <img src="/images/br5.png" alt="" title=""/>
                        </a>
                    </div>
                    <div class="slick-slide">
                        <a class="brand smooth" href="#" title="">
                            <img src="/images/br3.png" alt="" title=""/>
                        </a>
                    </div>
                </div>
            </div> -->

            <div class="copyright">
                <div class="row align-items-center">
                    <div class="col-lg-4 sm-center">
                        <p>© 2020 laptop247hn.com</p>
                    </div>
                    <div class="col-lg-4 text-center">
                        <img src="/images/payment.png" alt="" title=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

@if($is_mobile)
<div class="mobile_footer">
    <div class="wrap">
          <a href="tel:{!! @$config_general['hotline'] !!}">
              <div class="mobile_footer_icon">
                  <img src="/images/icons/goi-dien.png" alt="">
              </div>
              <span>Gọi ngay</span>
        </a>  
        <a href="">
            <div class="mobile_footer_icon">
                <img src="/images/icons/bang-gia.png" alt="">
            </div>
            <span>Bảng giá</span>
      </a> 
      <a href="">
        <div class="mobile_footer_icon">
            <img src="/images/icons/Chinhanh.png" alt="">
        </div>
        <span>Chi nhánh</span>
  </a> 
  <a href="/lien-he">
    <div class="mobile_footer_icon">
        <img src="/images/icons/lien-he.png" alt="">
    </div>
    <span>Liên hệ</span>
</a> 
    </div>
</div>
@endif