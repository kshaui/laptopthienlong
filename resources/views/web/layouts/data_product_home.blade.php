<div class="slick-slide">
    <div class="product smooth">
        <a class="img smooth" href="{{ $value->getUrl() }}" title="">
            <img src="{{ $value->getImage('small') }}" data-original="{{ $value->getImage('small') }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->name }}"/>
        </a>
        <div class="info">
            <h3 class="title"><a class="smooth" href="{{ $value->getUrl() }}" title="">{{ $value->name }}</a>
            </h3>
            <div class="rate-like">
                <div class="star-base">

                    @if(isset($avg1))
                        @include('web.layouts.star',compact('avg1','count'))
                       
                    @endif
                </div>
                <a class="smooth like" href="#" title=""><i class="ic ic-heart"></i></a>
            </div>
            <div class="price">
                <strong>{{ $value->getPrice()}}</strong>
                <del>{{ $value->getPriceOld() }}</del>
            </div>
            {{-- <a class="smooth buy-now" onclick="add_to_cart({!! $value->id !!},1);" href="javascript:;" title="">Thêm
                vào<br> giỏ hàng</a> --}}
        </div>
         @if (!empty($value->price_old))
                @php
                    $price_tick =(($value->price_old - $value->price)/($value->price_old))*100;
                @endphp
        <div class="tick">-{{  round( $price_tick) }}%</div>
         @endif
        @if(!empty($value->promotion))
            <div class="gif"><i class="icon_gift"></i></div>
        @endif
    </div>
</div>