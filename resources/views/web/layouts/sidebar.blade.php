@if($filter_size != '' || $filter_color != '' || $filter_price != '' || $filter_brand !='' || $filter_ram !='' || $filter_ocung !='' || $filter_core !='' )

                    <div class="remove-filter ">

                        <a href="{!! route('web.products_categories.index') !!}">Xoá tất cả</a>

                    </div>

                    @endif
<div class="sidebar">
    <h3 class="sb-title2">Danh mục laptop</h3>
    <div class="sb-ct hc-collapse">
        @php
            $menu_header = json_decode($config_general['menu_primary']);
        @endphp
        <ul>
            @foreach($menu_header as $value)
                <li><a class="smooth" href="#" title="">{{ $value->name }}  @if(isset($value->children)) ({{ count($value->children)}})  @endif</a>
                    @php
                        if(isset($value->children)){
                            $menu_header_level2 = $value->children;
                        }
                    @endphp
                    <ul style="display: none;">
                        @if(isset($value->children))
                        @foreach($menu_header_level2 as $val)
                            <li><a class="smooth " href="#" title="">{{ $val->name }}  @if(isset($val->children)) ({{ count($val->children)}})  @endif</a>
                               <!--  <ul>
                                    <li><a class="smooth" href="#" title="">Máy hàn que</a></li>
                                    <li><a class="smooth" href="#" title="">Máy hàn TIG</a></li>
                                    <li><a class="smooth" href="#" title="">Máy hàn MIG</a></li>
                                    <li><a class="smooth" href="#" title="">Máy cắt Plasma</a></li>
                                    <li><a class="smooth" href="#" title="">Phụ tùng tiêu hao</a></li>
                                    <li><a class="smooth" href="#" title="">Linh phụ kiện sửa chữa</a></li>
                                </ul> -->
                            </li>
                        @endforeach
                        @endif
                        
                    </ul>
                </li>
            @endforeach
            
        </ul>
    </div>
</div>
<form action="" method="get" id="form-filter" >
    <div class="sidebar">
        <h3 class="sb-title"><i class="fa fa-archive"></i> Thương hiệu</h3>
        <div class="sb-ct">
            @foreach($data_brand as $value)
                <p>
                    <label class="i-check">
                        <input type="radio" @if($filter_brand == $value->id) checked="checked" @endif  value="{!! $value->id !!}" name="brand"><i></i> {!! $value->name !!}
                    </label>
                </p>
            @endforeach
        </div>
    </div>
    <div class="sidebar">
        <h3 class="sb-title"><i class="fa fa-archive"></i>Ổ cứng</h3>
        <div class="sb-ct">
             @foreach(config('app.ocung') as $key => $value)
                 @if($key !=0)
                    <p>
                        <label class="i-check">
                            <input type="radio" @if($filter_ocung == $key) checked="checked" @endif  value="{!! $key !!}" name="ocung"><i></i> {!! $value !!}
                        </label>
                    </p>
                @endif
            @endforeach
        </div>
    </div>
</form>
<!-- <div class="sidebar">
    <h3 class="sb-title"><i class="fa fa-tag"></i> Giá</h3>
    <div class="sb-ct">
        <div class="price-filter">
            <span class="price-1"><span></span> đ</span>
            <span class="price-2"><span></span> đ</span>
            <div id="slider-range" data-max="30000000" data-min="200000"
                 data-value="200000,30000000"></div>
        </div>
    </div>
</div> -->