
@if (isset($breadcrumbs))
<div class="h-wrap default">
    <div class="container">
        <ul class="breadcrumb">
            <li><a class="smooth" href="{{route('web.home')}}" title="">Trang chủ</li>
            @foreach($breadcrumbs as $key=>$value)
            <li><a class="smooth" href="{{$value['url']}}" title="">{{$value['name']}} </a></li>
            @endforeach
        </ul>
    </div>
</div>
@endif
  