  <div class="comment-fr">
        <form class="form-input">
            <textarea class="cmt_content" placeholder="Mời bạn để lại bình luận"></textarea>
            <div class="fr-photo"></div>
            <div class="fr-ctrl">
                <label class="push-img" href="#" title="">
                    <input type="file" multiple accept="image/x-png,image/gif,image/jpeg">
                    <i class="fa fa-camera"></i>
                    <span>Gửi ảnh</span>
                </label>
                <a class="smooth" href="#" title="">Quy định đăng bình luận</a>
                <button type="button" class="smooth  btn-info-cmt send">Gửi</button>
            </div>

             <div class="comment-form" style="display: none">
                    <div class="comment-form-content">
                        <p>Thông tin bình luận</p>
                            @csrf
                            <div class="comment-sex">
                                <span><input type="radio" name="sex"> Anh</span>
                                <span><input type="radio" name="sex"> Chị</span>
                            </div>
                            <div class="comment-name-and-email">
                                <input type="text" name="name" class="cmt_name" id="name" placeholder="Họ và Tên...">
                                <input type="text" name="phone" class="cmt_phone" id="phone" placeholder="Số siện thoại">
                                <input type="email" name="email" class="cmt_email" id="email" placeholder="Email">
                            </div>
                            <div class="comment-btn">
                                <button type="button" id="send_comment" data-id="{!!$data_id!!}" data-type="{!!$type!!}" data-url="{!!route('add.comment')!!}">Gửi</button>
                                <a href="javascript:;" id="close-comment-form"><i class="fa fa-remove" aria-hidden="true"></i>Hủy</a>
                            </div>
                    </div>
                </div>

        </form>
        <div class="cmt-head">
            <div class="table">
                <div class="cell">
                    <strong>{{ count(@$comments) }} Bình Luận</strong>
                </div>
                <div class="cell text-right">
                    <form class="comment-search">
                        <input type="text" placeholder="Tìm theo nội dung, người gửi ..." name="">
                        <button type="submit"><i class="icon_search"></i></button>
                    </form>
                </div>
            </div>
        </div>
 @if(isset($comments) && count($comments)>0)
     <div class="cmt-list">
        @foreach($comments as $key => $value)
            <div class="hc-comment">
                <div class="head">
                    <div class="avt">{!!substr(removeSign($value->name),0,1)!!}</div>
                    <h4 class="name">{!!$value->name!!}</h4>
                </div>
                <div class="text">
                   {!!$value->content!!}
                </div>
                <div class="ctrl">
                    <a class="smooth reply-btn" data-id="1" href="#"  title="">Trả lời</a> •
                    <time>{!! date('H:i:s d/m/Y', strtotime($value->created_at)) !!}</time>
                </div>
                @php
                    $reply = $value->getReply($value->id);
                    $reply2 = $value->getReply2($value->id);
                @endphp
                @if(  count($reply) > 0 )
                <div class="replys">
                    @foreach($reply as $k => $val)
                        <div class="hc-comment f2">
                            <div class="head">
                                <div class="avt">{!!substr(removeSign($val->admin),0,1)!!}</div>
                                <h4 class="name">{!!$val->admin!!}</h4>
                                <label>Quản trị viên</label>
                            </div>
                            <div class="text">
                                {!!$val->content!!}
                            </div>
                            <div class="ctrl">
                                <time>{!!time_elapsed_string($val->created_at)!!}</time>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endif
            </div>
        @endforeach
    </div>
@endif

    <div class="s-pagination">
        <!-- <a class="smooth" href="#"><i class="fa fa-angle-double-left"></i></a>
        <a class="smooth" href="#"><i class="fa fa-angle-left"></i></a>
        <a class="smooth" href="#">1</a>
        <strong>2</strong>
        <a class="smooth" href="#">3</a>
        <a class="smooth" href="#"><i class="fa fa-angle-right"></i></a>
        <a class="smooth" href="#"><i class="fa fa-angle-double-right"></i></a> -->
    </div>
    <textarea class="cmt-focus" placeholder="Mời bạn để lại bình luận"></textarea>
</div>