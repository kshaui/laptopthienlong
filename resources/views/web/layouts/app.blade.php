<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#"
      xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{!! @$config_general['image_favicon'] !!}" type="image/x-icon" rel="shortcut icon"/>
    @include('web.layouts.seo')
<meta name="DC.title" content="thanhtranceo" />
<meta name="geo.region" content="VN-HN" />
<meta name="geo.placename" content="???ng xuan ph??ng ph??ng ph??ng canh nam t? liem ha n?i" />
<meta name="geo.position" content="13.290403;108.426511" />
<meta name="ICBM" content="13.290403, 108.426511" />

  <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="/fonts/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <!-- <link href="fonts/themify-icons/themify-icons.css" type="text/css" rel="stylesheet"> -->
    <link href="/fonts/elegantIcon/elegantIcon.css" type="text/css" rel="stylesheet">
    <link href="/fancybox/jquery.fancybox.css" type="text/css" rel="stylesheet"/>
    <!-- <link href="fancybox/helpers/jquery.fancybox-thumbs.css" type="text/css" rel="stylesheet" /> -->
    <!-- Custom CSS -->
    <!-- <link href="css/animate.min.css" type="text/css" rel="stylesheet"> -->
    <link href="/css/slick.min.css" type="text/css" rel="stylesheet">
    <link href="/css/jquery-ui.min.css" type="text/css" rel="stylesheet">
    <link href="/zoom/cloudzoom.css" type="text/css" rel="stylesheet">
    <link href="/css/jquery.mCustomScrollbar.min.css" type="text/css" rel="stylesheet">
    <link href="/css/main.min.css" type="text/css" rel="stylesheet">
    <link href="/css/responsive.min.css" type="text/css" rel="stylesheet">
    {!! @$config_general['html_head'] !!}
    @yield('schema')
   <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "ProfessionalService",
  "image": [
    "https://laptop247hn.com/uploads/2019/11/laptop247hn.png",
    "https://laptop247hn.com/uploads/2019/11/laptop247hnfooter.png ",
    "https://laptop247hn.com/uploads/2019/11/sua-chua-laptop247hn.PNG"
   ],
  "@id": "https://laptop247hn.com",
  "name": "Cong ty TNHH Laptop247hn",
  "logo": "https://www.facebook.com/sualaptop247giare/",
  "priceRange": "$",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "???ng xuan ph??ng, ph??ng ph??ng canh, qu?n nam t? liem , ha n?i",
    "addressLocality": "Ha Noi",
    "addressRegion": "Vietnam",
    "postalCode": "10000",
    "addressCountry": "VN"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 13.290403,
    "longitude": 108.426511
  },
  "url": "https://laptop247hn.com",
    "sameAs": [
    "https://www.facebook.com/sualaptop247giare/",
    "https://www.youtube.com/channel/UCqayr_yD4W4Ef75IfvLG_7Q?view_as=subscriber"
  ],
  "telephone": "+84866628426",
  "openingHoursSpecification": [
    {
		"@type": "OpeningHoursSpecification",
		"dayOfWeek": [
		  "Monday",
		  "Tuesday",
		  "Wednesday",
		  "Thursday",
      "Friday"
		],
		"opens": "08:00",
		"closes": "17:30"
    },
    {
      "@type": "OpeningHoursSpecification",
      "dayOfWeek": "Saturday",
      "opens": "08:00",
      "closes": "12:00"
    }
  ]
}
</script>
    <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "WebSite",
          "url": "https://laptop247hn.com/",
          "potentialAction": {
            "@type": "SearchAction",
            "target": "https://laptop247hn.com/tim-kiem?keyword={search_term_string}",
            "query-input": "required name=search_term_string"
          }
        }

    </script>
   <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "url": "https://laptop247hn.com/",
  "contactPoint": [{
    "@type": "ContactPoint",
    "telephone": "+84-866628426",
    "contactType": "customer service",
    "contactOption": "HearingImpairedSupported",
    "areaServed": "VN"
  },{
    "@type": "ContactPoint",
    "telephone": "+84-343274132",
    "contactType": "sales"
  },{
    "@type": "ContactPoint",
    "telephone": "+84-866628426",
    "contactType": "technical support",
    "contactOption": [
      "HearingImpairedSupported"
    ],
    "areaServed": "VN"
  }
    ]
  }]
}
</script>
    @yield('head')
</head>
<body>
{!! @$config_general['html_body'] !!}
@if(!$is_mobile)
    @include('web.layouts.adminbar')
@endif

 <img class="loading1" src="/images/icons/loading1.gif" alt=""
    style="    
    position: fixed;
    display: none;
    z-index: 9999;
    left: 50%;
    transform: translateX(-50%);
    top: 40%;
    width: 100px;
    "
    >


@include('web.layouts.header')
@include('web.layouts.breadcrumb')
@yield('content')
@include('web.layouts.footer')


</body>
</html>
<!-- jQuery -->
<script src="/js/jquery.min.js"></script>

<script src="/assets/lazyload.min.js"></script>


<!-- Bootstrap Core JavaScript -->
<script src="/js/tether.min.js" type="text/javascript"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- Custom JS -->
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/slick.min.js"></script>
<script src="/js/jquery.sticky-kit.js"></script>
<script src="/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/fancybox/jquery.fancybox.pack.js"></script>
<script src="/zoom/cloudzoom.min.js"></script>
<script src="/js/script.min.js"></script>
<script src="/assets/script.min.js"></script>

<script type="text/javascript" src="/assets/libs/jquery.bpopup.min.js"></script>

@yield('foot')



<div class="social-right">
    <a class="smooth y" href="#" title=""><i class="fa fa-youtube"></i></a>
    <a class="smooth f" href="#" title=""><i class="fa fa-facebook"></i></a>
    <a class="smooth g" href="#" title=""><i class="fa fa-google-plus"></i></a>
</div>

<!-- <div class="psy-pane">
    <a class="smooth" href="#sale-off-pro" title="">
        <i class="ic sec1 smooth"></i>
        <span class="smooth">Sản phẩm khuyến mãi</span>
    </a>
    <a class="smooth" href="#featured-pro" title="">
        <i class="ic sec2 smooth"></i>
        <span class="smooth">Sản phẩm nổi bật</span>
    </a>
    <a class="smooth" href="#machinal-pro" title="">
        <i class="ic sec3 smooth"></i>
        <span class="smooth">Thiết bị cơ khí</span>
    </a>
    <a class="smooth" href="#home-pro" title="">
        <i class="ic sec4 smooth"></i>
        <span class="smooth">Thiết bị gia đình</span>
    </a>
    <a class="smooth" href="#only-pro" title="">
        <i class="ic sec5 smooth"></i>
        <span class="smooth">Dành riêng cho bạn</span>
    </a>
</div> -->

<div class="modal fade popupLogin" id="popupLogin" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <span class="close-popup" data-dismiss="modal"><i class="icon_close"></i></span>
            <div class="modal-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#login" role="tab"
                           aria-controls="login" aria-selected="true">Đăng nhập</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#register" role="tab"
                           aria-controls="register" aria-selected="false">Tạo tài khoản</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                        <div class="popup-content">
                            <div class="po-left">
                                <div class="title">
                                    <h3 class="heading">đăng nhập</h3>
                                    <span class="desc">Đăng nhập để theo dõi đơn hàng, lưu danh sách sản phẩm yêu thích, nhận nhiều ưu đãi hấp dẫn.</span>

                                </div>
                                <div class="img">
                                    <img src="/images/img-login.png" alt="">
                                </div>

                            </div>
                            <div class="po-right">
                                <div class="fm-login">
                                    <input type="text" id="email-login" class="form-control" placeholder="Nhập email hoặc số điện thoại">
                                </div>
                                <div class="fm-login">
                                    <input type="password" id="password-login" class="form-control" placeholder="Mật khẩu">
                                </div>
                                <div class="qmk">Quên mật khẩu? Nhấn vào <a href="#" title="" data-toggle="modal"
                                                                            data-target="#reset-pass"
                                                                            data-dismiss="modal">đây</a></div>
                                <button type="button" onclick="login()" class="btn btn-login">Đăng nhập</button>
                                <span class="note-social">Hoặc đăng nhập với tài khoản mạng xã hội</span>
                                <ul>
                                    <li><a href="" title=""><i class="social_facebook"></i><span>Facebook</span></a>
                                    </li>
                                    <li><a href="" title=""><i class="social_googleplus"></i><span>Google+</span></a>
                                    </li>
                                    <li><a href="" title=""><i class="ic-zalo"></i><span>Zalo</span></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                        <div class="popup-content">
                            <div class="po-left">
                                <div class="title">
                                    <h3 class="heading">đăng ký</h3>
                                    <span class="desc">Tạo tài khoản để theo dõi đơn hàng, lưu danh sách sản phẩm yêu thích, nhận nhiều ưu đãi hấp dẫn.</span>

                                </div>
                                <div class="img">
                                    <img src="/images/img-login.png" alt="">
                                </div>

                            </div>
                            <div class="po-right register">
                                <div class="fm-login">
                                    <input type="text" class="form-control" placeholder="Họ và tên">
                                </div>
                                <div class="fm-login">
                                    <input type="tel" class="form-control" placeholder="Số điện thoại">
                                    <button class="verification-code" type="submit">Gửi mã xác minh</button>
                                </div>
                                <div class="fm-login">
                                    <input type="text" class="form-control" placeholder="Mã xác minh">
                                </div>
                                <div class="fm-login">
                                    <input type="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="fm-login">
                                    <input type="password" class="form-control" placeholder="Mật khẩu">
                                </div>
                                <div class="fm-login">
                                    <input type="password" class="form-control" placeholder="Xác nhận mật khẩu">
                                </div>
                                <div class="agree">Tôi đồng ý với <a href="#" title="">Chính sách mua hàng</a> của laptop247hn.com
                                </div>
                                <p class="notification">
                                    <label class="i-check">
                                        <input type="checkbox" name=""><i></i> Tôi muốn nhận các thông tin khuyến mãi từ
                                        laptop247hn.com
                                    </label>
                                </p>
                                <button type="submit" class="btn btn-login" data-dismiss="modal" data-toggle="modal"
                                        data-target="#acc-success">Đăng ký
                                </button>
                                <span class="note-social">Hoặc đăng ký tài khoản qua</span>
                                <ul>
                                    <li><a href="" title=""><i class="social_facebook"></i><span>Facebook</span></a>
                                    </li>
                                    <li><a href="" title=""><i class="social_googleplus"></i><span>Google+</span></a>
                                    </li>
                                    <li><a href="" title=""><i class="ic-zalo"></i><span>Zalo</span></a></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="acc-success" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content tks-content">
            <h4 class="title">Chào mừng bạn!</h4>
            <p><strong>Cảm ơn bạn đã tạo tài khoản tại Laptop247hn.com! </strong></p>
            <p>Hãy sử dụng tài khoản này để trải nghiệm mua sắm các sản phẩm cơ khí đa dạng cho bạn và gia đình tại
                Laptop247hn.com</p>
            <p>Mọi thông tin tài khoản của bạn sẽ được bảo mật an toàn tuyệt đối</p>

            <div class="ctrl">
                <a class="smooth view-more" href="#" title="">Xem các sản phẩm giá tốt</a>
                <a class="smooth back-home" href="#" title="">Về trang chủ</a>
            </div>
        </div>
    </div>
</div>

<div id="reset-pass" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content reset-pass-ct">
            <button type="button" class="i-close" data-dismiss="modal"><i class="icon_close"></i></button>
            <h4 class="title">Quên mật khẩu</h4>
            <p>Vui lòng cung cấp email để lấy lại mật khẩu</p>
            <form class="reset-pass-fr">
                <input type="text" placeholder="Nhập email hoặc số điện thoại" name="">
                <button type="submit" class="submit">Gửi</button>
            </form>
        </div>
    </div>
</div>

