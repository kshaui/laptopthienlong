@extends('web.layouts.app')
@section('content')

<div class="account-wrap bg">
        <div class="container">
            <div class="row">
                @include('web.users.sidebar')
                <div class="col-lg-9">
                <div class="ct-account">
                    <div class="account-pane">
                        <div class="head">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h2 class="title">Đơn hàng của tôi</h2>
                                </div>
                                <div class="col-md-6 text-right xs-left">
                                    <span>Hiển thị</span>
                                    <div class="i-select">
                                        <select>
                                            <option>Đơn hàng thành công</option>
                                            <option>Đơn hàng bị hủy</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="account-tb">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Mã đơn hàng</th>
                                        <th class="text-center">Ngày mua</th>
                                        <th>Sản phẩm</th>
                                        <th>Đơn giá</th>
                                        <th>Số lượng</th>
                                        <th class="text-center">Tổng tiền</th>
                                        <th class="text-center">Trạng thái đơn hàng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders as $value)
                                    @php
                                        $list_product = json_decode(base64_decode($value->content),1);
                                        // dump($list_product);die;
                                    @endphp
                                        @foreach ($list_product as $val)
                                        
                                    
                                            <tr>
                                                <td class="orange">{{ $value->code_order }}</td>
                                                <td class="text-center">{{ $value->created_at }}</td>
                                                <td><a class="smooth" href="#" title="">{{ @$val['name'] }}</a></td>
                                                <td>{{ @$val['price']}}</td>
                                                <td>{{ @$val['qty'] }}</td>
                                                <td class="text-center">{{ @$val['price']* @$val['qty'] }} đ</td>
                                        
                                                <td class="text-center">{{ $value->status == 2 ? 'Đang xử lý' : 'Giao hàng thành công' }}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pagination text-center">
                        {!! @$orders->links() !!}
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>


    @endsection