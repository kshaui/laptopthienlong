<div class="col-lg-3">
        <div class="sb-account stick-scroll">
            <button type="button" class="asb-btn"><i class="fa fa-cog"></i></button>
            <div class="ct">
                <div class="head clearfix">
                    <img class="avata" src="{{ @$config_general['logo_header'] }}" alt="" title="" />
                    <div class="text">
                        <span>Tài khoản</span>
                    <h3 class="name">{{ @Auth::user()->name }}</h3>
                    </div>
                </div>
                <ul>
                    <li><a class="smooth active" href="{{ route('web.users.information') }}" title=""><i class="fa fa-user"></i> Thông tin tài khoản</a></li>
                    <li><a class="smooth" href="{{ route('web.users.order') }}" title=""><i class="fa fa-clipboard"></i> Quản lý đơn hàng</a></li>
                    <li><a class="smooth" href="#" title=""><i class="fa fa-tags"></i> Mã giảm giá</a></li>
                    <li><a class="smooth" href="#" title=""><i class="fa fa-heart-o"></i> Sản phẩm yêu thích</a></li>
                </ul>
                <button type="button" class="asb-btn-back"><i class="fa fa-mail-reply"></i> Đóng</button>
            </div>
        </div>
    </div>