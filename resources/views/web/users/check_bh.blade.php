@extends('web.layouts.app')
@section('content')

@if(isset($guarantees) && count($guarantees)>0)
 <div class="account-wrap bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="sb-account stick-scroll">
                        <button type="button" class="asb-btn"><i class="fa fa-cog"></i></button>
                        <div class="ct">
                            <div class="not-login">
                                <h4 class="title">Chào mừng trở lại!</h4>
                                <p><!-- Vui lòng đăng nhập để có thể quản lý  --></p>
                                <a class="smooth ctrl" href="#" title="">Đăng nhập</a>
                            </div>
                            <button type="button" class="asb-btn-back"><i class="fa fa-mail-reply"></i> Đóng</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                <div class="ct-account">
                    <div class="account-pane">
                        <div class="head">
                            <h2 class="title">Kiểm tra bảo hành</h2>
                        </div>
                        <div class="account-pro-tb">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Sản phẩm</th>
                                        <th>Tên khách hàng</th>
                                        <th>Ngày xuất kho</th>
                                        <th>Ngày hết bảo hành</th>
                                        <th>Ước tính thời gian con lại</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                	 @foreach ($guarantees as $val)
	                                    <tr>
                                            <td>{!! $val->product_name !!}</td>
	                                        <td>
	                                            {!! $val->name !!}
	                                        </td>
	                                        <td>{!! $val->start_date !!}</td>
	                                        <td>{!! $val->end_date !!}</td>
	                                        <td>@php

                                                $date_now = date("Y-m-d");
                                                $end_date = date("Y-m-d", strtotime($val->end_date) ) ;
                                                $days = (strtotime($end_date) - strtotime($date_now)) / (60 * 60 * 24);

                                                @endphp
                                                Còn {{ $days }} ngày
                                            </td>
	                                       
	                                    </tr>
                                    @endforeach
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                             
                    </div>


                </div>
                </div>
            </div>
        </div>
 </div>
 @else
  <div class="account-wrap bg">
        <div class="container">
 <h1 style="text-align: center;color: #red;font-size: 20px;">Không tìm thấy</h1>
</div>
</div>
@endif

@endsection
