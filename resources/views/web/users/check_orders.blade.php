@extends('web.layouts.app')
@section('content')

@if(!empty($order))
 <div class="account-wrap bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="sb-account stick-scroll">
                        <button type="button" class="asb-btn"><i class="fa fa-cog"></i></button>
                        <div class="ct">
                            <div class="not-login">
                                <h4 class="title">Chào mừng trở lại!</h4>
                                <p>Vui lòng đăng nhập để có thể quản lý, huỷ hoặc đổi trả đơn hàng của bạn</p>
                                <a class="smooth ctrl" href="#" title="">Đăng nhập</a>
                            </div>
                            <button type="button" class="asb-btn-back"><i class="fa fa-mail-reply"></i> Đóng</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                <div class="ct-account">
                    <div class="account-pane">
                        <div class="head">
                            <h2 class="title">Chi tiết đơn hàng</h2>
                        </div>
                        <div class="content">
                            <div class="order-info">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Đơn hàng: {{ $order->code_order }}</p>
                                        <p><span>Ngày đặt: {{ $order->created_at }}</span></p>
                                    </div>
                                    <!-- <div class="col-md-6 text-right left-767">
                                        <div class="total">Tổng cộng: <strong>23.825.000đ</strong></div>
                                    </div> -->
                                </div>
                            </div>
                            <p>Nhận hàng vào Thứ 5  28 thg 06 - Thứ 3 03 thg 7</p>
                            <div class="order-process">
                                <div class="row col-mar-0">
                                    <div class="col-4 prs-item @if($order->status == 2) active @endif current">
                                        <div class="item ">
                                            <div class="icon"><i class="icon_loading"></i></div>
                                            <p>Đang xử lý</p>
                                        </div>
                                    </div>
                                    <div class="col-4 prs-item @if($order->status == 3) active @endif text-center">
                                        <div class="item">
                                            <div class="icon"><i class="ic ic-truck"></i></div>
                                            <p>Đang giao hàng</p>
                                        </div>
                                    </div>
                                    <div class="col-4 prs-item @if($order->status == 1) active @endif text-right">
                                        <div class="item">
                                            <div class="icon"><i class="icon_check_alt"></i></div>
                                            <p>Hoàn thành đơn hàng</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
						@php
							$list_product = json_decode(base64_decode($order->content),1);
							$total = 0;
						@endphp
                        <div class="account-pro-tb">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="big">Sản phẩm</th>
                                        <th>Giá</th>
                                        <th>Số lượng</th>
                                        <th>Giảm giá</th>
                                        <th>Tạm tính</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                	 @foreach ($list_product as $val)
                                	 	@php
                                	 		$total = $total+ @$val['price']*@$val['qty'];
                                	 	@endphp
	                                    <tr>
	                                        <td>
	                                            <a class="img" href="#" title="">
	                                                <img src="{{ @$val['image'] }} " alt="" title="" />
	                                            </a>
	                                            <a class="smooth" href="#" title="">{{ @$val['name'] }}</a>
	                                        </td>
	                                        <td>{{ @$val['price']}}</td>
	                                        <td>{{ @$val['qty'] }}</td>
	                                        <td>0đ</td>
	                                        <td>{{ @$val['price']* @$val['qty'] }} đ</td>
	                                        <td><a class="smooth" href="#" title="">Hủy</a></td>
	                                    </tr>
                                    @endforeach
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="account-pro-total">
                            <div class="row">
                                <div class="col-lg-3 col-md-6">
                                    Mã giảm giá: <strong>Đang cập nhật</strong>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    Tổng tạm tính: <strong>{{ $total }}đ</strong>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    Phí vận chuyển: <strong>25.000đ</strong>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <strong>Tổng cộng: <span>{{ $total }}đ</span></strong>
                                </div>
                            </div>
                        </div>

                        <div class="content">
                            <div class="row">
                                <div class="col-md-6">
                                    <p><strong>Thông tin người nhận</strong></p>
                                    <div class="receiver-box">
                                        <p>{{ $order->name }}</p>
                                        <p>Địa chỉ: {{ $order->address }}</p>
                                        <p>Địện thoại: {{$order->phone}}</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <p><strong>Thông tin người nhận</strong></p>
                                    <div class="receiver-box">
                                        <p>Thanh toán khi nhận hàng</p>
                                        <p><span>(laptop247hn sẽ đến thu tiền và giao sản phẩm tại địa chỉ của Quý khách)</span></p>
                                        <p><span>Phí vận chuyển: 25.000đ</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>      
                    </div>


                </div>
                </div>
            </div>
        </div>
 </div>
 @else
  <div class="account-wrap bg">
        <div class="container">
 <h1 style="text-align: center;color: #red;font-size: 20px;">Không tìm thấy đơn hàng</h1>
</div>
</div>
@endif

@endsection
