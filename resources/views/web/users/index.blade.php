@extends('web.layouts.app')
@section('content')


<div class="account-wrap bg">
    <div class="container">
        <div class="row">
            @include('web.users.sidebar')
            <div class="col-lg-9">
            <div class="ct-account">
                <div class="account-pane">
                    <div class="head">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h2 class="title">thông tin tài khoản</h2>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <form class="profile-fr" style="max-width: 725px">
                            <div class="fr-line">
                                <div class="text">Họ và tên</div>
                            <input type="text" value="{{ @Auth::user()->name }}" placeholder="Họ và tên" name="">
                            </div>
                            <div class="fr-line">
                                <div class="text">Điện thoại</div>
                                <input type="text" value="{{ @Auth::user()->phone }}" placeholder="Điện thoại" name="">
                            </div>
                            <div class="fr-line">
                                <div class="text">Email</div>
                                <input type="text" value="{{ @Auth::user()->email }}" placeholder="Email" name="">
                            </div>
                            <div class="fr-line">
                                <div class="text">Giới tính</div>
                                <label class="ac-radio">
                                    <input type="radio" name="gender"><i></i> Nam
                                </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="ac-radio">
                                    <input type="radio" name="gender"><i></i> Nữ
                                </label>
                            </div>
                            <div class="fr-line">
                                <div class="text">Ngày sinh <small>(Không bắt buộc)</small></div>
                                <div class="row col-mar-5">
                                    <div class="col-4">
                                        <div class="i-select">
                                            <select>
                                                <option>Ngày</option>
                                                <option>01</option>
                                                <option>02</option>
                                                <option>03</option>
                                                <option>04</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="i-select">
                                            <select>
                                                <option>Tháng</option>
                                                <option>01</option>
                                                <option>02</option>
                                                <option>03</option>
                                                <option>04</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="i-select">
                                            <select>
                                                <option>Năm</option>
                                                <option>1990</option>
                                                <option>1991</option>
                                                <option>1992</option>
                                                <option>1993</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fr-line">
                                <label class="ac-check">
                                    <input type="checkbox" class="show-change-pass"><i></i> Thay đổi mật khẩu
                                </label>
                            </div>
                            <div class="change-pass">
                                <div class="fr-line">
                                    <div class="text">Mật khẩu cũ</div>
                                    <input type="password" placeholder="Nhập khẩu cũ" name="">
                                </div>
                                <div class="fr-line">
                                    <div class="text">Mật khẩu mới</div>
                                    <input type="password" placeholder="Mật khẩu mới từ 6-32 ký tự" name="">
                                </div>
                                <div class="fr-line">
                                    <div class="text">Nhập lại</div>
                                    <input type="password" placeholder="Nhập lại mật khẩu mới" name="">
                                </div>
                            </div>
                            <div class="fr-line">
                                <button class="submit smooth">Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>



@endsection