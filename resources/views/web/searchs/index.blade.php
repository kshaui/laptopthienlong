@extends('web.layouts.app')
@section('content')
<div class="h-wrap">
	 
	 <div class="container">
	 		<h1 style="height: 50px;line-height: 50px;font-size: 20px;">Danh sách tìm kiếm sản phẩm cho từ khóa "{!! $keyword !!}"</h1>
        <div class="row">
            <div class="col-lg-12 order-lg-last">
                <div class="ctrl-head">
                    <div class="row align-items-center" id="align-items-center">
                        <div class="col-md-5">
                            <p>Có tất cả <strong>{{ count($products) }}</strong> sản phẩm</p>
                        </div>
                    </div>
                </div>
                <div class="row_laptop247">
                   
                   @if(isset($products) && count($products)>0)
                     @foreach($products as $value) 
                        @php
                            $agv_product = $agv_product_collect->where('type_id',$value->id)->avg('value');
                            $avg1 = round( $agv_product, 1);  
                             $count =  $agv_product_collect->where('type_id',$value->id)->count();
                        @endphp 
                            @include('web.layouts.data_product')
                        @endforeach 
                    @endif
                    
                </div>
               
            </div>
        </div>
    </div>

    
     <div class="container">
     	<h1 style="height: 50px;line-height: 50px;font-size: 20px;">Danh sách tìm kiếm tin tức cho từ khóa "{!! $keyword !!}"</h1>
        <div class="row">
        	<div class="col-lg-12">
            <div class="ctrl-head">
                <div class="row align-items-center" id="align-items-center">
                    <div class="col-md-5">
                        <p>Có tất cả <strong>{{ count($news) }}</strong> tin tức</p>
                    </div>
                </div>
            </div>
			@foreach ($news as $value)
				<div class="sm-post video clearfix">
					<a class="img" href="{!! $value->getUrl() !!}" title="">
						<img src="/template-admin/images/loading1.gif" data-original="{{ $value->getImage() }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->name }}" />
					</a>
					<div class="ct">
					<h3 class="title"><a class="smooth" href="{!! $value->getUrl() !!}" title="">{{ $value->name }}</a></h3>
					</div>
				</div>
			@endforeach
		</div>
        </div>
    </div>
		
</div>
@endsection

@section('foot')
	<script>
		$(document).ready(function(){
			$('#header .search i').on('click', function(){
				$('#form-header').submit();
			});
		});
	</script>
@endsection