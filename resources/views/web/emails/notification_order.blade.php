<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h3>Thông tin khách hàng</h3>
	<p>Họ tên: {!! $data['name'] !!}</p>
	<p>SĐT: {!! $data['phone'] !!}</p>
	<p>Địa chỉ: {!! $data['address'] !!}</p>
	<p>Ghi chú: {!! $data['note'] !!}</p>
	<p><a href="{!! config('app.url').'/admin_1996/order' !!}">Đi đến trang đơn hàng</a></p>
</body>
</html>