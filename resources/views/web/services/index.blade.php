@extends('web.layouts.app')
@section('head')

@if(strpos($_SERVER['HTTP_USER_AGENT'], 'coc_coc_browser') === false)
<script type="application/ld+json">
{
    "@graph": [{
            "@context": "http://schema.org/",
            "@type": "service",
            "sku": "{{$service->id}}",
            "id": "{{$service->id}}",
            "mpn": "MobileCity",
            "name": "{{$service->name}}",
            "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($service->detail),150) : $meta_seo['description']}}",
            "image": "{{$service->getImage()}}",
            "brand": "{{$service->name}}",
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "5",
                "reviewCount": "5"
            },
			"review": {
				"@type": "Review",
				"author": "Tran Le Hai",
				"reviewRating": {
					"@type": "Rating",
					"bestRating": "5",
					"ratingValue": "1",
					"worstRating": "1"
				}
			},
            "offers": {
                "@type": "Offer",
                "priceCurrency": "VND",
                "price": "{{ $service->price }}",
                "priceValidUntil": "2019-12-31",
                "availability": "http://schema.org/InStock",
                "url": "{{route('web.services.show',$service->slug)}}",
                "warranty": {
                    "@type": "WarrantyPromise",
                    "durationOfWarranty": {
                        "@type": "QuantitativeValue",
                        "value": "6 tháng",
                        "unitCode": "ANN"
                    }
                },
                "itemCondition": "mới",
                "seller": {
                    "@type": "Organization",
                    "name": "{{config('app.name')}}"
                }
            }
        },
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "name": "{{config('app.name')}}",
            "url": "{{config('app.url')}}"
        }
    ]
}
</script>
@endif
@endsection
@section('content')

@php
$type = 'services';
$data_id = $service->id;
@endphp

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <div class="pr-dt-left">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="pro-img">
                                <div class="item slick-slide">
                                    <a class="img" href="#" title="{{ $service->name }}" rel="gal">
                                        <img style="height: 100%;object-fit: cover" class="lazy" src="{{ $service->getImage('large') }}" class="cloudzoom" data-zoom="{{ $service->getImage('large') }}"
                                             alt="{{ @$service->slug }}" title="{{ @$service->name }}">
                                    </a>
                                </div>
                            </div>
                            <div class="pro-thumb">
                                <div class="item slick-slide">
                                    <div class="img">
                                        <img   src="{{ $service->getImage('small') }}" class="lazy" data-original="{{ $service->getImage('small') }}" alt="$service->slug">
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="pro-detail">
                           
                            <h1 class="title">{{ @$service->name }}</h1>
                            <div class="table info">
                                <div class="cell">Mã SP: <strong>{{ @$service->sku?? 'Đang cập nhật' }}</strong></div>
                                <div class="cell">
                                    <div class="cell">
                                    @if ($count_vote>0) 
                                        @foreach ($agv_collect as $key=>$value)
                                            <div class="star-base">
                                               @php
                                                    $avg1 = round( $value, 1);   
                                                @endphp                                  
                                                @include('web.layouts.star')  
                                            </div>
                                            <a class="smooth review" href="#comments" title="">{{(@$count_vote)}} reviews</a>
                                        @endforeach
                                    @else
                                        <div class="star-base">
                                            @for($i = 1; $i <=5; $i++)    
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            @endfor  
                                        </div>
                                        <a class="smooth review" href="#comments" title="">{{(@$count_vote)}} reviews</a>

                                    @endif
                                    <a class="smooth like" href="#" title=""><i class="ic ic-heart liked"></i></a>
                                </div>
                                </div>
                            </div>
                            <div class="price">
                                 <strong>{{ $service->getPrice()}}</strong>
                            </div>
                            <div class="row des s-content">
                                <div class="col-md-6">
                                       <p><i class="fa fa-check-circle"></i> <span>Bảo hành:</span> {{ $service->warranty}} tháng</p>                                       
                                </div>
                                <div class="col-md-6">
                                     {!! @$service->infor !!}
                                </div>
                            </div>
                            @if(!empty($service->promotion))
                                <div class="sale-off">
                                    <h4 class="i-title"><i class="icon_gift_alt"></i> Khuyến mãi</h4>
                                    <div class="ct">
                                        @php
                                            $promotion = preg_split('/\n|\r\n/',$service->promotion);
                                        @endphp
                                        @foreach($promotion as $value)
                                        <div class="item"><i class="icon_check_alt"></i>  
                                            {!! $value !!}
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                           @else
                                @if(!empty($config_general['promotion_service']) && isset($config_general['promotion_service']))
                                    @php
                                       $promotion_config =  preg_split('/\n|\r\n/',$config_general['promotion_service']);
                                    @endphp
                                    <div class="sale-off">
                                        <h4 class="i-title"><i class="icon_gift_alt"></i> Khuyến mãi</h4>
                                        <div class="ct">
                                            @foreach($promotion_config as $value)
                                            <div class="item"><i class="icon_check_alt"></i>  
                                                {!! $value !!}
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            @endif
                            @if(!empty($service->warehouse_status))
                                <div @if( $service->warehouse_status != 1 ) class="un-stock" @endif style="margin: 30px 0;" > <span>Kho hàng:</span>{{ $service->warehouse_status == 1 ? 'CÒN HÀNG':'HẾT HÀNG'}} </div>
                                <br>
                            @endif 
                           <p id="order_service">
                                <a href="javascript:;" class="buy-order-service" >Đặt lịch hẹn sửa chữa</a>
                                <a href="">Liên hệ với chúng tôi</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="pr-dt-right">
                <div class="pr-dt-sb">
                    <div class="item">
                        <i class="ic ic-truck2"></i> Hỗ trợ giao hàng toàn quốc
                    </div>
                    <div class="item">
                        <i class="ic ic-house2"></i> Cam kết chính hãng 100%
                    </div>
                    <div class="item">
                        <i class="ic ic-arm"></i> Bảo hành chính hãng {{ $service->warranty}} tháng
                    </div>
                    <div class="item">
                        <i class="ic ic-one"></i> {{ @$config_general['doi_tra']}}
                    </div>
                </div>
                <div class="pr-dt-sb">
                    <div class="item">
                        <i class="ic ic-phone-o"></i> Liên hệ mua hàng HOTLINE: <a class="smooth" href="tel:@$config_general['hotline'] !!}"
                                                                                   title=""
                                                                                   rel="nofollow,noindex"><strong>{!! @$config_general['hotline'] !!}</strong></a>
                        <p>
                            <small>({{ @$config_general['fa_clock_o'] }})</small>
                        </p>
                    </div>
                    <div class="item">
                        <i class="ic ic-mail-o"></i> Email: <a class="smooth" href="mailto:{!! @$config_general['email'] !!}" title="Email"
                                                               rel="nofollow,noindex">{!! @$config_general['email'] !!}</a>
                    </div>
                </div>
                <div class="pr-dt-sb">
                    <div class="item v2">
                        <i class="icon_pin_alt"></i><strong>Cửa hàng còn hàng</strong>
                    </div>
                    <div class="ct">
                        <p>{{ @$config_general['address'] }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="pro-content">
    <div class="container">
        <div class="nav nav-tabs pr-ct-tab">
            <a class="active" data-toggle="tab" href="#mo-ta-san-pham">Mô tả sản phẩm</a>
            <a class="" data-toggle="tab" href="#thong-so-ky-thuat">Bảng giá</a>
        </div>
    </div>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="mo-ta-san-pham">
                            <h2 class="i-title">{{ $service->name }}</h2>
                            <div class="expand-box">
                                <div class="s-content">
                                     {!! @$service->detail !!}
                                </div>
                            </div>
                            <button type="" class="expand-btn hidden">Xem thêm</button>
                        </div>
                        <div class="tab-pane fade" id="thong-so-ky-thuat">
                            <div class="s-content">
                                {!! @$service->price_table !!}
                            </div>
                        </div>
                    </div>

                    <div class="extension-pro">
                        <h3 class="h-title v2">Sản phẩm bán chạy</h3>
                        <div class="extension-cas">
                            @if(isset($products_banchay))
                                @foreach($products_banchay as $value)
                                    <div class="slick-slide">
                                        <div class="sm-pro" >
                                            <a class="img" href="{{ $value->getUrl() }}" title="">
                                                <img src="{{ $value->getImage('medium') }}" data-original="{{ $value->getImage('medium') }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->name }}"/>
                                            </a>
                                            <h3 class="title"><a class="smooth" href="{{ $value->getUrl() }}" title="">{{ $value->name }}</a></h3>
                                            <div class="price">
                                                <strong>{{ $value->getPrice()}}</strong>
                                                <del>{{ $value->getPriceOld() }} </del>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                           
                        </div>
                    </div>

                    <div class="extension-pro">
                        <h3 class="h-title v2">Dịch vụ liên quan</h3>
                        <div class="extension-cas">
                            @if(isset($relate_service))
                                @foreach($relate_service as $value)
                                    <div class="slick-slide">
                                        <div class="sm-pro" >
                                            <a class="img" href="{{ $value->getUrl() }}" title="">
                                                <img src="{{ $value->getImage('medium') }}" data-original="{{ $value->getImage('medium') }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->name }}"/>
                                            </a>
                                            <h3 class="title"><a class="smooth" href="{{ $value->getUrl() }}" title="">{{ $value->name }}</a></h3>
                                            <div class="price">
                                                <strong>{{ $value->getPrice()}}</strong>
                                                
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="comment-box" id="comments">
                        <h3 class="i-title">{{(@$count_vote)}} đánh giá {{ @$service->name }}</h3>
                        <div class="review-box">
                            <div class="row align-items-center">
                                <div class="col-xl-8">
                                    <div class="table">
                                        <div class="cell text-center">
                                            <div class="point">{{ @$avg1 }} <i class="icon_star"></i></div>
                                            <p>{{(@$count_vote)}} đánh giá</p>
                                        </div>
                                        <div class="cell">
                                           
                                            <!-- @dump($votes) -->
                                                <div class="review-pane">
                                                    @if(isset($votes) && count($votes)>0)
                                                         @php
                                                            $nami = 0;
                                                        @endphp
                                                        @foreach($votes as $key=>$value)
                                                            @if($value == 5)
                                                                @php 
                                                                    $nami++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $nam = round( (($nami)/(@$count_vote)),1 )*100;
                                                        @endphp
                                                    @endif
                                                    <div class="num">5 <i class="icon_star"></i></div>
                                                    <div class="bar"><span style="width: {{ @$nam ?? 0 }}%"></span></div>
                                                    <div class="text">{{ (@$nami) }} đánh giá</div>   
                                                </div>
                                                <div class="review-pane">
                                                    @if(isset($votes) && count($votes)>0)
                                                         @php
                                                            $boni = 0;
                                                        @endphp
                                                        @foreach($votes as $key=>$value)
                                                            @if($value == 4)
                                                                @php 
                                                                    $boni++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $bon = round( (($boni)/(@$count_vote)),1 )*100;
                                                        @endphp
                                                    @endif
                                                    <div class="num">4 <i class="icon_star"></i></div>
                                                    <div class="bar"><span style="width: {{ $bon ?? 0 }}%"></span></div>
                                                    <div class="text">{{ @$boni ?? 0}} đánh giá</div>
                                                </div>
                                                <div class="review-pane">
                                                    @if(isset($votes) && count($votes)>0)
                                                         @php
                                                            $bai = 0;
                                                        @endphp
                                                        @foreach($votes as $key=>$value)
                                                            @if($value == 3)
                                                                @php 
                                                                    $bai++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $ba = round( (($bai)/(@$count_vote)),1 )*100;
                                                        @endphp
                                                    @endif
                                                    <div class="num">3 <i class="icon_star"></i></div>
                                                    <div class="bar"><span style="width: {{ $ba ?? 0  }}%"></span></div>
                                                    <div class="text">{{ $bai ??0}} đánh giá</div>
                                                </div>
                                                <div class="review-pane">
                                                    @if(isset($votes) && count($votes)>0)
                                                         @php
                                                            $haii = 0;
                                                        @endphp
                                                        @foreach($votes as $key=>$value)
                                                            @if($value == 2)
                                                                @php 
                                                                    $haii++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $hai = round( (($haii)/(@$count_vote)),1 )*100;
                                                        @endphp
                                                    @endif
                                                    <div class="num">2 <i class="icon_star"></i></div>
                                                    <div class="bar"><span style="width: {{ $hai??0  }}%"></span></div>
                                                    <div class="text">{{ $haii ?? 0}} đánh giá</div>
                                                </div>
                                                <div class="review-pane">
                                                     @if(isset($votes) && count($votes)>0)
                                                         @php
                                                            $moti = 0;
                                                        @endphp
                                                        @foreach($votes as $key=>$value)
                                                            @if($value == 1)
                                                                @php 
                                                                    $moti++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $mot = round( (($moti)/(@$count_vote)),1 )*100;
                                                        @endphp
                                                    @endif
                                                    <div class="num">1 <i class="icon_star"></i></div>
                                                    <div class="bar"><span style="width: {{ $mot??0  }}%"></span></div>
                                                    <div class="text">{{$moti??0}} đánh giá</div>
                                                </div>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 text-center left-1199 center-767">
                                    <a class="smooth review-btn" href="#" title="">Gửi đánh giá của bạn</a>
                                    <div class="star-rating">
                                        <div class="star-base">
                                           @if(isset($service))
                                            @php
                                            $vote = 0;
                                            if(isset($_COOKIE['vote_'.$service->id])){
                                                $vote = $_COOKIE['vote_'.$service->id];
                                            }
                                            @endphp
                                            <div class="rating-action" data-id="{!! $service->id !!}" data-type="services">
                                            @for($i = 1; $i<= 5; $i ++)
                                                @if($vote != 0)
                                                    @if($i <= $vote)
                                                        <a class="rating-action-btn rating-action-btn1" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
                                                    @else
                                                        <a class="rating-action-btn rating-action-btn1" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star-o" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
                                                    @endif
                                                @else
                                                    <a class="rating-action-btn rating-action-btn2" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star-o" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
                                                @endif
                                            @endfor
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(isset($orders) && count($orders)>0 )
                            <div class="top-review" id="top-review">
                                @foreach($orders as $value)
                                    <div class="item">
                                        <div class="head">
                                            <strong>{{ $value->name }}</strong> <span><i class="icon_check_alt"></i> Đã mua tại laptop247hn.com</span>
                                        </div>
                                        <div class="ct">
                                            <div class="rate-result">
                                                <i class="icon_star act"></i>
                                                <i class="icon_star act"></i>
                                                <i class="icon_star act"></i>
                                                <i class="icon_star act"></i>
                                                <i class="icon_star"></i>
                                            </div>
                                            <p>{!! $value->note ?? 'nhân viên nhiệt tình, vui vẻ, phục vụ tận tình, máy mượt, đa nhiệm tốt, sạc pin nhanh.' !!}</p>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="pagination_top_review" id="pagination_top_review">
                                    {!! $orders->links() !!}
                                </div>
                            </div>
                            <hr>
                        @endif
                        @include('web.layouts.comment')
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="stick-scroll">
                        <div class="pr-dt-sb sb-s-pro v2">
                            <!-- <form method="" action=""> -->
                            <div class="pro clearfix">
                                <a class="img" href="{{ $service->getUrl() }}" title="">
                                    <img src="{{ $service->getImage() }}" alt="" title=""/>
                                </a>
                                <h3 class="title"><a class="smooth" href="javascript:;"  title="">{{ $service->name }}</a></h3>
                            </div>
                            <div class="line"><p>Giá:</p> <strong>{{ $service->getPrice()}}</strong></div>
                            <button type="" href="javascript:;"  class="submit smooth">Đặt lịch sửa chữa</button>
                            <!-- </form> -->
                        </div>
                        <div class="pr-dt-sb v2">
                            <div class="item">
                                <i class="ic ic-truck2"></i> Hỗ trợ giao hàng toàn quốc
                            </div>
                            <div class="item">
                                <i class="ic ic-house2"></i> Cam kết chính hãng 100%
                            </div>
                            <div class="item">
                                <i class="ic ic-arm"></i> Bảo hành chính hãng {{ $service->guarantee}} tháng
                            </div>
                            <div class="item">
                                <i class="ic ic-one"></i>{{ @$config_general['doi_tra']}}
                            </div>
                        </div>
                        <div class="pr-dt-sb v2">
                            <div class="item">
                                <i class="ic ic-phone-o"></i> Liên hệ mua hàng HOTLINE: <a class="smooth" href="tel:{!! @$config_general['hotline'] !!}"
                                                                                           title=""
                                                                                           rel="nofollow,noindex"><strong>{!! @$config_general['hotline'] !!}</strong></a>
                                <p>
                                    <small>(( {{ @$config_general['fa_clock_o'] }}))</small>
                                </p>
                            </div>
                            <div class="item">
                                <i class="ic ic-mail-o"></i> Email: <a class="smooth" href="mailto:{!! @$config_general['email'] !!}"
                                                                       title="Email" rel="nofollow,noindex">{!! @$config_general['email'] !!}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="popup">
	<div class="popup_title">
		<p class="title">{{$service->name}}</p>
		<span class="popup_close"><i class="fa fa-times" aria-hidden="true"></i></span>
	</div>
	<div class="popup_main">
		<div class="popup_product_info">
			<div class="popup-product-image">
				@if($service->image)
	                <img src="{{$service->getImage('medium')}}" alt="{{getAlt($service->getImage('medium'))}}" />
	            @else
	                <img src="{{getImageDefault('')}}" alt="{{getAlt(getImageDefault(''))}}" />
                @endif
			</div>
			<p class="popup_product_title">{{$service->name}}</p>
			{{-- <p class="popup_product_price" data-realprice="Liên hệ">{{$service->price}}</p> --}}
			<p class="popup_product_price" data-realprice="Liên hệ">{{$service->price,true}}</p>
			<div class="clearfix"></div>
			<div class="popup-custom-param-list">
				@if(isset($option_array))
				<div class="popup-custom-param-item">
					<p>Chọn link kiện </p>
					<select class="popup-param-option option_services">
						@for($i = 0; $i < count($option_array); $i++)
							<option value="{{$option_array[$i][1]}}">{{$option_array[$i][0]}}</option>
						@endfor
					</select>
				</div>
				@endif
			</div>
		</div>
		<div class="popup_customer_info">
			<p class="popup_customer_info_title">Thông tin người mua</p>
			<form action="">
				<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
				<input type="hidden" id="type_order" value="services">
				<select id="customer-gender" name="customer-gender">
					<option value="1">Anh</option>
					<option value="2">Chị</option>
				</select>
				<input type="text" id="customer-name" name="customer-name" placeholder="Họ tên bạn (bắt buộc)">
				<input type="text" id="customer-phone" name="customer-phone" placeholder="Số điện thoại (bắt buộc)">
				<input type="text" id="customer-email" name="customer-email" placeholder="Địa chỉ email (bắt buộc)">
				<textarea id="customer-address" name="customer-address" placeholder="Địa chỉ nhận hàng"></textarea>
				<textarea id="order-note" name="order-note" placeholder="Ghi chú đơn hàng"></textarea>
				<button type="button" class="btn-submit-buynow" data-type ="services" data-id="{{$service->id}}">Đặt lịch hẹn sửa chữa</button>
			</form>
		</div>
	</div>
</div>

@endsection
