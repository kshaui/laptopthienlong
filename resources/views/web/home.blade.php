@extends('web.layouts.app')

@section('content')

<div class="h-wrap v2">
    <div class="container">
        <div class="row col-mar-0">
            <div class="col-lg-9 offset-lg-3">
                <div class="row col-mar-0">
                    <div class="col-md-8">
                        <div class="slider-cas">
                            @if(isset($slides) && count($slides)>0 )
                                @foreach($slides as $key=>$value)
                                    <div class="item slick-slide">
                                        <div class="img">
                                            <img src="{{ $value->image }}" alt="" title=""/>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        @if(isset($news_home) && count($news_home)>0)
                            <div class="slide-bn">
                                <div class="banner_right_top">
                                    <div class="title">
                                        <h4 id="title">Tin tức</h4>
                                        <div class="redline">
                                            <div class="dot">
                                                <div class="wave"></div>
                                            </div>
                                            <a href="">Vòng quay may mắn </a>
                                        </div>
                                    </div>
                                   
                                </div>
                                @foreach($news_home as $value)
                                    <div class="row list">
                                        <div class="col-md-4" style="padding-left: 0 !important">
                                            <a href="{{ $value->getUrl() }}">
                                                <img height="50px" width="100px" h src="{{ $value->getImage('small') }}" data-original="{{ $value->getImage('small') }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->slug }}"></a>
                                        </div>
                                        <div class="col-md-8" style="padding-left: 0 !important">
                                            <h3><a href="{{ $value->getUrl() }}">{!! $value->name !!}</a></h3>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--End: header-->
<div class="h-policy">
    <div class="container">
        <div class="row col-mar-0">
            <div class="col-lg-3 col-6 item">
                <div class="policy">
                    <span><img src="/images/icons/icon-sales.png" alt="" title=""/></span>
                    <h3 class="title">Mã giảm giá</h3>
                    <div class="caption smooth">
                        <div class="ct">
                            <h3 class="title">Mã giảm giá</h3>
                            <p>{{ @$config_general['ma_giam_gia'] }}</p>
                            <a class="smooth more" href="#" title="">Xem thêm <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-6 item">
                <div class="policy">
                    <span><img src="/images/icons/icon-24.png" alt="" title=""/></span>
                    <h3 class="title">Hỗ trợ 24/7</h3>
                    <div class="caption smooth">
                        <div class="ct">
                            <h3 class="title">Hỗ trợ 24/7</h3>
                          <p>{{ @$config_general['ho_tro'] }}</p>
                            <a class="smooth more" href="#" title="">Xem thêm <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-6 item">
                <div class="policy">
                    <span><img src="/images/icons/icon-7.png" alt="" title=""/></span>
                    <h3 class="title">Chính sách đổi trả</h3>
                    <div class="caption smooth">
                        <div class="ct">
                            <h3 class="title">Chính sách đổi trả</h3>
                            <p>{{ @$config_general['chinh_sach_doi_tra'] }}</p>
                            <a class="smooth more" href="#" title="">Xem thêm <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-6 item">
                <div class="policy">
                    <span><img src="/images/icons/icon-partner.png" alt="" title=""/></span>
                    <h3 class="title">Khách hàng trung thành</h3>
                    <div class="caption smooth">
                        <div class="ct">
                            <h3 class="title">Khách hàng trung thành</h3>
                            <p>{{ @$config_general['khach_hang_trung_thanh'] }}</p>
                            <a class="smooth more" href="#" title="">Xem thêm <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(isset($product_khuyen_mai_hot) && count($product_khuyen_mai_hot) > 0)
    <div class="h-wrap">
        <div class="container">
            <h2 class="h-title">Sản phẩm <span>khuyến mãi</span></h2>
            <div class="row_laptop247">
                @foreach($product_khuyen_mai_hot as $value)
                     @php
                        $agv_product_khuyen_mai_hot = $agv_khuyen_mai_hot_collect->where('type_id',$value->id)->avg('value');
                        $avg1 = round( $agv_product_khuyen_mai_hot, 1);
                        $count =  $agv_khuyen_mai_hot_collect->where('type_id',$value->id)->count();
                    @endphp
                    @include('web.layouts.data_product',compact('avg1','count'))
                @endforeach
            </div>
        </div>
    </<div>
@endif



@if(isset($products_banchay) && count($products_banchay) >0)
 <div class="h-wrap">
    <div class="container">
        <h2 class="h-title">Sản phẩm <span>bán chạy</span></h2>   
        <div class="row_laptop247">
            @foreach($products_banchay as $key=>$value)
                @php
                    $agv_product_banchay = $agv_product_banchay_collect->where('type_id',$value->id)->avg('value');
                    $avg1 = round( $agv_product_banchay, 1);
                    $count =  $agv_product_banchay_collect->where('type_id',$value->id)->count();
                @endphp
                @include('web.layouts.data_product',compact('avg1','count'))                            
            @endforeach
        </div>
    </div>
</<div>
@endif


@if(isset($brand_home) && count($brand_home)>0 )
 <div class="h-wrap">
    <div class="container">
        <h2 class="h-title">Thương hiệu <span>laptop247hn</span></h2>   
        <div class="row_laptop247">
              
            @foreach($brand_home as $value)
                <a id="item_brand" href="https://laptop247hn.com/laptop?brand={{ $value->id }}">
                    <div class="img">
                        <img class="lazy" src="{{ image_by_link( $value->image ,'large') }}" data-original="{{ image_by_link( $value->image ,'large') }}" alt="{{$value->name}}">
                    </div>
                </a>
            @endforeach
            
        </div>
    </div>
</div>
@endif


@if(isset($products_categories_xu_huong) && count($products_categories_xu_huong) > 0)
@foreach ($products_categories_xu_huong as $key=>$value1)
 <div class="h-wrap">
    <div class="container">
        <h2 class="h-title"> Xu hướng  <span>{{ $value1->name }}</span></h2>
        <div class="row_laptop247">
            @if(isset($product_xu_huong_collect) && count($product_xu_huong_collect)>0 )
                @php
                    $product_xu_huong = $product_xu_huong_collect->where('category_id',$value1->id); 
                @endphp
                @foreach($product_xu_huong as $key=>$value)
                    @php
                        $agv_product_noibat = $agv_product_xu_huon_collect->where('type_id',$value->id)->avg('value');
                        $avg1 = round( $agv_product_noibat, 1);  
                       
                        $count =  $agv_product_xu_huon_collect->where('type_id',$value->id)->count();
                    @endphp 
                    @include('web.layouts.data_product',compact('avg1','count'))
                @endforeach
            @endif
        </div>
    </div>
</<div>
@endforeach
@endif


@if(isset($services_highlight) && count($services_highlight) >0)
 <div class="h-wrap">
    <div class="container">
        <h2 class="h-title">Dịch vụ <span>sửa chữa</span></h2>   
        <div class="row_laptop247">
            @foreach($services_highlight as $value)
                

                 <div class="col-xl-3 col-lg-4 col-sm-6 item">
                    <div class="product v-sm smooth">
                        <a class="img smooth" href="{{ $value->getUrl() }}" title="">
                            <img src="{{ $value->getImage('small') }}" data-original="{{ $value->getImage('small') }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->slug }}"/>
                        </a>
                        <div class="info">
                            <h3 class="title"><a class="smooth" href="{{ $value->getUrl() }}" title="">{{ $value->name }}</a></h3>
                            <div class="rate-like">
                                <div class="star-base">
                                    @if(isset($avg1))
                                        @include('web.layouts.star',compact('avg1'))
                                    @endif
                                </div>
                                <a class="smooth like" href="#" title=""><i class="ic ic-heart"></i></a>
                            </div>
                            <div class="price">
                                <strong>{{ $value->getPrice()}}</strong>
                              
                            </div>
                        
                        </div>
                         @if (!empty($value->price_old))
                            @php
                                $price_tick =(($value->price_old - $value->price)/($value->price_old))*100;
                            @endphp
                        <div class="tick">-{{  round( $price_tick) }}%</div>
                         @endif
                        @if(!empty($value->promotion))
                            <div class="gif"><i class="icon_gift"></i></div>
                        @endif
                    </div>
                </div>


            @endforeach
        </div>
    </div>
</<div>
@endif


<section id="only-pro" class="h-only-pro bg" style="margin-top: 30px">
    <div class="container">
        <h2 class="i-title"><span>Sản phẩm</span>Dành riêng cho bạn</h2>
        <div class="h-pro-cas">
                @foreach($all_product as $value)
                   
                    @include('web.layouts.data_product_home',compact('avg1'))
                @endforeach
        </div>
    </div>
</section>
@endsection

@section('foot')


    @if(isset($config_ads['status_popup']) && $config_ads['status_popup'] == 1)

        <style>

            #popup_banner_beta {

                position: fixed;

                width: 100%;

                height: 100vh;

                z-index: 99999;

                top: 0;

                left: 0

            }



            .mask_popup_banner_beta {

                background: rgba(0, 0, 0, .38);

                cursor: pointer;

                position: absolute;

                width: 100%;

                height: 100vh;

                top: 0;

                z-index: 9;

                left: 0

            }



            .content_popup_banner_beta {

                width: 800px;

                position: absolute;

                top: 50%;

                left: 50%;

                z-index: 10;

                transform: translate(-50%, -50%);

                -moz-transform: translate(-50%, -50%);

                -o-transform: translate(-50%, -50%);

                -webkit-transform: translate(-50%, -50%)

            }



            .close_icon {

                position: absolute;

                top: -50px;

                right: -50px;

                width: 50px;

                cursor: pointer

            }



            @media only screen and (max-width: 480px) {

                .content_popup_banner_beta {

                    width: 300px

                }



                .content_popup_banner_beta a img:nth-of-type(1) {

                    width: 100%

                }



                .close_icon {

                    top: -60px;

                    right: -20px;

                    width: 50px

                }

            }



            .content_popup_banner_beta img#banner_ads {

                width: 100%;

            }

        </style>

        <script>

            var link_image = '{{$config_ads['banner_ads']}}';

            var link = '{{$config_ads['link_popup']}}';

            var icon_close = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAABjUExURQAAAHoqKJETEsAWFYwNDZQICYMlI0dFQdkREXcuK9QSEoMcG5gbGoYSEpYYGNcSEZkaGYsREcgVFagWFZgICNITEskSEtsREHIuLMEZGMAaGYYSEr0bGscZGM8VFH8SEqQWFrMI1wgAAAAadFJOUwBFwNP+/iYE/Q85l3jxWLE33aii6n1a4W/rvkm7xQAABJlJREFUaN61mueaqyAQhlFRwJpo1EQs5/6v8qBrpShY5nn2z+4mb4YpfDMGgDOG0N+P1GL3DW6wPYZnQ/pG4BZDRPZLAL4upDQEN0Gkv+vdoNQGjxnDBrS3DwHoMQZ5DwzoPcZgSQUHhvt6hjG8aTAwOtd7LBiA2H9+fJ45q6FeLHdAdJ9n/BgYgS5jr1EcvMxeGMi4uPQY3luXwewVpkmWJWkW+iZEC44M95BBcqfCTd3gqm7a5pcWRJMx9KqBcVgfOa6rtTVV7eQ6/gQzwz9geGW7ZQycps78I8ZYHbSD8dGpYgmjGs7NifdSYmxWPeOouXutlDFyfl8kx7C+Ho0MesgAiZIxcNoyljNeCyM7Yvi7jMEcX8KI6cw4vqSKQwbLtUzoF+HMoO/jdI+bQ0gfmz6hkZSh09w9XGlYXZW+3A/X0qnZpKn0LJs9iVcMPWXiNa0epMXjh46jhRFo9lGvbOpK78zS3plwYcA30e7XOW71MM3vNeqeP4bRjU5CR4tTV9jpVgH5Gl4/Lz1O868zDQiXAt8Et/oQeFaOEivFu2mwQLrogjTZ56wgxTWdQ6xExZkhXQYuGwpLKWeCdJ97lOIrw0xXKCDsvr1Jj7LyaTl3Zoh9oyL1U1XgP68b1a7vKOoksm6kgMJZQrOCdLC4DdHLcEfaVigNzql0uRZt5JDOzW7zxWoVkL9xGpmey6H64yHQbLhSMYizvgF4CEsykzFR9Z/ZRvGLEAqvp3K8nSokEEqvthhOltWtE4kU95ovpOS6SklQ5lKBc2kNwSs/HA4rrXG+WsmWC77kXA9unT9JbAlnBk+3S2GsaOMp1UOXg8BTrR8Bwo2rVZ0uJTVtUxaMd4YBEuH69dZ/jq/7gkDOM+p8u4IkmcvXvinDFxiYrB0hq5WH/szFmdPwI2ohrG034xBr/bZhtYS8YG1K6Tt4n1UCQDOKhfkBDKuWDsGKEhlpcIdP31Ytr5eJnmmYUD/qIRZU3U7jQMEc/ygKdRniSDzVoep0Z2eiT6wJESfi+mBjtDSA6K3TLAkoBKldJxqrr9kXT4MBSrEOfY2EnI4sso+29ayQxXVLm2rdom84UcARg/yEiGBLr6UGWuXSN6SvkFpYewSNx8DAYF97SRxp9C9X773IZKXHsjqsHRMtatNpFNuhlOKkWBhpkTGXlX2f3UW5uOsoTZ4foXlzmCk2pGQ9isz5m5uKKmuQTPCryi1fnKqx2a2K0PR4C8okX59bgZi/pmua/o3R0MpkYVHkr7ls6z+trcpjxGv4Puy/MyqXvSZjvrii3mdxT8WQ5Gen2f5aFicklqk/sf+eFLmDLx2bKRFPt6pz/VfVL6EsjyW98QsuWEo7/kkwApm4Hby2p7Eht6hkx+WcuHYPpBXtXpxIERhVfnHmJDZ1uKFHzK3rz3IdbtSz2ttya+VL4HEC+Grf0lhCCMdV+eCGVeP2Hby6uXDvai5tED/4pHcw+H6/HUVP9639xVAorgbut216tSF4xDbn9SPPQIrmxr6lHn9Wl3sBHrJVPZbPfVunmGKPX+A5G0agWj2232Ne8qt/yZ3fpfkPLjkNSsPf3E8AAAAASUVORK5CYII=';



            function closePopupBeta() {

                document.getElementById("popup_banner_beta").remove(), setCookie("showPopupBannerBeta", 1, 1)

            }



            function setCookie(e, n, o) {

                var t = "";

                if (o) {

                    var i = new Date;

                    i.setTime(i.getTime() + 24 * 60 * 60 * 1000), t = "; expires=" + i.toUTCString()

                }

                document.cookie = e + "=" + (n || "") + t + "; path=/"

            }



            function getCookie(e) {

                for (var n = e + "=", o = document.cookie.split(";"), t = 0; t < o.length; t++) {

                    for (var i = o[t]; " " == i.charAt(0);) i = i.substring(1, i.length);

                    if (0 == i.indexOf(n)) return i.substring(n.length, i.length)

                }

                return null

            }



            1 != getCookie("showPopupBannerBeta") && (document.addEventListener("DOMContentLoaded", function (event) {

                var e = '<div id="popup_banner_beta"><div onclick="closePopupBeta()" class="mask_popup_banner_beta"></div><div class="content_popup_banner_beta"><img src="' + icon_close + '" class="close_icon" onclick="closePopupBeta()" alt="Close Image"><a href="' + link + '"><img id="banner_ads" src="' + link_image + '" alt="Image Popup Banner"/></a></div></div>';

                setTimeout(function () {

                    document.body.innerHTML += e

                },{{$config_ads['seconds']}})

            }));

        </script>

    @endif



@endsection

  