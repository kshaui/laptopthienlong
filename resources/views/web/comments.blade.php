<div id="detail-comment">
	@csrf
	<div id="detail-comment-tab" class="clearfix">
        <div class="detail-comment-title">Bình luận</div>
        <div class="detail-rating clearfix">
            <span class="title">Đánh giá của bạn:</span>
            @php
	        	$vote = 0;
	        	if(isset($_COOKIE['vote_'.$product->id])){
	        		$vote = $_COOKIE['vote_'.$product->id];
	        	}
	        @endphp
	        <div class="rating-action" data-id="{!! $product->id !!}" data-type="products">
	        	@for($i = 1; $i<= 5; $i ++)
	        		@if($vote != 0)
	            		@if($i <= $vote)
							<a class="rating-action-btn rating-action-btn1" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
	            		@else
							<a class="rating-action-btn rating-action-btn1" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star-o" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
	            		@endif
	        		@else
						<a class="rating-action-btn rating-action-btn2" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star-o" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
	        		@endif
	            @endfor
	        </div>
        </div>
    </div>
    <div id="detail-comment-main">
    	<div id="sudo-comments" data-url="">
    		<div class="comment_form comment_form_addnew" id="new-comment">
		        <textarea class="comment_editor new_comment" placeholder="Mời bạn để lại bình luận ..."></textarea>
		        <div class="comment_form_footer">
		            <div class="comment_form_footer_left"></div>
		            <div class="comment_form_footer_right">
		                <a href="javascript:void(0)" class="comment_btn comment_addnew_btn" data-author="1">Gửi</a>
		            </div>
		        </div>
		        <div class="comment_author_info" style="display: none;">
		            <p class="comment_author_title">Thông tin bình luận</p>
		            <div class="comment_author_info_item">
		                <label><input type="radio" name="comment_author_gender" class="comment_author_gender" value="1" checked="checked"> Anh</label>
		                <label><input type="radio" name="comment_author_gender" class="comment_author_gender" value="0"> Chị</label>
		            </div>
		            <div class="comment_author_info_item">
		                <input type="text" name="comment_author_name" class="comment_author_name" value="" placeholder="Họ và tên">
		            </div>
		            <div class="comment_author_info_item">
		                <input type="text" name="comment_author_email" class="comment_author_email" value="" placeholder="Địa chỉ email">
		            </div>
		            <div class="comment_author_info_item">
		                <input type="text" name="comment_author_phone" class="comment_author_phone" value="" placeholder="Số điện thoại">
		            </div>
		            <div class="comment_author_info_item">
		                <a href="javascript:void(0)" class="comment_btn comment_author_addnew_btn" onclick="addNewComment({!! $product->id !!},'products')">Gửi bình luận</a>
		            </div>
		        </div>
		    </div>
		    @if($comments)
		    <div id="comment_list">
		    	@foreach($comments as $key => $value)
		    		@php
		    			$reply_comment = $value->getReplyComment('products');
		    			$count_reply_comment = $value->countReplyComment('products');
		    		@endphp
			    	<div class="comment_item comment_item_parent" id="comment-item-{!! $value->id !!}">
			    		<div class="comment_item_author">
			                <div class="comment_item_author_avatext">{!! mb_substr($value->name,0,1) !!}</div>
			                <span class="comment_item_author_name">{!! $value->name !!}</span>
			            </div>
			            <div class="comment_item_content">{!! $value->content !!}</div>
			            <div class="comment_item_meta">
			                <a href="javascript:void(0)" class="comment_item_reply" onclick="replyComment({!! $value->id !!},'products')">Trả lời</a> &nbsp;&nbsp;&nbsp; <span class="comment_item_time">{!! change_time_to_text($value->created_at) !!}</span>
			            </div>
			            @if($count_reply_comment > 0)
				        <div class="comment_item_reply_wrap  has_child ">
			            	@foreach($reply_comment as $k => $val)
				            	<div class="comment_item comment_item_child">
		                            <div class="comment_item_author">
		                                <div class="comment_item_author_avatext">{!! mb_substr($val->name,0,1) !!}</div>
		                                <span class="comment_item_author_name">{!! $val->name !!}</span>
		                                @if($val->email == 'is_admin')
		                                <span class="comment_item_author_admin">Quản trị viên</span>
		                                @endif
		                            </div>
		                            <div class="comment_item_content">{!! $val->content !!}</div>
		                            <div class="comment_item_meta">
		                            	@if($val->email == 'is_admin')
		                                	<a href="javascript:void(0)" class="comment_item_reply" onclick="replyComment({!! $value->id !!})">Trả lời</a> &nbsp;&nbsp;&nbsp;
		                                @endif 
		                                <span class="comment_item_time">{!! change_time_to_text($val->created_at) !!}</span>
		                            </div>
		                        </div>
			            	@endforeach
				        </div>
				        @endif
			            <div class="comment_form comment_form_addnew" id="form-reply-{!! $value->id !!}" style="display: none;">
						    <textarea class="comment_editor" placeholder="Nhập nội dung trả lời ..."></textarea>
						    <div class="comment_form_footer">
						        <div class="comment_form_footer_left"></div>
						        <div class="comment_form_footer_right">
						            <a href="javascript:void(0)" class="comment_btn comment_addnew_btn" onclick="showFormInfo({!! $value->id !!})">Gửi</a>
						        </div>
						    </div>
						    <div class="comment_author_info" id="form-info-{!! $value->id !!}" style="display: none;">
						        <p class="comment_author_title">Thông tin bình luận</p>
						        <div class="comment_author_info_item">
						            <label><input type="radio" name="comment_author_gender_{!! $value->id !!}" checked="checked" class="comment_author_gender" value="1"> Anh</label>
						            <label><input type="radio" name="comment_author_gender_{!! $value->id !!}" class="comment_author_gender" value="2"> Chị</label>
						        </div>
						        <div class="comment_author_info_item">
						            <input type="text" name="comment_author_name" class="comment_author_name" value="" placeholder="Họ và tên">
						        </div>
						        <div class="comment_author_info_item">
						            <input type="text" name="comment_author_email" class="comment_author_email" value="" placeholder="Địa chỉ email">
						        </div>
						        <div class="comment_author_info_item">
						            <input type="text" name="comment_author_phone" class="comment_author_phone" value="" placeholder="Số điện thoại">
						        </div>
						        <div class="comment_author_info_item">
						            <a href="javascript:void(0)" class="comment_btn comment_author_addnew_btn" onclick="addReplyComment({!! $value->id !!})">Gửi bình luận</a>
						        </div>
						    </div>
						</div>
			    	</div>
		    	@endforeach
		    </div>
		    @endif
    	</div>
    </div>
</div>