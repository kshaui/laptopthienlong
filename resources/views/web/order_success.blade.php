@extends('web.layouts.app')
@section('content')
	<div class="h-wrap default clear" >
			<div class="container">
				<div class="bk-process">
					<div class="row col-mar-0">
						<div class="col-4 col-item">
							<div class="item active">
								<div class="num">1</div>
								<p>Thông tin giỏ hàng</p>
							</div>
						</div>
						<div class="col-4 col-item text-center">
							<div class="item active">
								<div class="num">2</div>
								<p>đặt hàng thanh toán</p>
							</div>
						</div>
						<div class="col-4 col-item text-right">
							<div class="item active current">
								<div class="num">3</div>
								<p>hoàn tất</p>
							</div>
						</div>
					</div>
				</div>
				<div class="bk-success">
					<h3 class="title"><i class="fa fa-smile-o"></i> CẢM ƠN BẠN ĐÃ ĐẶT HÀNG TẠI TRANTHANH!</h3>
					<p>Đơn hàng được khởi tạo thành công. Mã đơn hàng <strong>{!! $orders->code_order !!}</strong></p>
					<p>Thông tin chi tiết đơn hàng được gửi về {!! $orders->email !!}</p>
					<p><span>Nếu có bất kì thắc mắc hoặc cần thông tin hỗ trợ, bạn vui lòng liên hệ Hotline</span> <a class="smooth" href="tel:{!! $config_general['hotline'] !!}" title="" rel="nofollow,noindex">{!! $config_general['hotline'] !!}</a></p>
					<a class="smooth def-btn" href="/" title="">Tiếp tục mua sắm</a>
				</div>
			</div>
		</div>

@endsection