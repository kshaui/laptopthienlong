@extends('web.layouts.app')
@section('content')

@section('schema')

<script type="application/ld+json">
        {
            "@context":"http://schema.org",
            "@type":"FurnitureStore",
            "@id":"33",
            "url":"https://laptop247hn.com/laptop-dell",
            "logo":"$config_general['logo_header']",
            "image":"$config_general['logo_header']",
            "email": "tranthanhhauinb@gmail.com",
            "founder": "Laptop247hn",
            "description": "{!! @$meta_seo['description'] !!}",
            "name": "Laptop247hn",
            "telephone": "0866.628.426",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Hà nội",
                "addressLocality": "Hà Nội",
                "postalCode": "10000",
                "addressCountry": "VN"
            },
            "openingHoursSpecification": [{
                "@type": "OpeningHoursSpecification",
                    "dayOfWeek": ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],
                    "opens": "08:00","closes": "20:00"
            }],
            "sameAs": [
                "https://www.facebook.com/suachualaptop247.ducthanh",
                   
                    "https://www.youtube.com/channel/UCqayr_yD4W4Ef75IfvLG_7Q?view_as=subscriber"
               
                "https://www.pinterest.com/tranthanhceo/",
               
              
                "https://laptop247hn.com/",
                
                
              ],
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": "20.992266",
                "longitude": "105.804148"
            },
            "aggregateRating":{ 
                "@type":"AggregateRating",
                "ratingValue": "5",
                "reviewCount": "1"
            },
            "priceRange":"từ 1.000.000₫"
        }
        </script>
        <script type="application/ld+json">
            {
                "@context": "http://schema.org/",
                "@type": "Person",
                "name": "Trần Văn Thanh",
                "alternateName": "Tran thanh",
                "url": "https://laptop247hn.com/",
                "image": "",
                "sameAs": [
                    "https://www.facebook.com/suachualaptop247.ducthanh",
                   
                    "https://www.youtube.com/channel/UCqayr_yD4W4Ef75IfvLG_7Q?view_as=subscriber"
                ],
                "jobTitle": "SEO Leader",
                "worksFor": {
                    "@type": "Organization",
                    "name": "Laptop247hn"
                }   
            }
        </script>

@endsection


<div class="h-wrap">
    <div class="p-banner">
        <span class="bg" style="background-image: url({{ @$category->image ??  @$config_general['logo_header']  }});"></span>
        <div class="container">
            <h2 class="title">{{ @$title ?? 'Laptop247hn.com' }}</h2>
            <p>{!! @$category->detail !!}</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 order-lg-last">
                <div class="ctrl-head">
                    <div class="row align-items-center" id="align-items-center">
                        <div class="col-md-5">
                            <p>Có tất cả <strong>{{ count($products) }}</strong> sản phẩm</p>
                        </div>
                        <div class="col-md-7 text-right xs-left">
                            <div class="i-select">
                                <form action="" method="get" id="form-sort" >
                                   <select name="sort_price" id="sort_price">
                                        <option value="new">Mới nhất</option>
                                        <option value="asc">Giá thấp nhất</option>
                                        <option value="desc">Giá cao nhất</option>
                                    </select>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
                    <div class="row_laptop247">
                       @if(isset($products))
                         @foreach($products as $value) 
                            @php
                                $agv_product = $agv_product_collect->where('type_id',$value->id)->avg('value');
                                $avg1 = round( $agv_product, 1);  
                                 $count =  $agv_product_collect->where('type_id',$value->id)->count();
                            @endphp 
                                @include('web.layouts.data_product')
                            @endforeach 
                        @endif 
                    </div>
                <div class="text-center" id="pagnation" >
                    <a class="view-show-product" data-type="products"  data-count="{{$products->count()}}" href="javascript:;" >Xem thêm</a>
                </div>
            </div>
            <div class="col-lg-3 order-lg-first">
                <div class="stick-scroll">
                     @include('web.layouts.sidebar')
                </div>
            </div>
        </div>
    </div>
    <br>
</div>    



@endsection
@section('foot')
	<script>
		$(document).ready(function(){

            $('html, body').animate({scrollTop: $("#align-items-center").offset().top}, 1000);

			$('body').on('click','#sidebar .cate i.fa-plus',function (){
				$(this).addClass('fa-minus');
				$(this).removeClass('fa-plus');
				$(this).parent('li').find('.sub').slideToggle(400);
			});
			$('body').on('click','#sidebar .cate i.fa-minus',function (){
				$(this).addClass('fa-plus');
				$(this).removeClass('fa-minus');
				$(this).parent('li').find('.sub').slideToggle(400);
			});
			$('#form-filter input').on('click', function(){
				var name = $(this).attr('name');
				var checked = $(this).attr('checked');
				if(checked == 'checked'){
					$('input[name="'+name+'"]').attr('disabled','disabled');
					$('#form-filter').submit();
				}
			});
			$('#form-filter input').on('change', function(){
				$('#form-filter').submit();
			});
			$('#sort_price').on('change', function(){
				$('#form-sort').submit();
			});
			$('.btn-show-detail').on('click',function(){
				var attr = $(this).attr('data-attr');
				if(attr=='show'){
					$('.content-detail').css('height','auto');
					$(this).attr('data-attr','hide');
					$(this).html('<p><i class="fa fa-long-arrow-up" aria-hidden="true"></i>Ẩn bớt<i class="fa fa-long-arrow-up" aria-hidden="true"></i></p>');
				}else{
					$('.content-detail').animate({height:"200"},1000);
					$(this).attr('data-attr','show');
					$(this).html('<p><i class="fa fa-long-arrow-down" aria-hidden="true"></i>Xem thêm<i class="fa fa-long-arrow-down" aria-hidden="true"></i></p>');
				}
			});
		});
	</script>
@endsection