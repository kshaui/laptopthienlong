@extends('web.layouts.app')
@section('head')
<link rel="stylesheet" href="/assets/css/comment.css">
@endsection
@section('content')
@php
$type = 'products';
$data_id = $product->id;
@endphp



<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <div class="pr-dt-left">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="pro-img">
                                <div class="item slick-slide">
                                    <a class="img" href="#" title="{{ @$product->name }}" rel="gal">
                                        <img style="height: 100%;object-fit: cover" src="{{ @$product->image }}" class="cloudzoom" data-zoom="{{ @$product->image }}"
                                             alt="{{ @$product->name }}" title="{{ @$product->name }}">
                                    </a>
                                </div>
        
                            @php
                                $slides = explode(',', $product->slides);
                            @endphp
                            @if($product->slides != '')
                            @foreach($slides as $value)
                                <div class="item slick-slide">
                                    <a class="img" href="#" title="" rel="gal">
                                        <img  src="{!! $value !!}" style="height: 100%;object-fit: cover" class="cloudzoom"
                                             data-zoom="{!! $value !!}" alt="laptop247hn.com">
                                    </a>
                                </div>
                               @endforeach
                                @endif
                            </div>
                            <div class="pro-thumb">
                                <div class="item slick-slide">
                                    <div class="img">
                                        <img   src="{!! image_by_link(@$product->image,'small') !!}" class="lazy" data-original="{!! image_by_link(@$product->image,'small') !!}" alt="laptop247hn.com">
                                    </div>
                                </div>
                            @if($product->slides != '')
                            @foreach($slides as $value)
                                <div class="item slick-slide">
                                    <div class="img">
                                        <img  src="/template-admin/images/loading1.gif" class="lazy" data-original="{!! image_by_link($value,'small') !!}" alt="laptop247hn.com">
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="pro-detail">
                            <a class="brand-logo" href="#" title="">
                                <!-- <img src="/images/brand-logo.jpg" alt="" title=""/> -->
                            </a>
                            <h1 class="title">{{ @$product->name }}</h1>
                            <div class="table info">
                                <div class="cell">Mã SP: <strong>{{ @$product->sku?? 'Đang cập nhật' }}</strong></div>
                                <div class="cell">
                                    @if ($count_vote>0) 
                                        @foreach ($agv_collect as $key=>$value)
                                            <div class="star-base">
                                               @php
                                                    $avg1 = round( $value, 1);   
                                                @endphp                                  
                                                @include('web.layouts.star')  
                                            </div>
                                            <a class="smooth review" href="#comments" title="">{{(@$count_vote)}} reviews</a>
                                        @endforeach
                                    @else
                                        <div class="star-base">
                                            @for($i = 1; $i <=5; $i++)    
                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                            @endfor  
                                        </div>
                                        <a class="smooth review" href="#comments" title="">{{(@$count_vote)}} reviews</a>

                                    @endif
                                    <a class="smooth like" href="#" title=""><i class="ic ic-heart liked"></i></a>
                                    <!-- <a class="smooth like" href="#" title=""><i class="fa fa-heart"></i></a>  -->
                                </div>
                            </div>
                            <div class="price">
                                 <strong>{{ $product->getPrice()}}</strong>
                                <del>{{ $product->getPriceOld() }} </del>
                            </div>
                            <div class="row des s-content">
                                <div class="col-md-6">
                                       <p><i class="fa fa-check-circle"></i> <span>Bảo hành:</span> {{ $product->guarantee}} tháng</p>
                                        @foreach($brand_home as $brand)
                                            @if($brand->id == $product->brand_id)
                                            <p><i class="fa fa-check-circle"></i> <span>Thương hiệu:</span> {{ $brand->name}} </p> 

                                            @endif
                                        @endforeach
                                         
                                       
                                </div>
                                <div class="col-md-6">
                                     {!! @$product->specifications !!}
                                </div>
                            </div>
                              @if(!empty($product->promotion))
                                <div class="sale-off">
                                    <h4 class="i-title"><i class="icon_gift_alt"></i> Khuyến mãi</h4>
                                    <div class="ct">
                                        @php
                                            $promotion = preg_split('/\n|\r\n/',$product->promotion);
                                        @endphp
                                        @foreach($promotion as $value)
                                        <div class="item"><i class="icon_check_alt"></i>  
                                            {!! $value !!}
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                          
                            @endif
                            @if(!empty($product->warehouse_status))
                                <div @if( $product->warehouse_status != 1 ) class="un-stock" @endif style="margin: 30px 0;" > <span>Kho hàng:</span>{{ $product->warehouse_status == 1 ? 'CÒN HÀNG':'HẾT HÀNG'}} </div>
                                <br>
                            @endif 
                            <p id="order_service" style="margin-top: unset">
                                <a href="javascript:;" onclick="add_to_cart({!! $product->id !!},1);" class="buy_now smooth" >Thêm vào giỏ hàng</a>
                                <a href="">Liên hệ với chúng tôi</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="pr-dt-right">
                <div class="pr-dt-sb">
                    <div class="item">
                        <i class="ic ic-truck2"></i> Hỗ trợ giao hàng toàn quốc
                    </div>
                    <div class="item">
                        <i class="ic ic-house2"></i> Cam kết chính hãng 100%
                    </div>
                    <div class="item">
                        <i class="ic ic-arm"></i> Bảo hành chính hãng {{ $product->guarantee}} tháng
                    </div>
                    <div class="item">
                        <i class="ic ic-one"></i> {{ @$config_general['doi_tra']}}
                    </div>
                </div>
                <div class="pr-dt-sb">
                    <div class="item">
                        <i class="ic ic-phone-o"></i> Liên hệ mua hàng HOTLINE: <a class="smooth" href="tel:@$config_general['hotline'] !!}"
                                                                                   title=""
                                                                                   rel="nofollow,noindex"><strong>{!! @$config_general['hotline'] !!}</strong></a>
                        <p>
                            <small>({{ @$config_general['fa_clock_o'] }})</small>
                        </p>
                    </div>
                    <div class="item">
                        <i class="ic ic-mail-o"></i> Email: <a class="smooth" href="mailto:{!! @$config_general['email'] !!}" title="Email"
                                                               rel="nofollow,noindex">{!! @$config_general['email'] !!}</a>
                    </div>
                </div>
                <div class="pr-dt-sb">
                    <div class="item v2">
                        <i class="icon_pin_alt"></i><strong>Cửa hàng còn hàng</strong>
                    </div>
                    <div class="ct">
                        <p>{{ @$config_general['address'] }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="un-stock-popup">
    <p>Nhập số điện thoại để nhận thông báo khi có hàng lại</p>
    <form class="form">
        <input type="text" placeholder="Số điện thoại">
        <button type="submit">Gửi</button>
    </form>
    <button class="st-close">&times;</button>
</div>

<div class="pro-content">
    <div class="container">
        <div class="nav nav-tabs pr-ct-tab">
            <a class="active" data-toggle="tab" href="#mo-ta-san-pham">Mô tả sản phẩm</a>
            <a class="" data-toggle="tab" href="#thong-so-ky-thuat">Thông số kỹ thuật</a>
        </div>
    </div>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="mo-ta-san-pham">
                            <h2 class="i-title">{{ $product->name }}</h2>
                            <div class="expand-box">
                                <div class="s-content">
                                     {!! @$product->detail !!}
                                </div>
                            </div>
                            <button type="" class="expand-btn hidden">Xem thêm</button>
                        </div>
                        <div class="tab-pane fade" id="thong-so-ky-thuat">
                            <div class="s-content">
                                {!! @$product->description !!}
                            </div>
                        </div>
                    </div>
<!-- 
                    <div class="extension-pro">
                        <h3 class="h-title v2">phụ kiện đi kèm</h3>
                        <div class="extension-cas">
                            <div class="slick-slide">
                                <div class="sm-pro">
                                    <a class="img" href="#" title="">
                                        <img src="/images/pro1.png" alt="" title=""/>
                                    </a>
                                    <h3 class="title"><a class="smooth" href="#" title="">Súng hàn 15AK 5m - dùng cho
                                        máy hàn</a></h3>
                                    <strong>880.000 đ</strong>
                                </div>
                            </div>
                            <div class="slick-slide">
                                <div class="sm-pro">
                                    <a class="img" href="#" title="">
                                        <img src="../front-end/images/pro1.png" alt="" title=""/>
                                    </a>
                                    <h3 class="title"><a class="smooth" href="#" title="">Súng hàn 15AK 5m - dùng cho
                                        máy hàn</a></h3>
                                    <strong>880.000 đ</strong>
                                </div>
                            </div>
                            <div class="slick-slide">
                                <div class="sm-pro">
                                    <a class="img" href="#" title="">
                                        <img src="../front-end/images/pro1.png" alt="" title=""/>
                                    </a>
                                    <h3 class="title"><a class="smooth" href="#" title="">Súng hàn 15AK 5m - dùng cho
                                        máy hàn</a></h3>
                                    <strong>880.000 đ</strong>
                                </div>
                            </div>
                            <div class="slick-slide">
                                <div class="sm-pro">
                                    <a class="img" href="#" title="">
                                        <img src="../front-end/images/pro1.png" alt="" title=""/>
                                    </a>
                                    <h3 class="title"><a class="smooth" href="#" title="">Súng hàn 15AK 5m - dùng cho
                                        máy hàn</a></h3>
                                    <strong>880.000 đ</strong>
                                </div>
                            </div>
                            <div class="slick-slide">
                                <div class="sm-pro">
                                    <a class="img" href="#" title="">
                                        <img src="../front-end/images/pro1.png" alt="" title=""/>
                                    </a>
                                    <h3 class="title"><a class="smooth" href="#" title="">Súng hàn 15AK 5m - dùng cho
                                        máy hàn</a></h3>
                                    <strong>880.000 đ</strong>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <div class="extension-pro">
                        <h3 class="h-title v2">Sản phẩm liên quan</h3>
                        <div class="extension-cas">
                            @if(isset($relate_product))
                                @foreach($relate_product as $value)
                                    <div class="slick-slide">
                                        <div class="sm-pro" >
                                            <a class="img" href="{{ $value->getUrl() }}" title="">
                                                <img src="{{ $value->getImage('medium') }}" data-original="{{ $value->getImage('medium') }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->name }}"/>
                                            </a>
                                            <h3 class="title"><a class="smooth" href="{{ $value->getUrl() }}" title="">{{ $value->name }}</a></h3>
                                            <div class="price">
                                                <strong>{{ $value->getPrice()}}</strong>
                                                <del>{{ $value->getPriceOld() }} </del>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="comment-box" id="comments">
                        <h3 class="i-title">{{(@$count_vote)}} đánh giá {{ @$product->name }}</h3>
                        <div class="review-box">
                            <div class="row align-items-center">
                                <div class="col-xl-8">
                                    <div class="table">
                                        <div class="cell text-center">
                                            <div class="point">{{ @$avg1 }} <i class="icon_star"></i></div>
                                            <p>{{(@$count_vote)}} đánh giá</p>
                                        </div>
                                        <div class="cell">
                                           
                                            <!-- @dump($votes) -->
                                                <div class="review-pane">
                                                    @if(isset($votes) && count($votes)>0)
                                                         @php
                                                            $nami = 0;
                                                        @endphp
                                                        @foreach($votes as $key=>$value)
                                                            @if($value == 5)
                                                                @php 
                                                                    $nami++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $nam = round( (($nami)/(@$count_vote)),1 )*100;
                                                        @endphp
                                                    @endif
                                                    <div class="num">5 <i class="icon_star"></i></div>
                                                    <div class="bar"><span style="width: {{ @$nam ?? 0 }}%"></span></div>
                                                    <div class="text">{{ (@$nami) }} đánh giá</div>   
                                                </div>
                                                <div class="review-pane">
                                                    @if(isset($votes) && count($votes)>0)
                                                         @php
                                                            $boni = 0;
                                                        @endphp
                                                        @foreach($votes as $key=>$value)
                                                            @if($value == 4)
                                                                @php 
                                                                    $boni++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $bon = round( (($boni)/(@$count_vote)),1 )*100;
                                                        @endphp
                                                    @endif
                                                    <div class="num">4 <i class="icon_star"></i></div>
                                                    <div class="bar"><span style="width: {{ $bon ?? 0 }}%"></span></div>
                                                    <div class="text">{{ @$boni ?? 0}} đánh giá</div>
                                                </div>
                                                <div class="review-pane">
                                                    @if(isset($votes) && count($votes)>0)
                                                         @php
                                                            $bai = 0;
                                                        @endphp
                                                        @foreach($votes as $key=>$value)
                                                            @if($value == 3)
                                                                @php 
                                                                    $bai++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $ba = round( (($bai)/(@$count_vote)),1 )*100;
                                                        @endphp
                                                    @endif
                                                    <div class="num">3 <i class="icon_star"></i></div>
                                                    <div class="bar"><span style="width: {{ $ba ?? 0  }}%"></span></div>
                                                    <div class="text">{{ $bai ??0}} đánh giá</div>
                                                </div>
                                                <div class="review-pane">
                                                    @if(isset($votes) && count($votes)>0)
                                                         @php
                                                            $haii = 0;
                                                        @endphp
                                                        @foreach($votes as $key=>$value)
                                                            @if($value == 2)
                                                                @php 
                                                                    $haii++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $hai = round( (($haii)/(@$count_vote)),1 )*100;
                                                        @endphp
                                                    @endif
                                                    <div class="num">2 <i class="icon_star"></i></div>
                                                    <div class="bar"><span style="width: {{ $hai??0  }}%"></span></div>
                                                    <div class="text">{{ $haii ?? 0}} đánh giá</div>
                                                </div>
                                                <div class="review-pane">
                                                     @if(isset($votes) && count($votes)>0)
                                                         @php
                                                            $moti = 0;
                                                        @endphp
                                                        @foreach($votes as $key=>$value)
                                                            @if($value == 1)
                                                                @php 
                                                                    $moti++;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @php
                                                            $mot = round( (($moti)/(@$count_vote)),1 )*100;
                                                        @endphp
                                                    @endif
                                                    <div class="num">1 <i class="icon_star"></i></div>
                                                    <div class="bar"><span style="width: {{ $mot??0  }}%"></span></div>
                                                    <div class="text">{{$moti??0}} đánh giá</div>
                                                </div>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 text-center left-1199 center-767">
                                    <a class="smooth review-btn" href="#" title="">Gửi đánh giá của bạn</a>
                                    <div class="star-rating">
                                        <div class="star-base">
                                           @if(isset($product))
                                            @php
                                            $vote = 0;
                                            if(isset($_COOKIE['vote_'.$product->id])){
                                                $vote = $_COOKIE['vote_'.$product->id];
                                            }
                                            @endphp
                                            <div class="rating-action" data-id="{!! $product->id !!}" data-type="products">
                                            @for($i = 1; $i<= 5; $i ++)
                                                @if($vote != 0)
                                                    @if($i <= $vote)
                                                        <a class="rating-action-btn rating-action-btn1" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
                                                    @else
                                                        <a class="rating-action-btn rating-action-btn1" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star-o" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
                                                    @endif
                                                @else
                                                    <a class="rating-action-btn rating-action-btn2" href="javascript:;" data-point="{!! $i !!}"><i class="fa start fa-star-o" id="fa-star-{!! $i !!}" aria-hidden="true"></i></a>
                                                @endif
                                            @endfor
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(isset($orders) && count($orders)>0 )
                            <div class="top-review" id="top-review">
                                @foreach($orders as $value)
                                    <div class="item">
                                        <div class="head">
                                            <strong>{{ $value->name }}</strong> <span><i class="icon_check_alt"></i> Đã mua tại laptop247hn.com</span>
                                        </div>
                                        <div class="ct">
                                            <div class="rate-result">
                                                <i class="icon_star act"></i>
                                                <i class="icon_star act"></i>
                                                <i class="icon_star act"></i>
                                                <i class="icon_star act"></i>
                                                <i class="icon_star"></i>
                                            </div>
                                            <p>{!! $value->note ?? 'nhân viên nhiệt tình, vui vẻ, phục vụ tận tình, máy mượt, đa nhiệm tốt, sạc pin nhanh.' !!}</p>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="pagination_top_review" id="pagination_top_review">
                                    {!! $orders->links() !!}
                                </div>
                            </div>
                            <hr>
                        @endif
                        @include('web.layouts.comment')
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="stick-scroll">
                        <div class="pr-dt-sb sb-s-pro v2">
                            <!-- <form method="" action=""> -->
                            <div class="pro clearfix">
                                <a class="img" href="{{ $product->getUrl() }}" title="">
                                    <img src="{{ $product->getImage() }}" alt="" title=""/>
                                </a>
                                <h3 class="title"><a class="smooth" href="javascript:;" onclick="add_to_cart({!! $product->id !!},1);" title="">{{ $product->name }}</a></h3>
                            </div>
                            <div class="line"><p>Giá:</p> <strong>{{ $product->getPrice()}}</strong></div>
                            <div class="line"><p>Số lượng:</p>
                                <div class="i-number">
                                    <span class="n-ctrl down smooth"></span>
                                    <input type="text" class="numberic" min="1" max="6" value="1">
                                    <span class="n-ctrl up smooth"></span>
                                </div>
                            </div>
                            <button type="" href="javascript:;" onclick="add_to_cart({!! $product->id !!},1);" class="submit smooth">Mua ngay</button>
                            <!-- </form> -->
                        </div>
                        <div class="pr-dt-sb v2">
                            <div class="item">
                                <i class="ic ic-truck2"></i> Hỗ trợ giao hàng toàn quốc
                            </div>
                            <div class="item">
                                <i class="ic ic-house2"></i> Cam kết chính hãng 100%
                            </div>
                            <div class="item">
                                <i class="ic ic-arm"></i> Bảo hành chính hãng {{ $product->guarantee}} tháng
                            </div>
                            <div class="item">
                                <i class="ic ic-one"></i>{{ @$config_general['doi_tra']}}
                            </div>
                        </div>
                        <div class="pr-dt-sb v2">
                            <div class="item">
                                <i class="ic ic-phone-o"></i> Liên hệ mua hàng HOTLINE: <a class="smooth" href="tel:{!! @$config_general['hotline'] !!}"
                                                                                           title=""
                                                                                           rel="nofollow,noindex"><strong>{!! @$config_general['hotline'] !!}</strong></a>
                                <p>
                                    <small>(( {{ @$config_general['fa_clock_o'] }}))</small>
                                </p>
                            </div>
                            <div class="item">
                                <i class="ic ic-mail-o"></i> Email: <a class="smooth" href="mailto:{!! @$config_general['email'] !!}"
                                                                       title="Email" rel="nofollow,noindex">{!! @$config_general['email'] !!}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('schema')
    <script type="application/ld+json">
    {
        "@graph": [{
                "@context": "http://schema.org/",
                "@type": "Product",
                "sku": "{{$product->sku}}",
                "id": "{!! $product->id !!}",
                "mpn": "laptop247hn",
                "name": "{!! $product->name !!}",
                "description": "{!! cutString(strip_tags($product->detail), 100) !!}",
                "image": "{!! $product->image !!}",
                "brand": "{{ $brand->name ?? 'laptop247hn' }}",
                "aggregateRating": {
                    "@type": "AggregateRating",
                    "ratingValue": "5",
                    "reviewCount": "1"
                },
                "review": {
                    "@type": "Review",
                    "author": "thanh tran",
                    "reviewRating": {
                        "@type": "Rating",
                        "bestRating": "5",
                        "ratingValue": "1",
                        "worstRating": "1"
                    }
                },
                "offers": {
                    "@type": "AggregateOffer",
                    "priceCurrency": "VND",
                    "offerCount": 10,
                    "price": "{!! $product->price ==0? : $product->price!!}",
                    "lowPrice":"{!! $product->price ==0? : $product->price!!}",
                    "highPrice":"{!!$product->price_old == 0 ? : $product->price_old!!}",
                    @if(date('m') < 6)
            "priceValidUntil": "{{date('Y')}}-06-1",
                    @else
            "priceValidUntil": "{{date('Y')}}-12-31",
                    @endif
        "availability": "http://schema.org/InStock",
        "warranty": {
            "@type": "WarrantyPromise",
            "durationOfWarranty": {
                "@type": "QuantitativeValue",
                "value": "{{ $product->guarantee}} tháng",
                            "unitCode": "ANN"
                        }
                    },
                    "itemCondition": "mới",
                    "seller": {
                        "@type": "Organization",
                        "name": "laptop247hn"
                    }
                }
            }
        ]
    }
    </script>

    <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "NewsArticle",
          "mainEntityOfPage": {
            "@type": "WebPage",
            "@id": "https://google.com/article"
          },
          "headline": "Article headline",
          "image": [
            "{!!$product->image!!}"
           ],
          "datePublished": "2015-02-05T08:00:00+08:00",
          "dateModified": "2015-02-05T09:20:00+08:00",
          "author": {
            "@type": "Person",
            "name": "laptop247hn"
          },
           "publisher": {
            "@type": "Organization",
            "name": "Google",
            "logo": {
              "@type": "ImageObject",
              "url": "https://laptop247hn.com/uploads/2019/11/sua-chua-laptop247hn-tiny-1-tiny-1-tiny.PNG"
            }
          },
          "description": "{!!cutString(removeHTML($product->detail),160,'')!!}"
        }
        </script>
    <script type="application/ld+json">
            {
              "@context": "https://schema.org/",
              "@type": "Product",
              "name": "Executive Anvil",
              "image": [
                "{!!$product->image!!}"
               ],
              "description": "{!!cutString(removeHTML($product->detail),160,'')!!}",
              "sku": "{!!$product->id!!}",
              "mpn": "925872",
              "brand": {
                "@type": "Thing",
                "name": "laptop247hn"
              },
              "review": {
                "@type": "Review",
                "reviewRating": {
                  "@type": "Rating",
                  "ratingValue": "5",
                  "bestRating": "5"
                },
                "author": {
                  "@type": "Person",
                  "name": "thanhtran"
                }
              },
              "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "5",
                "reviewCount": "1"
              },
              "offers": {
                "@type": "AggregateOffer",
                "url": "https://example.com/anvil",
                "priceCurrency": "VNĐ",
                "offerCount":10,
                "price": "{!!$product->price!!}",
                "highPrice": "{!!$product->price_old == 0 ? : $product->price_old!!}",
                "lowPrice": "{!!$product->price!!}",
                "priceValidUntil": "2020-11-05",
                "itemCondition": "https://schema.org/UsedCondition",
                "availability": "https://schema.org/InStock",
                "seller": {
                  "@type": "Organization",
                  "name": "laptop247hn"
                }
              }
            }
        </script>
    <script type="application/ld+json">
        {
          "@context": "https://schema.org/",
          "@type": "Review",
          "itemReviewed": {
            "@type": "Restaurant",
            "image": "{!!$product->image!!}",
            "name": "{!!$product->name!!}",
            "servesCuisine": "Seafood",
            "priceRange":"5",
            "telephone": "{{$config_general['hotline'] ?? ''}}",
            "address" :{
              "@type": "PostalAddress",
              "streetAddress": "{{$config_general['address'] ?? ''}}",
              "addressLocality": "Hà Nội",
              "addressRegion": "Việt Nam",
              "postalCode": "10000",
              "addressCountry": "Việt Nam"
            }
          },

          "name": "{!!$product->name!!}",
          "author": {
            "@type": "Person",
            "name": "laptop247hn.com"
          },
          "reviewBody": "{!!cutString(removeHTML($product->detail),160,'')!!}",
          "publisher": {
            "@type": "Organization",
            "name": "laptop247hn.com"
          }
        }
        </script>
@endsection


