@extends('web.layouts.app')
@section('content')
<section id="machinal-pro" class="default clear product_top">
<div class="container">
    <div class="row">
		<div class="col-lg-6">
				<div id = "contact" class="contact">
					<h2 class="title">Công ty TNHH Laptop247hn.com</h2>
					<ul>
						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Địa chỉ: {!! $config_general['address'] !!}</li>
						<li><i class="fa fa-volume-control-phone" aria-hidden="true"></i> Hotline: {!! $config_general['phone'] !!}</li>
						<li><i class="fa fa-envelope-o" aria-hidden="true"></i> Email: {!! $config_general['email'] !!}</li>
					</ul>
					<div class="form">
						<p class="title">Liên hệ với chúng tôi</p>
						@if(session('messages'))
							<div class="alert alert-success">{{ session('messages') }}</div>
						@endif
						<form action="" id="form-contact" method="post">
							@csrf
							<p class="error" style="color: #ff0000;font-size: 14px;"></p>
							<div class="form-inline">
							<input class = "form-control" type="text" placeholder="Họ và tên (*)" value="{{ @Auth::user()->fullname }}" name="name">
							</div>
							<div class="form-inline">
								<input type="text"  class = "form-control" placeholder="Email (*)" value="{{ @Auth::user()->email }}" name="email">
							</div>
							<div class="form-inline">
								<input type="phone" class = "form-control"placeholder="Số điện thoại (*)" value="{{ @Auth::user()->phone }}" name="phone">
							</div>
							<div class="form-inline">
								<textarea name="content" class = "form-control" placeholder="Nội dung (*)"></textarea>
							</div>
							<div class="form-inline">
								<a href="javascript:;" id="btn-send">Gửi liên hệ của bạn</a>
								
							</div>
						</form>
					</div>
				</div>
				
		</div>
		<div class="col-lg-6">
				
			 {!! $config_general['maps_contact'] !!}
				
		</div>
			
	</div>
</div>
</section>
@endsection
@section('foot')
	<script>
		$(document).ready(function(){
			$('#btn-send').on('click', function(){
				var name = $('input[name="name"]').val();
				var email = $('input[name="email"]').val();
				var phone = $('input[name="phone"]').val();
				var content = $('textarea[name="content"]').val();
				$('.error').html('');
				if(name == ''){
					$('.error').html('Họ tên không được để trống !');
					$('input[name="name"]').focus();
				}else if(!isEmail(email)){
					$('input[name="email"]').focus();
					$('.error').html('Nhập sai định dạng Email !');
				}else if(phone == ''){
					$('input[name="phone"]').focus();
					$('.error').html('Số điện thoại không được để trống !');
				}else if(!isPhone(phone)){
					$('input[name="phone"]').focus();
					$('.error').html('Nhập sai định dạng số điện thoại !');
				}else if(content == ''){
					$('textarea[name="content"]').focus();
					$('.error').html('Nội dung liên hệ không được để trống !');
				}else{
					$('#form-contact').submit();
				}
			});
		});
	</script>
@endsection