@extends('web.layouts.app')

@section('head')
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "{{config('app.name')}}",
        "url": "{{config('app.url')}}"
    }, {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "{{config('app.url')}}",
        "@id": "{{config('app.url')}}/#organization",
        "name": "Laptop247hn.com laptop dell, laptop hp, laptop asus, laptop acer, laptop toshiba",
        "logo": "{{config('app.url')}}/public/assets/img/logo.png"
    }
</script>
@endsection
@section('content')


 <div class="h-wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 order-lg-last">
                <div class="ctrl-head">
                    <div class="row align-items-center" id="align-items-center">
                        <div class="col-md-5">
                            <p>Có tất cả <strong>{{ count($services) }}</strong> sản phẩm</p>
                        </div>
                        <div class="col-md-7 text-right xs-left">
                            <div class="i-select">
                                <form action="" method="get" id="form-sort" >
                                   <select name="sort_price" id="sort_price">
                                        <option value="new">Mới nhất</option>
                                        <option value="asc">Giá thấp nhất</option>
                                        <option value="desc">Giá cao nhất</option>
                                    </select>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
                    <div class="row_laptop247">
                        @foreach($services as $value)
                            <div class="col-xl-3 col-lg-4 col-sm-6 item">
                                <div class="product v-sm smooth">
                                    <a class="img smooth" href="{{ $value->getUrl() }}" title="">
                                        <img src="{{ $value->getImage('medium') }}" data-original="{{ $value->getImage('medium') }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->slug }}"/>
                                    </a>
                                    <div class="info">
                                        <h3 class="title"><a class="smooth" href="{{ $value->getUrl() }}" title="">{{ $value->name }}</a></h3>
                                        <div class="rate-like">
                                            <div class="star-base">
                                                @if(isset($avg1))
                                                    @include('web.layouts.star',compact('avg1'))
                                                @endif
                                            </div>
                                            <a class="smooth like" href="#" title=""><i class="ic ic-heart"></i></a>
                                        </div>
                                        <div class="price">
                                            <strong>{{ $value->getPrice()}}</strong>
                                          
                                        </div>
                                    
                                    </div>
                                     @if (!empty($value->price_old))
                                        @php
                                            $price_tick =(($value->price_old - $value->price)/($value->price_old))*100;
                                        @endphp
                                    <div class="tick">-{{  round( $price_tick) }}%</div>
                                     @endif
                                    @if(!empty($value->promotion))
                                        <div class="gif"><i class="icon_gift"></i></div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                <div class="pagination text-center">
                    {!! $services->links() !!}
                </div>
            </div>
            <div class="col-lg-3 order-lg-first">
                <div class="stick-scroll">
                    <div class="sidebar">
                        <h3 class="sb-title2">Danh mục sửa chữa laptop</h3>
                        <div class="sb-ct hc-collapse">
                            <ul>
                                @foreach($services_categories_11 as $value)
                                    <li><a class="smooth" href="{{ $value->getUrl() }}" title="">{{ $value->name }} </a> 
                                        <ul @if($slug_parent == $value->slug) style="display: block;" @endif>
                                            @php
                                                $chiled_services_categories = $chiled_services_categories_collect->where('parent_id',$value->id);
                                            @endphp
                                            @foreach($chiled_services_categories as $val)
                                                <li>
                                                    <a class="smooth @if($slug == $val->slug) active @endif" href="{{ $val->getUrl() }}" title="">
                                                        {{ $val->name }}   
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</<div>


@endsection