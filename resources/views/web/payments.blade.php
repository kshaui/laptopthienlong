@extends('web.layouts.app')
@section('head')
	<link rel="stylesheet" href="/assets/select2.min.css">
	
	<style>
		.select2-container .select2-selection--single {
		    height: 45px;
		}
		.select2-container--default .select2-selection--single .select2-selection__rendered {
		    color: #737373;
		    line-height: 45px;
		    font-size: 16px;
		}
		.select2-container--default .select2-selection--single .select2-selection__arrow {
		    top: 6px;
		}
		.select2-container--default .select2-selection--single {
		    border: 1px solid #d9d9d9;
            border-radius: 4px;
           
        }
        .select2-container--default{
            width: 100% !important;
            border-radius: 0 !important;
        }
	</style>
@endsection
@section('content')

 <section id="machinal-pro" class="clear default">
    <div class="container">
        <div class="bk-process">
            <div class="row col-mar-0">
                <div class="col-4 col-item">
                    <div class="item active">
                        <div class="num">1</div>
                        <p>Thông tin giỏ hàng</p>
                    </div>
                </div>
                <div class="col-4 col-item text-center">
                    <div class="item active current">
                        <div class="num">2</div>
                        <p>đặt hàng thanh toán</p>
                    </div>
                </div>
                <div class="col-4 col-item text-right">
                    <div class="item">
                        <div class="num">3</div>
                        <p>hoàn tất</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- <form method="" action=""> -->
        <div class="row">
            <div class="col-md-4">
                <div class="payment-box">
                    <h3 class="i-title">thông tin giao hàng</h3>
                    <p class="i-des">Bạn đã có tài khoản? <a class="smooth orange" href="#" title="">Đăng nhập</a> để
                        thanh toán nhanh hơn!</p>
                    <div class="pay-fr-line">
					<input class="name-form input" value="{{ @Auth::user()->name }}" type="text" placeholder="Họ tên (*)">
						<p class="error error-name"></p>
                    </div>
                    <div class="pay-fr-line">
						<input class="address-form input" type="text" value="{{ @Auth::user()->adress }}" placeholder="Địa chỉ nhận hàng (*)" name="">
						<p class="error error-address"></p>
                    </div>
                    <div class="pay-fr-line">
						<input class=" phone-form input" type="number" value="{{ @Auth::user()->phone }}" placeholder="Số điện thoại (*)" name="">
						<p class="error error-phone"></p>
                    </div>
                    <div class="pay-fr-line">
						<input class="email-form input" type="text" value="{{ @Auth::user()->email }}" placeholder="Email (*)" name="">
						<p class="error error-email"></p>
                    </div>
                    <div class="pay-fr-line">
                        <div class="i-select">
							<select  class="input" id="province_id">
								<option value="0">-- Chọn tỉnh thành --</option>
								@foreach($provinces as $value)
									<option value="{!! $value->id !!}">{!! $value->name !!}</option>
								@endforeach
							</select>
                        </div>
                    </div>
                    <div class="pay-fr-line">
                        <div class="i-select">
							<select name="" class="input" id="district_id">
								<option value="0">-- Chọn quận huyện --</option>
							</select>
                        </div>
                    </div>
                    <div class="pay-fr-line">
                        <textarea class="note-form textarea" placeholder="Ghi chú đơn hàng"></textarea>
					</div>
					<input type="hidden" class="payments-form" value="1">
                </div>
            </div>
            <div class="col-md-4">

					{{-- <div class="payments-info info">
							<p class="title">Vận chuyển</p>
							<div class="form-inline">
								<p><i class="fa fa-check-circle" aria-hidden="true"></i> Giao hàng tận nơi <span>0 VNĐ</span></p>
							</div>
							<p class="title">Hình thức thanh toán</p>
							<div class="form-inline form-payments">
								<p><i data-payment="1" class="fa fa-check-circle" aria-hidden="true"></i> Thanh toán khi giao hàng (COD)</p>
								<p><i data-payment="2" class="fa fa-circle" aria-hidden="true"></i> Thanh toán chuyển khoản (<a target="_blank" href="{!! $config_general['payments'] !!}" style="font-size: 12px;
								color: #155593">Hướng dẫn</a>)</p>
								<p class="last-child">Bạn chỉ phải thanh toán khi nhận được hàng</p>
							</div>
						</div> --}}

                <div class="payment-box">
                    <h3 class="i-title">chọn phương thức thanh toán</h3>
                    <div class="payment-collapse">
                        <div class="item">
                            <div class="head">Thanh toán khi giao hàng (COD) <i class="icon_wallet"></i>
                            </div>
                            <div class="ct">
                                Thanh toán khi quý khách nhận hàng
                            </div>
                        </div>
                        <div class="item">
                            <div class="head">
                                Thanh toán Chuyển khoản <i class="icon_currency"></i>
                            </div>
                            <div class="ct">
                                <p><strong>Vietcombank</strong></p>
                                <p>01224188123812</p>
                                <p>Nguyễn Văn A</p>
                                <p>Vietcombank Hà Nội</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="head">
                                ATM nội địa <i class="fa fa-credit-card"></i>
                            </div>
                            <div class="ct">
                                <p><strong>Vietcombank</strong></p>
                                <p>01224188123812</p>
                                <p>Nguyễn Văn A</p>
                                <p>Vietcombank Hà Nội</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="head">
                                Visa <i class="fa fa-cc-visa"></i>
                            </div>
                            <div class="ct">
                                <p><strong>Vietcombank</strong></p>
                                <p>01224188123812</p>
                                <p>Nguyễn Văn A</p>
                                <p>Vietcombank Hà Nội</p>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="payment-box vat-box">
                    <div class="vat-btn">
                        <h3 class="title">yêu cầu xuất hóa đơn VAT cho đơn hàng này</h3>
                        <p>Vui lòng điền đầy đủ thông tin để xuất hóa đơn GTGT</p>
                    </div>
                    <div class="vat-pane">
                        <div class="row col-mar-5">
                            <div class="col-sm-6">
                                <div class="pay-fr-line">
                                    <input class="input" type="text" placeholder="Mã số thuế" name="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pay-fr-line">
                                    <input class="input" type="text" placeholder="Tên công ty" name="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pay-fr-line">
                                    <input class="input" type="text" placeholder="Địa chỉ công ty" name="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="pay-fr-line">
                                    <input class="input" type="text" placeholder="Người đai diện" name="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                 {{-- <div class="payment-box font-f3">
                    <a class="smooth orange" href="#" title="">Thay đổi thông tin giỏ hàng</a>
                </div>  --}}
            </div>
            <div class="col-md-4">
                <div class="cart-info">
                    <h4 class="i-title">Thông tin đơn hàng ({!! Cart::count() !!} sản phẩm)</h4>
                    <div class="cart-info-tb">
                        <table>
							@foreach($cart as $value)
								<tr>
									<td>
										<a class="img" href="#" title="">
											<img  src="/template-admin/images/loading1.gif" data-original="{!! image_by_link($value->options['image'],'tiny') !!}" class="lazy"  alt="" title=""/>
											<span>{!! $value->qty !!}</span>
										</a>
									</td>
									<td><a class="smooth" href="#" title="">{!! $value->name !!}</a></td>
									<td class="text-right"><strong>{!! number_format($value->price*$value->qty) !!} VNĐ</strong></td>
								</tr>
							@endforeach
                          
                        </table>
                    </div>
                    {{-- <div class="change-cart-info">
                        <a class="smooth" href="#" title="">Thay đổi thông tin giỏ hàng</a>
                    </div>
                    <div class="coupon-label">
                        <label>Mã giảm giá <strong>0123AA</strong></label>
                        <p>Giảm <span>5% (62.000đ)</span> cho các sản phẩm máy khoan</p>
                    </div> --}}
                    <div class="table">
                        <div class="line">
                            <div class="cell">Tạm tính</div>
                            <div class="cell text-right"><strong>{!! Cart::subtotal(0,',') !!} VNĐ</strong></div>
                        </div>
                        <div class="line">
                            <div class="cell">Phí giao hàng</div>
                            <div class="cell text-right"><strong>miễn phí</strong></div>
                        </div>
                        <div class="line">
                            <div class="cell" style="vertical-align: top">Tổng cộng</div>
                            <div class="cell text-right"><strong class="orange"
                                                                 style="line-height: 1">{!! Cart::subtotal(0,',') !!} VNĐ</strong>
                                <p style="color: #7a7a7a">(Giá đã bao gồm thuế VAT)</p>
                            </div>
                        </div>
                    </div>
					
					<button type="button" class="booking-btn smooth "  id="btn-order">Thanh toán</button>
            
                </div>
            </div>
        </div>
        <br>
        <!-- </form> -->
    </div>
</section>

@endsection

@section('foot')
<script type="text/javascript" src="/assets/select2.min.js"></script>
<script>
	function isEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}
	function isPhone(phone) {
		var regex = /^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/;
		return regex.test(phone);
	}
	$(document).ready(function(){
		$('.form-payments i').on('click', function(){
			var data_payment = $(this).attr('data-payment');
			$('.form-payments i').removeClass('fa-check-circle');
			$('.form-payments i').addClass('fa-circle');
			$(this).addClass('fa-check-circle');
			$(this).removeClass('fa-circle');
			$('input.payments-form').val(data_payment);
		});
	});
	$('#btn-order').on('click',function(){
		var name=$('.name-form').val();
		var phone=$('.phone-form').val();
		var email = $('.email-form').val();
		var address=$('.address-form').val();
		var province_id=$('#province_id').val();
		var district_id=$('#district_id').val();
		var note=$('.note-form').val();
		var payments=$('.payments-form').val();
		var token = $("input[name='_token']").val();
		$('.payments .form-inline input').css('border', '1px solid #d9d9d9');
		$('.payments .form-inline .error').html('');
		if(name==''){
			$('.name-form').focus();
			$('.error-name').html('Vui lòng nhập họ tên');
			$('.name-form').css('border','2px solid #c23527');
		}else if(!isEmail(email)){
			$('.email-form').focus();
			$('.error-email').html('Vui lòng nhập đúng định dạng Email');
			$('.email-form').css('border','2px solid #c23527');
		}else if(!isPhone(phone)){
			$('.error-phone').html('Vui lòng nhập đúng định dạng số điện thoại');
			$('.phone-form').focus();
			$('.phone-form').css('border','2px solid #c23527');
		}else if(address == ''){
			$('.address-form').focus();
			$('.error-address').html('Vui lòng nhập địa chỉ');
			$('.address-form').css('border','2px solid #c23527');
		}else{
			$.ajax({
		        type: 'POST',
		        dataType: 'json',
		        data: {"_token":token,name:name,phone:phone,email:email,address:address,note:note,payments:payments,province_id:province_id,district_id:district_id},
		        url: '/ajax/payment',
		        success: function (data) {
		        	if(data==1){
		        		window.location.href="/dat-hang-thanh-cong";
		        	}
		        },
		        beforeSend: function(){
		        	$('#btn-order').html('Đang xử lý <img style="width: 20px;position: absolute;top: 8px;right: 8px;" src="/assets/images/loading2.gif" alt="">');
		        	$('#btn-order').css('opacity', 0.5);
		        }
		    });
		}
	});
	$(document).ready(function(){
		var height_web = $(window).height();
		$('.payments .right').css('height', height_web-20);
		$('#province_id').select2();
		$('#district_id').select2();
		
		$('#province_id').on('change', function(){
			var province_id = $(this).val();
			var token = $('input[name="_token"]').val();
			$.ajax({
				type: 'post',
				dataType: 'json',
				data: {"_token":token, province_id : province_id},
				url: '/ajax/get-district',
				success:function(data){
					$('#district_id').html(data.html);
				}
			});
		});
	})
</script>
@endsection