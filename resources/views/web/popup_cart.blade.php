
<div id="popup-cart">
	<div class="title-popup-cart">Bạn đã thêm <a href="{!! route('web.products.show', $product->slug) !!}" class="cart-popup-name">{!! $product->name !!}</a> vào giỏ hàng</div>
	<div class="close-popup" onclick="closepopup()"><i class="fa fa-times" aria-hidden="true"></i></div>
	<div class="title-quantity-popup">
		<a href="/cart">Giỏ hàng của bạn có <span class="cart-popup-count">{!! $count_cart !!}</span> sản phẩm</a>
	</div>
	<div class="content-popup-cart">
		<div class="thead-popup">
			<div style="width: 40%;" class="text-left">Sản phẩm</div>
			<div style="width: 20%;" class="text-center">Đơn giá</div>
			<div style="width: 20%;" class="text-center">Số lượng</div>
			<div style="width: 20%;" class="text-center">Thành tiền</div>
		</div>
		<div class="tbody-popup">
		@foreach($cart as $value)
			<div class="item-popup" id="item-popup-{!! $value->rowId !!}" data-id="{!! $value->rowId !!}">
				<div class="item-image" style="width: 10%;">
					<img class="product-image" src="{!! $value->options['image'] !!}" width="80">
				</div>
				<div class="item-info">
					<p class="item-name"><a target="_blank" href="{!! route('web.products.show', $value->options['slug']) !!}">{!! $value->name !!}</a></p>
					<p class="remove-item" onclick="remove_cart('{!! $value->rowId !!}');"><i class="fa fa-times" aria-hidden="true"></i> Bỏ sản phẩm</p>
				</div>
			
				<div class="item-price">
					<span class="price pricechange">{!! number_format($value->price) !!} VNĐ</span>
				</div>
				<div class="item-amount">
					<div class="tru" data-id="{!! $value->rowId !!}">-</div>
					<div class="value">
						<input type="text" value="{!! $value->qty !!}" disabled class="number-product" name="qty">
					</div>
					<div class="cong" data-id="{!! $value->rowId !!}">+</div>
				</div>
				<div class="cart-price">
					<span class="price">{!! number_format($value->qty*$value->price) !!} VNĐ</span>
				</div>
			</div>
		@endforeach
		</div>
		<div class="tfoot-popup">
			<div class="foot-popup-1">
				<div class="pull-left popupcon"><a onclick="closepopup()" data-fancybox-close class="btn-continue"><i class="fa fa-caret-left" aria-hidden="true"></i> Tiếp tục mua hàng</a></div>
				<div class="pull-right popup-total"><p>Thành tiền: <span class="total-price">{!! Cart::subtotal(0,',') !!} VNĐ</span></p></div>
			</div>
			<div class="foot-popup-2"><a class="btn-proceed-checkout" href="/gio-hang">Thanh toán đơn hàng</a></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#popup-cart .size').on('change', function(){
			var size_id = $(this).val();
			var rowId = $(this).closest('.item-popup').attr('data-id');
			$.ajax({
	        	type: 'post',
	        	dataType: 'json',
	        	data: {rowId:rowId, size_id:size_id},
	        	url: '/ajax/update-size',
	        	success:function(){
	        	}
	        });
		});
		$('#popup-cart .color').on('change', function(){
			var color_id = $(this).val();
			var rowId = $(this).closest('.item-popup').attr('data-id');
			$.ajax({
	        	type: 'post',
	        	dataType: 'json',
	        	data: {rowId:rowId, color_id:color_id},
	        	url: '/ajax/update-color',
	        	success:function(data){
	        	}
	        });
		});
	});
</script>