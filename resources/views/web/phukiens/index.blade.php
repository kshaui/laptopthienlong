@extends('web.layouts.app')
@section('content')


    
<section id="machinal-pro" class="h-wrap default clear">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 order-lg-last">
                <div class="ctrl-head">
                    <div class="row align-items-center">
                        <div class="col-md-5">
                            <p>Có tất cả <strong>{{ count($phukien_products) }}</strong> sản phẩm</p>
                        </div>
                        <div class="col-md-7 text-right xs-left">
                            <div class="i-select sort">
							<form action="" method="get" id="form-sort" style="float: right;">
				
							<select name="sort_price" id="sort_price">
								<option value="new">Mới nhất</option>
								<option value="asc">Giá thấp nhất</option>
								<option value="desc">Giá cao nhất</option>
							</select>
						</form>
                            </div>
                          
                        </div>
                    </div>
                </div>

                <div class="products-page">
                    <div class="row" style="margin: 0;border-top: 1px solid #f3f3f3;;">
						@if(isset($phukien_products))
                         @foreach($phukien_products as $value) 
                                @include('web.layouts.data_phukien_home',compact('value'))
                            @endforeach 
						@endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        {!! $phukien_products->links() !!}
                       
                    </div>
                </div>

               
            </div>
        
        </div>
    </div>
</section>

@endsection
@section('foot')
	<script>
		$(document).ready(function(){
			$('body').on('click','#sidebar .cate i.fa-plus',function (){
				$(this).addClass('fa-minus');
				$(this).removeClass('fa-plus');
				$(this).parent('li').find('.sub').slideToggle(400);
			});
			$('body').on('click','#sidebar .cate i.fa-minus',function (){
				$(this).addClass('fa-plus');
				$(this).removeClass('fa-minus');
				$(this).parent('li').find('.sub').slideToggle(400);
			});
			$('#form-filter input').on('click', function(){
				var name = $(this).attr('name');
				var checked = $(this).attr('checked');
				if(checked == 'checked'){
					$('input[name="'+name+'"]').attr('disabled','disabled');
					$('#form-filter').submit();
				}
			});
			$('#form-filter input').on('change', function(){
				$('#form-filter').submit();
			});
			$('#sort_price').on('change', function(){
				$('#form-sort').submit();
			});
			$('.btn-show-detail').on('click',function(){
				var attr = $(this).attr('data-attr');
				if(attr=='show'){
					$('.content-detail').css('height','auto');
					$(this).attr('data-attr','hide');
					$(this).html('<p><i class="fa fa-long-arrow-up" aria-hidden="true"></i>Ẩn bớt<i class="fa fa-long-arrow-up" aria-hidden="true"></i></p>');
				}else{
					$('.content-detail').animate({height:"200"},1000);
					$(this).attr('data-attr','show');
					$(this).html('<p><i class="fa fa-long-arrow-down" aria-hidden="true"></i>Xem thêm<i class="fa fa-long-arrow-down" aria-hidden="true"></i></p>');
				}
			});
		});
	</script>
@endsection