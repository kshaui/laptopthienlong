@extends('web.layouts.app')
@section('head')
<link rel="stylesheet" href="/assets/css/comment.css">
@endsection
@section('content')
@php
$type = 'phukien_products';
$data_id = $product_phukien->id;
@endphp
<div class="h-wrap default clear">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="pr-dt-left">
                    <div class="row">
                        <div class="col-xl-6 ">
                            <div class="pro-img">
                                <div class="item slick-slide">
                                    <a class="img" href="#" title="{{ @$product_phukien->name }}" rel="gal">
                                        <img src="{{ @$product_phukien->image }}" class="cloudzoom" data-zoom="{{ @$product_phukien->getImage() }}"
                                             alt="{{ @$product_phukien->name }}" title="{{ @$product_phukien->name }}">
                                    </a>
                                </div>
        
                            @php
                                $slides = explode(',', $product_phukien->slides);
                            @endphp
                            @if($product_phukien->slides != '')
                            @foreach($slides as $value)
                                <div class="item slick-slide">
                                    <a class="img" href="#" title="" rel="gal">
                                        <img  src="{!! $value !!}" class="cloudzoom"
                                             data-zoom="{!! $value !!}" alt="laptop247hn.com">
                                    </a>
                                </div>
                               @endforeach
                                @endif
                            </div>
                            <div class="pro-thumb">
                                <div class="item slick-slide">
                                    <div class="img">
                                        <img  src="/template-admin/images/loading1.gif" class="lazy" data-original="{!! image_by_link(@$product->image,'small') !!}" alt="laptop247hn.com">
                                    </div>
                                </div>
                            @if($product_phukien->slides != '')
                            @foreach($slides as $value)
                                <div class="item slick-slide">
                                    <div class="img">
                                        <img  src="/template-admin/images/loading1.gif" class="lazy" data-original="{!! image_by_link($value,'small') !!}" alt="laptop247hn.com">
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="pro-detail ">
                                <a class="brand-logo" href="#" title="">
                                    <img src="{!! @$config_general['logo_header'] !!}"  alt="laptop247hn.com" title="laptop247hn.com"/>
                                </a>
                                @if (!empty($product_phukien->price_old))
                                    @php
                                        $price_tick =(($product_phukien->price_old - $product_phukien->price)/($product_phukien->price_old))*100;
                                    @endphp
                                        <div class="tick" id="tick_khuyen_mai">-{{  round( $price_tick) }}%</div>
                                        <div id="dep"></div>
                                @endif
                                <h2 class="title">{{ @$product_phukien->name }}</h2>                            
                                <div class="table info">
                                    <div class="cell">Mã SP: <strong>{{ @$product_phukien->sku }}</strong></div>
                                    @if(!empty($product_phukien->guarantee))
                                        <div class="cell">
                                            Bảo hành: <strong>{{ $product_phukien->guarantee}} tháng</strong> 
                                        </div>
                                    @endif
                                    <div class="cell">
                                       
                                      
                                    </div>
                                </div>
                                <div class="price_list">
                                <div class="price">{{ $product_phukien->getPrice()}} </div>
                                <del>{{ $product_phukien->getPriceOld() }} </del>
                            </div>
                               
                        
                                @if (!empty($gift_product))
                                <div class="sale-off">
                                    <h4 class="i-title"><i class="fa fa-gift"></i> Khuyến mãi</h4>
                                    <div class="ct">
                                        <div class="item">
                                        {!! @$gift_product->description !!}
                                        </div>
                                       
                                    </div>
                                </div>
                                @else
                                    @if(!empty($product_phukien->promotion))
                                    <div class="sale-off">
                                        <h4 class="i-title"><i class="fa fa-gift"></i> Khuyến mãi</h4>
                                        <div class="ct">
                                            <div class="item">
                                            {!! @$product_phukien->promotion !!}
                                            </div>
                                        
                                        </div>
                                    </div>
                                    @else
                                     <div class="sale-off">
                                        <h4 class="i-title"><i class="fa fa-gift"></i> Khuyến mãi</h4>
                                        <div class="ct">
                                            <div class="item">
                                            {!! @$config_general['promotion_laptop'] !!}
                                             </div>
                                        
                                        </div>
                                    </div>
                                    @endif
                                @endif
                                <div class="ctrl">
                                    <button type="submit" onclick="add_to_cart({!! $product_phukien->id !!},1);" class="buy_now smooth">Mua hàng</button>
                                </div>
                                <p class="phone-call">Gọi đặt hàng: <a class="xanh" href="tel:{{ @$config_general['phone'] }}"><i class="fa fa-phone-square" aria-hidden="true"></i> {{ @$config_general['phone'] }} </a>(7:30 - 22:00) </p>
                                
                            </div>
                        </div>
                    </div>
                </div>

            
            </div>
            <div class="col-lg-3">
                <div class="pr-dt-right">
                    <div class="pr-dt-sb">
                        <div class="item">
                            <i class="ic ic-truck2"></i> Hỗ trợ giao hàng toàn quốc
                        </div>
                        <div class="item">
                            <i class="ic ic-house2"></i> Cam kết chính hãng 100%
                        </div>
                        @if(!empty($product_phukien->guarantee))
                        <div class="item">
                            <i class="ic ic-arm"></i> Bảo hành chính hãng {{ $product_phukien->guarantee}} tháng
                        </div>
                        @endif
                        <div class="item">
                            <i class="ic ic-one"></i>{{ @$config_general['doi_tra']}}
                        </div>
                    </div>
                    <div class="pr-dt-sb">
                        <div class="item">
                            <i class="ic ic-phone-o"></i> Liên hệ mua hàng HOTLINE: <a class="smooth" href="tel:{!! @$config_general['hotline'] !!}"
                                                                                       title=""
                                                                                       rel="nofollow,noindex"><strong>
                            {!! @$config_general['hotline'] !!}</strong></a>
                            <p>
                                <small>( {{ @$config_general['fa_clock_o'] }})</small>
                            </p>
                        </div>
                        <div class="item">
                            <i class="ic ic-mail-o"></i> Email: 
                            <a class="smooth" href="mailto:{!! @$config_general['email'] !!}" title="Email"
                                                                   >{!! @$config_general['email'] !!}</a>
                        </div>
                    </div>
                    <div class="pr-dt-sb">
                        <div class="item v2">
                            <i class="icon_pin_alt"></i><strong>Cửa hàng còn hàng</strong>
                        </div>
                        <div class="ct">
                        <p>{{ @$config_general['address'] }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pro-content default clear product_top">
    <div class="container">
        <div class="nav nav-tabs pr-ct-tab">
            <a class="active" data-toggle="tab" href="#mo-ta-san-pham">Mô tả sản phẩm</a>
            <a class="" data-toggle="tab" href="#thong-so-ky-thuat">Thông số kỹ thuật</a>
        </div>
    </div>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="mo-ta-san-pham">
                            <div class="expand-box">
                                <div class="s-content">
                                    {!! @$product_phukien->detail !!}
                                </div>
                            </div>
                            <button type="" class="expand-btn hidden">Xem thêm</button>
                        </div>
                        <div class="tab-pane fade" id="thong-so-ky-thuat">
                            <div class="s-content">
                                {!! @$product_phukien->description !!}
                            </div>
                        </div>
                    </div>

                    <div class="default" style="    clear: both;">
                            <h5 class="h-title v2">Bình luận</h5>
                            @include('web.layouts.comment')
                    </div>

               
                    <div class="extension-pro default clear">
                        <h5 class="h-title v2">Sản phẩm liên quan</h5>
                        <div class="extension owl-carousel relate_product" style="max-width: 100%">
                            @if(isset($relate_product))
                            @foreach($relate_product as $value)

                                @include('web.layouts.data_product_home',compact('value'))
                            @endforeach
                            @endif
                        </div>
                    </div>

                  
                </div>
                <div class="col-lg-3">
                    <div class="stick-scroll">
                        <div class="pr-dt-sb sb-s-pro v2">                            
                            <div class="pro clearfix">
                                <a class="img" href="#" title="">
                                    <img  src="/template-admin/images/loading1.gif" class="lazy" data-original="{!! $product_phukien->getImage() !!}"  alt="" title=""/>
                                </a>
                                <h3 class="title"><a class="smooth" href="#" title="">{{ @$product_phukien->name }}</a></h3>
                            </div>
                            <div class="line"><p>Giá:</p> <strong>{!! number_format(@$product_phukien->price) !!} vnd</strong></div>
                            <button  onclick="add_to_cart({!! @$product_phukien->id !!},1);" class="submit smooth">Mua ngay</button>                    
                        </div>
                        <div class="pr-dt-sb v2">
                            <div class="item">
                                <i class="ic ic-truck2"></i> Hỗ trợ giao hàng toàn quốc
                            </div>
                            <div class="item">
                                <i class="ic ic-house2"></i> Cam kết chính hãng 100%
                            </div>
                            @if(!empty($product_phukien->guarantee))
                                <div class="item">
                                    <i class="ic ic-arm"></i> Bảo hành chính hãng {{ $product_phukien->guarantee}} tháng
                                </div>
                            @endif
                            <div class="item">
                                <i class="ic ic-one"></i> {{ @$config_general['doi_tra']}}
                            </div>
                        </div>
                        <div class="pr-dt-sb v2">
                            <div class="item">
                                <i class="ic ic-phone-o"></i> Liên hệ mua hàng HOTLINE: <a class="smooth" href="tel:{!! @$config_general['hotline'] !!}"
                                                                                           title=""
                                                                                           rel="nofollow,noindex"><strong>{!! @$config_general['hotline'] !!}</strong></a>
                                <p>
                                    <small>( {{ @$config_general['fa_clock_o'] }})</small>
                                </p>
                            </div>
                            <div class="item">
                                <i class="ic ic-mail-o"></i> Email: <a class="smooth" href="mailto:{!! @$config_general['email'] !!}"
                                                                       title="Email" rel="nofollow,noindex">{!! @$config_general['email'] !!}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('schema')
    <script type="application/ld+json">
    {
        "@graph": [{
                "@context": "http://schema.org/",
                "@type": "Product",
                "sku": "{{$product_phukien->sku}}",
                "id": "{!! $product_phukien->id !!}",
                "mpn": "laptop247hn",
                "name": "{!! $product_phukien->name !!}",
                "description": "{!! cutString(strip_tags($product_phukien->detail), 100) !!}",
                "image": "{!! $product_phukien->image !!}",
                "brand": "{{ $brand->name ?? 'laptop247hn' }}",
                "aggregateRating": {
                    "@type": "AggregateRating",
                    "ratingValue": "5",
                    "reviewCount": "1"
                },
                "review": {
                    "@type": "Review",
                    "author": "thanh tran",
                    "reviewRating": {
                        "@type": "Rating",
                        "bestRating": "5",
                        "ratingValue": "1",
                        "worstRating": "1"
                    }
                },
                "offers": {
                    "@type": "AggregateOffer",
                    "priceCurrency": "VND",
                    "offerCount": 10,
                    "price": "{!! $product_phukien->price ==0? : $product_phukien->price!!}",
                    "lowPrice":"{!! $product_phukien->price ==0? : $product_phukien->price!!}",
                    "highPrice":"{!!$product_phukien->price_old == 0 ? : $product_phukien->price_old!!}",
                    @if(date('m') < 6)
            "priceValidUntil": "{{date('Y')}}-06-1",
                    @else
            "priceValidUntil": "{{date('Y')}}-12-31",
                    @endif
        "availability": "http://schema.org/InStock",
        "warranty": {
            "@type": "WarrantyPromise",
            "durationOfWarranty": {
                "@type": "QuantitativeValue",
                "value": "{{ $product_phukien->guarantee}} tháng",
                            "unitCode": "ANN"
                        }
                    },
                    "itemCondition": "mới",
                    "seller": {
                        "@type": "Organization",
                        "name": "laptop247hn"
                    }
                }
            }
        ]
    }
    </script>

    <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "NewsArticle",
          "mainEntityOfPage": {
            "@type": "WebPage",
            "@id": "https://google.com/article"
          },
          "headline": "Article headline",
          "image": [
            "{!!$product_phukien->image!!}"
           ],
          "datePublished": "2015-02-05T08:00:00+08:00",
          "dateModified": "2015-02-05T09:20:00+08:00",
          "author": {
            "@type": "Person",
            "name": "laptop247hn"
          },
           "publisher": {
            "@type": "Organization",
            "name": "Google",
            "logo": {
              "@type": "ImageObject",
              "url": "https://laptop247hn.com/uploads/2019/11/sua-chua-laptop247hn-tiny-1-tiny-1-tiny.PNG"
            }
          },
          "description": "{!!cutString(removeHTML($product_phukien->detail),160,'')!!}"
        }
        </script>
    <script type="application/ld+json">
            {
              "@context": "https://schema.org/",
              "@type": "Product",
              "name": "Executive Anvil",
              "image": [
                "{!!$product_phukien->image!!}"
               ],
              "description": "{!!cutString(removeHTML($product_phukien->detail),160,'')!!}",
              "sku": "{!!$product_phukien->id!!}",
              "mpn": "925872",
              "brand": {
                "@type": "Thing",
                "name": "laptop247hn"
              },
              "review": {
                "@type": "Review",
                "reviewRating": {
                  "@type": "Rating",
                  "ratingValue": "5",
                  "bestRating": "5"
                },
                "author": {
                  "@type": "Person",
                  "name": "thanhtran"
                }
              },
              "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "5",
                "reviewCount": "1"
              },
              "offers": {
                "@type": "AggregateOffer",
                "url": "https://example.com/anvil",
                "priceCurrency": "VNĐ",
                "offerCount":10,
                "price": "{!!$product_phukien->price!!}",
                "highPrice": "{!!$product_phukien->price_old == 0 ? : $product_phukien->price_old!!}",
                "lowPrice": "{!!$product_phukien->price!!}",
                "priceValidUntil": "2020-11-05",
                "itemCondition": "https://schema.org/UsedCondition",
                "availability": "https://schema.org/InStock",
                "seller": {
                  "@type": "Organization",
                  "name": "laptop247hn"
                }
              }
            }
        </script>
    <script type="application/ld+json">
        {
          "@context": "https://schema.org/",
          "@type": "Review",
          "itemReviewed": {
            "@type": "Restaurant",
            "image": "{!!$product_phukien->image!!}",
            "name": "{!!$product_phukien->name!!}",
            "servesCuisine": "Seafood",
            "priceRange":"5",
            "telephone": "{{$config_general['hotline'] ?? ''}}",
            "address" :{
              "@type": "PostalAddress",
              "streetAddress": "{{$config_general['address'] ?? ''}}",
              "addressLocality": "Hà Nội",
              "addressRegion": "Việt Nam",
              "postalCode": "10000",
              "addressCountry": "Việt Nam"
            }
          },

          "name": "{!!$product_phukien->name!!}",
          "author": {
            "@type": "Person",
            "name": "laptop247hn.com"
          },
          "reviewBody": "{!!cutString(removeHTML($product_phukien->detail),160,'')!!}",
          "publisher": {
            "@type": "Organization",
            "name": "laptop247hn.com"
          }
        }
        </script>
@endsection


