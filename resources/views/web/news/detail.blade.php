@extends('web.layouts.app')
@section('head')
<link rel="stylesheet" href="/assets/css/comment.css">


<script type="application/ld+json">
    {
        "@graph": [{
                "@context": "http://schema.org",
                "@type": "NewsArticle",
                "mainEntityOfPage": {
                    "@type": "WebPage",
                    "@id": "{{$new->getUrl()}}"
                },
                "headline": "{{$new->name}}",
                "image": {
                    "@type": "ImageObject",
                    "url": "{{$new->getImage()}}",
                    "height": 800,
                    "width": 800
                },
               
                "datePublished": "{{date('Y/m/d',strtotime($new->created_at))}}",
                "dateModified": "{{date('Y/m/d',strtotime($new->updated_at))}}",
                "author": {
                    "@type": "Person",
                    "name": "Laptop247hn"
                },
                "publisher": {
                    "@type": "Organization",
                    "name": "{{config('app.name')}}",
                    "logo": {
                        "@type": "ImageObject",
                        "url": "https://laptop247hn.com/uploads/2020/03/rectangle-version-2-large.png",
                        "width": 600,
                        "height": 60
                    }
                },
                "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($new->detail),150) : $meta_seo['description']}}"
            }, {
                "@context": "http://schema.org",
                "@type": "WebSite",
                "name": "{{config('app.name')}}",
                "url": "{{config('app.url')}}"
            },
            {
                "@context": "http://schema.org",
                "@type": "Organization",
                "url": "{{config('app.url')}}",
                "@id": "{{config('app.url')}}/#organization",
                "name": "Laptop247hn Laptop Dell, Asus, Acer, HP",
                "logo": "https://laptop247hn.com/uploads/2020/03/rectangle-version-2-large.png"
            }
        ]
    }




    </script>

@endsection
@section('content')
@php
$type = 'news';
$data_id = $new->id;
@endphp
<div class="container">
        <div class="single">
            <!-- <div class="thumb">
                <img src="/template-admin/images/loading1.gif" data-original="{{ $new->getImage() }}" class="lazy" alt="{{ $new->name }}" title="{{ $new->name }}" />
            </div> -->
            <div class="row">
                <div class="col-lg-9">
                    <div class="single-content">
                        <!-- <a class="smooth cate" href="#" title="">{!! $new->getAdminUser() !!} <i class="arrow_right"></i></a> -->
                        <h1 class="title">{{ @$new->name }}</h1>
                        <div class="s-content">
                            {!! $new->detail !!}
                        </div>
                        <h5 class="h-title v2">Bình luận</h5>
                        @include('web.layouts.comment')
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="stick-scroll">
                        @if(isset($news_related))
                            <div class="sb-posts">
                                <h3 class="h-title v2">Bài viết <span>liên quan</span></h3>
                                @foreach($news_related as $value)
                                    <div class="sb-post clearfix">
                                        <div class="img ">
                                            <a class="c-img hv-over" href="{!! $value->getUrl() !!}" title="">
                                                <img src="{{ $value->getImage('small') }}" data-original="{{ $value->getImage('small') }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->name }}"/>
                                            </a>
                                        </div>
                                        <div class="ct">
                                            <time><i class="icon_clock"></i>{!!time_elapsed_string($value->created_at)!!}</time>
                                            <h3 class="title"><a class="smooth"  href="{!! $value->getUrl() !!}" title="">{{ $value->name }}</a></h3>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <!-- <div class="sb-banner">
                            <a class="smooth" href="#" title="">
                                <img src="/images/sb-banner1.jpg" alt="" title="" />
                            </a>
                        </div>
                        <div class="sb-banner">
                            <a class="smooth" href="#" title="">
                                <img src="/images/sb-banner2.jpg" alt="" title="" />
                            </a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection