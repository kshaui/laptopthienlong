@extends('web.layouts.app')
@section('content')
@if(isset($news))
@php
	$new1 = $news->shift();
@endphp
<div class="h-wrap default clear">
<div class="container">

	<div class="row col-mar-8">
		@if(isset( $new1))
		<div class="col-lg-8">
			<div class="big-post video">
			<img src="{{ $new1->getImage() }}" data-original="{{ $new1->getImage() }}" class="lazy" alt="{{ $new1->name }}" title="{{ $new1->name }}" />
				<div class="ct">
					<div class="tag">Mới nhất</div>
				<h2 class="title"><a href="{!! $new1->getUrl() !!}">{{ $new1->name }}</a></h2>
					<p>{!! $new1->description !!}</p>
				</div>
			</div>
		</div>
		@endif
		<div class="col-lg-4">
			@foreach ($news as $value)
				<div class="sm-post video clearfix">
					<a class="img" href="{!! $value->getUrl() !!}" title="">
						<img src="{{ $value->getImage() }}" data-original="{{ $value->getImage() }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->name }}" />
					</a>
					<div class="ct">
						<div class="tag">Mới nhất</div>
					<h3 class="title"><a class="smooth" href="{!! $value->getUrl() !!}" title="">{{ $value->name }}</a></h3>
					</div>
				</div>
			@endforeach
		</div>
	</div>
	<hr>
	<div class="row">
	@if (isset($new_other))
		<div class="col-lg-9">
			
				@foreach($new_other as $value)
					<div class="post clearfix">
						<div class="img">
						<a class="c-img hv-over" href="{{ $value->getUrl() }}" title="">
							<img src="{{ $value->getImage() }}" data-original="{{ $value->getImage() }}" class="lazy" alt="{{ $value->name }}" title="{{ $value->name }}" />
							</a>
						</div>
						<div class="ct">
						<time><i class="icon_clock"></i>{!!time_elapsed_string($value->created_at)!!}</time>
						<h2 class="title"><a class="smooth" href="{{ $value->getUrl() }}" title="">{{ $value->name }}</a></h2>
							<p>{!! $value->description !!}</p>
							<div class="text-right">
								<a class="smooth more" href="{{ $value->getUrl() }}" title="">Xem chi tiết<i class="arrow_right"></i></a>
							</div>
						</div>
					</div>
				@endforeach
			
			<br>
			 <div class="row">
                    <div class="col-xl-12">
                        {!! $new_other->links() !!}
                       
                    </div>
            </div>
		</div>
		@endif
		{{-- <div class="col-lg-3">
			<div class="stick-scroll">
				<div class="sb-banner">
					<a class="smooth" href="#" title="">
						<img src="/images/sb-banner1.jpg" alt="" title="" />
					</a>
				</div>
				<div class="sb-banner">
					<a class="smooth" href="#" title="">
						<img src="/images/sb-banner2.jpg" alt="" title="" />
					</a>
				</div>
			</div>
		</div> --}}
	</div>
</div>
</div>
@endif

@endsection