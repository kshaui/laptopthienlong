$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$("img.lazy").lazyload({
    effect : "fadeIn"
});
$('#icon-search').on('click',function(){
    $('input[name="search"]').focus();
});
$('#icon-search').on('click',function(){
    $('.mb-search-box').css('display','block');
});

$('#close-box-search').on('click',function(){
    $('.mb-search-box').css('display','none');
});
$('.search_btn').on('click',function(){
    $('#form-search').submit();
});
$('body').on('click','#popup-cart .tru',function () {
	var rowId = $(this).attr('data-id');
    var number = $('#item-popup-'+rowId+' input.number-product').val();
    var type = -1;
    if(number > 1){
        number = Number(number)-1;
        $.ajax({
        	type: 'post',
        	dataType: 'json',
        	data: {rowId:rowId, type:type},
        	url: '/ajax/update-qty',
        	success:function(data){
    			$('#item-popup-'+rowId+' input.number-product').val(number);
    			$('#item-popup-'+rowId+' .cart-price .price').html(data.total_price);
    			$('#total-cart').html(data.count_cart);
    			$('#popup-cart .total-price').html(data.value_total);
        	}
        });
    }
});
$('body').on('click','#popup-cart .cong',function () {
	var rowId = $(this).attr('data-id');
    var number = $('#item-popup-'+rowId+' input.number-product').val();
    var type = 1;
    number = Number(number)+1;
    $.ajax({
    	type: 'post',
    	dataType: 'json',
    	data: {rowId:rowId, type:type},
    	url: '/ajax/update-qty',
    	success:function(data){
    		$('#item-popup-'+rowId+' input.number-product').val(number);
			$('#item-popup-'+rowId+' .cart-price .price').html(data.total_price);
			$('#total-cart').html(data.count_cart);
			$('#popup-cart .total-price').html(data.value_total);
    	}
    });
});
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function isPhone(phone) {
    var regex = /^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/;
    return regex.test(phone);
}
function add_to_cart(product_id, type ){
	var color_id = 0;
	var size_id = 0;
	var qty = 1;
	if(type == 1){
		color_id = $('#choose-color').val();
		size_id = $('#size_id').val();
	}
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'color_id': color_id,
			'size_id': size_id,
			'qty': qty,
			'product_id': product_id,
		},
		url: '/ajax/add-to-cart',
		success:function(data){
            alert('Thêm vào giỏi hàng');
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
			
        	$('#total-cart').html(data.count_cart);
			
		}, error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
	});
}
function remove_cart(rowId){
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: {rowId:rowId},
        url: '/ajax/remove-cart',
        success:function(data){
            if(data == 1){
                $('#item-popup-'+rowId).remove();
            }
        }
    });
}
$(document).ready(function(){
    $('#new-comment .comment_addnew_btn').on('click', function(){
        var content_comment = $('.new_comment').val();
        if(content_comment == ''){
            alert('Bạn chưa nhập nội dung bình luận')
            $('.new_comment').focus();
        }else{
            $('#new-comment .comment_author_info').css('display','block');
        }
    });
    $('.rating-action-btn2').on('click',function(){
        var vote = $(this).attr('data-point');
        var type_id = $('.rating-action').attr('data-id');
        var type = $('.rating-action').attr('data-type');
        $.ajax({
            type: 'post',
            dataType: 'json',
            data:{vote:vote, type_id:type_id, type:type},
            url: '/ajax/vote-start',
            success:function(data){
                if(data == 1){
                    for(i=1;i<=vote;i++){
                        $('#fa-star-'+i).removeClass('fa-star-o');
                        $('#fa-star-'+i).addClass('fa-star');
                    }
                    alert('Cảm ơn bạn đã đánh giá');
                }
            }
        });
    });
    $('.rating-action-btn1').on('click',function(){
        alert('Bạn đã đánh giá cho bài viết này trước đó. Xin cám ơn');
    });
});
function replyComment(id){
    $('#form-reply-'+id).css('display','block');
}
function showFormInfo(id){
    var content = $('#form-reply-'+id+' .comment_editor').val();
    if(content == ''){
        alert('Bạn chưa nhập nội dung bình luận');
        $('#form-reply-'+id+' .comment_editor').focus();
    }else{
        $('#form-info-'+id).css('display','block');
    }
}
function addNewComment(type_id, type){
    var token = $('input[name="_token"]').val();
    var gender = $('#new-comment .comment_author_gender:checked').val();
    var name = $('#new-comment .comment_author_name').val();
    var email = $('#new-comment .comment_author_email').val();
    var phone = $('#new-comment .comment_author_phone').val();
    var content = $('#new-comment .new_comment').val();
    if(name == ''){
        alert('Vui lòng nhập họ tên');
        $('#new-comment .comment_author_name').focus();
    }else if(email == ''){
        alert('Vui lòng nhập email của bạn');
        $('#new-comment .comment_author_email').focus();
    }else if(!isEmail(email)){
        alert('Vui lòng nhập email hợp lệ');
        $('#new-comment .comment_author_email').focus();
    }else if(phone == ''){
        alert('Vui lòng nhập SĐT của bạn');
        $('#new-comment .comment_author_phone').focus();
    }else if(!isPhone(phone)){
        alert('Vui lòng nhập SĐT hợp lệ');
        $('#new-comment .comment_author_phone').focus();
    }else{
        $.ajax({
            type: 'post',
            dataType: 'json',
            data:{"_token":token, gender:gender, name:name, email:email, phone:phone, content:content,type_id:type_id,type:type},
            url: '/ajax/add-comment',
            success:function(data){
                if(data == 1){
                    alert('Cảm ơn bạn đã bình luận');
                    window.location.reload();
                }
            }
        });
    }
}
function addReplyComment(comment_id){
    var token = $('input[name="_token"]').val();
    var gender = $('#form-reply-'+comment_id+' .comment_author_gender:checked').val();
    var name = $('#form-reply-'+comment_id+' .comment_author_name').val();
    var email = $('#form-reply-'+comment_id+' .comment_author_email').val();
    var phone = $('#form-reply-'+comment_id+' .comment_author_phone').val();
    var content = $('#form-reply-'+comment_id+' .comment_editor').val();
    if(name == ''){
        alert('Vui lòng nhập họ tên của bạn');
        $('#form-reply-'+comment_id+' .comment_author_name').focus();
    }else if(email == ''){
        alert('Vui lòng nhập email của bạn');
        $('#form-reply-'+comment_id+' .comment_author_email').focus();
    }else if(!isEmail(email)){
        alert('Vui lòng nhập email hợp lệ');
        $('#form-reply-'+comment_id+' .comment_author_email').focus();
    }else if(phone == ''){
        alert('Vui lòng nhập SĐT của bạn');
        $('#form-reply-'+comment_id+' .comment_author_phone').focus();
    }else if(!isPhone(phone)){
        alert('Vui lòng nhập SĐT hợp lệ');
        $('#form-reply-'+comment_id+' .comment_author_phone').focus();
    }else{
        $.ajax({
            type: 'post',
            dataType: 'json',
            data:{"_token":token, gender:gender, name:name, email:email, phone:phone, content:content,comment_id:comment_id},
            url: '/ajax/reply-comment',
            success:function(data){
                if(data == 1){
                    alert('Cảm ơn bạn đã bình luận');
                    window.location.reload();
                }
            }
        });
    }
}

$(document).mouseup(function(e) {
    var container = $(".suggest-search");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
    }
});

var timeoutID = null
$('input[name="search"').keyup(function() {
    clearTimeout(timeoutID);
    timeoutID = setTimeout(function() {
        search = $('input[name="search"').val();
        var token = $('input[name="_token"]').val();
        $.ajax({
            type: 'POST',
            data: 'array',
            data: { "_token": token, search: search },
            url: '/ajax/suggest',
            success: function(result) {
                $('#loading').css('display','none');
                $('body').css('opacity',1);
                $('.suggest-search ').css('display', 'block');
                str = '';
                $.each(result, function(key, value) {
                    str += '<li><a href="' + result[key].url + '"><div class="img"><img src="' + result[key].image + '" alt=""></div><div class="info"><p class="type">' + result[key].type + '</p><h4 class="name">' + result[key].name + '</h4><p class="price">' + result[key].price, ',', '.' + '</p></div></a></li>';
                });

                $('.suggest-search ul').html(str);
            },beforeSend:function(){
                $('#loading').css('display','block');
                $('body').css('opacity',0.3);
            }
        });
    }, 500);
});

$("body").on("click", ".remove_filter", function() {
    slug = $(this).data("slug");
    pathname = window.location.pathname;
    pathname = pathname.replace(slug, "");
    pathname = pathname.replace("--", "-");
    pathname_explore = pathname.split("/");
    pathname_url = pathname_explore[pathname_explore.length - 1];
    pathname_head = pathname_url.substr(0, 1);
    pathname_foot = pathname_url.substr(-1, 1);
    "-" == pathname_head && (pathname_url = pathname_url.substring(1, pathname_url.length));
    "-" == pathname_foot && (pathname_url = pathname_url.substring(0, pathname_url.length - 1));
    pathname_explore[pathname_explore.length - 1] = pathname_url;
    pathname = pathname_explore.join("/");
    link = window.location.origin + pathname;
    location.href = link;
});
$(".btn-info-cmt").click(function() {
    $(".comment-form").slideToggle();
});

function show_box_reply(id) {
    $('.box-add-new').css('display', 'none');
    $('#box-add-reply-' + id).slideToggle(300);
}

function show_box_info(id) {
    $('.comment_author_info').css('display', 'none');
    $('#box-info-' + id).slideToggle(300);
}


$(document).ready(function() {

    $("#send_comment").click(function(e) {
        e.preventDefault();
        var token = $('input[name="_token"]').val();
        var data_id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        var that = $(this);
        var cmt_name = $(this).closest('form').find('.cmt_name').val();
        var cmt_email = $(this).closest('form').find('.cmt_email').val();
        var cmt_phone = $(this).closest('form').find('.cmt_phone').val();
        var cmt_content = $('.cmt_content').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        if (cmt_content.trim() == '') {
            alert('Bạn chưa nhập nội dung bình luận');
        } else if (cmt_name.trim() == "") {
            alert("Bạn chưa nhập tên người gửi");
            $(this).closest('form').find('.cmt_name').focus();
        }else if(cmt_phone.trim() == ''){
            alert("Bạn không được để trống số điện thoại");
            $(this).closest('form').find('.cmt_phone').focus();
        }else if(!isPhone(cmt_phone)){
            alert("Bạn nhập không đúng định dạng số điện thoại");
        }
        else if (cmt_email == "") {
            alert('Vui lòng nhập email của bạn');
            $(this).closest('form').find('.cmt_email').focus();
        } else if (!isEmail(cmt_email)) {
            alert('Bạn nhập không đúng định dạng Email. Vui lòng kiểm tra lại!');
        } else {
           
            $.ajax({
                type: "POST",
                url: '/dang-gia-binh-luan',
                data: {data_id: data_id, type: type, cmt_name: cmt_name,cmt_phone:cmt_phone, cmt_email: cmt_email, cmt_content: cmt_content },
                dataType: "JSON",
                success: function(response) {
                    location.reload();
                    // $('.comment-form').css('display', 'none');
                    // $('.list-cmt .alert-success').css('display', 'block');
                    // $('.list-cmt .alert-success').append("Cảm ơn bạn đã phản hồi về bài viết. Quản trị viên sẽ xét duyệt bình luận của bạn trong thời gian sớm nhất");
                    // $(".cmt_user").append(response.msg);
                    // $('.cmt_content').val('');
                }
            });
        }
    });
    $("#close-comment-form").click(function(e) {
        e.preventDefault();
        $('.comment-form').css('display', 'none');
        $(this).closest('form').find('.cmt_content').val('');
    });

});

function reply_comment(id) {
    var type = $('input[name="type"]').val();
    var token = $('input[name="_token"]').val();
    var content = $('.comment-editor-' + id).val();
    var name = $('.comment-author-name-' + id).val();
    var email = $('.comment-author-email-' + id).val();
    var data_id = $('input[name="data_id"]').val();
    if (name == '') {
        alert('Vui lòng nhập họ tên của bạn');
        $('.comment-author-name-' + id).focus();
    } else if (email == '') {
        alert('Vui lòng nhập email của bạn');
    } else if ((email != '') && (!isEmail(email))) {
        alert('Vui lòng nhập một email hợp lệ');
        $('.comment-author-email-' + id).focus();
    } else if (content == '') {
        alert('Vui lòng nhập nội dung trả lời');
        $('.comment-editor-' + id).focus();
    } else {
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: { type: type, type_id: data_id, name: name, email: email, content: content, review_id: id },
            url: '/ajax/reply-comment',
            success: function(data) {
                if (data == 1) {
                    $('.box-add-new').css('display', 'none');
                    $('.comment_author_info').css('display', 'none');
                    $('.list-cmt .alert-success').css('display', 'block');
                    $('.list-cmt .alert-success').append("Cảm ơn bạn đã phản hồi về bài viết. Quản trị viên sẽ xét duyệt phản hồi của bạn trong thời gian sớm nhất !");
                }
            },
            beforeSend:function(){
                var html = '<div class="box-reply-item">';
                    html +='<p class="p-name"><span>'+name.charAt(0)+'</span>'+name+'</p>';
                    html +='<p class="p-content">'+content+'</p>';
                    html +='<p class="p-action"><span>Trả lời</span><span>Vừa xong</span></p>';
                    html +='</div>';
                $('#box-reply-'+id).append(html);
            }
        });
    }
}



    $(document).ready(function() {
        $('#brand_ajax .sub-list li a').on('click', function() {
            var slug = $(this).attr('data-id');
            var token = $('input[name="_token"]').val();
            e = $(this);
            $.ajax({
                type:'post',
                url:'/ajax/brand',
                dataType:'json',
                data:{slug:slug},
                success:function(data){
                   
                    $('#loading_brand #show_ajax_brand').css('opacity',1);
                    $('#loading_brand  img.loading').css('display','none');
                    if(data.html !=""){
                        $('#brand_ajax .tab').removeClass('active');
                        e.addClass('active');    
                         $('#show_ajax_brand').html(data.html);
                    }else{
                        alert('Không có dữ liệu');
                    }
                },beforeSend:function(){
                    $('#loading_brand #show_ajax_brand').css('opacity',0.3);
                    $('#loading_brand  img.loading').css('display','block');
                }
            });

        })
    })
    function ajaxBrand(){
        var slug = $('select[name="slug_brand"]').val();
        var token = $('input[name="_token"]').val();
        $.ajax({
            type:'post',
            url:'/ajax/brand',
            dataType:'json',
            data:{slug:slug},
            success:function(data){
               
                $('#loading_brand #show_ajax_brand').css('opacity',1);
                $('#loading_brand  img.loading').css('display','none');
                if(data.html !=""){
                     $('#show_ajax_brand').html(data.html);
                }else{
                    alert('Không có dữ liệu');
                }
            },beforeSend:function(){
                $('#loading_brand #show_ajax_brand').css('opacity',0.3);
                $('#loading_brand  img.loading').css('display','block');
            }
        });
    }


    function login(){
        var email = $('#email-login').val();
        var password = $('#password-login').val();
        if(email == ''){
           alert('Không để trống email');
            $('#email-login').focus();
            return false;
        }
        if(!isEmail(email)){
            alert('Không đúng định dạng email');
            $('#email-login').focus();
            return false;
        }
         if(password == ''){
           alert('Không để trống mật khẩu');
           $('#password-login').focus();
            return false;
        }
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: {email:email, password:password},
            url: '/ajax/login',
            success:function(data){
                if(data==1){
                    $('#acc-success').modal('show');
                    $('#popupLogin').modal('hide');
                    $('img.loading1').css('display','none');
                    $('body').css({'opacity':1});
                    $('.modal-body').css('opacity', 1);
                  
                }else if(data==0){
                     $('img.loading1').css('display','none');
                    $('body').css({'opacity':1});
                    $('.modal-body').css('opacity', 1);
                    alert('Sai thông tin đăng nhập hoặc mật khẩu');
                }
            },
             error: function (error) {
                $('img.loading1').css('display','none');
                $('body').css({'opacity':1});
                alert('Có lỗi xảy ra! Vui lòng thử lại.');
            }, beforeSend:function(){
                $('img.loading1').css('display','inline');
                $('body').css({'opacity':0.5});
            }
        });
        
    }

    function register(){
        var name = $('#name-register').val();
        var fullname = $('#fullname-register').val();
        var email = $('#email-register').val();
        var password = $('#password-register').val();
        var comfirm_password = $('#comfirm-password-register').val();
       if(name.trim() ==''){
           $('#register-error').html('Tên đăng nhập không được để trống');
           return false;
       }
       if(fullname.trim() ==''){
            $('#register-error').html('Tên đây đủ không được để trống');
            return false;
        }
        if(email.trim() ==''){
            $('#register-error').html('Email không được để trống');
            return false;
        }
        if(!isEmail(email)){
            $('#register-error').html('Nhập sai định dạng email');
            return false;
        }
        if(password.length <6){
            $('#register-error').html('Mật khẩu không được ít hơn 6 ký tự');
            return false;
        }
        if(password != comfirm_password){
            $('#register-error').html('Mật khẩu không trùng khớp');
            return false;
        }
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: {name:name, email:email, password:password, fullname:fullname},
            url: '/ajax/register',
            success:function(data){
                $('#popup-login img.loading').css('display','none');
                $('#btn-register').css('display','inline');
                if(data.status == -1){
                    $('#register-error').html(data.message);
                }else if(data.status == 0){
                    $('#register-error').html(data.message);
                }else if(data.status == 1){
                    $('#acc-success').modal('show');
                    $('#popupLogin').modal('hide');
                }
            },
            beforeSend:function(){
                // $('#popup-login img.loading').css('display','inline');
                // $('#btn-register').css('display','none');
            }
        });
    }

    function dang_ky_email(){
        var email = $('#email_dang_ky_chuong_trinh').val();
        if(email.trim() == ''){
            alert('Không để trống email');
            return false;
        }
        if(!isEmail(email)){
            alert('Không đúng định dạng email');
            return false;
        }
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: {email:email},
            url: '/ajax/dang-ky-email',
            success:function(data){
               alert('Bạn đăng ký thành công');
            },
        });
    }
  $(window).scroll(function() {
        if ($(this).scrollTop() != 0) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    $('#back-to-top').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });
    $(window).scroll(function(){
        if($(this).scrollTop()>=200){
            $('div.header_menu').css('position', 'fixed');
        } else {
            $('div.header_menu').css('position', 'relative');
        }
    });


/*
    đặt lịch sửa chữa

*/
$(document).ready(function() {
    openPopup('.buy-order-service', '.popup');
    closePopup('.popup_close', '.popup'); 
})
function openPopup(clickShowBtn, popupShow) {
    $(clickShowBtn).on('click',function(){
        $(popupShow).bPopup({
            speed: 450,
            transition: 'slideDown',
            zIndex:99999,
            onOpen: function() { 
                $(popupShow).css('visibility', 'visible'); 
            },
            onClose: function() { 
                $(popupShow).css('visibility', 'hidden'); 
            }
        });
    }); 
}
function closePopup(clickCloseBtn, popupClose) {
    $(clickCloseBtn).on('click' ,function() {
        $(popupClose).css('visibility', 'hidden');
        $(popupClose).bPopup().close();
    })
}

/**
 * đặ lịch sửa chữa
 */
$('.btn-submit-buynow').on("click",function(){
    var type = $(this).attr('data-type');
    var id = $(this).attr('data-id');
    var gender = $('#customer-gender').val();
    var name = $('#customer-name').val();
    var phone = $('#customer-phone').val();
    var email = $('#customer-email').val();
    var address = $('#customer-address').val();
    var note = $('#order-note').val();
    var link = window.location.href;
    var price_serice    = $('.option_services').val();
    if(gender.trim() == ''){
         alert('Không để trống giới tính');
         $('#customer-gender').focus();
         return false;
    }
    if(name.trim() === ''){
        alert('Không để trống Tên');
         $('#customer-name').focus();
         return false;
    }
    if(phone.trim() === ''){
        alert('Không để trống Số điện thoại');
         $('#customer-phone').focus();
         return false;
    }
    if(!isPhone(phone)){
        alert('Nhập sai Số điện thoại');
         $('#customer-phone').focus();
        return false;
    }
    if(email.trim() === ''){
        alert('Không để trống địa chỉ email');
         $('#customer-email').focus();
         return false;
    }
    if(!isEmail(email)){
       alert('Nhập sai định dạng email!');
        $('#customer-email').focus();
        return false;
    }
    if(address.trim() === ''){
        alert('Không để trống địa chỉ nhận hàng');
         $('#customer-address').focus();
         return false;
    }
    $.ajax({
        type:'post',
        url:'/ajax/dat-lich-sua-chua',
        dataType:'json',
        data:{
            _token: $('#_token').val(),
            type:type,
            id:id,
            name:name,
            phone:phone,
            email:email,
            gender:gender,
            address:address,
            note:note,
            link:link,
            price:price_serice,

        },success:function(data){
            $('#customer-name').val('');
            $('#customer-phone').val('');
            $('#customer-email').val('');
            $('#customer-address').val('');
            $('#order-note').val('');
            $(".popup").bPopup().close();
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
        }, 
        error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }

    });

})


/**
 * xem thêm phụ kiện và dịch vụ
 * 
 */
var page_product = 1;
$('.order-lg-last').on('click','.view-show-product',function(e){
    e.preventDefault();
    var that = $(this).closest('.order-lg-last');
    var type = $(this).data('type');
    var count = $(this).data('count');
    page_product++;
    $.ajax({
        type:'post',
        url:'/ajax/view-show-product',
        data:{
            page:page_product,type:type,count:count
        },
        success:function(response){
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            if(response == ""){
                alert('Hết dữ liệu');
                that.find('#pagnation').css('display','none');
            }
            console.log("zz");
            that.find('.row_laptop247').append(response);

        }
        , error: function (error) {
             $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
})
 