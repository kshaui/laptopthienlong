function initMap() {
    var arr = [];
    var item = $('[data-center]');
    item.each(function(index, el) {
        var lat = Number($(el).attr('data-center').split(',')[0]);
        var lng = Number($(el).attr('data-center').split(',')[1]);
        var data = {
            lat: lat,
            lng: lng,
        }
        arr.push(data);
    });

    var m = document.getElementById('map');
    var center = $(m).attr('data-center').split(',');
    var latlng_ct = {
        lat: Number(center[0]),
        lng: Number(center[1]),
    }

    if(m){
        var map, markers = [];
        map = new google.maps.Map(m, {
            zoom: 14,
        });
        map.setCenter(latlng_ct);

        var ic1 = 'images/marker1.png';
        var ic2 = 'images/marker2.png';

        function createMarker(latlng) {
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: {
                    url: ic1,
                },
            });

            google.maps.event.addListener(marker, 'click', function() {
                map.setCenter(latlng);
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setIcon(ic1);                        
                }
                marker.setIcon(ic2)
            });
            return marker;
        }

        for (var i = 0; i < arr.length; i++) {
            var latlng = new google.maps.LatLng(arr[i].lat,arr[i].lng);
            markers[i] = createMarker(latlng);
        }
    }
}

window.addEventListener('load',function(){
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAdDT2wrq1DnW5omSNGyVee7nxt6cRaOxo&callback=initMap';
    document.body.appendChild(script);
});