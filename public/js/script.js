$.fn.input_Length = function(options) {
    var settings = $.extend({
        result: ".len-result",
    }, options );
    $(this)[0].oninput = function(){
        var len = $(this).val().length;
        $(settings.result).text(len);
    }
}
var get_ytid = function(url){
    var rx = /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/;
    var arr = url.match(rx);
    return arr[1];
}
var get_currency = function(x){
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
/*.........................*/
$('.numberic').keydown(function(event) {
    if (!(!event.shiftKey
        && !(event.keyCode < 48 || event.keyCode > 57)
        || !(event.keyCode < 96 || event.keyCode > 105)
        || event.keyCode == 46
        || event.keyCode == 8
        || event.keyCode == 190
        || event.keyCode == 9
        || (event.keyCode >= 35 && event.keyCode <= 39)
        )) {
        event.preventDefault();
}
});

$(document).on('click', '.i-number .up', function(e) {
    e.preventDefault();
    var input = $(this).parents('.i-number').children('input');
    var max = Number(input.attr('max'));
    var val = Number(input.val());
    if(!isNaN(val)) {      
        if(!isNaN(max) && input.attr('max').trim()!='') { 
            if(val >= max){
                return false;
            }
        }
        input.val(val + 1);
        input.trigger('number.up');
    }
});
$(document).on('click', '.i-number .down', function(e) {
    e.preventDefault();
    var input = $(this).parents('.i-number').children('input');
    var min = Number(input.attr('min'));
    var val = Number(input.val());
    if(!isNaN(val)){
        if(!isNaN(min) && input.attr('max').trim()!=''){ 
            if(val <= min){
                return false;
            }
        }
        input.val(val - 1);
        input.trigger('number.down');
    }
});

var script = function(){

    var win = $(window);
    var html = $('html');
    var body = $('body');

    var mMenu = function(){
        var menu = $('.m-nav');
        var ct = menu.find('.nav-ct');
        var open = $('.nav-open');
        var close = $('.nav-close');

        /*home header*/
        ct.append($('header .main-bar .h-cart').clone());
        ct.append($('header .main-bar .h-account').clone());
        ct.append($('.main-nav').clone());
        /**/
        /*blog header*/
        ct.append($('.blog-header .h-cart').clone());
        ct.append($('.blog-header .h-account').clone());
        ct.append($('.blog-header .pro-nav').clone());
        ct.append($('.blog-header .blog-nav').clone());
        /**/
        /*about header*/
        ct.prepend($('.about-header .h-account').clone());
        ct.prepend($('.about-header .h-cart').clone());
        /**/

        open.click(function(e) {
            e.preventDefault();
            if(win.width()<992){
                menu.addClass('act');
            }           
        });
        close.click(function(e) {
            e.preventDefault();
            if(win.width()<992){
                menu.removeClass('act');
            }
        });

        win.click(function(e) {
            if(menu.has(e.target).length == 0 && !menu.is(e.target) && open.has(e.target).length == 0 && !open.is(e.target)){
                menu.removeClass('act');
            }
        });


        nav = menu.find('.main-nav');
        nav.find("ul li").each(function() {
            if($(this).find("ul>li").length > 0){
                $(this).prepend('<button class="nav-drop"></button>');
            }
        });


        /*blog header mobile*/
        // var b_nav = $('.blog-m-header');
        // var b_ct = b_nav.find('.nav-ct');

        // b_ct.append($('.blog-header .h-cart').clone());
        // b_ct.append($('.blog-header .h-account').clone());
        // b_ct.append($('.blog-header .pro-nav').clone());
        // b_ct.append($('.blog-header .blog-nav').clone());

        /*end blog header mobile*/







        $(".nav-drop").click(function(){
            var ul = $(this).nextAll("ul");
            if(ul.is(":hidden") === true){
                ul.parent('li').parent().children().children('ul').slideUp(200);
                ul.parent('li').parent().children().children('.nav-drop').removeClass("act");
                $(this).addClass("act");
                ul.slideDown(200);
            }
            else{
                ul.slideUp(200);
                $(this).removeClass("act");
            }
        });
    }

    var uiPsy = function(){
        var btn = $('.psy-pane a');
        btn.click(function(e) {
            e.preventDefault();
            btn.removeClass("active");
            $('html,body').animate({ scrollTop: $($(this).attr('href')).offset().top - 40}, 800);  
            $(this).addClass('active');
        });

        var section_act = function(){
            $('section').each(function() {
                if(win.scrollTop() + (win.height()/2) >= $(this).offset().top){
                    var id = $(this).attr('id'); 
                    btn.each(function() {
                        if('#'+id == $(this).attr('href')){
                            btn.removeClass("active");
                            $(this).addClass("active");
                        }
                    });
                }
            });
        }
        section_act();
        win.scroll(function() {
            section_act();
        });
    }  

    var uiSlick = function(){
        $('.slider-cas').slick({
            nextArrow: '<i class="fa fa-angle-right smooth next"></i>',
            prevArrow: '<i class="fa fa-angle-left smooth prev"></i>',
            dots: true,
        })

        $('.home-pro-cas').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            nextArrow: '<i class="fa fa-angle-right smooth next"></i>',
            prevArrow: '<i class="fa fa-angle-left smooth prev"></i>',
            swipeToSlide: true,
            autoplay: true,
            autoplaySpeed: 4000,
            responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
            ],
        })


        $('.machinal-cas').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            nextArrow: '<i class="fa fa-angle-right smooth next"></i>',
            prevArrow: '<i class="fa fa-angle-left smooth prev"></i>',
            swipeToSlide: true,
            autoplay: true,
            autoplaySpeed: 4000,
            dots: true,
            responsive: [,
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                }
            },
            ],
        })
        var num = 5, num1549 = 5;
        if($('.h-pro-cas').hasClass('v2')){
            num = 5;
            num1549 = 4;
        }
        if($('.h-pro-cas').hasClass('v3')){
            num = 6;
            num1549 = 5;
        }
        
        $('.h-pro-cas').slick({
            slidesToShow: num,
            slidesToScroll: 1,
            nextArrow: '<i class="fa fa-angle-right smooth next"></i>',
            prevArrow: '<i class="fa fa-angle-left smooth prev"></i>',
            swipeToSlide: true,
            autoplay: true,
            autoplaySpeed: 4000,
            responsive: [
            {
                breakpoint: 1549,
                settings: {
                    slidesToShow: num1549,
                }
            },
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
            ],
        })

        $('.brand-cas').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: false,
            swipeToSlide: true,
            autoplay: true,
            autoplaySpeed: 4000,
            responsive: [
            {
                breakpoint: 1191,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                }
            },
            
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                }
            }
            ],
        })

        $('.pro-img').slick({
            arrows: false,
            swipeToSlide: true,
            asNavFor: '.pro-thumb',
        })
        $('.pro-thumb').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            nextArrow: '<i class="fa fa-angle-right smooth next"></i>',
            prevArrow: '<i class="fa fa-angle-left smooth prev"></i>',
            // autoplay: true,
            swipeToSlide: true,
            autoplaySpeed: 5000,
            asNavFor: '.pro-img',
            focusOnSelect: true,
            responsive: [
            {
                breakpoint: 1549,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 5,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 450,
                settings: {
                    centerMode: false,
                    slidesToShow: 2,
                }
            },
            ],
        })
        $('.cloudzoom').each(function(index, el) {
            $(el).attr({
                'data-cloudzoom': "autoInside: 767, zoomWidth: 426,zoomHeight: 426, zoomImage: '"+$(el).attr('data-zoom')+"',disableZoom: 'auto'",
            });
        });
        if(typeof CloudZoom == 'function') CloudZoom.quickStart();



        $('.extension-cas').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            nextArrow: '<i class="fa fa-angle-right smooth next"></i>',
            prevArrow: '<i class="fa fa-angle-left smooth prev"></i>',
            swipeToSlide: true,
            autoplay: true,
            autoplaySpeed: 4000,
            responsive: [
            {
                breakpoint: 1191,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                }
            },
            
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                }
            }
            ],
        })

        $('.partner-cas').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: '<i class="fa fa-angle-right smooth next"></i>',
            prevArrow: '<i class="fa fa-angle-left smooth prev"></i>',
            swipeToSlide: true,
            autoplay: true,
            autoplaySpeed: 4000,
            responsive: [
            {
                breakpoint: 1191,
                settings: {
                    slidesToShow: 5,
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                }
            }
            ],
        })
    }

    var uiScroll = function(){
        if(win.scrollTop() > 400 ){
            $('.social-right').fadeIn(250);
            $('.psy-pane').fadeIn(250);
        }

        var last = win.scrollTop();
        win.scroll(function(e) {
            if(win.scrollTop() > 400 ){
                if(win.width() < 991){
                    if(win.scrollTop() < last){
                        $('.social-right').fadeIn(250);
                        $('.psy-pane').fadeIn(250);
                    }
                    else{
                        $('.social-right').fadeOut(100);
                        $('.psy-pane').fadeOut(100);
                    }
                    last = win.scrollTop();
                }
                else{
                    $('.social-right').fadeIn(100);
                    $('.psy-pane').fadeIn(100);
                }
            }
            else{
                $('.social-right').fadeOut(100);
                $('.psy-pane').fadeOut(100);
            }
        });

        // var cur_scroll = win.scrollTop();
        // win.scroll(function(e) {
            
        // });


        var h = $('header').innerHeight();
        if(win.width()>991){
            if(win.scrollTop()> 200 ){
                /*$('header').addClass('fixed');
                body.css('margin-top', h);*/
                $('header.fixed').fadeIn(200);
                if ($('.about-header').length > 0) {
                    $('.about-header').removeClass('fixtop');
                }
                
            }
        }
        else{
            if(win.scrollTop()> 300 ){
                $('header').addClass('m-fixed');
                body.css('margin-top', h);
            }
        }

        if(win.scrollTop()> 0){
            $('.about-header').addClass('fixtop');
            body.css('margin-top', $('.about-header').innerHeight());
        }

        win.scroll(function(e) {
            if(win.width()>991){
                if(win.scrollTop()> 200){
                    /*$('header').addClass('fixed');
                    body.css('margin-top', h);*/
                    $('header.fixed').fadeIn(100);
                }
                else{
                    /*$('header').removeClass('fixed');
                    body.css('margin-top', '');*/
                    $('header.fixed').fadeOut(100);
                }
            }
            else{
                if(win.scrollTop()> 300){
                    $('header').addClass('m-fixed');
                    body.css('margin-top', h);
                }
                else{
                    $('header').removeClass('m-fixed');
                    body.css('margin-top', '');

                    $('.h-category').removeClass('active');
                    $('.h-category .wrap-over').remove();
                }
            }


            if(win.scrollTop()> 1){
                $('.about-header').addClass('fixtop');
                body.css('margin-top', $('.about-header').innerHeight());
            }
            else{
                $('.about-header').removeClass('fixtop');
                body.css('margin-top', '');
            }
        });

        win.resize(function(event) {
            if(win.width()>991){
                body.css('margin-top', '');
            }
        });
    }

    var uiCollapse = function(){
        var ui = $('.hc-collapse');
        ui.find('ul>li').each(function(index, el) {
            if($(el).find("ul>li").length > 0){
                var cl = $(el).children("ul").is(':hidden') ? '' : 'active';
                $(el).prepend('<button class="cls-drop '+cl+'"></button>');
            }
        });

        $(".cls-drop").click(function(){
            var ul = $(this).nextAll("ul");
            if(ul.is(":hidden") === true){
                ul.parent('li').parent().children().children('ul').slideUp(200);
                ul.parent('li').parent().children().children('.cls-drop').removeClass("active");
                $(this).addClass("active");
                ul.slideDown(200);
            }
            else{
                ul.slideUp(200);
                $(this).removeClass("active");
            }
        });


        var pay = $('.payment-collapse');
        pay.find('.item .head').click(function(e) {
            var ct = $(this).nextAll(".ct");
            if(ct.is(":hidden") === true){
                ct.parent('.item').parent().children().children('.ct').slideUp(200);
                ct.parent('.item').parent().children().children('.head').removeClass("active");
                $(this).addClass("active");
                ct.slideDown(200);
            }
            else{
                ct.slideUp(200);
                $(this).removeClass("active");
            }
        });

        $('.vat-btn').click(function(e) {
            $('.vat-pane').is(':hidden')? $(this).addClass('active'): $(this).removeClass('active');
            $('.vat-pane').slideToggle(200);
        });
    }

    var stick_scroll = function(){
        stick_act();
        win.resize(function(e) {
            stick_act();
        });

        function stick(){
            $('.stick-scroll').stick_in_parent({
                offset_top: 68,
            });
        }
        function un_stick(){
            $('.stick-scroll').trigger("sticky_kit:detach");
        }
        function stick_act(){
            (win.width() < 992) ? un_stick() : stick();
        }
    }

    var uiAccount = function(){
        var sb = $('.sb-account');
        var open = sb.find('.asb-btn'), close = sb.find('.asb-btn-back');
        open.click(function(e) {
            if(win.width()<992) sb.addClass('active');
        });
        close.click(function(e) {
            if(win.width()<992) sb.removeClass('active');
        });
        win.click(function(e) {
            if(win.width()<992){
                if(sb.has(e.target).length == 0 && !sb.is(e.target)){
                    sb.removeClass('active')
                }
            }
        });

        /*Show change pass field*/
        $('.show-change-pass').change(function(e) {
            $(this).prop('checked') ? $('.change-pass').show() : $('.change-pass').hide();
        });
        
        /*Copy code to clipboard*/
        $('.copy-code').click(function(e) {
            e.preventDefault();
            $(this).parent().children('.code').select();
            document.execCommand("copy");
        });

        /*Show tootip cancel order*/
        var cancel = $('.account-pro-tb .cancel'),pop = $('.order-cancel-pop');
        cancel.click(function(e) {
            e.preventDefault();
            var left = $(this).offset().left - pop.innerWidth()/2;
            var top = $(this).offset().top;

            if( win.width() < 992){
                left = win.width() - (pop.innerWidth());
            }
            pop.fadeIn(200).css({
                'top': top,
                'left': left,
            });
        });
        win.click(function(e) {
            if(cancel.has(e.target).length == 0 && !cancel.is(e.target) ){
                pop.fadeOut(200);
            }
        });
        win.resize(function(event) {
            pop.fadeOut(200);
        });
        /*end*/

        if($('.cancel-order-message textarea').length > 0){
            $('.cancel-order-message textarea').input_Length({
                result: '.cancel-message-num span',
            });
        }
        
    }

    var uiCategory = function(){
        $('.h-category .title').click(function(e) {
            if(win.width()<992){
                e.preventDefault();
                var mCate = $(this).closest('.h-category');
                mCate.hasClass('active') ? mCate.removeClass('active') : mCate.addClass('active');
                mCate.append('<div class="wrap-over"></div>');
            }
        });
        $('body').on('click', '.wrap-over', function(event) {
            event.preventDefault();
            $('.h-category').removeClass('active');
            $(this).remove();
        });

        var flag = true;
        $('.h-category [data-sub]').hover(function() {
            if(win.width()>991){
                $('[data-sub-pane]').hide();
                $('[data-sub-pane="'+$(this).attr('data-sub')+'"]').show();
            }    
        }, function(e) {
            if(win.width()>991){
                var it = $('[data-sub-pane="'+$(this).attr('data-sub')+'"]');
                setTimeout(function(){
                    if(flag) it.hide();
                }, 200);
            }
        });
        $('.cate-sub-pane').mouseover(function(event) {
            if(win.width()>991){
                flag = false;
                $(this).show();
            }
        });
        $('.cate-sub-pane').mouseleave(function(event) {
            if(win.width()>991){
                flag = true;
                $(this).hide();
            }
        })


        $('.h-category .scroll-bar>ul>li>a').each(function(index, el) {
            if($(el)[0].hasAttribute("data-sub")){
                $(el).append('<i class="fa fa-caret-right"></i>');
            }
        });

        $('.h-category [data-sub] i').click(function(e) {         
            if(win.width()<=991){
                e.stopPropagation();
                var this_= $(this);
                $('[data-sub-pane]').hide();
                $('[data-sub-pane="'+$(this).parent().attr('data-sub')+'"]').show();
                $('.h-category .ct').css('overflow', 'hidden');
                setTimeout(function(){
                    $('[data-sub-pane="'+this_.parent().attr('data-sub')+'"]').addClass('act');
                }, 0);
            }
        });

        $('.h-category').on('click', '.back', function(e) {
            e.preventDefault();       
            $('[data-sub-pane="'+$(this).parent().attr('data-sub-pane')+'"]').removeClass('act');
            ('.h-category .ct').css('overflow', '');
            setTimeout(function(){
                $(this).parent('[data-sub-pane]').hide();
            }, 200);
        });
        // win.click(function(e) {
        //     if(win.width()<992){
        //         $('.h-category').each(function(index, el) {
        //             if($(el).children().has(e.target).length == 0 && !$(el).children().is(e.target)){
        //                 $(el).removeClass('active');
        //             }
        //         });
        //     }
        // });


        if($('.slider-cas').length>0 && win.width()>991){
            var cate = $('header').not('.fixed, .sub, .blog-header').find('.h-category');
            var he = $('.slider-cas').innerHeight();
            cate.find('.ct').css('height', he);
            cate.find('.ct .scroll-bar').mCustomScrollbar({
                autoHideScrollbar: true,
                advanced:{  
                    updateOnContentResize:true,   
                    updateOnBrowserResize:true   
                }
            });
        }
        /*win.resize(function(event) {
            if($('.slider-cas').length>0 && win.width()>991){
                var he = $('.slider-cas').innerHeight();
                cate.children('.ct').css('max-height', he);
                cate.children('.ct').mCustomScrollbar("update");
            }
        });*/
    }

    var uiFancybox = function(){
        if($.fn.fancybox) { 
            $(".fbx-video").fancybox({
                openEffect  : 'none',
                closeEffect : 'none',
                nextEffect  : 'none',
                prevEffect  : 'none',
                'speedIn'       :   600, 
                'speedOut'      :   200, 
                'overlayShow'   :   false,
                'autoScale': true,
                /*helpers: {
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                },*/
                afterLoad : function() {
                    this.title = 'Video ' + (this.index + 1) + ' / ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });
        }  
    }

    var uiExpand = function(){
        $('.expand-box').each(function(index, el) {
            var btn = $(el).parent().find('.expand-btn');
            if($(el).children().innerHeight() > $(el).innerHeight()){
                $(el).addClass('hide');
                btn.removeClass('hidden');
            }
            btn.click(function(event) {
                if($(el).hasClass('expand')){
                    $(el).removeClass('expand');
                    $(this).text('Xem thêm');
                }
                else{
                    $(el).addClass('expand');
                    $(this).text('Thu gọn');
                }
            });
        });
    }

    var uiVideopopup = function(){
        var pop = $('.video-popup');

        var cas = pop.find('.video-cas').slick({
            nextArrow: '<i class="arrow_triangle-right smooth next"></i>',
            prevArrow: '<i class="arrow_triangle-left smooth prev"></i>',
            infinite: false,
            asNavFor: '.video-thumb',
        })
        var thumb = pop.find('.video-thumb').slick({
            slidesToShow: 3,
            infinite: false,
            centerMode: true,
            variableWidth: true,
            arrows: false,
            asNavFor: '.video-cas',
            focusOnSelect: true,
        })

        thumb.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            pop.find('.video-pane iframe').remove();
        });

        
        $('.gallery-btn').click(function(e) {
            e.preventDefault();
            body.css('overflow', 'hidden');
            pop.fadeIn(200);
            cas.slick('setPosition');
            thumb.slick('setPosition');
        });
        pop.children('.pp-close').click(function(e) {
            e.preventDefault();
            pop.fadeOut(200);
            pop.find('.video-pane iframe').remove();
            body.css('overflow', '');
        });

        cas.find('.item').click(function(e) {
            e.preventDefault();
            var id = get_ytid($(this).attr('data-yt'));
            pop.find('.video-pane').append('<iframe src="https://www.youtube.com/embed/'+ id + '?rel=0&amp;autoplay=1&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>');
        });

        
    }

    var uiOrdercheck = function(){
        var btn = $('.show-order-check'), pane = $('.order-check');
        btn.mouseenter(function(e) {
            if($(window).width()> 991){
                var this_ = $(this);
                pane.css({
                    left: this_.offset().left + this_.innerWidth()/2 - pane.innerWidth()/2,
                    top: this_.offset().top + this_.innerHeight(),
                    display: 'block',
                });
            }
        });
        body.mousemove(function(e) {
            if($(window).width()> 991){
                if(pane.has(e.target).length == 0 && !pane.is(e.target) && btn.has(e.target).length == 0 && !btn.is(e.target)){
                    if(pane.is(':visible'))
                    pane.css({
                        left: '',
                        top: '',
                        display: '',
                    });
                }
            }
        });

        body.on('click', '.show-order-check', function(e) {
            if($(window).width()< 992){
                e.preventDefault();
                body.append('<div class="order-check-over"></div>');
                pane.show();
                $('.m-nav').removeClass('act');
                $('.m-nav .nav-close').removeClass('act');
            }
        });
        body.on('click', '.order-check-over', function(e) {
            if($(window).width()< 992){
                e.preventDefault();
                $(this).remove();
                pane.hide();
            }
        });

    }


     var uiShowBaoHanh = function(){
        var btn = $('.show-bao-hanh'), pane = $('.order-bao-hanh');
        btn.mouseenter(function(e) {
            if($(window).width()> 991){
                var this_ = $(this);
                pane.css({
                    left: this_.offset().left + this_.innerWidth()/2 - pane.innerWidth()/2,
                    top: this_.offset().top + this_.innerHeight(),
                    display: 'block',
                });
            }
        });
        body.mousemove(function(e) {
            if($(window).width()> 991){
                if(pane.has(e.target).length == 0 && !pane.is(e.target) && btn.has(e.target).length == 0 && !btn.is(e.target)){
                    if(pane.is(':visible'))
                    pane.css({
                        left: '',
                        top: '',
                        display: '',
                    });
                }
            }
        });

        body.on('click', '.show-bao-hanh', function(e) {
            if($(window).width()< 992){
                e.preventDefault();
                body.append('<div class="order-check-over"></div>');
                pane.show();
                $('.m-nav').removeClass('act');
                $('.m-nav .nav-close').removeClass('act');
            }
        });
        body.on('click', '.order-check-over', function(e) {
            if($(window).width()< 992){
                e.preventDefault();
                $(this).remove();
                pane.hide();
            }
        });

    }

    return {

        uiInit: function($fun){
            switch ($fun) {
                case 'mMenu':
                mMenu();
                break;
                case 'uiPsy':
                uiPsy();
                break;
                case 'uiSlick':
                uiSlick();
                break;
                case 'uiScroll':
                uiScroll();
                break;
                case 'uiCollapse':
                uiCollapse();
                break;
                case 'stick_scroll':
                stick_scroll();
                break;
                case 'uiAccount':
                uiAccount();
                break;
                case 'uiCategory':
                uiCategory();
                break;
                case 'uiFancybox':
                uiFancybox();
                break;
                case 'uiExpand':
                uiExpand();
                break;
                case 'uiVideopopup':
                uiVideopopup();
                break;
                case 'uiOrdercheck':
                uiOrdercheck();
                break;

                 case 'uiShowBaoHanh':
                uiOrdercheck();
                break;

                default:
                mMenu();
                uiPsy();
                uiSlick();
                uiScroll();
                uiCollapse();
                stick_scroll();
                uiAccount();
                uiCategory();
                uiFancybox();
                uiExpand();
                uiVideopopup();
                uiOrdercheck();
                uiShowBaoHanh();
            }
        }
    }

}();


jQuery(function($) {
    /*var wow = new WOW({offset:50,mobile:false}); wow.init();*/
    script.uiInit();

    $('.yt-box .play').click(function(e) {
        var id = get_ytid($(this).closest('.yt-box').attr('data-yt'));
        $(this).closest('.yt-box iframe').remove();
        $(this).closest('.yt-box').append('<iframe src="https://www.youtube.com/embed/'+ id + '?rel=0&amp;autoplay=1&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>');
    });

    



    
    $( "#slider-range" ).each(function() {
        var this_ = $(this);
        var min = parseInt(this_.attr('data-min'));
        var max = parseInt(this_.attr('data-max'));
        var valu = this_.attr('data-value').split(",");
        var rang = max- min;

        this_.slider({
            range: true,
            min: min,
            max: max,
            values: valu,
            slide: function( event, ui ) {

                var left1 = (ui.values[0] - min)/rang*100;
                this_.prevAll('.price-1').text(get_currency(ui.values[0]));

                var left2 = 100 - (ui.values[1] - min)/rang*100;
                this_.prevAll('.price-2').text(get_currency(ui.values[1]));
            }
        });

        var left1 = (this_.slider( "values", 0 ) - min)/rang*100;
        this_.prevAll('.price-1').text(get_currency(this_.slider( "values", 0 )));

        var left2 = 100 - (this_.slider( "values", 1 ) - min)/rang*100;
        this_.prevAll('.price-2').text(get_currency(this_.slider( "values", 1 )));
    });


    $('.review-btn').click(function(e) {
        e.preventDefault();
        $('.star-rating').show();
    });
    /*rating*/
    $('.star-rating').each(function(index, el) {
        var base = $(el).find('.star-base');
        var rate = $(el).find('.star-rate');
        base.hover(function() {
            starw = rate.width();
        }, function() { });
        base.mousemove(function(e) {
            rate.width(e.pageX - $(this).offset().left);
        });
        base.mouseleave(function(e) {
            rate.width(starw);
        });
    });
    /*end rate*/

    /*comment*/
    var fr = $('.comment-fr');
    var frInput = fr.find('.form-input'), reply = fr.find('.replys');

    // fr.find('.push-img input').change(function(e) {

    fr.on('change', '.push-img input', function(event) {
        var arr = event.target.files;
        for(var i = 0; i< arr.length; i ++){
            var tmppath = URL.createObjectURL(arr[i]), photos = $(this).closest('.form-input').find('.fr-photo');
            photos.append('<div class="item">'
                +'<img src="'+tmppath+'"/>'
                +'<input type="hidden" name="photo[]" data-path="'+tmppath+'" data-name="'+arr[i].name+'" data-size="'+arr[i].size+'" data-type="'+arr[i].type+'">'
                +'<button class="pt-remove" type="button">&times;</button>'
                +'</div>');
        }
    });
    fr.on('click', '.fr-photo .pt-remove', function(e) {
        e.preventDefault();
        $(this).parent('.item').remove();
    });
    fr.find('.cmt-focus').focus(function(e) {
        fr.find('.hc-comment .form-input').remove();
        fr.find('.form-input textarea').focus();
    });

    fr.on('click', '.reply-btn', function(e) {
        e.preventDefault();
        var cmtID = $(this).attr('data-id'), name = $(this).closest('.hc-comment').children('.head').find('.name').text();

        fr.find('.hc-comment .form-input').remove();
        var clone = frInput.clone();
        clone.find('.fr-photo').html('');
        clone.find('textarea').val("@"+name+': ');
        clone.append('<input type="hidden" id="comment-id" value="'+cmtID+'">');
        $(this).parent('.ctrl').after(clone);
        clone.find('textarea').focus();
    });


    /*end comment*/

    var cap = true;
  
    $('body').on('mouseleave', '.extension-cas .sm-pro', function(e) {
        setTimeout(function(){
            if(cap) $('body .cas-caption').remove();
        },0)
    })

    $('body').on('mouseover', '.cas-caption', function(e) {
        cap= false;
    });
    $('body').on('mouseleave', '.cas-caption', function(e) {
        cap= true;
        $(this).remove();
    });


    $('.up-core a').click(function(e) {
        $('.tab-imgs .item.active').removeClass('active');
        $('.tab-imgs .item[data-tab="'+$(this).attr('href')+'"]').addClass('active');
    });

    var flag = true;
    $('.un-stock').mouseover(function(e) {
        if($(window).width() > 991){
            $('.un-stock-popup').css({
                top: $(this).offset().top + $(this).innerHeight() + 10,
                left: $(this).offset().left,
            })
        }
        else{
            $('body').append('<div class="un-stock-over" style="display: none;"></div>');
            $('body > .un-stock-over').fadeIn(200);
        }
        $('.un-stock-popup').fadeIn(200).find('input').focus();
    });
    $('.un-stock').mouseleave(function(e) {
        if($(window).width() > 991){
            setTimeout(function(){
                if(flag) $('.un-stock-popup').fadeOut(200);
            },100)
        }
    })

    $('.un-stock-popup').mouseover(function(e){
        flag = false;
        $(this).show();
    });
    $('.un-stock-popup').mouseleave(function(e) {
        if($(window).width() > 991){
            $(this).fadeOut(200);
            flag = true;
        }
    })

    $('body').on('click', '.un-stock-over, .un-stock-popup .st-close', function(e) {
        e.preventDefault();
        setTimeout(function(){
            $('body > .un-stock-over').remove();
        },200);
        $('body > .un-stock-over').fadeOut(200);
        $('.un-stock-popup').fadeOut(200);
    });

    $(window).resize(function(e) {
        if($(window).width() > 991){
            $('body > .un-stock-over').remove();
            $('.un-stock-popup').css({
                display: '',
            })
        }
        else{
            $('.un-stock-popup').css({
                top: '',
                left: '',
            })
        }
    });

    var item = $('.up-statis .number span'), flag = true;
    if(item.length > 0){
        run(item);
        $(window).scroll(function() {
            if(flag == true){
                run(item);
            }
        });

        function run(item){
            if($(window).scrollTop() + 70 < item.offset().top && item.offset().top + item.innerHeight() < $(window).scrollTop() + $(window).height()){
                count(item);
                flag = false;
            }
        }

        function count(item){
            item.each(function(){
                var this_ = $(this);
                var num = Number(this_.text().replace(".", ""));
                var incre = num/99;
                function start(counter){
                    if(counter <= num){
                        setTimeout(function(){
                            var no = counter < 10 ? '0' : '';
                            this_.text(no + Math.ceil(counter));
                            counter = counter + incre;
                            start(counter);
                        }, 10);
                    }
                    else{
                        var no = num < 10 ? '0' : '';
                        this_.text(no + num);
                    }
                }
                start(0);
            });
        }

    }
    
});