<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/check-don-hang', 'Web\UsersController@check_orders')->name('web.check_orders');

Route::get('/kiem-tra-bao-hanh', 'Web\UsersController@Kiem_Tra_bao_hanh')->name('web.Kiem_Tra_bao_hanh');

Route::get('search','Web\HomeController@search')->name('web.home.search');

Route::get('/', 'Web\HomeController@index')->name('web.home');
Route::get('/test', 'Web\TestController@index')->name('web.test');

Route::get('sitemap.xml','Web\SitemapController@index');
Route::get('sitemap-misc.xml','Web\SitemapController@misc');
Route::get('sitemap-contact.xml','Web\SitemapController@contact');
Route::get('sitemap-categories.xml','Web\SitemapController@categories');
Route::get('sitemap-detail.xml','Web\SitemapController@detail');
Route::get('sitemap-pages.xml','Web\SitemapController@pages');
Route::get('sitemap-{slug}-page-{page}.xml','Web\SitemapController@show')->where(['slug' => '([^/]*)', 'page' => '[0-9]+']);
Route::get('404.html','Web\HomeController@page_not_found')->name('web.page_not_found');
Route::get('/rss','Web\FeedsController@rss');

Route::get('gio-hang', 'Web\OrderController@show')->name('web.orders.show');
Route::get('/tien-hanh-thanh-toan','Web\OrderController@payment')->name('web.orders.payment');
Route::get('/dat-hang-thanh-cong','Web\OrderController@success')->name('web.order.success');
Route::get('tim-kiem','Web\SearchController@show')->name('web.search.show');

Route::get('lien-he','Web\ContactController@show')->name('web.contact.show');
Route::post('lien-he', 'Web\ContactController@store')->name('web.contact.store');

Route::get('trang/{slug}.html','Web\PageController@show')->name('web.pages.show');

Route::get('tin-tuc-{slug}.html','Web\NewsController@show')->name('web.news.show');
Route::get('tin-tuc-{slug?}','Web\NewsController@index')->name('web.news_categories.show');

Route::get('khuyen-mai','Web\ProductCategoryController@khuyenMai')->name('web.products_categories.khuyen-mai');

// Sảm phẩm dịch vụ
Route::get('dich-vu-{slug?}.html','Web\ServiceController@index')->name('web.services.show');
// danh mục dịch vụ
Route::get('sua-chua-laptop','Web\ServiceCategoryController@showall')->name('web.service_categories.showall');
Route::get('dich-vu-{slug?}','Web\ServiceCategoryController@index')->name('web.service_categories.show');

Route::get('laptop','Web\ProductCategoryController@index')->name('web.products_categories.index');

Route::get('{slug}.html','Web\ProductController@show')->name('web.products.show');

Route::get('{slug}','Web\ProductCategoryController@show')->name('web.products_categories.show');


Route::get('phu-kien','Web\PhukienProductCategoryController@index')->name('web.phu_kien_products_categories.index');

Route::get('phu-kien-247hn-{slug}.html','Web\PhukienProductController@show')->name('web.phu_kien_products.show');

Route::get('phu-kien-{slug}','Web\PhukienProductCategoryController@show')->name('web.phu_kien_products_categories.show');


Route::get('user/thong-tin-ca-nhan', 'Web\UsersController@index')->name('web.users.information')->middleware('check-login');
Route::get('user/quan-ly-don-hang', 'Web\UsersController@order')->name('web.users.order')->middleware('check-login');
Route::post('ajax/login','Web\AjaxController@login');
Route::post('ajax/register','Web\AjaxController@register');

Route::post('/ajax/add-to-cart', 'Web\AjaxController@addToCart');
Route::post('/ajax/update-qty', 'Web\AjaxController@updateQty');
Route::post('/ajax/update-color', 'Web\AjaxController@updateColor');
Route::post('/ajax/update-size', 'Web\AjaxController@updateSize');
Route::post('/ajax/remove-cart', 'Web\AjaxController@removeCart');
Route::post('/ajax/payment', 'Web\AjaxController@payment');
Route::post('/ajax/sub-email', 'Web\AjaxController@subEmail');
Route::post('/ajax/get-size', 'Web\AjaxController@getSize');
Route::post('/ajax/get-district', 'Web\AjaxController@getDistricts');

Route::post('/ajax/reply-comment', 'Web\AjaxController@replyComment');
Route::post('/ajax/vote-start', 'Web\AjaxController@voteStart');

Route::post('/ajax/view-show-product', 'Web\AjaxController@viewShowProduct');

Route::post('/dang-gia-binh-luan', 'Web\ReviewController@addComment')->name('add.comment');

Route::post('/ajax/reply-comment', 'Web\ReviewController@replyComment');

Route::post('/ajax/suggest', 'Web\SearchController@suggest')->name('web.search.suggest');

Route::post('/ajax/brand', 'Web\AjaxController@AjaxBrand');
Route::post('/ajax/dang-ky-email', 'Web\AjaxController@dangKyEmail');

 //đặt lịch sửa chữa dịch vụ
  Route::post('/ajax/dat-lich-sua-chua','Web\AjaxController@orderService')->name('web.ajax.order_service');

