<?php return array (
  'app' => 
  array (
    'name' => 'Laravel',
    'env' => 'local',
    'debug' => true,
    'url' => 'http://localhost:20117/',
    'timezone' => 'Asia/Ho_Chi_Minh',
    'locale' => 'en',
    'fallback_locale' => 'en',
    'key' => 'base64:jsezhWp8B9aOtrGIqyTWlNzU630bBMB6mfikiPg4ug0=',
    'cipher' => 'AES-256-CBC',
    'log' => 'single',
    'log_level' => 'debug',
    'storage_type' => 'local',
    'providers' => 
    array (
      0 => 'Illuminate\\Auth\\AuthServiceProvider',
      1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
      2 => 'Illuminate\\Bus\\BusServiceProvider',
      3 => 'Illuminate\\Cache\\CacheServiceProvider',
      4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
      5 => 'Illuminate\\Cookie\\CookieServiceProvider',
      6 => 'Illuminate\\Database\\DatabaseServiceProvider',
      7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
      8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
      9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
      10 => 'Illuminate\\Hashing\\HashServiceProvider',
      11 => 'Illuminate\\Mail\\MailServiceProvider',
      12 => 'Illuminate\\Notifications\\NotificationServiceProvider',
      13 => 'Illuminate\\Pagination\\PaginationServiceProvider',
      14 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
      15 => 'Illuminate\\Queue\\QueueServiceProvider',
      16 => 'Illuminate\\Redis\\RedisServiceProvider',
      17 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
      18 => 'Illuminate\\Session\\SessionServiceProvider',
      19 => 'Illuminate\\Translation\\TranslationServiceProvider',
      20 => 'Illuminate\\Validation\\ValidationServiceProvider',
      21 => 'Illuminate\\View\\ViewServiceProvider',
      22 => 'Laravel\\Tinker\\TinkerServiceProvider',
      23 => 'App\\Providers\\AppServiceProvider',
      24 => 'App\\Providers\\AuthServiceProvider',
      25 => 'App\\Providers\\EventServiceProvider',
      26 => 'App\\Providers\\RouteServiceProvider',
      27 => 'App\\Providers\\SudoOpsServiceProvider',
    ),
    'aliases' => 
    array (
      'App' => 'Illuminate\\Support\\Facades\\App',
      'Artisan' => 'Illuminate\\Support\\Facades\\Artisan',
      'Auth' => 'Illuminate\\Support\\Facades\\Auth',
      'Blade' => 'Illuminate\\Support\\Facades\\Blade',
      'Broadcast' => 'Illuminate\\Support\\Facades\\Broadcast',
      'Bus' => 'Illuminate\\Support\\Facades\\Bus',
      'Cache' => 'Illuminate\\Support\\Facades\\Cache',
      'Config' => 'Illuminate\\Support\\Facades\\Config',
      'Cookie' => 'Illuminate\\Support\\Facades\\Cookie',
      'Crypt' => 'Illuminate\\Support\\Facades\\Crypt',
      'DB' => 'Illuminate\\Support\\Facades\\DB',
      'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
      'Event' => 'Illuminate\\Support\\Facades\\Event',
      'File' => 'Illuminate\\Support\\Facades\\File',
      'Gate' => 'Illuminate\\Support\\Facades\\Gate',
      'Hash' => 'Illuminate\\Support\\Facades\\Hash',
      'Lang' => 'Illuminate\\Support\\Facades\\Lang',
      'Log' => 'Illuminate\\Support\\Facades\\Log',
      'Mail' => 'Illuminate\\Support\\Facades\\Mail',
      'Notification' => 'Illuminate\\Support\\Facades\\Notification',
      'Password' => 'Illuminate\\Support\\Facades\\Password',
      'Queue' => 'Illuminate\\Support\\Facades\\Queue',
      'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
      'Redis' => 'Illuminate\\Support\\Facades\\Redis',
      'Request' => 'Illuminate\\Support\\Facades\\Request',
      'Response' => 'Illuminate\\Support\\Facades\\Response',
      'Route' => 'Illuminate\\Support\\Facades\\Route',
      'Schema' => 'Illuminate\\Support\\Facades\\Schema',
      'Session' => 'Illuminate\\Support\\Facades\\Session',
      'Storage' => 'Illuminate\\Support\\Facades\\Storage',
      'URL' => 'Illuminate\\Support\\Facades\\URL',
      'Validator' => 'Illuminate\\Support\\Facades\\Validator',
      'View' => 'Illuminate\\Support\\Facades\\View',
    ),
    'log_table' => 
    array (
      'products_categories' => 'Danh mục sản phẩm',
      'products' => 'Sản phẩm',
      'service_categories' => 'Danh mục dịch vụ',
      'services' => 'Dịch vụ',
      'admin_users' => 'Tài khoản quản trị',
      'pages' => 'Trang đơn',
      'news' => 'Tin tức',
      'news_categories' => 'Danh mục tin tức',
      '' => 'Đăng nhập',
    ),
    'log_type' => 
    array (
      'store' => 'Thêm mới',
      'update' => 'Cập nhật',
      'delete' => 'Xóa',
      'login' => 'Đăng nhập',
    ),
    'fix_link' => 
    array (
      '/lien-he' => 'Liên hệ',
      '/san-pham' => 'Danh mục sản phẩm tổng',
      '/tin-tuc' => 'Danh mục tin tức tổng',
    ),
    'filter_price' => 
    array (
      1 => 'Dưới 3.000.000đ',
      2 => '3.000.000đ - 5.000.000đ',
      3 => '5.000.000đ - 10.000.0000đ',
      4 => '10.000.000đ - 15.000.0000đ',
      5 => '15.000.0000đ - 20.000.0000đ',
      6 => 'Trên 20.000.0000đ',
    ),
    'ocung' => 
    array (
      0 => '--Chọn ổ cứng--',
      1 => 'HDD 320G',
      2 => 'HDD 500G',
      3 => 'HDD 1T',
      4 => 'SSD 120G',
      5 => 'SSD 128G',
      6 => 'SSD 240G',
      7 => 'SSD 256G',
      8 => 'SSD 500G',
      9 => 'SSD 1T',
      10 => 'HDD 250G',
    ),
    'ram' => 
    array (
      0 => '--Chọn Ram--',
      1 => 'Ram 2G',
      2 => 'Ram 4G',
      3 => 'Ram 6G',
      4 => 'Ram 8G',
      5 => 'Ram 12G',
    ),
    'core' => 
    array (
      0 => '--Chọn thế hệ máy--',
      1 => 'Core i2',
      2 => 'Core i3',
      3 => 'Core i5',
      4 => 'Core i7',
    ),
    'type_laptop' => 
    array (
      0 => '--Chọn Thể loại laptop--',
      1 => 'Laptop chơi game',
      2 => 'Laptop đồ họa',
      3 => 'Laptop doanh nhân',
      4 => 'Laptop cho lập trình viên',
      5 => 'Laptop văn phòng',
      6 => 'Laptop cho học sinh/sinh viên',
      7 => 'Laptop chơi game, đồ họa',
    ),
    'payments' => 
    array (
      1 => 'Thanh toán tại nhà (COD)',
      2 => 'Chuyển khoản ngân hàng',
    ),
    'warehouse_status' => 
    array (
      1 => 'Còn hàng',
      2 => 'Hết hàng',
    ),
    'instock_status' => 
    array (
      1 => 'Còn hàng',
      2 => 'Hết hàng',
      3 => 'Sắp về',
      4 => 'Sắp ra mắt',
      5 => 'Ngừng kinh doanh',
    ),
  ),
  'auth' => 
  array (
    'defaults' => 
    array (
      'guard' => 'web',
      'passwords' => 'users',
    ),
    'guards' => 
    array (
      'web' => 
      array (
        'driver' => 'session',
        'provider' => 'users',
      ),
      'admin' => 
      array (
        'driver' => 'session',
        'provider' => 'admin_users',
      ),
      'api' => 
      array (
        'driver' => 'token',
        'provider' => 'users',
      ),
    ),
    'providers' => 
    array (
      'users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\User',
      ),
      'admin_users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\AdminModel\\Admin_user',
      ),
    ),
    'passwords' => 
    array (
      'users' => 
      array (
        'provider' => 'users',
        'table' => 'password_resets',
        'expire' => 60,
      ),
    ),
  ),
  'broadcasting' => 
  array (
    'default' => 'log',
    'connections' => 
    array (
      'pusher' => 
      array (
        'driver' => 'pusher',
        'key' => '',
        'secret' => '',
        'app_id' => '',
        'options' => 
        array (
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
      'log' => 
      array (
        'driver' => 'log',
      ),
      'null' => 
      array (
        'driver' => 'null',
      ),
    ),
  ),
  'cache' => 
  array (
    'default' => 'file',
    'stores' => 
    array (
      'apc' => 
      array (
        'driver' => 'apc',
      ),
      'array' => 
      array (
        'driver' => 'array',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'cache',
        'connection' => NULL,
      ),
      'file' => 
      array (
        'driver' => 'file',
        'path' => 'D:\\xampp73\\htdocs\\web247hn\\laptopthienlong\\storage\\framework/cache/data',
      ),
      'memcached' => 
      array (
        'driver' => 'memcached',
        'persistent_id' => NULL,
        'sasl' => 
        array (
          0 => NULL,
          1 => NULL,
        ),
        'options' => 
        array (
        ),
        'servers' => 
        array (
          0 => 
          array (
            'host' => '127.0.0.1',
            'port' => 11211,
            'weight' => 100,
          ),
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
    ),
    'prefix' => 'laravel',
  ),
  'database' => 
  array (
    'default' => 'mysql',
    'connections' => 
    array (
      'sqlite' => 
      array (
        'driver' => 'sqlite',
        'database' => 'laptopthienlong',
        'prefix' => '',
      ),
      'mysql' => 
      array (
        'driver' => 'mysql',
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'laptopthienlong',
        'username' => 'root',
        'password' => '',
        'unix_socket' => '',
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'strict' => true,
        'engine' => NULL,
      ),
      'pgsql' => 
      array (
        'driver' => 'pgsql',
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'laptopthienlong',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'prefix' => '',
        'schema' => 'public',
        'sslmode' => 'prefer',
      ),
      'sqlsrv' => 
      array (
        'driver' => 'sqlsrv',
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'laptopthienlong',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'prefix' => '',
      ),
    ),
    'migrations' => 'migrations',
    'redis' => 
    array (
      'client' => 'predis',
      'default' => 
      array (
        'host' => '127.0.0.1',
        'password' => NULL,
        'port' => '6379',
        'database' => 0,
      ),
    ),
  ),
  'filesystems' => 
  array (
    'default' => 'local',
    'cloud' => 's3',
    'disks' => 
    array (
      'local' => 
      array (
        'driver' => 'local',
        'root' => 'D:\\xampp73\\htdocs\\web247hn\\laptopthienlong\\public\\uploads',
      ),
      'public' => 
      array (
        'driver' => 'local',
        'root' => 'D:\\xampp73\\htdocs\\web247hn\\laptopthienlong\\storage\\app/public',
        'url' => 'http://localhost:20117//storage',
        'visibility' => 'public',
      ),
      's3' => 
      array (
        'driver' => 's3',
        'key' => NULL,
        'secret' => NULL,
        'region' => NULL,
        'bucket' => NULL,
      ),
      'do' => 
      array (
        'driver' => 's3',
        'key' => 'DAPQX4OCWWNAF3FLAAXA',
        'secret' => 'X6hukP4kwub9haOGd0BmXdhpIcPChA4tOGLutdMRtR0',
        'region' => 'sgp1',
        'bucket' => 'laptop247hn',
        'endpoint' => '',
        'domain' => '',
        'folder' => 'core',
      ),
      'sop' => 
      array (
        'driver' => 'sop',
        'endpoint' => '',
        'username' => 'zvnu85713105',
        'password' => 'ChanhTuoi1^',
        'tenant_id' => '6c13a3172446411fab7837a8a5479710',
        'tenant_name' => 'zvnt85713105',
        'container' => 'test',
        'container_url' => '',
        'region' => 'tyo1',
        'service_name' => 'Object Storage Service',
      ),
    ),
  ),
  'mail' => 
  array (
    'driver' => 'smtp',
    'host' => 'smtp.mailtrap.io',
    'port' => '2525',
    'from' => 
    array (
      'address' => NULL,
      'name' => NULL,
    ),
    'encryption' => NULL,
    'username' => NULL,
    'password' => NULL,
    'sendmail' => '/usr/sbin/sendmail -bs',
    'markdown' => 
    array (
      'theme' => 'default',
      'paths' => 
      array (
        0 => 'D:\\xampp73\\htdocs\\web247hn\\laptopthienlong\\resources\\views/vendor/mail',
      ),
    ),
  ),
  'modules' => 
  array (
    'module' => 
    array (
      'products_categories' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'products' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'phukien_product_categories' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'phukien_products' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'news_categories' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'news' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'service_categories' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'services' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'classes' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'pages' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'orders' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'guarantees' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'slides' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'contacts' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'comments' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'votes' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'email_subscribe' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'coupons' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'brands' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'sync_link' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'admin_users' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
      'settings' => 
      array (
        0 => 'access',
        1 => 'general',
        2 => 'advertisement',
        3 => 'home',
        4 => 'google_shopping',
        5 => 'seo_category',
      ),
      'system_logs' => 
      array (
        0 => 'access',
        1 => 'create',
        2 => 'edit',
        3 => 'delete',
      ),
    ),
    'name' => 
    array (
      'products_categories' => 'Danh mục sản phẩm',
      'products' => 'Sản phẩm',
      'phukien_product_categories' => 'Danh mục phụ kiện laptop',
      'phukien_products' => 'Sản phẩm phụ kiện',
      'news_categories' => 'Danh mục tin tức',
      'news' => 'Tin tức',
      'pages' => 'Trang',
      'service_categories' => 'Danh mục Dịch vụ',
      'services' => 'Dịch vụ',
      'classes' => 'Danh mục theo loại',
      'orders' => 'Đơn hàng',
      'guarantees' => 'Chính sách bảo hành',
      'slides' => 'Quản lý slides',
      'contacts' => 'Liên hệ',
      'comments' => 'Bình luận',
      'votes' => 'Quản lý đáng giá sao',
      'email_subscribe' => 'Email đăng ký nhận tin',
      'coupons' => 'Chương trình khuyến mại',
      'brands' => 'Thương hiệu',
      'sync_link' => 'Link chuyển tiếp',
      'admin_users' => 'Tài khoản quản trị',
      'settings' => 'Cấu hình',
      'system_logs' => 'Logs hệ thống',
      'access' => 'Truy cập',
      'create' => 'Thêm',
      'edit' => 'Sửa',
      'delete' => 'Xóa',
      'general' => 'Cấu hình chung',
      'home' => 'Cấu hình trang chủ',
      'advertisement' => 'Banner quang cao',
      'google_shopping' => 'Cấu hình Google shopping',
      'seo_category' => 'Seo danh mục',
    ),
    'icon' => 
    array (
      'products_categories' => 'fa-bars',
      'products' => 'fa-archive',
      'phukien_product_categories' => 'fa-bars',
      'phukien_products' => 'fa-archive',
      'news_categories' => 'fa-bars',
      'news' => 'fa-newspaper-o',
      'pages' => 'fa-file-text',
      'service_categories' => 'fa-bars',
      'services' => 'fa-wrench',
      'classes' => 'fa-bars',
      'orders' => 'fa-shopping-cart',
      'guarantees' => 'fa-shopping-cart',
      'slides' => 'fa-sliders',
      'contacts' => 'fa-phone',
      'comments' => 'fa-comments',
      'votes' => 'fa-star',
      'email_subscribe' => 'fa-envelope-o',
      'coupons' => 'fa-gift',
      'brands' => 'fa',
      'sync_link' => 'fa-link',
      'admin_users' => 'fa-users',
      'settings' => 'fa-cogs',
      'system_logs' => 'fa-repeat',
    ),
  ),
  'queue' => 
  array (
    'default' => 'sync',
    'connections' => 
    array (
      'sync' => 
      array (
        'driver' => 'sync',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'jobs',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'beanstalkd' => 
      array (
        'driver' => 'beanstalkd',
        'host' => 'localhost',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'sqs' => 
      array (
        'driver' => 'sqs',
        'key' => 'your-public-key',
        'secret' => 'your-secret-key',
        'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
        'queue' => 'your-queue-name',
        'region' => 'us-east-1',
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
        'queue' => 'default',
        'retry_after' => 90,
      ),
    ),
    'failed' => 
    array (
      'database' => 'mysql',
      'table' => 'failed_jobs',
    ),
  ),
  'services' => 
  array (
    'mailgun' => 
    array (
      'domain' => NULL,
      'secret' => NULL,
    ),
    'ses' => 
    array (
      'key' => 'AKIAJ2EZDEIEK7N5YGAQ',
      'secret' => '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',
      'region' => 'us-east-1',
    ),
    'sparkpost' => 
    array (
      'secret' => NULL,
    ),
    'stripe' => 
    array (
      'model' => 'App\\User',
      'key' => NULL,
      'secret' => NULL,
    ),
  ),
  'session' => 
  array (
    'driver' => 'file',
    'lifetime' => 120,
    'expire_on_close' => false,
    'encrypt' => false,
    'files' => 'D:\\xampp73\\htdocs\\web247hn\\laptopthienlong\\storage\\framework/sessions',
    'connection' => NULL,
    'table' => 'sessions',
    'store' => NULL,
    'lottery' => 
    array (
      0 => 2,
      1 => 100,
    ),
    'cookie' => 'laravel_session',
    'path' => '/',
    'domain' => NULL,
    'secure' => false,
    'http_only' => true,
  ),
  'view' => 
  array (
    'paths' => 
    array (
      0 => 'D:\\xampp73\\htdocs\\web247hn\\laptopthienlong\\resources\\views',
    ),
    'compiled' => 'D:\\xampp73\\htdocs\\web247hn\\laptopthienlong\\storage\\framework\\views',
  ),
  'debugbar' => 
  array (
    'enabled' => NULL,
    'except' => 
    array (
      0 => 'telescope*',
    ),
    'storage' => 
    array (
      'enabled' => true,
      'driver' => 'file',
      'path' => 'D:\\xampp73\\htdocs\\web247hn\\laptopthienlong\\storage\\debugbar',
      'connection' => NULL,
      'provider' => '',
    ),
    'include_vendors' => true,
    'capture_ajax' => true,
    'add_ajax_timing' => false,
    'error_handler' => false,
    'clockwork' => false,
    'collectors' => 
    array (
      'phpinfo' => true,
      'messages' => true,
      'time' => true,
      'memory' => true,
      'exceptions' => true,
      'log' => true,
      'db' => true,
      'views' => true,
      'route' => true,
      'auth' => false,
      'gate' => true,
      'session' => true,
      'symfony_request' => true,
      'mail' => true,
      'laravel' => false,
      'events' => false,
      'default_request' => false,
      'logs' => false,
      'files' => false,
      'config' => false,
      'cache' => false,
      'models' => false,
    ),
    'options' => 
    array (
      'auth' => 
      array (
        'show_name' => true,
      ),
      'db' => 
      array (
        'with_params' => true,
        'backtrace' => true,
        'timeline' => false,
        'explain' => 
        array (
          'enabled' => false,
          'types' => 
          array (
            0 => 'SELECT',
          ),
        ),
        'hints' => true,
      ),
      'mail' => 
      array (
        'full_log' => false,
      ),
      'views' => 
      array (
        'data' => false,
      ),
      'route' => 
      array (
        'label' => true,
      ),
      'logs' => 
      array (
        'file' => NULL,
      ),
      'cache' => 
      array (
        'values' => true,
      ),
    ),
    'inject' => true,
    'route_prefix' => '_debugbar',
    'route_domain' => NULL,
  ),
  'cart' => 
  array (
    'tax' => 21,
    'database' => 
    array (
      'connection' => NULL,
      'table' => 'shoppingcart',
    ),
    'destroy_on_logout' => false,
    'format' => 
    array (
      'decimals' => 2,
      'decimal_point' => '.',
      'thousand_separator' => ',',
    ),
  ),
  'image' => 
  array (
    'driver' => 'gd',
  ),
  'tinker' => 
  array (
    'commands' => 
    array (
    ),
    'dont_alias' => 
    array (
      0 => 'App\\Nova',
    ),
  ),
);
