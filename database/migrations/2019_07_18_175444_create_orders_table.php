<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('type_id')->nullable();
            $table->string('type')->nullable();
            $table->string('code_order',191)->nullable();
            $table->text('content')->nullable();
            $table->string('name',191);
            $table->string('email',191);
            $table->string('phone',191);
            $table->string('address',191);
            $table->string('note',191)->nullable();
            $table->tinyInteger('payments')->nullable();
            $table->integer('province_id')->default(0);
            $table->integer('district_id')->default(0);
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
