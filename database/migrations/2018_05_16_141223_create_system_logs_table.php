<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->string('ip',191);
            $table->timestamp('time');
            $table->string('title',191)->nullable();
            $table->string('type',191)->nullable();
            $table->string('table',191)->nullable();
            $table->integer('table_id')->default(0);
            $table->longText('detail')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->unique('id','id_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_logs');
    }
}
