<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_attribute', function (Blueprint $table) {
            $table->integer('service_id');
            $table->tinyInteger('index')->nullable()->default(0);
            $table->string('color_name',191)->nullable();
            $table->string('color_code',191)->nullable();
            $table->text('warranty')->nullable();
            $table->integer('price',191)->nullable();
            $table->string('image',191)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_attribute');
    }
}
