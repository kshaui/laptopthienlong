<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->default(0);
            $table->integer('class_id')->default(0);
            $table->string('name',191);
            $table->string('slug',191);
            $table->string('image',191)->nullable();
            $table->integer('price')->nullable()->default(0);
             $table->text('price_table')->nullable();
            $table->string('link_interal',191)->nullable();
            $table->string('warranty')->nullable();
            $table->text('promotion')->nullable();
            $table->text('infor')->nullable();
            $table->string('related_service',191)->nullable();
            $table->string('related_news',191)->nullable();
          
            $table->text('detail')->nullable();
            $table->tinyInteger('instock_status')->nullable()->default(1);
            $table->text('tags')->nullable();
            $table->tinyInteger('showhome')->nullable()->default(0);
            $table->integer('order')->nullable()->default(9999);
         
            
            $table->text('videos')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->unique('id','id_UNIQUE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
