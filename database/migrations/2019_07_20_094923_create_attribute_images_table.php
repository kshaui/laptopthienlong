<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->nullable()->default(0);
            $table->integer('color_id')->nullable()->default(0);
            $table->string('image',191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_images');
    }
}
