<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->default(0);
            $table->integer('user_id')->nullable();
            $table->integer('brand_id')->default(0);
            $table->string('name',191);
            $table->string('slug',191);
            $table->integer('price')->nullable()->default(0);
            $table->integer('price_old')->nullable()->default(0);
            $table->string('image',191)->nullable();
            $table->text('slides')->nullable();
            $table->text('specifications')->nullable();
            $table->text('description')->nullable();
            $table->longText('detail')->nullable();
            $table->string('sku',191)->nullable();//mã sp
            $table->text('promotion')->nullable();
            $table->integer('ocung')->nullable();
            $table->integer('ram')->nullable();
            $table->integer('core')->nullable();
            $table->integer('type_laptop')->nullable();
            $table->integer('guarantee')->nullable();
            $table->tinyInteger('warehouse_status')->default(1);//array(1=>'Còn hàng',2=>'Hết hàng',3=>'Sắp về',4=>'Sắp ra mắt',5=>'Ngừng kinh doanh');
            $table->string('related_products',191)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->unique('id','id_UNIQUE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
