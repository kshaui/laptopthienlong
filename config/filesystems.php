<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => public_path('uploads'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],

        'do' => [
            'driver' => 's3',
            'key' => env('DO_KEY','DAPQX4OCWWNAF3FLAAXA'),
            'secret' => env('DO_SECRET','X6hukP4kwub9haOGd0BmXdhpIcPChA4tOGLutdMRtR0'),
            'region' => env('DO_REGION','sgp1'),
            'bucket' => env('DO_BUCKET','laptop247hn'),
            'endpoint' => env('DO_ENDPOINT',''),
            'domain' => env('DO_DOMAIN',''),
            'folder' => env('DO_FOLDER','core'),//ưu tiên thay đổi ngoài .env - đặt tên theo domain kiểu sudo-vn (đổi . thành -)
        ],

     
        'sop' => [
            'driver'        => 'sop',
            'endpoint'      => env('OS_ENDPOINT', ''),
            'username'      => env('OS_USERNAME', 'zvnu85713105'),
            'password'      => env('OS_PASSWORD', 'ChanhTuoi1^'),
            'tenant_id'     => env('OS_TENANT_ID', '6c13a3172446411fab7837a8a5479710'),
            'tenant_name'   => env('OS_TENANT_NAME', 'zvnt85713105'),
            'container'     => env('OS_CONTAINER', 'test'),
            'container_url' => env('OS_CONTAINER_URL', ''),
            'region'        => env('OS_REGION', 'tyo1'),
            'service_name'  => env('OS_SERVICE_NAME', 'Object Storage Service'),
        ],

    ],

];
